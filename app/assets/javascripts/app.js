/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */

//Ext.Loader.setPath('Ext.app', 'Portal');

/*
Ext.define('Portal.Application', {
    extend: 'Ext.app.Application',

    name: 'Portal'
});

Ext.application({
    extend: 'Portal.Application',

    requires: [
        'Portal.*'
    ],

    name: 'Portal',

    autoCreateViewport: 'Portal.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to Portal.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});


Ext.onReady(function()
{
    Ext.create('Ext.app.Portal');
});
*/


Ext.application(
{
    name: 'Netra',
    appFolder: '/assets/app',

    requires: [
        //'Netra.view.component.window.NewFlavor',    
        'Ext.toolbar.Paging',
        'Ext.ux.ProgressBarPager',        
        'Ext.layout.container.Accordion',
        
        'Netra.store.Hosts',
        'Netra.store.XenHosts',
        'Netra.store.Clusters',
        'Netra.store.Repositorys',
        'Netra.store.HostStats',

        'Netra.view.main.Main',
    ],
    
    //controllers: [ 'View.main.MainController'],

    autoCreateViewport: false,

    createDataStores: function()
    {
        Netra.manager.createDataStore('Netra.store.Hosts');
        Netra.manager.createDataStore('Netra.store.Clusters');
        Netra.manager.createDataStore('Netra.store.Templates');
        Netra.manager.createDataStore('Netra.store.Tasks');
        Netra.manager.createDataStore('Netra.store.HostStats');
        Netra.manager.createDataStore('Netra.store.HostStatDetails');                
        Netra.manager.createDataStore('Netra.store.XenHosts');
        Netra.manager.createDataStore('Netra.store.XenServers');
        Netra.manager.createDataStore('Netra.store.OrganizationUnits');
        Netra.manager.createDataStore('Netra.store.AdUsers');
        Netra.manager.createDataStore('Netra.store.PBDs');
        Netra.manager.createDataStore('Netra.store.StorageRepositories');
        Netra.manager.createDataStore('Netra.store.Subtasks');       
        Netra.manager.createDataStore('Netra.store.MachineCatalogs');
        Netra.manager.createDataStore('Netra.store.DeliveryGroups');
        Netra.manager.createDataStore('Netra.store.HypervisorConnections');
        Netra.manager.createDataStore('Netra.store.XenVDIs');
        Netra.manager.createDataStore('Netra.store.UserInfos');
/*
        Netra.manager.createDataStore('Netra.store.TaskReports');

        Netra.manager.createDataStore('Netra.store.SecurityGroupRules');        
        Netra.manager.createDataStore('Netra.store.Volumes');
        Netra.manager.createDataStore('Netra.store.VolumeTypes');
*/        
    },

    loadDataStores: function()
    {
        //Netra.service.loadTenantsData();
        //Netra.service.loadVolumesData();
        //Netra.service.loadNetworksData();

        Netra.service.loadPoolData(function() {
            console.debug("App.loadDataStores : loadPoolData done");
        });
    },



    launch: function()
    {
        //console.debug("app.js : onlaunch");
        this.createDataStores();
        this.loadDataStores();

        //Ext.create('Netra.view.main.Main');
        Ext.create('Netra.view.login.Login');
    }  

});

//console.debug("app.js");