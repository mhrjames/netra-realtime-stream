Ext.define("Netra.controller.BaseController", 
{
	extend : "Ext.app.ViewController",
    referenceHolder: true,


    updatePanelTitle: function(panel,title)
    {
        console.debug("updatePanelTitle");
        console.debug(panel);

        panel.title = title;
    },

    updateTreePanelTitle: function(title)
    {
        this.updatePanelTitle(this.getPanelTree(),title);
    },

    updateGridPanelTitle: function(title)
    {
        this.updatePanelTitle(this.getPanelGrid(),title);
    },

    updateContentsPanelTitle: function(title)
    {
        this.updatePanelTitle(this.getPanelContents(),title);
    },

    updateListPanelTitle: function(title)
    {
        console.debug("updateListPanelTitle");
        console.debug(this);

        this.updatePanelTitle(this.getPanelList(),title);
    },


    addNodeToTree: function(tree,parent_node,text,node_type,record,leaf)
    {
        var node = parent_node.appendChild({
                leaf: leaf,
                text: text,
                nodeType: node_type,
                nodeRecord: record
        });

        parent_node.expand();

        tree.getView().refresh();

        return node;
    },

    addRootNodeToTree: function(tree,text)
    {
        var rootNode = tree.getRootNode();
        var node = this.addNodeToTree(tree,rootNode, text, 'openstack', false, null);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    addItemNodeToTree: function(tree,text,record)
    {
        var rootNode = tree.getRootNode().childNodes[0];

        //console.debug("addItemNodeToTree : node = " , rootNode);

        var node = this.addNodeToTree(tree,rootNode, text, 'openstack', true, record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },


    collapsePanel: function(panel)
    {
        console.debug("BaseController.collapsePanel");
        console.debug(panel);

        panel.collapse("Ext.Component.DIRECTION_LEFT", true);
    },

    collapseListPanel: function(panel)
    {
        panel.collapse("left", true);
    },

    expandListPanel: function()
    {
        this.getPanelList().expand(true);
    },
    
});