Ext.define('Netra.builder', 
{
    singleton: true,    
    comboMessageBox: null,
    comboMessageBoxCallback: null,


    createNumberWidget: function (title, number, icon) 
    {
        var icon_output = "<span class='"+ icon + " number-widget-icon'></span>";
        
        var output = "<div class='number-widget-box'>";
        output += "<div class='number-widget-title'>" + title + "</div>";
        output += "<div class='number-widget-contents'>";
        output += "<div class='number-widget-number'>" + icon_output + number + "</div>";
        output += "</div>";
        output += "</div>";

        return output;          
    },

    createTextField: function(name, label, value)
    {
        console.debug("Netra.manager.createTextField");

        var text = Ext.create('Ext.form.field.Text', {
            name: name,
            fieldLabel: label,
            labelWidth: 170,
            readOnly: true,
            value: value
        });

        return text;
    },

    createTextAreaField: function(name, label, value)
    {
        console.debug("Netra.manager.createTextField");

        var text = Ext.create('Ext.form.field.TextArea', {
            name: name,
            fieldLabel: label,
            labelWidth: 170,
            readOnly: true,
            height: 100,
            value: value
        });

        return text;
    },

    createTitleField: function(title, desc)
    {

        var component = Ext.create('Ext.Component', {
            frame: false, 
            border: 0,
            html: Netra.helper.createFormTitle(title,desc),
        });
        
        return component;
    },

    createTitleFieldWithIcon: function(title, desc, icon)
    {

        var component = Ext.create('Ext.Component', {
            frame: false, 
            border: 0,
            html: Netra.helper.createFormTitle(title,desc),
        });
        
        return component;
    },

    buildForm: function(form,store,model, title, desc)
    {
        var _this = this;

        console.debug("Netra.manager.buildForm : form = ", form);
        console.debug("Netra.manager.buildForm : store = ", store);

        form.removeAll();

        
        var titlefield = this.createTitleField(title,desc);
        form.add(titlefield);


        var name = "", label = "";

        var fields = store.getModel().getFields();

        fields.forEach(function(field) 
        {
            console.debug("Netra.manager.buildForm : field = " , field);

            label = field.name;
            if (field.hasOwnProperty('label')) {
                label = field.label;
            }

            if (model[field.name] instanceof Object) {
                var textfield = _this.createTextAreaField(field.name, label, JSON.stringify(model[field.name]));
            } else {
                var textfield = _this.createTextField(field.name, label, model[field.name]);                
            }


            form.add(textfield);
        });

    },

    buildFormExt: function(form,model, title, desc)
    {
        var _this = this;

        console.debug("Netra.manager.buildForm : form = ", form);

        form.removeAll();

        
        var titlefield = this.createTitleField(title,desc);
        form.add(titlefield);


        var name = "", label = "";

        var fields = model.getFields();

        fields.forEach(function(field) 
        {
            console.debug("Netra.manager.buildForm : field = " , field);

            label = field.name;
            if (field.hasOwnProperty('label')) {
                label = field.label;
            }

            if (model[field.name] instanceof Object) {
                var textfield = _this.createTextAreaField(field.name, label, JSON.stringify(model.get(field.name)));
            } else {
                var textfield = _this.createTextField(field.name, label, model.get(field.name));
            }


            form.add(textfield);
        });

    },

    generateForm: function(store, model, title, desc, icon)
    {
        var _this = this;

        console.debug("Netra.manager.buildForm : store = ", store);

        var form = Ext.create('Ext.form.Panel');
        
        var titlefield = this.createTitleField(title,desc);
        form.add(titlefield);


        var name = "", label = "";

        var fields = store.getModel().getFields();

        fields.forEach(function(field) 
        {
            console.debug("Netra.manager.buildForm : field = " , field);

            label = field.name;
            if (field.hasOwnProperty('label')) {
                label = field.label;
            }

            if (model[field.name] instanceof Object) {
                var textfield = _this.createTextAreaField(field.name, label, JSON.stringify(model[field.name]));
            } else {
                var textfield = _this.createTextField(field.name, label, model[field.name]);                
            }


            form.add(textfield);
        });

        return form;
    },


    createComboMessageBox: function(store,disp_field,value_field, title, msg) 
    {
        var _this = this;

        console.debug("Netra.helper.ComboMessageBox.buildMessageBox()");

        if (this.comboMessageBox != null ) {
            var dialog = Netra.helper.getComponent('#window_combo_messagebox');
            dialog.setTitle(title);
            var label = Netra.helper.getComponent('#label_messagebox');
            label.setText(msg);
        } else
        {
            this.comboMessageBox = Ext.create('Ext.window.Window', 
            {
                title: title,
                height: 200,
                width: 250,
                modal: true,
                onEsc: function() {},
                closable: false,

                autoClose: false,
                autoDestroy: false,
                slideInDuration: 500,
                slideBackDuration: 500,

                id: 'window_combo_messagebox',

                layout: {
                    type: 'vbox',
                    pack: 'start',
                    align: 'stretch',
                },

                items: 
                [
                    {
                        xtype: 'label',
                        id: 'label_messagebox',
                        text: msg,
                        margin: '25 20 15 20',
                    },
                    {
                        xtype: 'combobox',
                        displayField: disp_field,
                        valueField: value_field,
                        id: 'combo_messagebox',
                        store: {
                            type: store
                        },
                        mode: 'local',  // local
                        triggerAction: 'all',
                        forceSelection: true,
                        editable: false,
                        autoSelect: false,
                        minChars: 1,
                        fieldLabel: '',
                        enableKeyEvents: true,
                        margin: '0 20 30 20',
                    }
                ],
                dockedItems: 
                [
                    {
                        xtype: 'toolbar',
                        dock: 'bottom',
                        ui: 'footer',
                        //defaults: {minWidth: minButtonWidth},
                        items: [                    
                            { xtype: 'component', flex: 1 },
                            { 
                                text: 'Ok', 
                                id: 'btn_combo_message_ok',
                                handler: function() {
                                    console.debug("ComboMessageBox.ok");

                                    var combobox = Netra.helper.getComponent('#combo_messagebox');
                                    _this.comboMessageBoxCallback('ok',combobox.getValue());
                                }
                            },
                            { 
                                text: 'Cancel', 
                                id: 'btn_combo_message_cancel',
                                handler: function() {
                                    console.debug("ComboMessageBox.cancel");
                                    _this.comboMessageBoxCallback('cancel','');
                                }
                            },
                        ]
                    }
                ],

                prompt: function(callback) 
                {
                    _this.comboMessageBoxCallback = callback;
                    _this.comboMessageBox.show();
                }
            });
        }

        return this.comboMessageBox;
/*        
        var index = this.promptContainer.items.indexOf(this.textField);

        if (removeText) {
            this.promptContainer.remove(this.textField);  // remove standard prompt            
        } else {
            this.textField.setFieldLabel('Hello');
        }

        this.comboField = this._createComboBoxField(store,disp_field,value_field);
        this.promptContainer.add(this.comboField);
        //this.promptContainer.insert(index, this.textField);
*/

        
    },


    createPiePanelChart: function(title,chart_ref)
    {
        var panel = Ext.create('Ext.panel.Panel', 
        {
            title: title,
            titleAlign: 'center',
            layout: 'fit',
            flex: 1,
        });

        var chart = Ext.create('Chart.ux.Highcharts', 
        {
            reference: chart_ref,
            initAnimAfterLoad: false,
            layout: 'fit',
            id: chart_ref,
            debug: false,
            flex: 1,

            chartConfig: {
              chart: {
                //showAxes: true,
              },
              legend: { enabled: false },
              title: {
                text: title
              },
              xAxis: {},
              yAxis: {},
                credits: {
                    enabled: false
                },

            },
            series: 
            [
                {
                    type: 'pie',
                    categorieField: 'label',
                    dataField: 'value',
                    size: '100%',
                    totalDataField: true,
                }
            ],
            
        });

        panel.add(chart);

        return panel;
    },

    createGaugePanelChart: function(title,chart_ref)
    {
        var panel = Ext.create('Ext.panel.Panel', 
        {
            title: title,
            titleAlign: 'center',
            layout: 'fit',
            flex: 1,
        });

        var chart = Ext.create('Chart.ux.Highcharts', 
        {
            reference: chart_ref,
            initAnimAfterLoad: false,
            layout: 'fit',
            id: chart_ref,
            debug: false,
            flex: 1,

            chartConfig: 
            {
                chart: {
                    type: 'bar',
                },

                title: title,

                xAxis: {
                    categories: [ 
                        'label',
                    ]
                },
                yAxis: {
                    min: 0,
                },
                credits: {
                    enabled: false
                },
                legend: {
                    reversed: true
                },

                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
   
            },
      
            series: 
            [
                {
                    //categorieField: 'label',
                    //dataField: 'value'
                }
            ]
        });

        panel.add(chart);

        return panel;
    },

    createLineChart: function(title,chart_ref)
    {
        var panel = Ext.create('Ext.panel.Panel', 
        {
            title: title,
            titleAlign: 'center',
            layout: 'fit',
            flex: 1,
        });

        var chart = Ext.create('Chart.ux.Highcharts', 
        {
            reference: chart_ref,
            initAnimAfterLoad: false,
            layout: 'fit',
            id: chart_ref,
            debug: false,
            flex: 1,

            chartConfig: 
            {
                chart: {
                    type: 'bar',
                },

                title: title,

                xAxis: {
                    categories: [ 
                        'label',
                    ]
                },
                yAxis: {
                    min: 0,
                },
                credits: {
                    enabled: false
                },
                legend: {
                    reversed: true
                },

                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
   
            },
      
            series: 
            [
                {
                    //categorieField: 'label',
                    //dataField: 'value'
                }
            ]
        });

        panel.add(chart);

        return panel;
    },

    getTextValue: function()
    {
        return this.textField.getValue();
    },

    getComboValue: function()
    {
        return this.comboField.getValue();
    },

    _createComboBoxField: function (store,disp_field,value_field) 
    {
        console.debug("Netra.helper.ComboMessageBox._createComboBoxField()");

        //copy paste what is being done in the initComonent to create the combobox
        return Ext.create('Ext.form.field.ComboBox', {

            id: this.id + '-combo',

            displayField: disp_field,
            valueField: value_field,
            store: {
                type: store
            },
            mode: 'local',  // local
            triggerAction: 'all',
            forceSelection: true,
            editable: false,
            autoSelect: false,
            minChars: 1,
            fieldLabel: '',
            enableKeyEvents: true,

            listeners: 
            {
                change: function (obj, newValue, oldValue, eOpts) {
                    //someStore.proxy.extraParams.keyword = newValue;
                    //someStore.load();
                }
            }  // listeners
        });
    }
});