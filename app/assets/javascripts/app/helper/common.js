Ext.Loader.setConfig({
    enabled : true,
    disableCaching : true, // For debug only
    paths : {
        'Ext.ux' : '/assets/ux',
        'Plugin' : '/assets/lib',
        'Chart' : '/assets/lib/highchart',
        'View' : '/assets/app/view',
        'Model' : '/assets/app/model',
        'Store' : '/assets/app/store',
        'Window' : '/assets/app/view/component/window',
        'Controller' : '/assets/app/controller',
    },    
});

/*
Ext.define('Ext.ux.layout.container.HorizontalAccordion', {
    extend: 'Ext.layout.container.Accordion',

    alias: ['layout.horizontal-accordion'],

    defaultAnimatePolicy: {
        x: true,
        width: true
    }
},
function() {
    Ext.require([
        'Ext.layout.container.HBox'
    ],
    function() {
        Ext.ux.layout.container.HorizontalAccordion.borrow(
            Ext.layout.container.HBox,
            [
                'type',
                'direction',
                'horizontal',
                'names',
                'sizePolicy'
            ]
        );
    });
});
*/

/*
Ext.define('Ext.ux.CheckColumnPatch', {
    override: 'Ext.ux.CheckColumn',


     * @cfg {Boolean} [columnHeaderCheckbox=false]
     * True to enable check/uncheck all rows
     
    columnHeaderCheckbox: false,

    constructor: function (config) {
        var me = this;
        me.callParent(arguments);

        me.addEvents('beforecheckallchange', 'checkallchange');

        if (me.columnHeaderCheckbox) {
            me.on('headerclick', function () {
                this.updateAllRecords();
            }, me);

            me.on('render', function (comp) {
                var grid = comp.up('grid');
                this.mon(grid, 'reconfigure', function () {
                    if (this.isVisible()) {
                        this.bindStore();
                    }
                }, this);

                if (this.isVisible()) {
                    this.bindStore();
                }

                this.on('show', function () {
                    this.bindStore();
                });
                this.on('hide', function () {
                    this.unbindStore();
                });
            }, me);
        }
    },

    onStoreDateUpdate: function () {
        var allChecked,
            image;

        if (!this.updatingAll) {
            allChecked = this.getStoreIsAllChecked();
            if (allChecked !== this.allChecked) {
                this.allChecked = allChecked;
                image = this.getHeaderCheckboxImage(allChecked);
                this.setText(image);
            }
        }
    },

    getStoreIsAllChecked: function () {
        var me = this,
            allChecked = true;
        me.store.each(function (record) {
            if (!record.get(this.dataIndex)) {
                allChecked = false;
                return false;
            }
        }, me);
        return allChecked;
    },

    bindStore: function () {
        var me = this,
            grid = me.up('grid'),
            store = grid.getStore();

        me.store = store;

        me.mon(store, 'datachanged', function () {
            this.onStoreDateUpdate();
        }, me);
        me.mon(store, 'update', function () {
            this.onStoreDateUpdate();
        }, me);

        me.onStoreDateUpdate();
    },

    unbindStore: function () {
        var me = this,
            store = me.store;

        me.mun(store, 'datachanged');
        me.mun(store, 'update');
    },

    updateAllRecords: function () {
        var me = this,
            allChecked = !me.allChecked;

        if (me.fireEvent('beforecheckallchange', me, allChecked) !== false) {
            this.updatingAll = true;
            me.store.each(function (record) {
                record.set(this.dataIndex, allChecked);
            }, me);

            this.updatingAll = false;
            this.onStoreDateUpdate();
            me.fireEvent('checkallchange', me, allChecked);
        }
    },

    getHeaderCheckboxImage: function (allChecked) {
        var cls = [],
            cssPrefix = Ext.baseCSSPrefix;

        if (this.columnHeaderCheckbox) {
            allChecked = this.getStoreIsAllChecked();
            //Extjs 4.2.x css
            cls.push(cssPrefix + 'grid-checkcolumn');
            //Extjs 4.1.x css
            cls.push(cssPrefix + 'grid-checkheader');

            if (allChecked) {
                //Extjs 4.2.x css
                cls.push(cssPrefix + 'grid-checkcolumn-checked');
                //Extjs 4.1.x css
                cls.push(cssPrefix + 'grid-checkheader-checked');
            }
        }
        return '<div style="margin:auto" class="' + cls.join(' ') + '">&#160;</div>'
    }
});
*/



Ext.define("Netra.helper",
{
    singleton   : true,

    progressBoxInstance: null,


	createFormTitle: function (title,html) 
	{
		var output = "<h2><i class='entypo-right-circled'></i> " + title + "</h2>";
		if (html.length>0) output += "<p class='netra-form-title-description'>" + html + "</p>";
		output += "<hr><br>";

		return output;			
	},

	createFormSubTitle: function (title,html) 
	{
		var output = "<h3><i class='entypo-right-dir'></i> " + title + "</h3>";
		if (html.length>0) output += "<p class='netra-form-title-description'>" + html + "</p>";
		output += "<br>";

		return output;			
	},

	createActiveIcon: function (val)
	{
		if (val) return "<span class='"+ Icons.icoActive + "' style='color:red'></span>";
		if (! val) return "<span class='"+ Icons.icoInactive + "' style='color:black'></span>";

		return "<span class='" + Icons.icoInactive + "' style='color:black'></span>";
	},

	createNotification: function (title,msg) 
	{
	    Ext.create('widget.uxNotification', {
	        title: title,
	        position: 'tr',
	        manager: 'demo1',
	        useXAxis: false,
	        iconCls: 'ux-notification-icon-information',
	        html: msg
	    }).show();
	},

    updatePropertyGrid: function (id,params) 
    {
        var grid = Netra.helper.getComponent(id);
        if (! grid) {
            console.error("Netra.helper.updatePropertyGrid : Fail to find property grid named '" + id + "'");
            return;
        }

        grid.setSource(params);
    },

	getActiveButtonIcon: function() 
	{
		return "x-btn-glyph fontawesome-spinner";
	},

	getSaveButtonIcon: function() 
	{
		return "x-btn-glyph fontawesome-save";
	},

	showLoadingMask: function (loadingMessage) 
	{
		var loadText = loadingMessage;
		
		if (Ext.isEmpty(loadingMessage)) {
			loadText = 'Loading... Please wait';
		}

		//Use the mask function on the Ext.getBody() element to mask the body element during Ajax calls
		Ext.Ajax.on('beforerequest',function(){Ext.getBody().mask(loadText, 'loading') }, Ext.getBody());
		Ext.Ajax.on('requestcomplete',Ext.getBody().unmask ,Ext.getBody());
		Ext.Ajax.on('requestexception', Ext.getBody().unmask , Ext.getBody());
	},

	showPanelLoadingMask: function (panel, value) 
	{
		if (panel == null) {
			console.error("Common.showPanelLoadingMask : error!!");
			console.error(panel);
			return;
		}
		
		console.debug("Common.showPanelLoadingMask");

    	panel.setLoading(value);
	},

    showCardView: function(id, index, direction, duration)
    {
        var container = Netra.helper.getComponent('#'+id);
        if (! container) { return; }

        container.setActiveItem(index);
    },

    setActiveTab: function(id, index)
    {
        var container = Netra.helper.getComponent('#'+id);
        if (! container) { return; }

        container.setActiveTab(index);
    },

    setActivePanelItem: function(panel,index)
    {
        panel.items.items[index].expand();
    },

    getSelectedColumnValues: function(grid, column)
    {
        var selected = grid.getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            return null;
        }

        var ids = [];
        Ext.each(selected, function (item) 
        {
            var json = {};
            json[column] = item.get(column);
            
            ids.push(json);
            //console.debug("getSelectedColumnValues : " , item);
        });        

        return ids;
    },

    getSelectedIds: function(grid)
    {
        var selected = grid.getSelectionModel().getSelection();
        
        return this.getSelectedColumnValues(grid,'id');
    },
    
    getSelectedIdsInJson: function(grid)
    {
        var json = {};
        json['item'] = this.getSelectedIds(grid);
        if (json['item']) {
            json['count'] = json['item'].length ;            
        } else {
            json['count'] = 0;
        }


        return json;
    },

    getFormParameterInJson: function(form,pattern)
    {
        console.debug('Netra.helper.getFormParameterInJson : Pattern = ' + pattern);

        var json = {};

        form.query(pattern).forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
    },

    showToast: function(view, title, msg)
    {
        var me = this,
            toastAlignTo = view.getHeader(),
            toast = me.toast;

        if (!toast) {
            me.toast = Ext.toast({
                cls: 'x-toast-light',
                closeAction: 'hide', // we plan to reuse this toast
                html: msg,
                bodyPadding: 10,
                header: false,
                //anchor: toastAlignTo,
                maxWidth: Math.floor(view.getWidth() * 1),
                //title:'Description', // might need this if closable:true
                //closable: true, //TODO enable when styling is better
                //autoCloseDelay: 1000000,
                //anchorAlignment: 't-b',
                slideInDuration: 400,
                slideBackDuration: 1500,
                align: 't'
            });
        } else {
            toast.maxWidth = Math.floor(view.getWidth() * 1);
            toast.update(msg);
        }


    },

    showErrorToast: function(view, title, msg)
    {
        this.showToast(view, title, msg);
    },

    formatDate: function(date)
    {
        var yyyy = date.getFullYear().toString();                                    
        var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = date.getDate().toString();             
        var time = ("00" + date.getHours()).slice(-2) + ":" + ("00" + date.getMinutes()).slice(-2) + ":" + ("00" + date.getSeconds()).slice(-2);
        
        return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]) + " " + time;
    },

    formatDateTime: function(date)
    {
        var yyyy = date.getFullYear().toString();                                    
        var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = date.getDate().toString();             
                            
        return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);
    },

    getComponent: function(selector)
    {   
        return Ext.ComponentQuery.query(selector)[0];
    },

    refreshData: function(storeId, gridId, callbackFunc)
    {
        Netra.manager.getStore(storeId).load( 
        {
            scope: this,
            callback: function(records, operation, success) {
                
                var grid = Netra.helper.getComponent(gridId);
                
                grid.getView().refresh();

                if (callbackFunc != null) {
                    callbackFunc(records, operation, success);
                }
            }

        } );

    },

    refreshDataWithParameter: function(storeId, storeParams, gridId, callbackFunc)
    {
        Netra.manager.getStore(storeId).load( 
        {
            params: storeParams,
            scope: this,
            callback: function(records, operation, success) 
            {
                var grid = Netra.helper.getComponent(gridId);
                
                grid.getView().refresh();

                if (callbackFunc != null) {
                    callbackFunc();
                }
            }

        } );

    },

    showWaitDialog: function(msg, progress_msg, anim_id)
    {
       
        if (msg.length == 0 ) {
            msg = "Work in progress...";
        }

        if (progress_msg.length == 0 ) {
            progress_msg = "Work in progress...";
        }

        if (this.progressBoxInstance == null) {
            this.progressBoxInstance = Ext.create('Ext.window.MessageBox');
        }

        this.progressBoxInstance.animateTarget = anim_id;
        this.progressBoxInstance.wait(msg,progress_msg, {
            msg: msg,
            progressText: progress_msg,
            width:300,
            wait: {
                interval:200
            },
            animateTarget: anim_id
        });
       
    },

    hideWaitDialog: function()
    {
        //Netra.ProgressBox.hide();

        if (this.progressBoxInstance == null) return;

        this.progressBoxInstance.hide();
        //this.msgInstance.destroy();
    },
	
    reloadPage: function()
    {
        window.location.reload();
    },

    getCurrentNetworkData: function(field)
    {
        var store = Netra.manager.getStore('storeNetworks');
        return store.first().get(field);        
    },

    validateIp: function(ip) 
    {
        var x = ip.split("."), x1, x2, x3, x4;

        if (x.length == 4) {
            x1 = parseInt(x[0], 10);
            x2 = parseInt(x[1], 10);
            x3 = parseInt(x[2], 10);
            x4 = parseInt(x[3], 10);

            if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(x4)) {
                return false;
            }

            if ((x1 >= 0 && x1 <= 255) && (x2 >= 0 && x2 <= 255) && (x3 >= 0 && x3 <= 255) && (x4 >= 0 && x4 <= 255)) {
                return true;
            }
        }
        return false;
    },

    formatByteNumber: function(bytes, fixed)
    {
        if(bytes < 1024) return bytes + " Bytes";
        else if(bytes < 1048576) return(bytes / 1024).toFixed(fixed) + " KB";
        else if(bytes < 1073741824) return(bytes / 1048576).toFixed(fixed) + " MB";
        else return(bytes / 1073741824).toFixed(fixed) + " GB";
    },

    prettyNumber: function(pBytes, pUnits) 
    {
        // Handle some special cases
        if(pBytes == 0) return '0 Bytes';
        if(pBytes == 1) return '1 Byte';
        if(pBytes == -1) return '-1 Byte';

        var bytes = Math.abs(pBytes)
        if(pUnits && pUnits.toLowerCase() && pUnits.toLowerCase() == 'si') {
            // SI units use the Metric representation based on 10^3 as a order of magnitude
            var orderOfMagnitude = Math.pow(10, 3);
            var abbreviations = ['Bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        } else {
            // IEC units use 2^10 as an order of magnitude
            var orderOfMagnitude = Math.pow(2, 10);
            var abbreviations = ['Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        }
        var i = Math.floor(Math.log(bytes) / Math.log(orderOfMagnitude));
        var result = (bytes / Math.pow(orderOfMagnitude, i));

        // This will get the sign right
        if(pBytes < 0) {
            result *= -1;
        }

        // This bit here is purely for show. it drops the percision on numbers greater than 100 before the units.
        // it also always shows the full number of bytes if bytes is the unit.
        if(result >= 99.995 || i==0) {
            return result.toFixed(0) + ' ' + abbreviations[i];
        } else {
            return result.toFixed(2) + ' ' + abbreviations[i];
        }
    },

    getTemplateTotalStorageSize: function(templateRecord_storage)
    {
        var storage_size = 0;

        templateRecord_storage.forEach(function(record) 
        {
            console.debug("Netra.helper.getTemplateTotalStorageSize : record = ",record.size);

            if (record['size']>0) {
                storage_size += parseInt(record['size']);                
            }
        });

        return Netra.helper.formatByteNumber(storage_size);
    },

    getStorageSize: function(templateRecord_storage)
    {
        var storage_size = 0;
        var used_size = 0;
        
        if (templateRecord_storage == null) {
            return { 'total': storage_size, 'used': used_size };
        }

        templateRecord_storage.forEach(function(record) 
        {
            //console.debug("Netra.helper.getTemplateTotalStorageSize : record = ",record.size);

            storage_size += parseInt(record.size);
            used_size += parseInt(record.used_size);

        });

        return { 'total': storage_size, 'used': used_size };
    },

    arrayContains: function(needle, arrhaystack)
    {
        for (var i=0; i<arrhaystack.length; i++) {
            if (arrhaystack[i] == needle.toLowerCase()) {
                return i;
            }
        }
        return -1;
    },

    isLocalStorage: function(storage_type)
    {
        var types = ['lvm','ext','nfs','lvmoiscsi'];
        return this.arrayContains(storage_type,types);
    },

    getXenHostStorageSize: function(templateRecord_storage)
    {
        var _this = this;

        var storage_size = 0;
        var used_size = 0;
        
        if (templateRecord_storage == null) {
            return { 'total': storage_size, 'used': used_size };
        }

        templateRecord_storage.forEach(function(record) 
        {
            if (_this.isLocalStorage(record.type)>-1)
            {
                //console.debug("Netra.helper.getXenHostStorageSize : record = ",record);

                storage_size += parseInt(record.physical_size);
                used_size += parseInt(record.physical_utilisation);                
            }
        });

        return { 'total': storage_size, 'used': used_size };
    },

    metricToArrayData: function(metric_data)
    {
        arr = [];
        for (var i=0;i<metric_data.length;i++) 
        {
            //console.debug("metricToArrayData : metric_data = ", metric_data[i]);
            //arr.push( [parseInt(metric_data[i].last_updated), parseFloat(metric_data[i].value)] );
            
            var value = parseFloat(metric_data[i].value);
            if (isNaN(value)) {
                value = 0;
            }

            arr.push( value );
        }

        return arr;
    },

    doesContainString: function(arr, key)
    {
        for (var index=0; index<arr.length; index++) 
        {
            if (key.indexOf(arr[index]) > -1) {
                return true;
            }
        }
        return false;
    },

    sanitizeValue: function(value)
    {
        if (isNaN(value)) {
            return "";
        }
        return value;
    },

    utcToDate: function(value)
    {
        //return new Date(parseInt(value));
        var mEpoch = parseInt(value); 
        if(mEpoch<10000000000) mEpoch *= 1000; // convert to milliseconds (Epoch is usually expressed in seconds, but Javascript uses Milliseconds)

        var dDate = new Date();
        dDate.setTime(mEpoch)

        return Netra.helper.formatDate(dDate);
    },

    extractString: function(str,key1,key2)
    {
        var index1 = str.indexOf(key1);
        var index2 = str.indexOf(key2,index1);

        if ((index1<0) || (index2<0)) {
            return "";
        }

        return str.substring(index1+key1.length,index2);
    },

    clearTree: function(tree)
    {
        if (tree == null) return;

        tree.getRootNode().removeAll();
    },

    selectTreeNode: function(tree,index)
    {
        var node = tree.getRootNode().getChildAt(index);
        tree.getSelectionModel().select(node);
        //tree.fireEvent('itemclick', node, node, index);
    },
    
});



Ext.override(Ext.form.Panel, 
{
    clearForm: function ()
    {
        me = this;
		// Get form returns the ext basic form object
        me.getForm().getFields().each(function(f) {
  			f.setValue('');
    	});
	}
});




//console.debug("common.js");