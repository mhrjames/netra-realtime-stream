Ext.define("Netra.component.NumberLabel", {
	extend: 'Ext.container.Container',
	alias: 'widget.numberlabel',
	title: 'Title',
	number: '12',
	icon: '',

	initComponent : function() 
	{
		this.html = this.createNumberWidget(this.title, this.number, this.icon);

		this.callParent(arguments);
	},

	createNumberWidget: function (title, number, icon) 
    {
        var icon_output = "<span class='"+ icon + " number-widget-icon'></span>";
        
        var output = "<div class='number-widget-box'>";
        output += "<div class='number-widget-title'>" + title + "</div>";
        output += "<div class='number-widget-contents'>";
        output += "<div class='number-widget-number'>" + icon_output + number + "</div>";
        output += "</div>";
        output += "</div>";

        return output;          
    },

    updateValue: function(title, number, icon)
    {
        var new_html = this.createNumberWidget(title, number, icon);
        this.update(new_html);
        //this.callParent(arguments);
    },
});


Ext.define("Netra.component.SummaryLabel",
{
	extend: 'Ext.container.Container',
	alias: 'widget.summarylabel',

	
	//config
	title: 'Title',
	

	initComponent : function() 
	{
		Ext.create('Ext.panel.Panel', {
			layout: 'fit',
			title: this.title,
			items: [],

		}).show();
	}

});


Ext.define("Netra.component.HostDetail",
{
	extend: 'Ext.panel.Panel',
	alias: 'widget.hostdetail',
	
	summaryTitle: 'host',

    title: 'Host',
    titleAlign: 'center',

	items: [
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align : 'stretch',
                pack  : 'start',
            },

            items: 
            [
                {
                    xtype: 'numberlabel',
                    title: 'CPU', number: '2.6 GHz', icon: '', id: 'hostdetail_number_cpu'
                },            
                {
                    xtype: 'numberlabel',
                    title: 'OS', number: '2', icon: '', id: 'hostdetail_number_cpu_count'
                },            
                {
                    xtype: 'numberlabel',
                    title: 'Memory', number: '1Gb', icon: '', id: 'hostdetail_number_memory'
                },            
                {
                    xtype: 'numberlabel',
                    title: 'Disk', number: '1TB', icon: '', id: 'hostdetail_number_disk'
                },            
                {
                    xtype: 'numberlabel',
                    title: 'XEN Ver.', number: '6.2', icon: '', id: 'hostdetail_number_xen_version'
                },                            
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'table',
                columns: 2,
                tableAttrs: { 
                    style: { 
                        width: '100%',
                        height: '100%' 
                    } 
                }, 
                tdAttrs: { 
                    style: 'padding: 15px 15px 0px 0px; vertical-align: top;' 
                }                                        
            },

            items: [
                {
                    xtype: 'propertygrid',
                    id: 'grid_general',
                    title: 'General',
                    titleAlign: 'center',
                    colspan: 2,
                    nameColumnWidth: 180,
                    source: {},
                    sourceConfig: {
                        borderWidth: {
                            displayName: 'Border Width'
                        },
                        tested: {
                            displayName: 'QA'
                        }
                    }                                            
                },
                {
                    xtype: 'propertygrid',
                    id: 'grid_network',
                    title: 'Network',
                    titleAlign: 'center',
                    nameColumnWidth: 180,
                    source: {},

                    sourceConfig: {
                        borderWidth: {
                            displayName: 'Border Width'
                        },
                        tested: {
                            displayName: 'QA'
                        }
                    }                                            
                },
                {
                    xtype: 'propertygrid',
                    id: 'grid_disk',
                    title: 'Disk',
                    titleAlign: 'center',
                    nameColumnWidth: 180,
                    source: {},
                },

            ]
        }


    ],

	initComponent: function()
	{
		console.debug("Netra.component.HostDetail");

		this.callParent(arguments);
	}

});


Ext.define("Netra.component.ServerDetail",
{
    extend: 'Ext.panel.Panel',
    alias: 'widget.serverdetail',
    
    summaryTitle: 'host',

    title: 'Host',
    titleAlign: 'center',

    items: 
    [
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align : 'stretch',
                pack  : 'start',
            },

            items: 
            [
                { xtype: 'numberlabel', title: 'State', number: '', icon: '', id: 'serverdetail_number_state' },
                { xtype: 'numberlabel', title: 'CPU', number: '2.6 GHz', icon: '', id: 'serverdetail_number_cpu' },
                { xtype: 'numberlabel', title: 'Memory', number: '1Gb', icon: '', id: 'serverdetail_number_memory' },            
                { xtype: 'numberlabel', title: 'Disk', number: '1TB', icon: '', id: 'serverdetail_number_disk' },            
            ]
        },
        {
            xtype: 'container',
            layout: {
                type: 'table',
                columns: 3,
                tableAttrs: { 
                    style: { 
                        width: '100%',
                        height: '100%' 
                    } 
                }, 
                tdAttrs: { 
                    style: 'padding: 15px 15px 0px 0px; vertical-align: top;' 
                }                                        
            },

            items: [
                {
                    xtype: 'propertygrid',
                    id: 'grid_server_general',
                    title: 'General',
                    titleAlign: 'center',
                    //colspan: 2,
                    nameColumnWidth: 150,
                    source: {},
                    sourceConfig: {
                        borderWidth: {
                            displayName: 'Border Width'
                        },
                        tested: {
                            displayName: 'QA'
                        }
                    }                                            
                },
                {
                    xtype: 'propertygrid',
                    id: 'grid_server_network',
                    title: 'Network',
                    titleAlign: 'center',
                    nameColumnWidth: 150,
                    source: {},

                    sourceConfig: {
                        borderWidth: {
                            displayName: 'Border Width'
                        },
                        tested: {
                            displayName: 'QA'
                        }
                    }                                            
                },
                {
                    xtype: 'propertygrid',
                    id: 'grid_server_disk',
                    title: 'Disk',
                    titleAlign: 'center',
                    nameColumnWidth: 150,
                    source: {},
                },

            ]
        }


    ],

    initComponent: function()
    {
        console.debug("Netra.component.HostDetail");

        this.callParent(arguments);
    },

    updateServerDetailPanel: function(poolRecord, xenserverRecord, container_name, index)
    {
        var _this = this;

        console.debug("XenController.updateServerDetailPanel : pool_record = ",xenserverRecord);

        Netra.service.getXenserverDetail(poolRecord.data.name, xenserverRecord.data.reference, function(retcode,response) {

            console.debug("XenController.updateServerDetailPanel : response = ",response);


            var disk_size = Netra.helper.getStorageSize(response.data.storage);
            
            Netra.helper.getComponent('#serverdetail_number_cpu').updateValue('CPU Count',response.data.vcpus_max,'');
            Netra.helper.getComponent('#serverdetail_number_memory').updateValue('Max Memory',Netra.helper.formatByteNumber(response.data.memory_max),'');
            Netra.helper.getComponent('#serverdetail_number_disk').updateValue('Disk',Netra.helper.formatByteNumber(disk_size['total']),'');


            var params_general = { 'Name': response.data.name,
                            'Reference': response.data.reference,
                            'CPU Count': response.data.vcpus_max,
                            'Base Template': response.data.template_name,
                            'Power State': response.data.power_state,
                            'Total Memory': Netra.helper.formatByteNumber(response.data.memory_max),
                            'Memory Overhead': Netra.helper.formatByteNumber(response.data.memory_overhead),
                            'Template?': response.data.is_template,
                            'PV Args': response.data.pv_args,
                            'PV Bootloader': response.data.pv_bootloader,
                            'UUID': response.data.uuid,
                            'Description': response.data.description,
                            'Host': response.data.host_ref,
            }

            Netra.helper.updatePropertyGrid('#grid_server_general', params_general);

            var params_network = {
                'NIC Count': response.data.network.length,
                'Network Backend': response.data.network_backend,
            }

            for (var index=0; index < response.data.network.length; index++) 
            {
                name_prefix = 'NIC'+index.toString();
                params_network[name_prefix+" Device"] = response.data.network[index].device;
                params_network[name_prefix+" MAC"] = response.data.network[index].mac;
                params_network[name_prefix+" Name"] = response.data.network[index].name;
            }

            Netra.helper.updatePropertyGrid('#grid_server_network', params_network);

            if (response.data.storage != null) {

                var params_disk = {
                    'Storage Count': response.data.storage.length,
                    'Total Size': Netra.helper.formatByteNumber(disk_size['total'])
                }

                var name_prefix = "";
                for (var index=0; index < response.data.storage.length; index++) 
                {
                    name_prefix = 'storage'+index.toString();
                    params_disk[name_prefix+" Name"] = response.data.storage[index].name;
                    params_disk[name_prefix+" Type"] = response.data.storage[index].type;
                    params_disk[name_prefix+" Size"] = Netra.helper.formatByteNumber(response.data.storage[index].size);
                }

                Netra.helper.updatePropertyGrid('#grid_server_disk', params_disk);

            }


            Netra.helper.showCardView(container_name,index,'','');        

            Netra.helper.hideWaitDialog();    
        });

    },



});

Ext.define("Netra.component.SoftwareDetail",{
	extend: 'Ext.panel.Panel',
	alias: 'widget.softwaredetail',
	bodyPadding: '20 20 20 20',

    layout: {
        type: 'table',
        columns: 2,
        tableAttrs: { 
            style: { 
                width: '100%',
                height: '100%' 
            } 
        }, 
        tdAttrs: { 
            style: 'padding: 15px 15px 0px 0px; vertical-align: top;' 
        }                                        
    },

	items: [
		{
			xtype: 'image',
			width: 220,
			height: 220,
			src: 'http://www.sencha.com/img/20110215-feat-html5.png',
			//autoEl: 'div', // wrap in a div													
		},
		{
			xtype: 'component',
			flex: 2,
			rowspan: 2,
			html: '<h1>this is what!!!</h1>'
		},

		{
			xtype: 'form',
			width: 220,
			layout: 'vbox',

            defaults: {
                xtype: "textfield",
                anchor: '100%',						                                
                //align: 'stretch',
                //labelWidth: 140,
				labelAlign: 'top',
				msgTarget: 'side',
				padding: '0 10 0 10',
            },
            padding: '10 0 0 0',

			items: [
				{
                    fieldLabel : "Version",
                    id: 'text_instance_version',
                    value: "Netra",         
				},
				{
                    fieldLabel : "Author",
                    id: 'text_instance_author',
                    value: "Netra",         
				},
				{
                    fieldLabel : "Source",
                    id: 'text_instance_source',
                    value: "Netra",         
				},
				{
                    fieldLabel : "Date",
                    id: 'text_instance_date',
                    value: "Netra",         
				},

			]
		},

	],


	initComponent: function()
	{
		console.debug("Netra.component.HostDetail");

		this.callParent(arguments);
	}
	
});


Ext.define("Netra.component.LabelSlider", {
    extend: 'Ext.container.Container',
    alias: 'widget.labelslider',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },

    label: 'label',
    height: 50,
    value: 0,


    initComponent : function() 
    {
        console.debug("Netra.component.labelslider : this = ",this);

        this.items = 
        [   
            {
                xtype: 'label',
                text: this.label,
            },
            {
                xtype: 'progressbar',
                height: this.height,
                value: this.value,
            }
        ];


        this.callParent(arguments);
    },

    updateValue: function(number)
    {
        var progressbar = this.items.getAt(1);
        console.debug("Netra.component.labelslider.updateValue : item = ",progressbar);

        progressbar.setValue(number);
        //this.callParent(arguments);
    },
});


Ext.define("Netra.component.Grid.XenHost", {
    extend: 'Ext.container.Container',
    alias: 'widget.gridxenhost',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },

    
    initComponent : function() 
    {
        console.debug("Netra.component.Grid.XenHost : this = ",this);

        this.items = 
        [   
            {
                xtype: 'gridpanel',
                id: 'component_grid_xenhost',
                title: 'Xen Host List',
                titleAlign: 'center',
                stripeRows: true,
                columnLines: true,

                store: Netra.manager.getStore('storeXenHosts'),

                selType: 'checkboxmodel',
                selModel: {
                    checkOnly: true,
                    injectCheckbox: 0
                },

                columns: 
                [
                    {
                        text     : 'Name',
                        flex     : 1,
                        sortable : true,
                        dataIndex: 'name'
                    },                                    
                    {
                        text     : 'IP',
                        flex     : 2,
                        sortable : true,
                        dataIndex: 'ip'
                    },
                    {
                        text     : 'CPU',
                        flex     : 1,
                        dataIndex: 'cpu_count',
                    },
                    {
                        text     : 'Total Memory',
                        flex     : 1,
                        dataIndex: 'total_memory',
                        renderer : function(value) 
                        {
                            return Netra.helper.formatByteNumber(value);
                        }                                            
                    },
                    {
                        text     : 'Free Memory',
                        flex     : 1,
                        dataIndex: 'free_memory',
                        renderer : function(value) 
                        {
                            return Netra.helper.formatByteNumber(value);
                        }                                            

                    },
                ],

            },
        ];
        
        this.callParent(arguments);
    },

});


Ext.define("Netra.component.Grid.Pool", {
    extend: 'Ext.container.Container',
    alias: 'widget.gridpool',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },

    
    initComponent : function() 
    {
        console.debug("Netra.component.Grid.Pool : this = ",this);

        this.items = 
        [   
            {
                xtype: 'gridpanel',
                id: 'grid_cluster',
                flex: 1,
                title: 'Cluster List',
                titleAlign: 'center',
                stripeRows: true,
                columnLines: true,

                store: Netra.manager.getStore('storeClusters'),

                selType: 'checkboxmodel',
                selModel: {
                    checkOnly: true,
                    injectCheckbox: 0
                },
                
                columns: [
                    {
                        text     : 'Name',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'name'
                    },        
                    {
                        text     : 'Reference',
                        flex     :  3,
                        sortable : true,
                        dataIndex: 'reference'
                    },                        
                    {
                        text     : 'Master IP',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'master_ip'
                    },
                    {
                        text     : 'Description',
                        flex     :  3,
                        dataIndex: 'description'    
                    }
                ],
            },

        ];
        
        this.callParent(arguments);
    },

});

Ext.define("Netra.component.Grid.XenServer", {
    extend: 'Ext.container.Container',
    alias: 'widget.gridxenserver',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },

    
    initComponent : function() 
    {
        console.debug("Netra.component.Grid.XenServer : this = ",this);

        this.items = 
        [   
            {
                xtype: 'gridpanel',
                id: 'component_grid_xenserver',
                flex: 1,
                //title: 'XenServer List',
                titleAlign: 'center',
                stripeRows: true,
                columnLines: true,

                store: Netra.manager.getStore('storeXenServers'),

                selType: 'checkboxmodel',
                selModel: {
                    checkOnly: true,
                    injectCheckbox: 0
                },
                
                columns: [
                    {
                        text     : 'Name',
                        flex     :  2,
                        sortable : true,
                        dataIndex: 'name'
                    },        
                    {
                        text     : 'Power State',
                        flex     :  1.5,
                        sortable : true,
                        dataIndex: 'power_state',
                        renderer : function(value) 
                        {
                            var ret = false;
                            if (value == "Running") {
                                ret = true;
                            }

                            return Netra.helper.createActiveIcon(ret) + " " + value;
                        }
                    },
                    {
                        text     : 'Template Name',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'template_name'
                    },
                    {
                        text     : 'vCPU',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'vcpus_max'
                    },                    
                    {
                        text     : 'Memory',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'memory_max',
                        renderer : function(value) 
                        {
                            return Netra.helper.formatByteNumber(value);
                        }                                                                    
                    },
                    {
                        text     : 'Storage',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'storage',
                        renderer : function(value) 
                        {
                            return Netra.helper.getTemplateTotalStorageSize(value);
                        }                                                                    
                    },                    
                    {
                        text     : 'Host Ref',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'host_ref'
                    },                                                            
                    {
                        text     : 'Description',
                        flex     :  3,
                        dataIndex: 'description'    
                    },
                    {
                        text     : 'Reference',
                        flex     :  1,
                        sortable : true,
                        dataIndex: 'reference'
                    },                    
                ],
            },

        ];
        
        this.callParent(arguments);
    },

});



Ext.define("Netra.component.XenTree", {
    extend: 'Ext.tree.Panel',
    alias: 'widget.xentree',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },
    //id: 'xentree',
    //title: 'Xen',
    width: 200,
    rootVisible: false,
    useArrows: true,

    root: {
        text: 'Root',
        expanded: true,
        children: []
    },
    
    initComponent : function(config) 
    {
        console.debug("Netra.component.XenTree.initComponent");
        
        Ext.apply(this,config);
        this.callParent(arguments);
    },

    updateTree: function()
    {
        console.debug("Netra.component.XenTree.updateXenTree");

        var _this = this;

        //var store_pool = Netra.manager.getStore("storeClusters");
        var store_host = Netra.manager.getStore("storeXenHosts");
        var store_server = Netra.manager.getStore("storeXenServers");

        var xenhost_node, server_records = null;


        this.clearTree();

        store_host.data.removeAll();

        //console.debug("Netra.component.XenTree.updateXenTree : record = ",record_pool);
        
        pool_node = _this.addPoolNodeToTree("XEN", null);

        Netra.service.loadXenServerData("pool", function(xenserver_records, operation, success)  {

            Netra.service.loadXenHostData("pool", function(xenhost_records, operation, success) 
            {
                
                xenhost_records.forEach(function(record_xenhost) 
                {

                    xenhost_node = _this.addXenHostNodeToTree(pool_node,record_xenhost.data.name, record_xenhost);

                    var found_xenserver = store_server.query('host_ref',record_xenhost.data.reference);
                    
                    //console.debug("component.updateTree : found_xenserver = ", found_xenserver);
          
                    for (var index=0;index<found_xenserver.length;index++)
                    {
                        var xenserver = found_xenserver.items[index];
                        xenserver_node = _this.addXenServerNodeToTree(xenhost_node, xenserver.data.name, xenserver);
                    }

                });

                Netra.helper.showPanelLoadingMask(_this,false);
            });

        });

    },

/*
    updateTree: function()
    {
        console.debug("Netra.component.XenTree.updateXenTree");

        var _this = this;

        var store_pool = Netra.manager.getStore("storeClusters");
        var store_host = Netra.manager.getStore("storeXenHosts");
        var store_server = Netra.manager.getStore("storeXenServers");

        var pool_node, xenhost_node, server_records = null;


        this.clearTree();

        store_host.data.removeAll();

        //Netra.service.loadPoolData(this, function(pool_records, operation, success) 
        {
            console.debug("Netra.component.XenTree.updateXenTree --- 1");

            //pool_records.forEach(function(record_pool) 
            {

                console.debug("Netra.component.XenTree.updateXenTree : record = ",record_pool);
                
                pool_node = _this.addPoolNodeToTree(record_pool.data.name, record_pool);

                Netra.service.loadXenServerData(record_pool.data.name, function(xenserver_records, operation, success)  {

                    Netra.service.loadXenHostData(record_pool.data.name, function(xenhost_records, operation, success) 
                    {
                        
                        xenhost_records.forEach(function(record_xenhost) 
                        {

                            xenhost_node = _this.addXenHostNodeToTree(pool_node,record_xenhost.data.name, record_xenhost);

                            var found_xenserver = store_server.query('host_ref',record_xenhost.data.reference);
                            
                            //console.debug("component.updateTree : found_xenserver = ", found_xenserver);
                  
                            for (var index=0;index<found_xenserver.length;index++)
                            {
                                var xenserver = found_xenserver.items[index];
                                xenserver_node = _this.addXenServerNodeToTree(xenhost_node, xenserver.data.name, xenserver);
                            }

                        });

                    });

                });

            });
        });

    },
*/


    addNodeToTree: function(parent_node,text,node_type,leaf,record)
    {
        var node = parent_node.appendChild({
                leaf: leaf,
                text: text,
                nodeType: node_type,
                nodeRecord: record
        });

        parent_node.expand();

        this.getView().refresh();

        return node;
    },

    addPoolNodeToTree: function(text,record)
    {
        var node = this.addNodeToTree(this.getRootNode(), text, Netra.const.NODE_POOL, false, record);
        this.getView().refresh();

        return node;
    },

    addXenHostNodeToTree: function(parent,text,record)
    {
        var node = this.addNodeToTree(parent, text, Netra.const.NODE_HOST, false, record);
        //node.set("icon",Icons.nodeRole);
        this.getView().refresh();

        return node;
    },

    addXenServerNodeToTree: function(parent,text,record)
    {
        var node = this.addNodeToTree(parent, text, Netra.const.NODE_SERVER, true, record);
        //node.set("icon",Icons.nodeRole);
        this.getView().refresh();  

        return node;      
    },

    clearTree: function() 
    {
        this.getRootNode().removeAll();
    },


});


Ext.define("Netra.component.MultiChart", {
    extend: 'Chart.ux.Highcharts',
    alias: 'widget.multichart',
    layout: {
        type: 'vbox',
        align: 'stretch',
    },

    
    initComponent : function(config) 
    {
        console.debug("Netra.component.MultiChart.initComponent");
        
        Ext.apply(this,config);
        this.callParent(arguments);
    },

    clear: function()
    {
        this.removeAllSeries();
    },

    addYAxis: function(title, top, height)
    {
        this.chart.addAxis({
            title: {
                text: ''
            },
            height: height.toString()+"%",
            top: top.toString()+"%"
        }, false);

    },

    addGraph: function(type, name, data)
    {
/*
     addSeries([{
     *         name: 'Series A',
     *         data: [ [ 3, 5 ], [ 4, 6 ], [ 5, 7 ] ]
     *     }], true);
*/

        //console.debug("Netra.component.MultiChart.addGraph : count = ",this.series.length);
        
        //console.debug("addGraph : top = " + top + " , height = " + height + " , series_count = "+ this.series.length);

        var config = { type: type, name: name, data: data }
        
        //this.addYAxis(name, top,height);
        this.addSeries( [ config ], true);
    },


    drawChartWithData: function(data, chart_ref)
    {
        console.debug("drawChart : chart ref = " + chart_ref + " , data = ", data);

        var chart = this.getReference(chart_ref);
        var series = chart.series[0];

        console.debug("drawChartWithData : series.data = " , series.data);

        chart.setData(0,data);
    },
});
