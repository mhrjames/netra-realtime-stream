Ext.define("Netra.const", {
	singleton: true,

	REQ_OK: 0,
	REQ_FAIL: -1,

	NODE_POOL: 'pool',
	NODE_HOST: 'host',
	NODE_SERVER: 'server',

	NODE_CATALOG: 'catalog',
	NODE_DELIVERY: 'delivery',
	
	NODE_USER_ALL: 'all',
	NODE_USER_OU: 'ou',
	
	VM_RUNNING: 'Running',

});

Ext.define("Icons", {
	singleton: true,

	btnActive: "x-btn-glyph fontawesome-indent-left",
	btnSave: "x-btn-glyph fontawesome-save",
	btnPreview: "x-btn-glyph fontawesome-reply",
	btnApply: "x-btn-glyph fontawesome-spinner",
	btnRefresh: "x-btn-glyph fontawesome-refresh",
	btnClose: "x-btn-glyph fontawesome-minus-sign",
	btnValidate: "x-btn-glyph fontawesome-tint",
	btnDeploy: "x-btn-glyph fontawesome-repeat",
	btnDetail: "x-btn-glyph fontawesome-tags",

	icoActive: "fontawesome-check",
	icoInactive: "fontawesome-check-empty",

    icoEnabled: "/assets/famfam/icons/accept.png",
    icoDisabled: "/assets/famfam/icons/cancel.png",

	nodeServer: "/assets/node/server.png",
	nodeServerGroup: "/assets/node/images.png",
	nodeRole: "/assets/node/images.png",
	nodeDocument: "/assets/node/layout.png",
	nodeAll: "/assets/node/plugin.png",
	nodeOpenstack : "/assets/node/application_view_columns.png",
	nodeKickstart : "/assets/node/page_green.png",
	nodePxe : "/assets/node/page_save.png",
	nodeSubnet : "/assets/node/image_link.png",
	nodeSteps: "/assets/node/lightning.png",
	nodeReport: "/assets/node/report_disk.png",
	nodeNotification: "/assets/node/note.png",
	nodeConfigAll: "/assets/node/table_gear.png",
	nodeDhcp: "/assets/node/server_connect.png",
	nodeTftp: "/assets/node/server_compressed.png",
	nodeVerification: "/assets/node/text_signature.png",
	nodeRepoGlobal: "/assets/node/world.png",
	nodeRepoLocal: "/assets/node/script.png",

	menuBaremetal: "/assets/node/shape_square.png",
	menuDeployment: "/assets/node/shape_move_front.png",
	menuInfra: "/assets/node/drive_network.png",
	menuReport: "/assets/node/page_white_text.png",
	menuConfig: "/assets/node/cog.png",
});

