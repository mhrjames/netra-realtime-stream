Ext.define("Netra.manager",
{
    singleton: true,
    controllers: {},

    createController: function(name)
    {
        console.debug("Netra.manager.createController : name = " + name);

        var controller = Netra.app.getApplication().getController(name);
        if (controller == null) {
            controller = Ext.create(name);
        }

        return controller;
    },

    removeController: function(name)
    {
        console.debug("Netra.manager.removeController : name = " + name);

        var controller = Netra.app.getApplication().destroyController(name);
    },

    getController: function(name)
    {
        console.debug("Netra.manager.getController : name = " + name);
    
        return Netra.app.getApplication().getController(name, true);
    },

    dumpComponents: function(id)
    {
        console.debug("Netra.manager.dumpComponents : id = " + id);

        var comps = Ext.ComponentManager.getAll();
        for (var i=0;i<comps.length;i++) 
        {
            if (! id) {
                console.debug("   index = " + i, comps[i]);
                continue;
            } 

            if (comps[i].id.indexOf(id)>-1) {
                console.debug("   index = " + i, comps[i]);
            }

        }
    },

    getStore: function(id)
    {
        var store = Ext.data.StoreManager.get(id);
        if (store == null) {
            //store = this.createDataStore(id);
        }

        return store;
    },

    createDataStore: function(name)
    {
        console.debug("Netra.manager.createDataStore : name = " + name);
        
        var store = Ext.create(name);
        return store;
    },




    createController_old: function(name)
    {
        console.debug("Netra.manager.createController : name = " + name);

        var cont = this.getController(name);
        if (! cont) {
            cont = Ext.create(name);    
            this.addController(name, cont);
        }
        
        return cont;
    },

    removeController_old: function(name)
    {
        console.debug("Netra.manager.removeController : name = " + name);

        if (this.getController(name)) 
        {
            delete this.controllers[name];
        }
    },

    addController_old: function(name, controller)
    {
        console.debug("Netra.manager.addController : name = " + name);

        this.controllers[name] = controller;
    },

    getController_old: function(name)
    {
        console.debug("Netra.manager.getController : name = " + name);

        for (var prop in this.controllers) {
            if (this.controllers.hasOwnProperty(prop)) {
                console.debug("found!!!");
                return this.controllers[prop];
            }
        }

        return null;
    },

});

