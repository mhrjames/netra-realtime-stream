Ext.define("Netra.service",
{
    singleton   : true,


    sendRequest: function( url, method, data, callback)
    {
		console.debug("Netra.service.sendRequest");

		Ext.Ajax.request({
		    url: url,
		    method: method,
		    jsonData: data,
            scope: this,
		    success: function(response, opts) 
		    {
		        console.debug('sendRequest Response = ', response);
		        var json = JSON.parse(response.responseText);
		        callback(json.retcode,json);
		        	
		    },
		    failure: function(response, opts) 
		    {
		        console.debug('sendRequest : woops , response = ', response);
		        var error_msg = response.status + " : " + response.statusText;
		        callback(Netra.const.REQ_FAIL,error_msg);
		    }
		});

    },

    sendScopeRequest: function(scope, url, method, data, callback)
    {
        console.debug("Netra.service.sendRequest");

        Ext.Ajax.request({
            url: url,
            method: method,
            jsonData: data,
            scope: scope,
            success: function(response, opts) 
            {
                console.debug('sendRequest Response = ', response);
                var json = JSON.parse(response.responseText);
                callback(scope, json.retcode,json);
                    
            },
            failure: function(response, opts) 
            {
                console.debug('sendRequest : woops , response = ', response);
                var error_msg = response.status + " : " + response.statusText;
                callback(scope, Netra.const.REQ_FAIL,error_msg);
            }
        });

    },

    addNewHost: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.addNewHost");
		
		this.sendRequest('/api/host/add','POST',parameters_in_json, callback);
    },

    updateNewHost: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.updateNewHost");
		
		this.sendRequest('/api/host/update','POST',parameters_in_json, callback);
    },

    deleteHostRecord: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.deleteHostRecord");
		
		this.sendRequest('/api/host/delete','POST',parameters_in_json, callback);
    },

    addNewCluster: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.addNewCluster");
		
		this.sendRequest('/api/clusters/new','POST',parameters_in_json, callback);
    },

    updateNewCluster: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.updateNewCluster");
		
		this.sendRequest('/api/pool/update','POST',parameters_in_json, callback);
    },

    deleteClusterRecord: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.deleteClusterRecord");
		
		this.sendRequest('/api/clusters/delete','POST',parameters_in_json, callback);
    },

    updateClusterMember: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.updateClusterMember");
		
		this.sendRequest('/api/clusters/member','POST',parameters_in_json, callback);
    },

    installNewSoftware: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.installNewSoftware");
		
		this.sendRequest('/api/tasks/new','POST',parameters_in_json, callback);
    },

    inspectPool: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.inspectPool");
		
		this.sendRequest('/api/pool/inspect','GET',parameters_in_json, callback);
    },

    deleteTemplate: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.deleteTemplate");
		
		this.sendRequest('/api/xenserver/destroy','POST',parameters_in_json, callback);
    },

	makeCachedTemplate: function(parameters_in_json,callback)
    {
		console.debug("Netra.service.makeCachedTemplateComplete");
		
		this.sendRequest('/api/provision/cache_template','POST',parameters_in_json, callback);
    },

    loadPoolData: function(controller, callback_func)
    {
        //console.debug("Netra.service.loadPoolData : controller = ",controller);

        var store = Netra.manager.getStore('storeClusters');
        store.data.removeAll();

        store.load({
            scope: this,
            callback: callback_func
        });

    },

    loadAllTemplates: function(callback)
    {
		console.debug("Netra.service.loadAllTemplates");
		
		this.sendRequest('/api/template/all','GET',null, callback);
    },


    loadTemplateData: function(pool, callback_func)
    {
        var template_store = Netra.manager.getStore('storeTemplates');
        template_store.removeAll();

        template_store.load({
            params: {
                'pool': pool
            },
            callback: callback_func,
            scope: this
        });

    },

    loadBaseTemplateData: function(pool, callback_func)
    {
        var template_store = Netra.manager.getStore('storeTemplates');
        template_store.removeAll();

        template_store.load({
            params: {
                'pool': pool,
                'base': true,
            },
            callback: callback_func,
            scope: this
        });

    },

    loadXenHostData: function(pool, callback_func)
    {
        var store = Netra.manager.getStore('storeXenHosts');
        store.removeAll();

        store.load({
            params: {
                'pool': pool
            },
            callback: callback_func,
            scope: this
        });

    },

    loadXenServerData: function(pool, callback_func) 
    {
        console.debug("Netra.service.loadXenServerData");

        var store = Netra.manager.getStore('storeXenServers');
        store.data.removeAll();

        store.load({
            scope: this,
            params: {
				'pool': pool
            },
            callback: callback_func,
        });

    },

    loadPBDData: function(pool, callback_func)
    {
        var store = Netra.manager.getStore('storePBDs');
        store.removeAll();

        store.load({
            params: {
                'pool': pool
            },
            callback: callback_func,
            scope: this
        });

    },

    loadStorageRepositoryData: function(pool, callback_func)
    {
        var store = Netra.manager.getStore('storeStorageRepositories');
        store.removeAll();

        store.load({
            params: {
                'pool': pool
            },
            callback: callback_func,
            scope: this
        });

    },

    loadMachineCatalogData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeMachineCatalogs');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    loadHostData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeHosts');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    loadDeliveryGroupData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeDeliveryGroups');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    loadHypervisorConnectionData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeHypervisorConnections');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    loadTaskData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeTasks');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    loadSubtaskData: function(task_id, callback_func)
    {
        var store = Netra.manager.getStore('storeSubtasks');
        store.removeAll();

        store.load({
            params: {
                'task_id': task_id
            },
            callback: callback_func,
            scope: this
        });

    },

    loadXenVDIData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeXenVDIs');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    loadUserInfoData: function(callback_func)
    {
        var store = Netra.manager.getStore('storeUserInfos');
        store.removeAll();

        store.load({
            callback: callback_func,
            scope: this
        });

    },

    getLatestHostPerfData: function(pool, host_ref, callback_func)
    {
        console.debug("Netra.service.getLatestHostPerfData");

        var parameters_in_json = { 'pool': pool, 'host_ref': host_ref};

        this.sendRequest('/api/perf/xenhost','POST',parameters_in_json, callback_func);
    },

    getLatestVMPerfData: function(pool, host_ref, vm_ref, callback_func)
    {
        console.debug("Netra.service.getLatestHostPerfData");

        var parameters_in_json = { 'pool': pool, 'host_ref': host_ref, 'vm_ref': vm_ref };

        this.sendRequest('/api/perf/xenserver','POST',parameters_in_json, callback_func);
    },


    getHostDetail: function(pool,host_ip,callback)
    {
        console.debug("Netra.service.getHostDetail");
        
        var parameters_in_json = { 'pool': pool, 'ip': host_ip };        
        this.sendRequest('/api/xenhost/detail','POST',parameters_in_json, callback);
    },

    getXenserverDetail: function(pool,vm_ref,callback)
    {
        console.debug("Netra.service.getXenserverDetail");
        
        var parameters_in_json = { 'pool': pool, 'vm_ref': vm_ref };
        this.sendRequest('/api/xenserver/get','POST',parameters_in_json, callback);
    },


});
