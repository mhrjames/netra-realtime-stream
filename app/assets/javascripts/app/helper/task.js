Ext.define("Netra.task",
{
	statics: 
	{

	    sendRequest: function(controller, url, method, data, callback)
	    {
			console.debug("Netra.task.sendRequest");

			Ext.Ajax.request({
			    url: url,
			    method: method,
			    jsonData: data,

			    success: function(response, opts) 
			    {
			        console.debug('sendRequest Response = ', response);
			        var json = JSON.parse(response.responseText);
			        callback(controller, json.retcode, json);
			        	
			    },
			    failure: function(response, opts) 
			    {
			        console.debug('sendRequest : woops , response = ', response);
			        var error_msg = response.status + " : " + response.statusText;
			        callback(controller, Netra.const.REQ_FAIL,error_msg);
			    }
			});

	    },

	    sendExcelRequest: function(controller, url, method, data, callback)
	    {
			console.debug("Netra.task.sendExcelRequest");

			Ext.Ajax.request({
			    url: url,
			    method: method,
			    jsonData: data,
         		headers: {
                	'Content-type': 'multipart/form-data; application/vnd.ms-excel',
                	'Content-Disposition': 'attachment'
            	},

			    success: function(response, opts) 
			    {
			        console.debug('sendRequest Response = ', response);
			        var json = JSON.parse(response.responseText);
			        callback(controller, json.retcode, json);
			        	
			    },
			    failure: function(response, opts) 
			    {
			        console.debug('sendRequest : woops , response = ', response);
			        var error_msg = response.status + " : " + response.statusText;
			        callback(controller, Netra.const.REQ_FAIL,error_msg);
			    }
			});

	    },

	    sendServerActionRequest: function(action,json,success_callback,failure_callback)
	    {
	        console.debug("Netra.task.sendServerActionRequest");

	        Ext.Ajax.request({
	            url: '/api/openstack/instances/' + action,
	            method: 'POST',
	            jsonData: json,
	            success: function(response, opts) 
	            {
	                var json = JSON.parse(response.responseText);

	                if (json.retcode == 0) {
	                    success_callback(json);
	                } else {
	                    failure_callback(json);
	                }            

	            },
	            failure: function(response, opts) 
	            {
	                console.debug('sendDeleteRequest : woops');

	                Ext.MessageBox.alert('Error', 'Fail to send request to server');
	            }            
	        });
	    },

	    sendStartRequest: function(controller, json, callback_func)
	    {
	        console.debug("Netra.task.sendStartRequest");

	        this.sendServerActionRequest('start',json, function(json) 
	        {
	            console.debug('sendStartRequest.Success : JSON = ' , json);
	            
	            Ext.GlobalEvents.fireEvent('onInstanceStart', {} );

	        }, function(msg) {
	            console.debug('sendStartRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });
	    },

	    sendStopRequest: function(json)
	    {
	        console.debug("Netra.task.sendStopRequest");

	        this.sendServerActionRequest('stop',json, function(json) {
	            console.debug('sendStopRequest.Success : JSON = ', json);
	            
	            Ext.GlobalEvents.fireEvent('onInstanceStop', {} );

	        }, function(json) {
	            console.debug('sendStopRequest.Failure : JSON = ' , json);
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendSuspendRequest: function(json)
	    {
	        console.debug("Netra.task.sendSuspendRequest");

	        this.sendServerActionRequest('suspend',json, function(json) {
	            console.debug('sendSuspendRequest.Success : JSON = ', json);
	            Ext.GlobalEvents.fireEvent('onInstanceSuspend', {} );
	        }, function(json) {
	            console.debug('sendSuspendRequest.Failure : Msg = ', json);            
	            //Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendResumeRequest: function(json)
	    {
	        console.debug("Netra.task.sendResumeRequest");

	        this.sendServerActionRequest('resume',json, function(json) {
	            console.debug('sendResumeRequest.Success : JSON = ', json);
	            Ext.GlobalEvents.fireEvent('onInstanceResume', {} );
	        }, function(json) {
	            console.debug('sendResumeRequest.Failure : JSON = ' , json);
	            //Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendRebootRequest: function(json)
	    {
	        console.debug("Netra.task.sendRebootRequest");

	        this.sendServerActionRequest('reboot',json, function(json) {
	            console.debug('sendRebootRequest.Success : JSON = ' , json);
	            Ext.GlobalEvents.fireEvent('onInstanceReboot', {} );
	        }, function(msg) {
	            console.debug('sendRebootRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendTerminateRequest: function(json)
	    {
	        console.debug("Netra.task.sendTerminateRequest");

	        this.sendServerActionRequest('terminate',json, function(json) {
	            console.debug('sendTerminateRequest.Success : JSON = ' , json);
	            Ext.GlobalEvents.fireEvent('onInstanceTerminate', {} );
	            //_this.refreshData();
	        }, function(msg) {
	            console.debug('sendTerminateRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendSnapshotRequest: function(json)
	    {
	        console.debug("Netra.task.sendSnapshotRequest");

	        //var _this = this;

	        this.sendServerActionRequest('snapshot',json, function(json) {
	            console.debug('sendSnapshotRequest.Success : JSON = ' , json);
	            Ext.GlobalEvents.fireEvent('onInstanceSnapshot', {} );
	            //_this.refreshData();
	        }, function(msg) {
	            console.debug('sendSnapshotRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendResizeRequest: function(json)
	    {
	        console.debug("Netra.task.sendResizeRequest");

	        //var _this = this;

	        this.sendServerActionRequest('resize',json, function(json) {
	            console.debug('sendResizeRequest.Success : JSON = ' , json);
	            Ext.GlobalEvents.fireEvent('onInstanceResize', {} );
	            //_this.refreshData();
	        }, function(msg) {
	            console.debug('sendResizeRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendVolumeAttachRequest: function(json)
	    {
	        console.debug("Netra.task.sendVolumeAttachRequest");

	        //var _this = this;

	        this.sendServerActionRequest('attach_volume',json, function(json) {
	            console.debug('sendVolumeAttachRequest.Success : JSON = ' , json);
	            Ext.GlobalEvents.fireEvent('onInstanceAttachVolume', {} );
	            //_this.refreshData();
	        }, function(msg) {
	            console.debug('sendVolumeAttachRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendVolumeDetachRequest: function(json)
	    {
	        console.debug("Netra.task.sendVolumeDetachRequest");

	        //var _this = this;

	        this.sendServerActionRequest('detach_volume',json, function(json) {
	            console.debug('sendVolumeDetachRequest.Success : JSON = ' , json);
	            Ext.GlobalEvents.fireEvent('onInstanceDetachVolume', {} );
	            //_this.refreshData();
	        }, function(msg) {
	            console.debug('sendVolumeDetachRequest.Failure : Msg = ' + msg);            
	            Ext.MessageBox.alert('Error', msg);
	        });

	    },

	    sendVncConsoleRequest: function(record)
	    {
	        var _this = this;

	        console.debug("InstanceController.sendVncConsoleRequest");
   			
   			var json = {};
			json["instance_id"] = record.data.id;

	        this.sendServerActionRequest('console',json, function(json) {
	            console.debug('sendVncConsoleRequest.Success : json = ' , json);				
				window.open(json.data.console.url);

	            //_this.refreshData();
	        }, function(json) {
	            console.debug('sendResizeRequest.Failure : json = ' , json);
	            //Ext.MessageBox.alert('Error', msg);
	        });

/*
	        Ext.Ajax.request({
	            url: '/api/openstack/instances/console',
	            method: 'POST',
	            jsonData: { "instance_id": record.data.id },
	            success: function(response, opts) 
	            {
	                var json = JSON.parse(response.responseText);

	                console.debug("getVncConsole : ", json);

	                if (json.retcode == 0) {

	                } else {

	                }

	                window.open(json.data.console.url);
	                
	                //console.debug("updateConsoleLog : area = " , _this.getReference('area_instance_log'));
	                //_this.getReference('area_instance_log').setValue(json.data.output);

	            },
	            failure: function(response, opts) 
	            {
	                console.debug('sendDeleteRequest : woops');
	                Ext.MessageBox.alert('Error', 'Fail to get console window from server');
	            }            
	        });
*/

	    },

	    getVolumeLimit: function(success_callback,failure_callback)
	    {
	        console.debug("Netra.task.getVolumeLimit");

	        Ext.Ajax.request({
	            url: '/api/openstack/volumes/limit',
	            method: 'POST',

	            success: function(response, opts) 
	            {
	                var json = JSON.parse(response.responseText);
	                success_callback(json);
	            },
	            failure: function(response, opts) 
	            {
	                console.debug('getVolumeLimit : woops');
	                failure_callback(response);
	            }            
	        });
	    },


	    sendNewInstanceRequest: function(json)
	    {
	        console.debug("Netra.task.sendNewInstanceRequest");
	        
	        Ext.Ajax.request({
	            url: '/api/openstack/instances/new',
	            method: 'POST',
	            jsonData: json,
	            success: function(response, opts) 
	            {
	                console.debug('sendNewInstanceRequest : success');
	                Ext.GlobalEvents.fireEvent('onNewInstance', {} );
	            },
	            failure: function(response, opts) {
	                console.debug('sendNewInstanceRequest : woops');
	                Ext.GlobalEvents.fireEvent('onNewInstance', {} );
	            }
	        });

	    },

	    sendUpdateQuotaRequest: function(json)
	    {
	        console.debug("Netra.task.sendUpdateQuotaRequest");
	        
	        Ext.Ajax.request({
	            url: '/api/openstack/tenants/update_quota',
	            method: 'POST',
	            jsonData: json,
	            success: function(response, opts) 
	            {
	                console.debug('sendUpdateQuotaRequest : success');
	                Ext.GlobalEvents.fireEvent('onUpdateQuota', {} );
	            },
	            failure: function(response, opts) {
	                console.debug('sendUpdateQuotaRequest : woops');
	                Ext.GlobalEvents.fireEvent('onUpdateQuota', {} );
	            }
	        });

	    },

		sendAssignFloatingRequest: function(json)
		{
    		console.debug("Netra.task.sendAssignFloatingRequest : json = ",json);

	        this.sendServerActionRequest('assign_floating',json, function(json) {
	            console.debug('sendAssignFloatingRequest.Success : json = ' , json);
	            //_this.refreshData();
	        }, function(json) {
	            console.debug('sendAssignFloatingRequest.Failure : json = ' , json);
	            //Ext.MessageBox.alert('Error', msg);
	        });

		},

		sendUnassignFloatingRequest: function(json)
		{
    		console.debug("Netra.task.sendUnassignFloatingRequest : json = ",json);

	        this.sendServerActionRequest('unassign_floating',json, function(json) {
	            console.debug('sendUnassignFloatingRequest.Success : json = ' , json);
	            //_this.refreshData();
	        }, function(json) {
	            console.debug('sendUnassignFloatingRequest.Failure : json = ' , json);
	            //Ext.MessageBox.alert('Error', msg);
	        });

		},

		sendVMProvision: function(json, callback_func)
		{
    		console.debug("Netra.task.sendVMProvision : json = ",json);

    		this.sendRequest('/api/provision/xenserver','POST',json, callback_func);
		},

		sendVMProvisionTask: function(json, callback_func)
		{
    		console.debug("Netra.task.sendVMProvisionTask : json = ",json);

    		this.sendRequest('/api/provision/new_provision_task','POST',json, callback_func);
		},




	    startVM: function(controller, pool,vm_ref,callback)
	    {
	        console.debug("Netra.service.startVM");
	        
	        var parameters_in_json = { 'pool': pool, 'vm_ref': vm_ref };
	        this.sendRequest(controller, '/api/xenserver/start','POST',parameters_in_json, callback);
	    },

	    rebootVM: function(controller, pool,vm_ref,callback)
	    {
	        console.debug("Netra.service.rebootVM");
	        
	        var parameters_in_json = { 'pool': pool, 'vm_ref': vm_ref };
	        this.sendRequest(controller, '/api/xenserver/hard_reboot','POST',parameters_in_json, callback);
	    },

	    shutdownVM: function(controller, pool,vm_ref,callback)
	    {
	        console.debug("Netra.service.shutdownVM");
	        
	        var parameters_in_json = { 'pool': pool, 'vm_ref': vm_ref };
	        this.sendRequest(controller, '/api/xenserver/stop','POST',parameters_in_json, callback);
	    },

	    brutalShutdownVM: function(controller, pool,vm_ref,callback)
	    {
	        console.debug("Netra.service.brutalShutdownVM");
	        
	        var parameters_in_json = { 'pool': pool, 'vm_ref': vm_ref };
	        this.sendRequest(controller, '/api/xenserver/brutal_shutdown','POST',parameters_in_json, callback);
	    },

	    destroyVM: function(pool,vm_ref,callback)
	    {
	        console.debug("Netra.service.destroyVM");
	        
	        var parameters_in_json = { 'pool': pool, 'vm_ref': vm_ref };
	        this.sendRequest('/api/xenserver/destroy','POST',parameters_in_json, callback);
	    },

	    getTaskStatus: function(controller, pool, task_id, callback)
	    {
	        console.debug("Netra.task.getTaskStatus");
	        
	        var parameters_in_json = {'pool':pool, 'task_id': task_id };
	        this.sendRequest(controller, '/api/task/status','POST',parameters_in_json, callback);
	    },

		runProvisionTask: function(controller, json, callback_func)
		{
    		console.debug("Netra.task.runProvisionTask : json = ",json);

    		this.sendRequest(controller,'/api/task/run','POST',json, callback_func);
		},

		deleteTaskRecord: function(controller, pool, task_id, callback_func)
		{
    		console.debug("Netra.task.deleteTaskRecord : task_id = ",task_id);
    		
	        var parameters_in_json = {'pool':pool, 'task_id': task_id };
    		this.sendRequest(controller,'/api/task/delete','POST',parameters_in_json, callback_func);
		},

		runAssignTask: function(controller, json, callback_func)
		{
			console.debug("Netra.task.runAssignTask : json = ",json);
			this.sendRequest(controller,'/api/desktop/do_assign','POST',json, callback_func);	
		},

		updateUserinfo: function(controller, json, callback_func)
		{
    		console.debug("Netra.task.updateUserinfo : json = ",json);

    		this.sendRequest(controller,'/api/user/update','POST',json, callback_func);
		},

		deleteUserinfo: function(controller, json, callback_func)
		{
    		console.debug("Netra.task.deleteUserinfo : json = ",json);

    		this.sendRequest(controller,'/api/user/delete','POST',json, callback_func);
		},

		unassignVM: function(controller, json, callback_func)
		{
			console.debug("Netra.task.unassignVM : json = ",json);
			this.sendRequest(controller,'/api/desktop/do_unassign','POST',json, callback_func);	
		},

		getConfig: function(controller, json, callback_func)
		{
			console.debug("Netra.task.getConfig : json = ",json);
			this.sendRequest(controller,'/api/config','POST',json, callback_func);	
		},

		updateConfig: function(controller, json, callback_func)
		{
			console.debug("Netra.task.updateConfig : json = ",json);
			this.sendRequest(controller,'/api/config/update','POST',json, callback_func);	
		},

	    requestDailyAllHostData: function(controller, pool, start_time, end_time, callback_func)
	    {
	        console.debug("Netra.task.requestDailyAllHostData");

	        var parameters_in_json = { 'pool': pool, 'start_time': start_time, 'end_time': end_time };

	        this.sendRequest(controller,'/api/report/xenhost/all','POST',parameters_in_json, callback_func);
	    },

	    requestDailyAllHostDataInExcel: function(controller, pool, start_time, end_time, callback_func)
	    {
	        console.debug("Netra.task.requestDailyAllHostDataInExcel");

	        var parameters_in_json = { 'pool': pool, 'start_time': start_time, 'end_time': end_time };

	        this.sendExcelRequest(controller,'/api/report/xenhost/all/excel','POST',parameters_in_json, callback_func);
	    },

	}
});
