Ext.define("Netra.user",
{
	singleton	: true,

	LOGIN_URL	: "/login.do",
	LOGOUT_URL	: "/logout.do",

	GET_USERS_URL	:  "/users/all",
	GET_USER_URL	: "/users/get",

	currentUser: null,


	hasRole : function(controller, action){
		// console.log("Inside the hasRole");
		var currentUser = Ext.decode( localStorage.getItem('currentUser'));

		if( !currentUser || !currentUser['role'] ){
			return false; 
		}

		if( 
				(
					currentUser['role']['system'] &&
					currentUser['role']['system']['administrator']  
				) || 
				(
						currentUser['role'][controller] && 
						currentUser['role'][controller][action]  
				) ){

			return true; 
		}else{
			return false; 
		}
	},

	
	login: function(user_instance)
	{
		console.debug("Netra.user.login : user = ",user_instance);

		this.currentUser = user_instance;
	},

	logout: function()
	{
		console.debug("Netra.user.logout : user = ",this.currentUser);

		this.currentUser = null;
	},

	isLogin: function()
	{
		return (this.currentUser != null);
	},

	getCurrentUser: function()
	{
		return this.currentUser;
	},

});

