Ext.define('Netra.model.AdUser', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name' },
        {name: 'dn' },
        {name: 'cn' },
        {name: 'ou' },
        {name: 'distinguishedname' },
        {name: 'userprincipalname' },
        {name: 'objectcategory' },
        {name: 'account' },
        {name: 'last_logon' },
        {name: 'last_logoff' },
        {name: 'computer_account' },
        {name: 'machine_uuid' },
    ]
});