Ext.define('Netra.model.Cluster', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', label: 'Name' },
        {name: 'reference', label: 'Reference' },

        {name: 'master_ip', label: 'Master IP'  },

        {name: 'created_at', label: 'Created at'  },
        {name: 'updated_at', label: 'Updated at'  },

        {name: 'description', label: 'Description'  },

    ]
});



/*
  create_table "clusters", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "creator"
    t.datetime "created_at"
    t.datetime "updated_at"
  end
*/  
