Ext.define('Netra.model.DeliveryGroup', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'Name'},
        {name: 'Description'},
        {name: 'DeliveryType'},
        {name: 'ColorDepth'},
        {name: 'DesktopKind'},
        {name: 'DesktopsAvailable'},
        {name: 'DesktopsDisconnected'},
        {name: 'DesktopsInUse'},
        {name: 'DesktopsNeverRegistered'},
        {name: 'DesktopsPreparing'},
        {name: 'DesktopsUnregistered'},
        {name: 'InMaintenanceMode'},
        {name: 'MachineConfigurationUids'},
        {name: 'SessionSupport'},
        {name: 'PublishedName'},
        {name: 'UUID'},
        {name: 'Uid'},
        {name: 'TotalDesktops'},
    ]
});