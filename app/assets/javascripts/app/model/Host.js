Ext.define('Netra.model.Host', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id' },
        {name: 'name' },
        {name: 'ip' },
        {name: 'mac' },
        {name: 'cluster_name' },
        {name: 'cpu' },
        {name: 'memory' },
        {name: 'disk' },
        {name: 'os' },
        {name: 'ssh_port' },        
        {name: 'description' },
        {name: 'facter' },        
        {name: 'created_at' },
        {name: 'updated_at' },        
        {name: 'userid' },  
        {name: 'password' },  
    ]
});


/*
  create_table "hosts", force: true do |t|
    t.string   "name"
    t.string   "ip"
    t.string   "mac"
    t.string   "cluster_name"
    t.string   "cpu"
    t.string   "memory"
    t.string   "disk"
    t.string   "os"
    t.string   "userid"
    t.string   "password"
    t.text     "description"
    t.text     "facter"
    t.datetime "created_at"
    t.datetime "updated_at"
  end
*/