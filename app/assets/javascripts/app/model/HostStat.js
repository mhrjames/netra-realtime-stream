Ext.define('Netra.model.HostStat', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ip' },
        {name: 'cpu_avg' },
        {name: 'loadavg' },
        {name: 'memory_free_kib' },
        {name: 'memory_total_kib' },
        {name: 'pif_aggr_rx' },
        {name: 'pif_aggr_tx' },   
    ]
});