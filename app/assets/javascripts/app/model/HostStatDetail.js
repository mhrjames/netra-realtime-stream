Ext.define('Netra.model.HostStatDetail', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'date'},
        {name: 'cpu_avg' },
        {name: 'loadavg' },
        {name: 'memory_free_kib' },
        {name: 'memory_total_kib' },
        {name: 'pif_aggr_rx' },
        {name: 'pif_aggr_tx' },   
    ],

});
