Ext.define('Netra.model.HypervisorConnection', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'Name' },
        {name: 'HypHypervisorConnectionUid'},
        {name: 'IsReady' },
        {name: 'MachineCount' },
        {name: 'MaxAbsoluteActiveActions' },
        {name: 'MaxPvdPowerActionsPercentageOfDesktops' },
        {name: 'PreferredController' },   
        {name: 'State' },   
        {name: 'Uid' },   
    ],

});

