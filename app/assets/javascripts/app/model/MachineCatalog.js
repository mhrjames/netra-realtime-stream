Ext.define('Netra.model.MachineCatalog', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'Name' },    
        { name: 'Description' },
        { name: 'AllocationType' },
        { name: 'AssignedCount' },
        { name: 'AvailableCount' },
        { name: 'AvailableAssignedCount' },
        { name: 'IsRemotePC' },
        { name: 'MinimumFunctionalLevel' },
        { name: 'MachinesArePhysical' },
        { name: 'PersistUserChanges' },
        { name: 'ProvisioningType' },
        { name: 'SessionSupport' },
        { name: 'UUID' },
        { name: 'Uid' },
        { name: 'UnassignedCount' },
        { name: 'UsedCount' },        
    ],

});
