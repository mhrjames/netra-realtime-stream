Ext.define('Netra.model.OrganizationUnit', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'dn', label: 'ID' },    
        {name: 'name', label: 'Name' },        
        {name: 'ou', label: 'Owner' },        
        {name: 'distinguishedname', label: 'Public' },
    ]
});