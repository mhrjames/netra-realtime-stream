Ext.define('Netra.model.PBD', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'uuid'},
        {name: 'reference'},
        {name: 'host'},      
        {name: 'host_ref'},
        {name: 'sr'},
        {name: 'sr_reference'},
        {name: 'sr_size'},
        {name: 'sr_used_size'},
        {name: 'attached'},
    ]
});

/*

        {name: 'id'},
        {name: 'name'},
        {name: 'network_address'},
        {name: 'updated_at', type: 'date'},
        {name: 'created_at', type: 'date'},
        {name: 'mask'},
        {name: 'gateway'},
        {name: 'domain'},
        {name: 'dhcp_proxy'},
        {name: 'primary_dns'},
        {name: 'start_ip_range'},        
        {name: 'end_ip_range'},                
        {name: 'broadcast_addr'},
        {name: 'pxe_file'},        
        {name: 'next_server'},      
        {name: 'status'},
        {name: 'vlan_id'},      
        {name: 'description'},      
        {name: 'name_prefix'},                
        {name: 'max_lease_time'},        
        {name: 'default_lease_time'},                
        {name: 'dhcp_interface'}

    t.string   "name"
    t.string   "network_address"
    t.string   "mask"
    t.string   "gateway"
    t.string   "primary_dns"
    t.string   "secondary_dns"
    t.string   "start_ip_range"
    t.string   "end_ip_range"
    t.string   "vlan_id"
    t.string   "domain"
    t.string   "dhcp_proxy"
    t.string   "tftp_proxy"
    t.string   "dns_proxy"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "next_server"
    t.string   "broadcast_addr"
    t.string   "pxe_file",           :default => "pxelinux.0"
    t.integer  "default_lease_time", :default => 300
    t.integer  "max_lease_time",     :default => 7200
    t.string   "name_prefix"
    t.string   "dhcp_interface"
*/