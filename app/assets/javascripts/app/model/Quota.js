Ext.define('Netra.model.Quota', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID' },    
        {name: 'cores', label: 'VCPU Cores' },
        {name: 'fixed_ips', label: 'Priavet IP' },
        {name: 'floating_ips', label: 'Public IP' },
        {name: 'injected_file_content_bytes', label: 'Inject File Size' },
        {name: 'injected_file_path_bytes', label: 'Inject File Path Size' },
        {name: 'injected_files', label: 'Inject Files' },
        {name: 'instances', label: 'Virtual Instance' },
        {name: 'key_pairs', label: 'Key Paris' },
        {name: 'metadata_items', label: 'Metadata Items' },
        {name: 'ram', label: 'RAM' },
        {name: 'security_group_rules', label: 'Security Group Rules' },
        {name: 'security_groups', label: 'Security Groups' },
    ]
});