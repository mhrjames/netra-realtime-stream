Ext.define('Netra.model.Repository', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', label: 'Name' },        
        {name: 'version', label: 'Version' },
        {name: 'source', label: 'Source' },
        {name: 'author', label: 'Author' },
        {name: 'license', label: 'License' },
        {name: 'summary', label: 'Summary' },
        {name: 'project_page', label: 'Project Page' },
        {name: 'dependency', label: 'Dependency' },
        {name: 'description', label: 'Description' },        
/*
        {name: 'id', label: 'ID' },    
        {name: 'created_at', label: 'Created at' },
        {name: 'updated_at', label: 'Updated at' },                
*/        
    ]
});