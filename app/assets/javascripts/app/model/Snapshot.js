Ext.define('Netra.model.Snapshot', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID' },    
        {name: 'display_name', label: 'Name' },        
        {name: 'created_at', label: 'Created At' },        
        {name: 'display_description', label: 'Description' },        
        {name: 'status', label: 'Status' },

        {name: 'os-extended-snapshot-attributes:progress', label: 'Progress' },
        {name: 'volume_id', label: 'Volume ID' },
        {name: 'os-extended-snapshot-attributes:project_id', label: 'Project ID' },
        {name: 'metadata', label: 'Meta Data' },

        {name: 'size', label: 'Size(GB)' },   
    ]
});