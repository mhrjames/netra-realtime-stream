Ext.define('Netra.model.StorageRepository', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name'},
        {name: 'uuid'},
        {name: 'reference'},
        {name: 'description'},
        {name: 'type'},
        {name: 'physical_size'},
        {name: 'physical_utilisation'},
        {name: 'virtual_allocation'},
        {name: 'shared'}
    ]
});