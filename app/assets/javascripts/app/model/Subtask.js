Ext.define('Netra.model.Subtask', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'created_at'},
        {name: 'updated_at'},
        {name: 'deleted_at'},
        {name: 'id'},
        {name: 'task_id'},
        {name: 'command'},
        {name: 'state'},
        {name: 'progress'},
        {name: 'response'},
        {name: 'task_ref'},
        {name: 'params'},
        {name: 'started_at'},
        {name: 'finished_at'},
        {name: 'last_checked_at'},
    ]
});