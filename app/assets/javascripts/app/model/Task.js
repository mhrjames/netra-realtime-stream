Ext.define('Netra.model.Task', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID' },
        {name: 'name', label: 'Name' },
        {name: 'description', label: 'Description' },
        {name: 'command', label: 'Type' },
        {name: 'progress', label: 'Progress' },
        {name: 'status', label: 'Status' },
        {name: 'task_ref', label: 'Task Ref' },
        {name: 'task_uuid', label: 'Task UUID' },

        {name: 'created_at', label: 'Created At' },                        
        {name: 'updated_at', label: 'Updated At' },
        {name: 'started_at', label: 'Started At' },                        
        {name: 'finished_at', label: 'Finished At' },

    ]
});

/*
  create_table "tasks", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "owner"
    t.text     "script"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "task_type"
    t.integer  "package_id"
    t.string   "package_name"
    t.integer  "progress"
    t.text     "log"
    t.text     "report"
    t.string   "status"
    t.string   "host"
    t.datetime "started_at"
    t.datetime "finished_at"
  end
*/