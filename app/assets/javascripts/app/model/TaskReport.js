Ext.define('Netra.model.TaskReport', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID' },
        {name: 'task_id', label: 'Task ID' },
        {name: 'runner', label: 'Runner' },
        {name: 'msg', label: 'Log Message' },
        {name: 'result', label: 'Result' },

        {name: 'created_at', label: 'Created At' },                        
        {name: 'started_at', label: 'Started At' },
        {name: 'finished_at', label: 'Finished At' },
    ]

/*

      t.integer :task_id
      t.string :runner
      t.string :result
      t.text :msg
      t.date :created_at
      t.date :started_at
      t.date :finished_at
*/

});

