Ext.define('Netra.model.TaskTemplate', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID' },
        {name: 'owner_id', label: 'Owner' },
        {name: 'name', label: 'Task Name' },
        {name: 'task_type', label: 'Task Type' },
        {name: 'description', label: 'Description' },
        {name: 'created', label: 'Created At' },                        

        {name: 'instance_name', label: 'Instance Name' },
        {name: 'instance_count', label: 'Instance Count' },
        {name: 'flavor_id', label: 'Flavor ID' },
        {name: 'image_id', label: 'Image ID'},

        {name: 'volume_name', label: 'Volume Name' },
        {name: 'volume_id', label: 'Volume ID' },

        {name: 'assign_floating', label: 'Assign Floating IP'},
        {name: 'security_id', label: 'Security ID' },
    ]
});

