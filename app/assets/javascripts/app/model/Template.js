Ext.define('Netra.model.Template', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'uuid', label: 'ID'  },       
        {name: 'reference', label: 'Reference'  },         
        {name: 'name', label: 'Name' },
        {name: 'description', label: 'Description'  },
        {name: 'memory_max', label: 'Memory Max'  },
        {name: 'storage', label: 'Storage'  },
        {name: 'network', label: 'Network'  },
        {name: 'vcpus_max', label: 'VCPU'  },
    ]
});