Ext.define('Netra.model.Tenant', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID' },
        {name: 'name', label: 'Name' },
        {name: 'enabled', label: 'Enabled' },
        {name: 'description', label: 'Description' },
    ]
});

/*
    t.integer  "task_id"
    t.string   "task_name"
    t.string   "server_name"
    t.date     "executed_at"
    t.date     "updated_at",  :null => false
    t.text     "log"
    t.datetime "created_at",  :null => false
*/