Ext.define('Netra.model.UserInfo', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID'  },
		{name: 'account', label: 'User Name'  },
        {name: 'hostname', label: 'Name'  },
        {name: 'ip', label: 'Enabled'  },
        {name: 'gateway', label: 'EMail'  },
        {name: 'netmask', label: 'Tenant ID'  },
        {name: 'dns1', label: 'Tenant ID'  },
        {name: 'dns2', label: 'Tenant ID'  },
        {name: 'machine_uuid', label: 'Tenant ID'  },
        {name: 'dn', label: 'Tenant ID'  },
        {name: 'principalusername', label: 'Tenant ID'  },
    ]
});