Ext.define('Netra.model.Volume', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', label: 'ID'  },       
        {name: 'display_name', label: 'Display Name'  },         
        {name: 'os-vol-tenant-attr:tenant_id', label: 'Tenant ID'  },

        {name: 'status', label: 'Status' },
        {name: 'size', label: 'Size'  },
        {name: 'attachments', label: 'Attachments'  },
        {name: 'availability_zone', label: 'Availability Zone'  },
        {name: 'bootable', label: 'Bootable'  },
        {name: 'encrypted', label: 'Encrypted'  },

        {name: 'created_at', label: 'Created At'  },

        {name: 'display_description', label: 'Description'  },

        {name: 'os-vol-host-attr:host', label: 'Host Attributes'  },
        {name: 'volume_type', label: 'Volume Type'  },
		{name: 'snapshot_id', label: 'Sanpshot ID'  },
        {name: 'source_volid', label: 'Source Volid'  },
        {name: 'os-vol-mig-status-attr:name_id', label: 'Status Attributes'  },
		{name: 'metadata', label: 'Meta Data'  },


        {name: 'os-vol-mig-status-attr:migstat', label: 'Stat'  },      
    ]
});