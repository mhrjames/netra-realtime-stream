Ext.define('Netra.model.XenHost', {
    extend: 'Ext.data.Model',
    
    fields: [
        {name: 'uuid'},
        {name: 'reference'},
        {name: 'ip'},
        {name: 'name'},
        {name: 'hostname'},
        {name: 'description'},
        {name: 'enabled'},
        {name: 'hostname'},
        {name: 'cpu_count'},
        {name: 'cpu_speed'},
        {name: 'cpu_model'},
        {name: 'xenserver_version'},
        {name: 'linux'},
        {name: 'boottime'},
        {name: 'storage'},
        {name: 'network'},
        {name: 'total_memory'},
        {name: 'free_memory'},
        {name: 'xenserver'},
    ]
});
