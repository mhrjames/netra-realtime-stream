Ext.define('Netra.model.XenServer', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'uuid' },
        {name: 'reference'},
        {name: 'name' },
        {name: 'description' },
        {name: 'host_ref' },
		{name: 'memory_max' },
		{name: 'storage' },
		{name: 'metric' },
		{name: 'vcpus_max' },
		{name: 'template_name' },
		{name: 'network' },
		{name: 'affinity' },
		{name: 'power_state' },
    ]
});