Ext.define('Netra.model.XenVDI', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'AllocationType' },
        {name: 'AssociatedUserNames' },
        {name: 'AssociatedUserSIDs' },
        {name: 'AssociatedUserUPNs' },
        {name: 'CatalogName' },
        {name: 'CatalogUid' },
        {name: 'CatalogUUID' },
        {name: 'DeliveryType' },

        {name: 'DesktopGroupName' },
        {name: 'DesktopGroupUUID' },
        {name: 'DesktopGroupUid' },
        {name: 'DesktopUid' },
        {name: 'HostedMachineId' },
        {name: 'HostedMachineName' },
        {name: 'HypervisorConnectionUid' },
        {name: 'HypHypervisorConnectionUid' },

        {name: 'IsAssigned' },
        {name: 'IsPhysical' },
        {name: 'MachineName' },
        {name: 'PowerState' },
        {name: 'SummaryState' },
        {name: 'UUID' },
        {name: 'Uid' },
        {name: 'WindowsConnectionSetting' },
        
    ]
});