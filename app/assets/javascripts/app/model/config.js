Ext.define('Netra.model.Config', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'netra_server_ip'},
        {name: 'netra_server_url'},
        {name: 'netra_server_fqdn'},
        {name: 'proxy_server_url'},
        {name: 'rabbitmq_ip'},
        {name: 'rabbitmq_userid'},
        {name: 'rabbitmq_password'},
        {name: 'ssh_execution_timeout'},
        {name: 'message_channel'},
        {name: 'message_delay'},
        {name: 'puppet_path'},
        {name: 'iso_file_download_url'},
        {name: 'hostfile'},
        {name: 'tftproot'},
        {name: 'tftp_server_url'},
        {name: 'dhcp_config'},
    ]
});

/*
---
config:
  netra_server_url: http://192.168.56.2:3000
  netra_server_fqdn: 'netra.netranet.local'
  proxy_server_url: http://192.168.56.2:9292
  rabbitmq_url: http://192.168.56.2:9393
  rabbitmq_userid: 'test'
  rabbitmq_password: 'test'
  ssh_execution_timeout: 1200
  message_channel: /notify
  message_delay: 20000
  puppet_path: /etc/puppet
  iso_file_download_url: http://redhat.com
  hostfile: ''
*/