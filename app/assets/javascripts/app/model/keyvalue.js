Ext.define('Netra.model.Keyvalue', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'index', type: 'int'},
        {name: 'key'},
        {name: 'value'},
        {name: 'state'}
    ]
});