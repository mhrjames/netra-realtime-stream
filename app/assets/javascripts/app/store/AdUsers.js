Ext.define('Netra.store.AdUsers', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.AdUser',    
    storeId: 'storeAdUsers',
    alias: 'store.adusers',
    model: 'Netra.model.AdUser',

    proxy: {
        type: 'ajax',
        url: '/api/ad/user',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});