Ext.define('Netra.store.Clusters', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Cluster',    
    model: 'Netra.model.Cluster',
    alias: 'store.clusters',
    storeId: 'storeClusters',
    autoSync: true,

    proxy: {

        type: 'ajax',
        url: '/api/pool',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }

});