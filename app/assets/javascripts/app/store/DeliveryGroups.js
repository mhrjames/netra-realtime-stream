Ext.define('Netra.store.DeliveryGroups', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.DeliveryGroup',    
    model: 'Netra.model.DeliveryGroup',
    alias: 'store.deliverygroups',
    storeId: 'storeDeliveryGroups',
    autoSync: false,

    proxy: {

        type: 'ajax',
        url: '/api/desktop/delivery_group',
        
        reader: {
            type: 'json',
            rootProperty: 'items'
        }

    }

});