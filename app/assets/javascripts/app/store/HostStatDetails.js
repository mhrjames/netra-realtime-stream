Ext.define('Netra.store.HostStatDetails', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.HostStatDetail',    
    alias: 'store.hoststatdetails',
    storeId: 'storeHostStatDetails',
    model: 'Netra.model.HostStatDetail',


});