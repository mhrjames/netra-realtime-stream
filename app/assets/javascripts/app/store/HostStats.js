Ext.define('Netra.store.HostStats', {
    extend: 'Ext.data.JsonStore',
    requires: 'Netra.model.HostStat',    
    model: 'Netra.model.HostStat',
    storeId: 'storeHostStats',
    alias: 'store.hoststats',

});