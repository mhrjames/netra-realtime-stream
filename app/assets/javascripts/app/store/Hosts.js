Ext.define('Netra.store.Hosts', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Host',    
    alias: 'store.hosts',
    storeId: 'storeHosts',
    model: 'Netra.model.Host',
    
    autoLoad: false,
    autoSync: true,

    proxy: 
    {
        type: 'ajax',
        url: '/api/host',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },

    filterByCluster: function(cluster_name)
    {
        console.debug("Netra.store.Hosts.filterByCluster : cluster_name = ", cluster_name);

        //var store = Netra.manager.getStore('storeInstances');
        
        this.clearFilter();
        
        if (cluster_name != null) 
        {
            this.filterBy(function(record, index) 
            {
                console.debug("Netra.store.Hosts.filterByCluster : cluster_name = " + cluster_name + " , record = ", record);

                if (record.data.cluster_name == null) {
                    return false;
                }

                if (record.get('cluster_name').toUpperCase() == cluster_name.toUpperCase()) {
                    return true;
                } 
                
                return false;
            });
        }        
    },    

/*
    proxy: {
        type: 'ajax',
        url: '/api/resource/kickstart.json',
        
        reader: {
            type: 'json',
            root: 'kickstarts'
        }
    },
    
    saveToServer: function(name, kickstart, fn) 
    {
    	console.debug("Kickstarts.saveToServer");

        Ext.Ajax.request({
            url: '/api/resource/pxetemplates/list',
            method: 'GET',
            params: { },
            scope: this,

            success: function(response, opts) 
            {
                console.debug('Kickstarts.saveToServer : success!!!');

                var json = JSON.parse(response.responseText);

                fn(json);

            },
            failure: function(response, opts) 
            {
            	fn();
            }    	
        });

    }
*/

});