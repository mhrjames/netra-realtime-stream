Ext.define('Netra.store.HypervisorConnections', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.HypervisorConnection',    
    alias: 'store.hypervisorconnections',
    storeId: 'storeHypervisorConnections',
    model: 'Netra.model.HypervisorConnection',

    
    autoLoad: false,
    autoSync: true,

    proxy: 
    {
        type: 'ajax',
        url: '/api/desktop/hypervisor_connection',
        
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },
});