Ext.define('Netra.store.Instances', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Instance',    
    alias: 'store.instances',
    storeId: 'storeInstances',
    model: 'Netra.model.Instance',
    autoSync: true,

    proxy: {

        type: 'ajax',
        url: '/api/openstack/instances',
        
        reader: {
            type: 'json',
            rootProperty: 'instances'
        }
    }

});