Ext.define('Netra.store.MachineCatalogs', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.MachineCatalog',
    storeId: 'storeMachineCatalogs',
    alias: 'store.machinecatalogs',
    model: 'Netra.model.MachineCatalog',


    proxy: {
        type: 'ajax',
        url: '/api/desktop/machine_catalog',
        
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }

});