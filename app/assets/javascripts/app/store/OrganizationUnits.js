Ext.define('Netra.store.OrganizationUnits', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.OrganizationUnit',    
    alias: 'store.ous',
    storeId: 'storeOus',
    model: 'Netra.model.OrganizationUnit',
    autoSync: false,

    proxy: {

        type: 'ajax',
        url: '/api/ad/ou',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});