Ext.define('Netra.store.PBDs', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.PBD',
    storeId: 'storePBDs',
    alias: 'store.pbds',
    model: 'Netra.model.PBD',
    
    autoLoad: false,

    proxy: {

        type: 'ajax',
        url: '/api/pbd/provision',

        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
                
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});