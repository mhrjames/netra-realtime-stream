Ext.define('Netra.store.Repositorys', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Repository',    
    storeId: 'storeRepositorys',
    alias: 'store.repositorys',
    model: 'Netra.model.Repository',


    proxy: {

        type: 'ajax',
        url: '/api/repository',
        
        reader: {
            type: 'json',
            rootProperty: 'repository'
        }

    }

});