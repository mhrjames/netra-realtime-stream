Ext.define('Netra.store.Snapshots', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Snapshot',    
    alias: 'store.snapshots',
    storeId: 'storeSnapshots',
    model: 'Netra.model.Snapshot',
    autoSync: true,

    proxy: {

        type: 'ajax',
        url: '/api/openstack/snapshots',
        
        reader: {
            type: 'json',
            rootProperty: 'snapshots'
        }

    }

});