Ext.define('Netra.store.StorageRepositories', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.StorageRepository',    
    alias: 'store.storagerepositories',
    storeId: 'storeStorageRepositories',
    model: 'Netra.model.StorageRepository',

    proxy: {

        type: 'ajax',
        url: '/api/sr/provision',

        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
                
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }
    
});