Ext.define('Netra.store.Subtasks', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Subtask',    
    storeId: 'storeSubtasks',
    alias: 'store.subtasks',
    model: 'Netra.model.Subtask',
    autoSync: true,

    proxy: {

        type: 'ajax',
        url: '/api/task/subtask',

        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});