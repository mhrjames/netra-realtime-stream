Ext.define('Netra.store.TaskReports', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.TaskReport',    
    storeId: 'storeTaskReports',
    alias: 'store.taskreports',
    model: 'Netra.model.TaskReport',
    autoSync: false,
    autoLoad: true,

    proxy: {

        type: 'ajax',
        url: '/api/task/report',
        
        reader: {
            type: 'json',
            rootProperty: 'report'
        }

    }

});