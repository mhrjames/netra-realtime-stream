Ext.define('Netra.store.Tasks', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Task',    
    storeId: 'storeTasks',
    alias: 'store.tasks',
    model: 'Netra.model.Task',
    autoSync: false,
    autoLoad: true,

    proxy: {

        type: 'ajax',
        url: '/api/task',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});