Ext.define('Netra.store.Templates', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Template',    
    storeId: 'storeTemplates',
    alias: 'store.templates',
    model: 'Netra.model.Template',
    autoSync: false,

    proxy: {
        type: 'ajax',
        url: '/api/template',

        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});