Ext.define('Netra.store.Tenants', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Tenant',    
    storeId: 'storeTenants',
    alias: 'store.tenants',
    model: 'Netra.model.Tenant',
    //autoSync: false,
    //autoLoad: true,

    proxy: {

        type: 'ajax',
        url: '/api/openstack/tenants',
        
        reader: {
            type: 'json',
            rootProperty: 'tenants'
        }

    }

});