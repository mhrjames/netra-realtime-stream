Ext.define('Netra.store.UserInfos', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.UserInfo',    
    storeId: 'storeUserInfos',
    alias: 'store.userinfos',
    model: 'Netra.model.UserInfo',
    autoSync: false,
    autoLoad: true,

    proxy: {

        type: 'ajax',
        url: '/api/user',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});