Ext.define('Netra.store.Volumes', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Volume',    
    storeId: 'storeVolumes',
    alias: 'store.volumes',
    model: 'Netra.model.Volume',
    autoSync: true,

    proxy: {

        type: 'ajax',
        url: '/api/openstack/volumes',
        
        reader: {
            type: 'json',
            rootProperty: 'volumes'
        }

    }

});