Ext.define('Netra.store.XenHosts', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.XenHost',
    model: 'Netra.model.XenHost',
    storeId: 'storeXenHosts',
    alias: 'store.xenhosts',
    autoLoad: false,
    //autoSync: true,

    proxy: {        
        type: 'ajax',
        url: '/api/xenhost',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
                
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }


});