Ext.define('Netra.store.XenServers', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.XenServer',    
    storeId: 'storeXenServers',
    alias: 'store.xenservers',
    model: 'Netra.model.XenServer',
    autoSync: false,

    proxy: {        
        type: 'ajax',
        url: '/api/xenserver',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
                
        reader: {
            type: 'json',
            rootProperty: 'data'
        }

    }

});