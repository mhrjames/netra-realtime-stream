Ext.define('Netra.store.XenVDIs', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.XenVDI',    
    alias: 'store.xenvdis',
    storeId: 'storeXenVDIs',
    model: 'Netra.model.XenVDI',
    
    autoSync: false,

    proxy: {

        type: 'ajax',
        url: '/api/desktop/xenserver',
        
        reader: {
            type: 'json',
            rootProperty: 'items'
        }

    }

});