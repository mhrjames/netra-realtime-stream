Ext.define('Netra.store.Keyvalues', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Keyvalue',    
    itemId: 'storeKeyvalues',
    model: 'Netra.model.Keyvalue',

    autoLoad: false,
    
});