Ext.define('Netra.store.Notifications', {
    extend: 'Ext.data.Store',
    requires: 'Netra.model.Notification',    
    alias: 'store.notifications',
    storeId: 'storeNotifications',
    model: 'Netra.model.Notification',

    groupField: 'channel',
    groupDir  : 'DESC'    
});