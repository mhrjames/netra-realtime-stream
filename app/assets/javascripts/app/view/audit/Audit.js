Ext.define('Netra.view.audit.Audit', {
    extend: 'Ext.panel.Panel',
    referenceHolder: true,
    //animCollapse: true,
    //collapsible: true,
    //collapseDirection: 'left',
    //title: '',
    //titleAlign: 'center',

    requires: [
        'Netra.view.audit.AuditController',        
    ],

    controller: 'audit',
    id: 'panel_audit',
    
    layout: {
        type: 'border',
        //vertical: false,
        //animate: true,        
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: true,
        //headerPosition: 'left',
        titleAlign: 'center',
        anchor: '100%',
        align: 'stretch',
    },

    items: 
    [
        {
            xtype: 'treepanel',
            id: 'tree_instance',
            reference: 'tree_instance',
            region: 'west',
            title: 'Instances',
            width: 200,
            rootVisible: false,
            useArrows: true,
            split: true,
            root: {
                text: 'Root',
                children: []
            }
        },
        {
            xtype: 'gridpanel',
            region: 'center',
            id: 'grid_audit',
            reference: 'grid_audit',
            title: 'Audit List',
            stripeRows: true,
            columnLines: true,

            store: Netra.manager.getStore('storeInstanceAudits'),

            plugins: [
                //'cellupdating'
            ],

            selType: 'checkboxmodel',
            selModel: {
                checkOnly: true,
                injectCheckbox: 0
            },

            columns: [
                {
                    text     : 'id',
                    flex     :  1,
                    sortable : false,
                    hidden   : false,
                    dataIndex: 'id'
                },
                {
                    text     : 'Created At',
                    flex     :  3,
                    dataIndex: 'created_at'
                },        
                {
                    text     : 'Updated At',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'updated_at'
                },
                {
                    text     : 'Action',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'action'
                },
                {
                    text     : 'Instance ID',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'instance_uuid'
                },
                {
                    text     : 'Request ID',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'request_id'
                },
                {
                    text     : 'User ID',
                    flex     :  2,
                    dataIndex: 'user_id'
                },

                {
                    text     : 'Tenant ID',
                    flex     :  2,
                    sortable : true,
                    //renderer : this.change,
                    dataIndex: 'project_id'
                },
                {
                    text     : 'Start Time',
                    flex     :  2,
                    sortable : true,
                    //renderer : this.pctChange,
                    dataIndex: 'start_time'    
                },
                {
                    text     : 'Finish Time',
                    flex     :  2,
                    sortable : true,
                    //renderer : this.pctChange,
                    dataIndex: 'finish_time'    
                },
                {
                    text     : 'Message',
                    flex     :  2,
                    sortable : true,
                    //renderer : this.pctChange,
                    dataIndex: 'message'    
                }

            ],
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: Netra.manager.getStore('storeInstanceAudits'),
                displayInfo: true,
                plugins: new Ext.ux.ProgressBarPager()
            },

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    alias: 'widget.toolbar_audit',
                    id: 'toolbar_audit',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: 
                    [  
                        {
                            xtype: 'datefield',
                            fieldLabel : "Start Date",
                            labelWidth: 70,
                            id: 'text_audit_start',
                            reference: 'text_audit_start',
                            name: 'date_start',
                            format: 'Y-m-d',
                            editable: false,
                            margin: '0 20 0 20',
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel : "End Date",
                            labelWidth: 70,
                            id: 'text_audit_end',
                            reference: 'text_audit_end',
                            name: 'date_end',
                            format: 'Y-m-d',
                            editable: false,
                            margin: '0 10 0 20',
                        },                        
                        { 
                            xtype: 'button', 
                            id: 'btn_audit_search', 
                            text: 'Search',
                        },                                  
                    ]
                }
            ],   
        }
    ],


    initComponent: function()
    {
        this.callParent(arguments);
        //console.debug("initComponent");
    }


});
