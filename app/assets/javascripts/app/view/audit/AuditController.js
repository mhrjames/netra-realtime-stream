Ext.define('Netra.view.audit.AuditController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.audit',

    refs: [
        { ref: 'panelImage', selector: '#panel_audit' },
        { ref: 'textStart', selector: '#text_audit_start' },
        { ref: 'textEnd', selector: '#text_audit_end' },        
    ],


    auditRecord: null,



    init: function() 
    {
        console.debug("AuditController.init");

        this.control({
            '#btn_audit_search': { click: this.onButtonSearchClick },
            '#tree_instance': {
                itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },
            '#panel_audit': { beforedestroy: this.onDestroy },
        });


        this.getReference('grid_audit').bindStore( Netra.manager.getStore('storeInstanceAudits') );

        this.onStart();
    },

    loadInstanceAuditsData: function(start, end, instance_id) 
    {
        console.debug("AuditController.loadInstanceAuditsData");

        var params = { 'date_start': start, 'date_end': end, 'instance_id': instance_id };

        Netra.helper.showWaitDialog('Loading data from server','work in progress','btn_audit_search');

        Netra.helper.refreshDataWithParameter('storeInstanceAudits', params, '#grid_audit', this.onInstanceAuditsLoaded);

    },

    loadInstanceData: function() 
    {
        console.debug("loadInstanceData");

        var store = Netra.manager.getStore("storeInstances");
        store.data.removeAll();

        store.load({
            callback: this.onInstancesLoaded,
            scope: this
        });

    },


    getRequestParameter: function(meter_id)
    {
        var cur_date = new Date();

        var json = {};

        json['tenant_id'] = this.adminTenant.id;
        json['group_by'] = "";
        json['period'] = 60;
        json['meter_id'] = meter_id;
        json['start'] = Ext.Date.format(cur_date,'Y-m-d');
        json['end'] = Ext.Date.format(cur_date,'Y-m-d');

        return json;
    },

    sendUsageRequest: function(json)
    {
        var _this = this;

        console.debug("AuditController.sendUsageRequest");
        
        Netra.helper.showPanelLoadingMask(this.getReference('grid_audit'),true);        

        Ext.Ajax.request({
            url: '/api/openstack/ceilometer/stat',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendUsageRequest Response = ', response);
                var json = JSON.parse(response.responseText);

                _this.drawGraph(json);

                Netra.helper.showPanelLoadingMask(_this.getReference('grid_audit'),false);
            },
            failure: function(response, opts) 
            {
                console.debug('sendUsageRequest : woops');

                Netra.helper.showPanelLoadingMask(_this.getReference('grid_audit'),false);
            }
        });

    },


    addRootNodeToTree: function(text,record)
    {
        var rootNode = this.getReference('tree_instance').getRootNode();

        var node = this.addNodeToTree(this.getReference('tree_instance'),rootNode, text, 'openstack', false, record);
        node.set("icon",Icons.nodeOpenstack);
        node.set("id","node_all");

        return node;
    },

    addItemNodeToTree: function(text,record)
    {
        //var rootNode = this.getReference('tree_instance').getRootNode();
        
        var allNode = this.getReference('tree_instance').getStore().getNodeById('node_all');

        var node = this.addNodeToTree(this.getReference('tree_instance'),allNode, text, 'openstack', true, record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    selectInstance: function(record)
    {
        console.debug("selectInstance : record = ",record);

        //var index = this.getStore("Templates").find('name','havana');
        //this.templateRecord = this.getStore("Templates").getAt(index);
        //console.debug('templateRecord');

        this.instanceRecord = record;
    },

    refreshData: function()
    {
        //store: Netra.manager.getStore('storeInstanceAudits'),
        Netra.helper.refreshData('storeInstanceAudits','#grid_audit', null);
    },




    onStart: function()
    {
        console.debug("AuditController.onStart");
        
        this.loadInstanceData();
    },

    onInstanceAuditsLoaded: function()
    {
        //var store = Netra.manager.getStore('storeInstanceAudits');

        //console.debug("AuditController.onInstanceAuditsLoaded : total count = " + store.getTotalCount());

        //this.getReference('grid_audit').getStore().reload();
        //this.getReference('grid_audit').getView().refresh();
        Netra.helper.hideWaitDialog();
    },

    onInstancesLoaded: function()
    {
        var _this = this;

        console.debug("AuditController.onInstancesLoaded");
        //this.getReference('combo_report_meter').bindStore(Netra.manager.getStore("storeMeters"));

        this.clearTree(this.getReference('tree_instance'));

        this.addRootNodeToTree("Instances", null);

        Netra.manager.getStore("storeInstances").each(function(record,idx) 
        {
            console.debug("AuditController.onInstancesLoaded : record = ", record);

            _this.addItemNodeToTree(record.get("name"), record);            
        });

        this.getReference('tree_instance').getView().refresh();
        this.getReference('tree_instance').getRootNode().expand();

    },


    onFormRender: function(form, opts)
    {
        console.debug("AuditController.onFormRender");
    },

    onTreeMenuRendered: function() 
    {
        console.debug("onTreeMenuRendered");
        //this.selectFirstNode();
    },

    onDestroy: function()
    {
        console.debug("AuditController.onDestroy");

        Netra.manager.getStore('storeInstanceAudits').removeAll();
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        console.debug(record);
        
        this.selectInstance(record.data.nodeRecord);

    },

    onButtonSearchClick: function()
    {
        console.debug("AuditController.onButtonSearchClick");

        var start = this.getReference('text_audit_start').getRawValue();
        var end = this.getReference('text_audit_end').getValue();

        var instance_id = "";
        if (this.instanceRecord) {
            if (this.instanceRecord.data) {
                instance_id = this.instanceRecord.data.id;
            }
        }

        console.debug("AuditController.onButtonSearchClick : start = " + start);

        this.loadInstanceAuditsData(start,end,instance_id);

    },

});
