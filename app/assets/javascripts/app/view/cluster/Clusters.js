Ext.define('Netra.view.cluster.Clusters', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.clusters',
    id: 'container_clusters',

    requires: [
        //'Ext.layout.container.Accordion',
        'Netra.model.Cluster',
        'Netra.store.Clusters',
        'Netra.view.cluster.ClustersController',        
    ],

    title: 'Pool Manager',
    titleAlign: 'center',
    referenceHolder: true,
    //bodyPadding: '10 10 10 10',
    frame: false,
    border: false,

    layout: {
        type: 'card',
        animate: true,        
        vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        autoScroll: true,
        frame: false,
        border: false,
        titleAlign: 'center',
    },

    controller: 'clusters',

    items: 
    [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch',
            },

            items: 
            [
                {
                    xtype: 'gridpanel',
                    id: 'grid_cluster',
                    flex: 1,
                    //title: 'Cluster List',
                    //titleAlign: 'center',
                    //stripeRows: true,
                    //columnLines: true,

                    store: Netra.manager.getStore('storeClusters'),

                    selType: 'checkboxmodel',
                    selModel: {
                        checkOnly: true,
                        injectCheckbox: 0
                    },
                    
                    ui: 'footer',
                    header: {
                        //titlePosition: 1,
                        margin: '5 5 5 0',

                        items: [
/*
                            { 
                                xtype: 'button', 
                                id: 'btn_cluster_new', 
                                text: 'New'
                            },
                            { 
                                xtype: 'button', 
                                id: 'btn_cluster_delete', 
                                text: 'Delete',
                            },           
*/                            
                            { 
                                xtype: 'button', 
                                id: 'btn_cluster_inspect', 
                                text: 'Inspect',
                            },                                  
                            { 
                                xtype: 'button', 
                                id: 'btn_cluster_refresh', 
                                text: 'Refresh',
                            },                                  
                        ]
                    },

                    columns: [
                        {
                            text     : 'Name',
                            flex     :  1,
                            sortable : true,
                            dataIndex: 'name'
                        },        
                        {
                            text     : 'Reference',
                            flex     :  3,
                            sortable : true,
                            dataIndex: 'reference'
                        },                        
                        {
                            text     : 'Master IP',
                            flex     :  1,
                            sortable : true,
                            dataIndex: 'master_ip'
                        },
                        {
                            text     : 'Description',
                            flex     :  3,
                            dataIndex: 'description'    
                        }
                    ],
                },
/*                
                {
                    xtype: 'tabpanel',
                    flex: 1,
                    padding: '20 0 0 0',
                    items: [
                        {
                            title: 'Cluster',
                            html: 'hghjgjhg',
                        },
                        {
                            title: 'Hosts',
                            html: '23424324324',
                        }

                    ]
                }
*/
            ],


        },
 
        {
            xtype: 'panel',
            title: '',
            items: 
            [
                {
                    xtype: 'gridpanel',
                    id: 'grid_host_instance',
                    reference: 'grid_host_instance',
                    title: 'Host List',
                    titleAlign: 'center',

                    //stripeRows: true,
                    //columnLines: true,

                    store: Netra.manager.getStore('storeHosts'),

                    plugins: [
                        //'cellupdating'
                    ],

                    selType: 'checkboxmodel',
                    selModel: {
                        checkOnly: true,
                        injectCheckbox: 0
                    },
                    
                    ui: 'footer',   
                    header: {
                        padding: '5 5 5 5',
                        titlePosition: 1,
                        items: [
                            { 
                                xtype: 'button', 
                                id: 'btn_cluster_back', 
                                text: 'Back'
                            },
                            { 
                                xtype: 'button', 
                                id: 'btn_cluster_addhost', 
                                text: 'Add Host'
                            },                        
                        ]
                    },

                    columns: 
                    [
                        {
                            text     : 'id',
                            width     : 45,
                            sortable : false,
                            hidden   : false,
                            dataIndex: 'id'
                        },
                        {
                            text     : 'IP',
                            width    : 125,
                            sortable : true,
                            dataIndex: 'ip'
                        },
                        {
                            text     : 'Name',
                            width    : 125,
                            sortable : true,
                            //renderer : 'usMoney',
                            dataIndex: 'name'
                        },
                        {
                            text     : 'Cluster Name',
                            width    : 125,
                            dataIndex: 'cluster_name',
                        },
                        {
                            text     : 'User ID',
                            width    : 125,
                            dataIndex: 'userid',
                        },
                        {
                            text     : 'Password',
                            width    : 125,
                            dataIndex: 'password',
                        },                                                
                        {
                            text     : 'Updated At',
                            width    : 175,
                            sortable : true,
                            //renderer : 'usMoney',
                            dataIndex: 'updated_at'
                        },            
                        {
                            text     : 'OS',
                            width    : 95,
                            sortable : true,
                            //renderer : this.pctChange,
                            dataIndex: 'os'
                        }
                    ],
                }
            ],
        },
        {
            xtype: 'hostdetail',
            title: 'Host Detail',
            titleAlign: 'center',
            
            ui: 'footer',
            header: {
                margin: '5 5 5 5',
                titlePosition: 1,
                items: [
                    {
                        xtype: 'button',
                        text: 'Back',
                        id: 'btn_cluster_host_back',
                    }
                ]
            },

        }
    ],



    initComponent: function() 
    {
        console.debug("Flavor.initComponent");
        this.callParent(arguments);
    },


});
