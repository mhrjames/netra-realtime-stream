Ext.define('Netra.view.cluster.ClustersController', {
    extend: 'Netra.controller.BaseController',
    alias: 'controller.clusters',

    refs: [
        { ref: 'panelFlavor', selector: '#panel_cluster' },
        { ref: 'gridFlavor', selector: '#grid_cluster' },
    ],

    clusterRecord: null,
    gridPopupMenu: null,

    clusterWindowController: null,
    clusterHostWindowController: null,


    init: function() 
    {
        console.debug("ClusterController.init");

        this.control({
            '#grid_cluster': { 
                itemdblclick: this.onGridColumnDoubleClick,
                rowcontextmenu: this.onGridContextMenuClick,
            },
            '#grid_host_instance': { 
                itemdblclick: this.onHostGridColumnDoubleClick,
            },

            '#form_cluster': { render: this.onFormRender },
            '#btn_cluster_new': { click: this.onButtonNewClick },
            '#btn_cluster_edit': { click: this.onButtonEditClick },
            '#btn_cluster_delete': { click: this.onButtonDeleteClick },
            '#btn_cluster_refresh': { click: this.onButtonRefreshClick },
            '#btn_cluster_inspect': { click: this.onButtonInspectClick },

            '#btn_cluster_back': { click: this.onButtonBackClick },            
            '#btn_cluster_addhost': { click: this.onButtonAddHostClick },      
            '#btn_cluster_host_back': { click: this.onButtonHostBackClick },                  
        });

        //this.createStoreFlavor();
        this.onStart();
    },



    selectCluster: function(record)
    {
        this.clusterRecord = record;
    },

    filloutForm: function(record)
    {
        console.debug("ClusterController.filloutForm : record = ", record);
     
        var form = this.getReference('form_cluster');
        var store = Netra.manager.getStore('storeClusters');

        Netra.builder.buildForm(form,store,record.data,'Cluster Details','');

/*
        var form = this.getReference('form_cluster');
        var tpl = new Ext.XTemplate(
            '<h2>hello</h2>'
        );

        console.debug(record);
        console.debug(form);

        form.update({ flavor: record.data });
*/
    },

    showNewClusterWindow: function(record) 
    {
        console.debug("ClusterController.showNewClusterWindow");

        //Ext.create("Netra.view.component.window.ClusterController").show();

        //this.clusterWindowController = Ext.app.Application.createController("Netra.view.component.window.ClusterController");
        //this.clusterWindowController = Netra.app.getApplication().createController("Netra.view.component.window.ClusterController");
        //this.clusterWindowController = Netra.manager.createController("Netra.view.component.window.ClusterController");
        this.clusterWindowController = Netra.manager.createController("Netra.view.component.window.NewCluster");
        this.clusterWindowController.show(record);
        //this.clusterWindowController.init();

    },

    showClusterHostWindow: function(record) 
    {
        console.debug("ClusterController.showClusterHostWindow");

        this.clusterHostWindowController = Netra.manager.createController("Netra.view.component.window.ClusterHost");
        this.clusterHostWindowController.show(record);
        //this.clusterWindowController.init();

    },

    sendDeleteRequest: function(json)
    {
        var _this = this;
        
        console.debug("ClusterController.sendRequest");

        //Netra.helper.showPanelLoadingMask(this.getReference('grid_cluster'),true);

        Ext.Ajax.request({
            url: '/api/openstack/flavors/delete',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendDeleteRequest Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'Selected Template is deleted!!!');
                    _this.refreshData();
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }

                Netra.helper.hideWaitDialog();

            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Netra.helper.hideWaitDialog();

                Ext.MessageBox.alert('Error', 'Fail to delete');
            }
        });

    },

    refreshData: function()
    {
        Netra.helper.refreshData('storeClusters','#grid_cluster',function() {
            Netra.helper.hideWaitDialog();            
        });
    },

    inspectPool: function()
    {
        console.debug("ClusterController.inspectPool");
        Netra.service.inspectPool(null, function(ret_code, json) {
            Netra.helper.hideWaitDialog();

            console.debug("ClusterController.inspectPool : response = ", json);
            if (json['ret_code'] != 0) {
                Ext.MessageBox.alert('Error', json['msg']);   
            }
        });
    },

    loadHostData: function(record)
    {
        console.debug("ClusterController.loadHostData : record = ", record);

        var _this = this;

        Netra.helper.refreshData('storeHosts', '#grid_host_instance', function() {
            var store = Netra.manager.getStore('storeHosts');
            store.filterByCluster(record.data.name);
        });

    },

    showGridPopupMenu: function(grid, index, record, event) 
    {
        console.debug("storeClusters.showGridPopupMenu");

        var _this = this;

        event.stopEvent();

        if (! this.gridPopupMenu) 
        {
            this.gridPopupMenu = new Ext.menu.Menu({
                items: 
                [
                    {
                        text: 'Edit',
                        handler: function() 
                        {
                            _this.showNewClusterWindow(record);
                        }
                    }, 
                    {
                        text: 'Delete',
                        handler: function() 
                        {
                            _this.deleteClusterRecord(record);
                        }
                    }
                ]
            });
        }
        
        this.gridPopupMenu.showAt(event.getXY());
    },

    deleteClusterRecord:function(record)
    {
        console.debug("ClustersController.deleteClusterRecord : Record Jons = ", record);

        Netra.service.deleteClusterRecord(record.data,this.onRequestComplete);
    },






    onStart: function()
    {
        console.debug("ClusterController.onStart");

        this.clusterRecord = null;
        this.refreshData();

        //this.createclusterWindowController();
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_clusters'));

        this.selectCluster(record);
        this.loadHostData(record);
        Netra.helper.showCardView('container_clusters',1,'','');

        //var comps = Ext.ComponentManager.getAll();
        //console.debug(comps);
        //Netra.helper.dumpComponents(null);
    },

    onHostGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("ClusterController.onHostGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_clusters'));

        //this.selectCluster(record);
        //this.loadHostData(record);
        //Netra.helper.setActivePanelItem(this.getView(),2);
        Netra.helper.showCardView('container_clusters',2,'','');
        //var comps = Ext.ComponentManager.getAll();
        //console.debug(comps);
        //Netra.helper.dumpComponents(null);
    },

    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
        //console.debug(form.body);
/*
        var html = "<h3>World!!!</h3>";

        var panel = form.up("panel");
        //var tpl = Ext.create('Ext.Template', 'World!!!');

        tpl.overwrite(panel.body, null);
        //form.setConfig('html', html);
*/
    },


    onButtonNewClick: function()
    {
        console.debug("onButtonNewClick");
        this.showNewClusterWindow(null);
    },

    onButtonDeleteClick: function()
    {
        var _this = this;

        console.debug("ClusterController.onButtonDeleteClick");

        //var selected = this.getReGridFlavor().getSelectionModel().getSelection();
        var selected = this.getReference('grid_cluster').getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select templates to delete!');
            return;
        }

        console.debug(selected);

        var ids = "";

        Ext.each(selected, function (item) 
        {
            ids = ids + item.data.id + ",";
        });        
        
        var json = {};        
        json['id'] = ids;
    
        Netra.helper.showWaitDialog("","",'btn_cluster_delete');

        this.sendDeleteRequest(json);
    },

    onButtonEditClick: function()
    {
        console.debug("onButtonEditClick");
        this.showNewClusterWindow(this.clusterRecord);
    },

    onButtonRefreshClick: function()
    {
        console.debug("ClusterController.onButtonRefreshClick");
        
        Netra.helper.showWaitDialog("","",'btn_cluster_refresh');
        this.refreshData();
    },

    onButtonInspectClick: function()
    {
        console.debug("ClusterController.onButtonInspectClick");
        
        Netra.helper.showWaitDialog("","",'btn_cluster_inspect');
        this.inspectPool();
    },

    onButtonBackClick: function()
    {
        console.debug("ClusterController.onButtonBackClick");
        Netra.helper.showCardView('container_clusters',0,'','');
    },

    onButtonHostBackClick: function()
    {
        console.debug("ClusterController.onButtonHostBackClick");
        Netra.helper.showCardView('container_clusters',1,'','');
    },

    onGridContextMenuClick: function(grid, record, tr, index, event, opts)     
    {
        console.debug("ClusterController.onGridContextMenuClick : event = ",event);
        
        this.selectCluster(record);
        this.showGridPopupMenu(grid, index, record, event);
    },

    onRequestComplete: function(retcode, response)
    {
        console.debug("onHostRequestComplete : ret = ", response);

        if (retcode == Netra.const.REQ_OK) {
            Ext.MessageBox.alert('Success', response.msg);
        } else if (retcode == Netra.const.REQ_FAIL) {
            Ext.MessageBox.alert('Fail', response);
        } else {
            Ext.MessageBox.alert('Fail', response.msg);
        }

    },

    onButtonAddHostClick: function()
    {
        console.debug("ClusterController.onButtonAddHostClick");
        this.showClusterHostWindow(this.clusterRecord);
    },

});
