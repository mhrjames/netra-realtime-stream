Ext.define("Chart2.ChartTitleMixin", {

 	createTitleItem: function() 
 	{
        this.chartTitle = new Ext.draw.Sprite({
            type: "text",
            //"text-anchor": "middle",
            fill: "black",
            //"font-size": "12px",
            //"font-weight": "bold",
            //"font-family": "Arial",
            text: this.title
        });
     
        this.on("resize", function (cmp, width) 
        {
/*
            this.chartTitle.setAttributes({
                translate: {
                    x: (width/2) - 80,
                    y: 10
                }
            }, true);
*/            
        });
     
        this.items = this.items || [];
     
        this.items.push(this.chartTitle);
    },
    
    updateTitle: function(newTitle) 
    {
/*    	
        this.chartTitle.setAttributes({
            text: newTitle
        }, true);
*/        
    },

    initComponent: function () 
    {
        this.createTitleItem();
    }

});

Ext.define('Netra.view.component.Chart2', {
    extend: "Ext.chart.Chart",
    alias: 'widget.chart2',


    initComponent: function () 
    {
        this.createChartTitle("hello");
        this.callParent(arguments);
    },

 	createChartTitle: function(title) 
 	{
        var chartTitle = Ext.create('Ext.draw.Sprite', {
            type: "text",
            "text-anchor": "middle",
            fill: "black",
            "font-size": "12px",
            "font-weight": "bold",
            "font-family": "Arial"
        });

        chartTitle.setAttributes({
            'text': title
        }, true);

     	chartTitle.text = title;

        this.on("resize", function (cmp, width) 
        {
            chartTitle.setAttributes({
                translate: {
                    x: (width/2) - 80,
                    y: 10
                }
            }, true);

        });

        this.items = this.items || [];
     
        this.items.push(chartTitle);
        
    },

});