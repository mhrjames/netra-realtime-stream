document.title = 'Netra';

Ext.define('Netra.view.component.Header', 
{
    extend: 'Ext.Container',

    xtype: 'app-header',

    title: document.title,
    cls: 'netra-header',
    height: 72,

    layout: {
        type: 'hbox',
        align: 'middle'
    },

    items: 
    [
/*    
        {
            xtype: 'component',
            cls: 'netra-header-title'
        },
*/        
        {
            xtype: 'component',
            cls: 'netra-header-title',
            html: 'Realtime-Stream',
            flex: 1
        },

        {
            xtype: 'component',
            id: 'header_user_info',
            margin: '0 20 0 20',
            html: "",
        },

        {
            xtype: 'button',
            text: 'Config',
            id: 'btn_header_config',
            margin: '0 0 0 0',
        },
        {
            xtype: 'button',
            text: 'Logout',
            id: 'btn_header_logout',
            margin: '0 20 0 0',
        }
    ]
});
