Ext.define('Netra.view.component.dialog.FloatingIp', {
    extend: 'Ext.app.Controller',
    alias: 'controller.dialog_floating',
    id: 'dialog_floating',
    reference: 'dialog_floating',

    requires: [
        'Netra.store.IpPools'
    ],

    refs: [
        { ref: 'formInput', selector: '#form_newfloating' },
    ],


    floatingDialog: null,


    init: function() 
    {
        console.debug("dialogFloating.init");

        this.control({
            '#btn_floating_close': { click: this.onButtonCloseClick },
            '#btn_floating_create': { click: this.onButtonCreateClick },
        });

        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

        this.floatingDialog = Ext.create('Ext.window.Toast', 
        {
            title: 'Create Floating IP',
            height: 400,
            width: 600,
            border:false,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

            layout: {
                type: 'fit',
                pack: 'start',
                align: 'stretch',
            },
            defaults: {
                bodyPadding: '15 30 30 30',
                autoScroll: false,
            },

            modal: true,
            onEsc: function() {},
            closable: false,
            
            items : 
            [
                {
                    xtype: 'form',
                    id: 'form_newfloating',
                    border: false,
                    //title: 'Input Forms',
                    //titleAlign: 'center',
                    defaults: {
                        xtype: "textfield",
                        align: 'stretch',
                        anchor: '100%',
                        labelWidth: 140,
                    },

                    items: 
                    [
                        {
                            xtype: 'component',
                            frame: false,
                            border: 0,
                            html: Netra.helper.createFormTitle('New Floating IPs','')
                        },  
                        {
                            xtype:'combo',
                            fieldLabel: 'Floating IP Pool',
                            name:'pool',
                            valueField: 'name',
                            queryMode:'local',
                            store: {
                                type: 'ippools',
                                autoLoad: true,
                            },
                            displayField:'name',
                            editable: false,
                            autoSelect: true,
                            forceSelection: true,
                        },
                        {
                            fieldLabel : "Floating Count",
                            xtype: 'numberfield',
                            id: 'text_floating_count',
                            name: 'count',
                            value: "1",         
                            maskRe: /[0-9.]/,
                        },
                    ],
                },
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_floating',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        { xtype: 'button', id: 'btn_floating_create', text: 'Create' },
                        //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
                        { xtype: 'button', id: 'btn_floating_close', text: 'Close' }
                    ]
                }
            ],


        });

        this.onStart();
    },

    show: function()
    {
        console.debug("dialogFloating.show");

        this.floatingDialog.show();        
    },

    close: function()
    {
        console.debug("dialogFloating.close : flavorWindow = " , this.floatingDialog);

        this.floatingDialog.close();
        Netra.manager.removeController(this);
    },

    sendRequest: function(json)
    {
        console.debug("dialogFloating.sendRequest");
        
        Ext.Ajax.request({
            url: '/api/openstack/floating_ips/new',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendParameters Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'New Floating IPs are created!!!');
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }
                    
            },
            failure: function(response, opts) 
            {
                console.debug('sendParameters : woops');
            }
        });

    },

    getParameter: function()
    {
        console.debug('dialogFloating.getParameter');

        var json = {};

        var form = this.getFormInput();

        form.query('.textfield').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
    },

    saveToServer: function()
    {
        var form = this.getFormInput();

        console.debug("dialogFloating.saveToServer : form = ", form);
        
        var json = this.getParameter();
        this.sendRequest(json);
    },






    onStart: function()
    {
        console.debug("dialogFloating.onStart");
    },

    onBroadcastMessage: function(param)
    {
        console.debug("onBroadcastMessage");
    },

    onButtonCloseClick: function()
    {
        this.close();
    },

   onButtonCreateClick: function()
   {
        console.debug("dialogFloating.onButtonCreateClick");
        this.saveToServer();
   },


});