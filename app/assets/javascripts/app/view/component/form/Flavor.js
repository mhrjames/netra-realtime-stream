Ext.define("Netra.view.component.form.Flavor", {
	extend : "Ext.form.Panel",
	//title : 'Template',
	//titleAlign: 'center',
	//frame : false,
	//autoScroll: true,
	//id: 'form_flavor_input',
    titleAlign: 'left',
    autoScroll: true,
    //headerPosition: 'left',        
	bodyPadding: '30 30 30 30',

    defaults: {
    	xtype: "textfield",
        //align: 'stretch',
        anchor: '100%',
        labelWidth: 160,
    },


	items: [
		{
			xtype: 'component',
			frame: false,
			border: 0,
			html: Netra.helper.createFormTitle('New Template','')
		},	
		{
			fieldLabel : "Name",
			id: 'text_name_multi23',
			name: 'flavor_name',
			value: "Netra",			
		},
		{
			fieldLabel : "CPUs",
			name : 'flavor_vcpus',
			id: 'text_admin_multi23',
			margin: '30 0 0 0',
			value: "secret",
		},
		{
			fieldLabel : "RAM (MB)",
			name : 'flavor_ram',
			id: 'text_network_vlan_ranges_multi23',
			value: "",
		},									
		{
			fieldLabel : "Root Disk (GB)",
			id: 'text_internal_address_multi23',
			name: 'flavor_root_disk',					
			value: "172.16.0.102",
		},		
		{
			fieldLabel : "Emphemeral Disk (GB)",
			id: 'text_floating_multi12',
			name: 'flavor_ephemeral',
			value: "192.168.56.200/27",
		},
		{
			fieldLabel : "Swap Disk (MB)",
			name : 'flavor_swap_disk',
			id: 'text_fixed_multi12',
			value: "10.0.0.0/24",
		},
	],

    initComponent: function()
    {
        this.callParent(arguments);
    }

});