Ext.define("Netra.view.component.form.Openstack", {
	extend : "Ext.form.Panel",
	title : 'Openstack',
	//titleAlign: 'center',
	//frame : false,
	//autoScroll: true,
	id: 'form_openstack_multi',
    titleAlign: 'left',
    autoScroll: true,
    headerPosition: 'left',        
	bodyPadding: '30 30 30 30',

    defaults: {
    	xtype: "textfield",
        //align: 'stretch',
        anchor: '100%',
        labelWidth: 160,
    },


	items: [
		{
			xtype: 'panel',
			frame: false,
			border: 0,
			html: Netra.helper.createFormTitle('New OpenStack : Multi Nodes','')
		},	
		{
			fieldLabel : "Name",
			id: 'text_name_multi',
			name: 'name',
			value: "Netra",			
		},
		{
			xtype : 'textareafield',
			fieldLabel : 'Description',
			id: 'area_description_multi',
			name: 'description',
			grow: true,
			allowBlank : true,			
			height: 150,
			value: "Please Description",			
		},
		{
			xtype: 'hiddenfield',
			name: 'Template',
			id: 'hidden_template',
			value: ''
		},
		{
			fieldLabel : "Admin Password",
			name : 'admin_password',
			id: 'text_admin_multi',
			margin: '30 0 0 0',
			value: "secret",
		},
		{
			xtype: 'radiogroup',
			fieldLabel : "Tenant Network Model",
			name : 'tenant_network_model_multi',
			id: 'radio_tenant_network_model',
			margin: '30 0 0 0',
			items: [
				{ boxLabel: 'GRE', name: 'network_model_multi', inputValue: 1 , checked: true, },
				{ boxLabel: 'VLAN', name: 'network_model_multi', inputValue: 2 },
			]
		},
		{
			fieldLabel : "VLAN Range",
			name : 'network_vlan_ranges',
			id: 'text_network_vlan_ranges_multi',
			value: "",
			disabled: true,
		},									
		{
			fieldLabel : "Internal IP",
			id: 'text_internal_address_multi',
			name: 'internal_address',					
			value: "172.16.0.102",
		},		
		{
			fieldLabel : "Floating IP Range",
			id: 'text_floating_multi',
			name: 'floating_range',
			value: "192.168.56.200/27",
		},
		{
			fieldLabel : "Fixed IP Range",
			name : 'fixed_range',
			id: 'text_fixed_multi',
			value: "10.0.0.0/24",
		},
		{
			fieldLabel : "Public Interface",
			name : 'public_interface',
			id: 'text_public_multi',
			value: "eth0",
		},
		{
			fieldLabel : "Private Interface",
			name : 'private_interface',
			id: 'text_private_multi',
			value: "eth1",
		},
		{
			fieldLabel : "Bridge Interface",
			name : 'bridge_interface',
			id: 'text_bridge_multi',
			value: "physnet1:br-eth1",
		},
		{
			fieldLabel : "Libvirt Type",
			name : 'libvirt_type',
			id: 'text_libvirt_multi',
			value: "kvm",
		},					
		{
			xtype: 'combo',
			fieldLabel : "Controller Server",
			name : 'public_address',
			id: 'combo_public_ip_multi',
			queryMode: 'local',
			triggerAction: 'all', 
			editable: false,
			value: "",
			forceSelection: true,
			allowBlank: false,
			displayField: 'name',
			valueField: 'ip_addr',
			margin: '30 0 0 0',
			emptyText: 'Please select a host to install openstack controller',
		},
		{
			xtype: 'combo',
			fieldLabel : "Compute Servers",
			name : 'compute_ips',
			id: 'combo_compute_ip_multi',
			queryMode: 'local',
			triggerAction: 'all', 
			editable: false,
			multiSelect: true,
			value: "",
			forceSelection: true,
			allowBlank: false,
			displayField: 'name',
			valueField: 'name',
			emptyText: 'Please select a host to install openstack compute server',
		},		
		{
			xtype : 'textareafield',
			fieldLabel : '   ',
			id: 'area_compute_multi',
			name: 'compute_multi',
			grow: true,
			allowBlank : false,
			readOnly: true,
			height: 150,
			value: "",			
		},
	],

    initComponent: function()
    {
        this.callParent(arguments);
    }

});