Ext.define("Netra.view.component.form.ProviderOpenstack", {
	extend : "Ext.form.Panel",
	title : 'OpenStack',
	//titleAlign: 'center',
	//frame : false,
	//autoScroll: true,
	alias: 'widget.form_provider_openstack',
    titleAlign: 'left',
    autoScroll: true,
    headerPosition: 'left',        
	bodyPadding: '30 30 30 30',

    defaults: {
    	xtype: "textfield",
        //align: 'stretch',
        anchor: '100%',
        labelWidth: 160,
    },


	items: [
		{
			xtype: 'panel',
			frame: false,
			border: 0,
			html: Netra.Helper.createFormTitle('OpenStack Provider','')
		},	
		{
			fieldLabel : "Name",
			id: 'text_name',
			name: 'name',
			value: "Netra",			
		},
		{
			xtype : 'textareafield',
			fieldLabel : 'Description',
			id: 'area_description',
			name: 'description',
			grow: true,
			allowBlank : true,			
			height: 150,
			value: "Please Description",			
		},
		{
			xtype: 'hiddenfield',
			name: 'Template',
			id: 'hidden_template',
			value: ''
		},
		{
			fieldLabel : "Admin Password",
			name : 'admin_password',
			id: 'text_admin',
			value: "secret",
		},
		{
			xtype: 'combo',
			fieldLabel : "OpenStack Server",
			name : 'public_address',
			id: 'combo_public_ip',
			queryMode: 'local',
			triggerAction: 'all', 
			editable: false,
			value: "",
			forceSelection: true,
			allowBlank: false,
			displayField: 'name',
			valueField: 'ip_addr',
			emptyText: 'Please select a host to install openstack all-in-one',
		},
		{
			fieldLabel : "Floating IP Range",
			id: 'text_floating',
			name: 'floating_range',					
			value: "192.168.56.200/27",
		},
		{
			fieldLabel : "Fixed IP Range",
			name : 'fixed_range',
			id: 'text_fixed',
			value: "10.0.0.0/24",
		},
		{
			fieldLabel : "Public Interface",
			name : 'public_interface',
			id: 'text_public',
			value: "eth0",
		},
		{
			fieldLabel : "Private Interface",
			name : 'private_interface',
			id: 'text_private',
			value: "eth1",
		},
		{
			fieldLabel : "Libvirt Type",
			name : 'libvirt_type',
			id: 'text_libvirt',
			value: "kvm",
		},					
	],

    initComponent: function()
    {
        console.debug("initComponent");
        this.callParent(arguments);
    },

    populateForm: function(json)
    {
    	console.debug("populateForm");
    },

});