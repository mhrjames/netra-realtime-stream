Ext.define("Netra.form.FormPxetemplate", {
	extend : "Ext.form.Panel",
	alias : 'widget.form_pxetemplate',
	title : 'PXE Template Contents',
	titleAlign: 'center',
	frame : false,
	autoScroll: true,

    defaults: {
    	xtype: "textfield",
        anchor: '100%',
        labelWidth: 130
    },


	items : [
		{
			xtype: 'component',
			html: Netra.Helper.createFormTitle('PXE Template','')
		},	

		{
			xtype: 'checkboxfield',
			fieldLabel : "State",
			boxLabel: "Active",
			id: 'kickstart_active',
			name : 'status'
		},
		{
			allowBlank : false,
			fieldLabel : "Kickstart Name",
			name : 'name',
			emptyText: "name",
		},
		{
			allowBlank : false,
			xtype : 'textareafield',
			fieldLabel : "Description",
			name : 'description',
			emptyText: "Please write a description",
			height: 100,
		},				
		{
			allowBlank : false,
			xtype: 'combo',
			fieldLabel : "Subnet",
			name : 'subnet',
			store: 'Subnets',
			valueField: 'name',
			displayField: 'name',
			queryMode : 'local',
			triggerAction: 'all',
			editable: false,
		},				
		{
			allowBlank : false,
			fieldLabel : "OS Family",
			name : 'os_name',
			emptyText: "name",
		},		
		{
			allowBlank : false,
			fieldLabel : "Version",
			name : 'version',
			emptyText: "name",
		},		
		{
			allowBlank : false,
			fieldLabel : "ISO url",
			name : 'iso_url',
			emptyText: "please type your iso url",
			allowBlank: false
		},		
		{
			allowBlank : false,
			fieldLabel : "Root password",
			name : 'root_password',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "Netra Proxy URL",
			name : 'proxy_url',
			emptyText: "please type your netra proxy url",
			allowBlank: false
		},				
		{
			allowBlank : false,
			xtype: 'combo',
			fieldLabel : "Template File Name",
			name : 'template_file',
			store: 'Kickstarts',
			valueField: 'name',
			queryMode : 'local',
			displayField: 'name',
			id: 'combo_template',
			triggerAction: 'all',
			editable: false,
		},				
		{
			allowBlank : false,			
			xtype : 'textareafield',
			fieldLabel : 'Kickstart',
			name : 'kickstart',
			id: 'text_kickstart',
			grow: true,
			height: 400,
			fieldCls: 'netra-code-box'
		},
		{
			xtype : 'hiddenfield',
			name : 'id'
		}

	],

    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            alias: 'widget.toolbar_form',
            id: 'toolbar_form',
            ui: 'footer',
            //defaults: {minWidth: minButtonWidth},
            items: [                    
                { xtype: 'component', flex: 1 },
                
                { 
                    xtype: 'button', id: 'btn_preview', text: 'Preview',
                    iconCls: Icons.btnPreview
                },                
                { 
                    xtype: 'button', id: 'btn_kickstart_active', text: 'Active',
                    iconCls: Icons.btnActive
                },                
                { 
                    xtype: 'button', id: 'btn_kickstart_validate', text: 'Validate',
                    iconCls: Icons.btnValidate
                },
                { 
                    xtype: 'button', id: 'btn_save_kickstart', text: 'Save',
                    iconCls: Icons.btnSave
                }
            ]
        }
    ],



    updateForm: function (record,readonly) 
    {
        console.log("updateForm");
        
        if (record == null) return;

        for (var i=0;i<this.items.length;i++) 
        {
            var inputs = this.items.get(i);
            //field_name = inputs.name.replace("text_","");

            if (inputs.xtype == "checkboxfield") 
            {
                
                if (record.get(inputs.name) == "on") {
                    inputs.setValue(true);
                } else {
                	inputs.setValue(false);
                }
            } else if (inputs.xtype == "component") {

            } else {
                inputs.setValue(record.get(inputs.name));
            }

            //make readonly fields
            if (inputs.xtype != "component") {
                inputs.setReadOnly(readonly);
            }
        } 
    },



});