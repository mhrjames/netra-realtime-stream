Ext.define("Netra.form.FormSubnet", {
	extend : "Ext.form.Panel",
	alias : 'widget.form_subnet',
	title : 'Subnet Contents',
	titleAlign: 'center',
	frame : false,
	//bodyPadding : 30,
	autoScroll: true,

    defaults: {
    	xtype: "textfield",
        anchor: '100%',
        labelWidth: 130
    },

	items : [
		{
			xtype: 'component',
			html: Netra.Helper.createFormTitle('Subnet','')
		},
		{
			xtype: 'checkboxfield',
			fieldLabel : "State",
			boxLabel: "Active",
			id: 'subnet_active',
			name : 'status'
		},	
		{
			allowBlank : false,
			fieldLabel : "Subnet Name",
			name : 'name',
			emptyText: "name",
		},
		{
			allowBlank : false,
			xtype : 'textareafield',
			fieldLabel : "Description",
			name : 'description',
			emptyText: "Please write a description",
			height: 100,
		},		
		{
			allowBlank : false,
			fieldLabel : "Network Address",
			name : 'network_address',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "Domain",
			name : 'domain',
			emptyText: "Please type root password!!!",
		},				
		{
			allowBlank : false,
			fieldLabel : "DHCP Interface",
			name : 'dhcp_interface',
			emptyText: "eth2",
			allowBlank: false
		},		
		{
			allowBlank : false,
			fieldLabel : "DHCP Host",
			name : 'dhcp_host',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "DNS Hosts",
			name : 'dns_host',
			emptyText: "Please type root password!!!",
		},				
		{
			allowBlank : false,
			fieldLabel : "Router",
			name : 'router',
			emptyText: "Please type root password!!!",
		},				
		{
			allowBlank : false,
			fieldLabel : "Start IP Range",
			name : 'start_ip_range',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "End IP Range",
			name : 'end_ip_range',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "Name Prefix",
			name : 'name_prefix',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "PXE Template File",
			name : 'pxe_file',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "Max Lease Time",
			name : 'max_lease_time',
			emptyText: "Please type root password!!!",
		},		
		{
			allowBlank : false,
			fieldLabel : "Local Postfix",
			name : 'local_postfix',
			emptyText: "Please type root password!!!",
		},		
		{
			xtype : 'hiddenfield',
			name : 'id'
		}

	],

    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            alias: 'widget.toolbar_form',
            id: 'toolbar_form',
            ui: 'footer',
            //defaults: {minWidth: minButtonWidth},
            items: [                    
                { xtype: 'component', flex: 1 },
                { 
                    xtype: 'button', id: 'btn_subnet_active', text: 'Active',
                    iconCls: Icons.btnActive
                },                
                { 
                    xtype: 'button', id: 'btn_save_subnet', text: 'Save',
                    iconCls: Icons.btnSave
                }
            ]
        }
    ],


    updateForm: function (record, readonly) 
    {
        console.log("updateForm");
        
        if (record == null) return;


        for (var i=0;i<this.items.length;i++) 
        {
            var inputs = this.items.get(i);

            if (inputs.xtype == "checkboxfield") 
            {
                //console.debug(inputs);
                console.debug(" checkbox = " + inputs.name + " , value="+record.get(inputs.name));
                
                if (record.get(inputs.name) == "on") {
                    inputs.setValue(true);
                } else {
                    inputs.setValue(false);
                }
            } else if (inputs.xtype == "component") {

            } else {
                inputs.setValue(record.get(inputs.name));
            }

            //make readonly fields
            if (inputs.hasOwnProperty('readonly')) {
                inputs.setReadOnly(readonly);
            }

        } 

    },



});