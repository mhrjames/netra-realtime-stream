Ext.define('Netra.view.ListDetailViewport', {
    
    extend: 'Ext.container.Viewport',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.container.ButtonGroup',
        'Ext.button.Button',
    ],

    alias: 'widget.mainView',
    id: 'mainView',
    layout: 'border',


    initComponent: function() 
    {
        var me = this;

        Ext.applyIf(me, {

            items: [
                {
                    xtype: 'netra_header',
                    region: 'north'
                },
                {
                    xtype: 'panel_list',
                    region: 'west',
                    split: true                    
                },
                {
                    xtype: 'panel_contents',
                    region: 'center',
                    split: true,
                    titleAlign: 'center'
                }
            ]
        });

        me.callParent(arguments);
    }

});