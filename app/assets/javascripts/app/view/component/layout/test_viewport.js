Ext.define('Netra.view.TestViewport', {
    
    extend: 'Ext.container.Viewport',
    requires: [],

    alias: 'widget.mainView',
    id: 'mainView',
    layout: 'fit',


    initComponent: function() 
    {
        var me = this;

        Ext.applyIf(me, {

            items: [
                {
                    xtype: 'label',
                    text: 'value!!!'
                },
            ]
        });

        me.callParent(arguments);
    }

});