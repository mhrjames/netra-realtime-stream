Ext.define('Netra.view.TreeGridDetailView', {
    
    extend: 'Ext.container.Viewport',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.container.ButtonGroup',
        'Ext.button.Button'
    ],

    alias: 'widget.mainView',
    id: 'mainView',
    layout: 'border',


    initComponent: function() 
    {
        var me = this;

        Ext.applyIf(me, {

            items: [
                {
                    xtype: 'netra_header',
                    region: 'north'
                },
                {
                    xtype: 'panel_tree',
                    region: 'west',
                    title: 'Resource',
                    width: 220,
                    split: true                    
                },
                {
                    xtype: 'panel_list',
                    region: 'west',
                    title: 'List',
                    titleAlign: 'center',
                    split: true,
                    flex: 6
                },                
                {
                    xtype: 'panel_contents',
                    region: 'center',
                    split: true,
                    //width: 220,
                    title: 'Resource Details',
                    titleAlign: 'center'
                }
            ]
        });

        me.callParent(arguments);
    }

});