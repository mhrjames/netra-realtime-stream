Ext.define('Netra.component.TreePanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.panel_tree',
    id: 'panel_tree',
    animCollapse: false,
    layout: 'fit',
    title: '',

    items : [
        {
            xtype: 'treepanel',
            id: 'tree_menu',
            alias: 'widget.tree_menu',
            title: '',
            width: 200,
            rootVisible: false,
            useArrows: true,

            root: {
                text: 'Root',
                children: []
            }
        }
    ],


    selectNodeByIndex: function (index) 
    {
        console.debug("selectNodeByIndex : index = " + index);

        var rootNode = tree.getRootNode();
        var node = rootNode.getChildAt(index);

        tree.getSelectionModel().select(node);
    },


    initComponent: function()
    {
        this.callParent(arguments);
    }

});
