Ext.define("Netra.component.mainToolbar", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.toolbar_main',
    baseCls: 'netra-toolbar',    
    frame: false,
    //width: 105,

    items: [
        {
            xtype: 'toolbar',

            //dock: 'top',
            //vertical: false,
            //align: 'stretchmax',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            defaults: {
                xtype: 'button',
                //scale: 'large',
                glyph: 72,
                iconAlign: 'left'
            },

            items: [
                {
                    text: 'Dashboard',
                    handler: function() {
                        window.location = "/";
                    } 
                },
                {
                    text: 'Deployment',
                    menu: {
                        items: [
                            {
                                text: 'Bare Metal',
                                handler: function() {
                                    window.location = "/deployment/bare_metal";
                                }                                             
                            },                        
                            {
                                text: 'New OpenStack',
                                handler: function() {
                                    window.location = "/deployment/new_openstack";
                                }                                             
                            },
                            {
                                text: 'Deploy',
                                handler: function() {
                                    window.location = "/deployment/deploy";
                                }                                             
                            },                            
                            {
                                text: 'Report',
                                handler: function() {
                                    window.location = "/deployment/report";
                                }
                            }
                        ]
                    },
                    handler: function() {
                        //window.location = "/new_openstack";
                    }             
                },
                {
                    text: 'Infrastructure',
                    menu: {
                        items: [
                            {
                                text: 'Host',
                                handler: function() {
                                    window.location = "/infra/host";
                                }                                                         
                            }
                        ]
                    }
                },                  
                {
                    text: 'OpenStack',
                    handler: function() {
                        window.location = "/stack";
                    }             
                },
                {
                    text: 'Service',
                    handler: function() {
                        window.location = "/stack";
                    }             
                },
                {
                    text: 'Report',
                    menu: {
                        items: [
                            {
                                text: 'Configuration',
                                handler: function() {
                                    window.location = "/report/configuration";                                    
                                }
                            },
                            {
                                text: 'Deployment',
                                handler: function() {
                                    window.location = "/report/deploy";
                                }
                            },
                            {
                                text: 'Notification',
                                handler: function() {
                                    window.location = "/report/notification";
                                }             
                            }
                        ]
                    },
                    handler: function() {

                    }                         
                },
                {
                    text: 'Config',
                    handler: function() {
                        window.location = "/config";
                    }                         
                }                                          
            ]
        }
    ]

});