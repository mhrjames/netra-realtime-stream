Ext.define("Netra.component.mainToolbar", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.toolbar_main',
    baseCls: 'netra-toolbar',    
    frame: false,
    width: 105,

    items: [
        {
            xtype: 'toolbar',
            dock: 'left',
            vertical: true,
            align: 'stretchmax',
            layout: 'fit',
            defaults: {
                xtype: 'button',
                scale: 'large',
                glyph: 72,
                iconAlign: 'top'
            },

            items: [
                {
                    text: 'Home',
                    handler: function() {
                        window.location = "/";
                    } 
                },
                {
                    text: 'New!',
                    handler: function() {
                        window.location = "/new_openstack";
                    }             
                },
                {
                    text: 'OpenStack',
                    handler: function() {
                        window.location = "/stack";
                    }             
                },
                {
                    text: 'Resource',
                    handler: function() {
                        window.location = "/resource";
                    }                         
                },                  
                {
                    text: 'Report',
                    handler: function() {
                        window.location = "/report";
                    }                         
                },
                {
                    text: 'Notification',
                    handler: function() {
                        window.location = "/notification";
                    }             
                },          
                {
                    text: 'Config',
                    handler: function() {
                        window.location = "/config";
                    }                         
                }                                          

            ]
        }
    ]

});