Ext.define('Netra.component.TreeRole', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.tree_role',
    layout: 'fit',
    title: '',
    animCollapse: false,

    items : [
        {
            xtype: 'treepanel',
            id: 'tree_role',
            title: '',
            width: 200,
            rootVisible: false,
            useArrows: true,

            root: {
                text: 'Root',
                children: []
            }
        }
    ],

    initComponent: function()
    {
        this.callParent(arguments);
    }

});
