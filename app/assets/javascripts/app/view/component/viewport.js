Ext.define('Netra.view.NetraView', {
    
    extend: 'Ext.container.Viewport',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.container.ButtonGroup',
        'Ext.button.Button'
    ],

    layout: 'border',

    initComponent: function() 
    {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'netra_header',
                    region: 'north'
                },
                {
                    xtype: 'panel_tree',
                    region: 'west',
                    split: true,
                    width: 180,
                    title: 'Notification Channel'
                },
                {
                    xtype: 'panel_contents',
                    region: 'center',
                    split: true,
                    title: 'Title',
                    titleAlign: 'center'
                }
            ]
        });

        me.callParent(arguments);
    }

});