Ext.define('Netra.view.component.window.ClusterHost', {
    extend: 'Ext.app.Controller',
    alias: 'controller.dialog_clusterhost',
    id: 'dialog_clusterhost',
    reference: 'dialog_clusterhost',

    requires: [
        'Netra.store.Hosts'
    ],

    refs: [
        { ref: 'formInput', selector: '#form_newfloating' },
    ],

    clusterRecord: null,
    clusterHostDialog: null,


    init: function() 
    {
        console.debug("ClusterHost.init");

        this.control({
            '#grid_clusterhost': { 
                render: this.onGridRender,
            },
            '#btn_clusterhost_close': { click: this.onButtonCloseClick },
            '#btn_clusterhost_save': { click: this.onButtonSaveClick },
        });

        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

        this.clusterHostDialog = Ext.create('Ext.window.Toast', 
        {
            title: 'Cluster Host Manager',
            height: 600,
            width: 800,
            border:false,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

            layout: {
                type: 'fit',
                pack: 'start',
                align: 'stretch',
            },
            defaults: {
                bodyPadding: '15 30 30 30',
                autoScroll: false,
            },

            modal: true,
            closable: false,
            onEsc: function() {},
            

            items : 
            [
                {
                    xtype: 'form',
                    id: 'form_newfloating',
                    border: false,
                    //title: 'Input Forms',
                    //titleAlign: 'center',
                    defaults: {
                        xtype: "textfield",
                        align: 'stretch',
                        anchor: '100%',
                        labelWidth: 140,
                    },

                    items: 
                    [
                        {
                            xtype: 'component',
                            frame: false,
                            border: 0,
                            html: Netra.helper.createFormTitle('Cluster Host Management','')
                        },  
                        {
                            xtype: 'gridpanel',
                            id: 'grid_clusterhost',
                            reference: 'grid_clusterhost',
                            title: 'Please check/uncheck host machines',

                            stripeRows: true,
                            columnLines: true,

                            store: Netra.manager.getStore('storeHosts'),

                            plugins: [
                                //'cellupdating'
                            ],

                            selType: 'checkboxmodel',
                            selModel: {
                                checkOnly: true,
                                injectCheckbox: 0,
                                width: 55,
                            },

                            columns: 
                            [
                                {
                                    text     : 'IP',
                                    width    : 125,
                                    sortable : true,
                                    dataIndex: 'ip'
                                },
                                {
                                    text     : 'Name',
                                    width    : 125,
                                    sortable : true,
                                    //renderer : 'usMoney',
                                    dataIndex: 'name'
                                },
                                {
                                    text     : 'OS',
                                    width    : 95,
                                    sortable : true,
                                    //renderer : this.pctChange,
                                    dataIndex: 'os'
                                }
                            ],
                        },
                    ],
                },
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_floating',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        { xtype: 'button', id: 'btn_clusterhost_save', text: 'Save' },
                        //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
                        { xtype: 'button', id: 'btn_clusterhost_close', text: 'Close' }
                    ]
                }
            ],


        });

        this.onStart();
    },

    show: function(record)
    {
        console.debug("ClusterHost.show");

        this.clusterRecord = record;
        this.refreshData();
        this.clusterHostDialog.show();        
    },

    close: function()
    {
        console.debug("ClusterHost.close : flavorWindow = " , this.clusterHostDialog);

        this.clusterHostDialog.close();
        Netra.manager.removeController(this);
    },

    refreshData: function()
    {
        var _this = this;

        Netra.helper.refreshData('storeHosts','#grid_clusterhost',function(records, operation, success) {
            //console.debug("refreshData -- cluster : ", records);
            
            var grid = Netra.helper.getComponent('#grid_clusterhost');

            for (var i=0;i<records.length;i++) {
                if (records[i].data.cluster_name != null) {
                    if (_this.clusterRecord.data.cluster_name == records[i].data.cluster_name) {
                        grid.getSelectionModel().select(records[i]);
                    }
                }
                //grid.getSelectionModel().select(records[i]);
            }

            Netra.helper.hideWaitDialog();            
        });
    },

    sendRequest: function(json)
    {
        console.debug("ClusterHost.sendRequest");
        
        Ext.Ajax.request({
            url: '/api/openstack/floating_ips/new',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendParameters Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'New Floating IPs are created!!!');
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }
                    
            },
            failure: function(response, opts) 
            {
                console.debug('sendParameters : woops');
            }
        });

    },

    getParameter: function()
    {
        console.debug('ClusterHost.getParameter');

        var json = {};

        var form = this.getFormInput();

        form.query('.textfield').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
    },

    saveToServer: function()
    {
        var json = this.getSelectedHosts();

        json['cluster_name'] = this.clusterRecord.data.name;
        console.debug("ClusterHost.saveToServer : json = ", json);
        
        Netra.service.updateClusterMember(json,this.onRequestComplete);
        //this.sendRequest(json);
    },

    getSelectedHosts: function()
    {
        console.debug("ClusterHost.getSelectedHosts");
        
        var grid = Netra.helper.getComponent('#grid_clusterhost');

        var json = {};
        json['data'] = Netra.helper.getSelectedColumnValues(grid,'ip');

        return json;
    },





    onStart: function()
    {
        console.debug("ClusterHost.onStart");
    },

    onBroadcastMessage: function(param)
    {
        console.debug("onBroadcastMessage");
    },

    onButtonCloseClick: function()
    {
        this.close();
    },

    onButtonSaveClick: function()
    {
        console.debug("ClusterHost.onButtonSaveClick");

        this.saveToServer();
    },

    onGridRender: function (grid, eOpts)
    {
        console.debug("ClusterHost.onGridRender");
    },

    onRequestComplete: function(retcode, response)
    {
        console.debug("ClusterHost.onRequestComplete : ret = ", response);

        if (retcode == Netra.const.REQ_OK) {
            Ext.MessageBox.alert('Success', response.msg);
        } else if (retcode == Netra.const.REQ_FAIL) {
            Ext.MessageBox.alert('Fail', response);
        } else {
            Ext.MessageBox.alert('Fail', response.msg);
        }
    },

});