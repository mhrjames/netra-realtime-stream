Ext.define('Netra.view.component.window.Config', {
    extend: 'Ext.app.Controller',
    id: 'addHost',

    refs: [
        { ref: 'progressBar', selector: '#pbar_progress' },    
        { ref: 'textIp', selector: '#text_ip' },
        { ref: 'textUserid', selector: '#text_userid' },
        { ref: 'textPassword', selector: '#text_password' },
        { ref: 'areaLog', selector: '#area_output' },
        { ref: 'btnAddHost', selector: '#btn_addhost' },
    ],

    configWindow: null,
    doesRefreshStore: false,


    init: function() 
    {
    	console.debug("ConfigWindow.init");

        this.control({
            '#btn_config_close': { click: this.onCloseButtonClick },
            '#btn_config_save': { click: this.onButtonSaveClick },
        });

        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

    	this.configWindow = Ext.create('Ext.window.Window', {
		    title: 'Netra XEN Edition Config',
		    layout: {
		    	type: 'vbox',
		    	align: 'stretch',
		    },
		    height: 500,
		    width: 600,
		    modal: true,
		    bodyPadding: '20 20 20 20',


		    items: 
		    [
				{
					xtype: 'panel',
					frame: false,
					border: 0,
					html: Netra.helper.createFormTitle('Netra Configuration','Please fill out following columns')
				},	
		    	{
		    		xtype: 'tabpanel',
		    		frame: false,
		    		border: false,
		    		layout: {
		    			type: 'fit',
		    			align: 'stretch'
		    		},

				    defaults: {
					    bodyBorder: false,
					    border: false,
						padding: '10 20 20 20',
						bodyPadding: '10 0 0 0',
				    },

		    		items:
		    		[
		    			{
		    				title: 'General',

					    	xtype: 'form',
							id: 'form_config_general',

						    layout: {
						        type: 'vbox',
						        align: 'stretch',
						    },
						    defaults: {
						    	xtype: "textfield",
						        anchor: '100%',
						        labelWidth: 170
						    },    	
							items : 
							[
								{
									fieldLabel : "Netra Server IP",
									id: 'text_config_netra_ip',
									name: 'general_netra_ip',
									value: "root",			
								},
								{
									fieldLabel : "Netra Username",
									id: 'text_config_netra_username',
									name: 'general_username',
									value: "vagrant",			
								},
								{
									fieldLabel : "Netra Password",
									id: 'text_config_netra_password',
									name: 'general_password',
									inputType:'password',
									value: "vagrant",			
								},								
								{
									fieldLabel : "XEN Master IP",
									id: 'text_config_master_ip',
									name: 'general_master_ip',
									value: "vagrant",
								},
								{
									fieldLabel : "XEN Master Username",
									id: 'text_config_username',
									name: 'general_master_username',
									value: "vagrant",			
								},
								{
									fieldLabel : "XEN Master Password",
									id: 'text_config_password',
									name: 'general_master_password',
									inputType:'password',
									value: "vagrant",			
								},

							]
						},
		    			{
		    				title: 'Active Directory',
		    				
					    	xtype: 'form',
					    	id: 'form_config_ad',

						    layout: {
						        type: 'vbox',
						        align: 'stretch',
						    },
						    defaults: {
						    	xtype: "textfield",
						        anchor: '100%',
						        labelWidth: 170
						    },    	
							items : 
							[
								{
									fieldLabel : "AD Host IP",
									id: 'text_ad_ip',
									name: 'ad_ip',
									value: "192.168.56.159",			
								},
								{
									fieldLabel : "AD Port",
									id: 'text_ad_port',
									name: 'ad_port',
									value: "root",			
								},
								{
									fieldLabel : "Base",
									id: 'text_ad_base',
									name: 'ad_base',
									value: "vagrant",			
								},
								{
									fieldLabel : "OU",
									id: 'text_ad_ou',
									name: 'ad_ou',
									value: "vagrant",			
								},																
								{
									fieldLabel : "User Name",
									id: 'text_ad_username',
									name: 'ad_username',
									value: "vagrant",			
								},
								{
									fieldLabel : "Password",
									id: 'text_ad_password',
									name: 'ad_password',
									value: "vagrant",			
									inputType:'password',
								},

							]
						},
		    			{
		    				title: 'XEN Desktop',
		    				
					    	xtype: 'form',
					    	id: 'form_config_xen',

						    layout: {
						        type: 'vbox',
						        align: 'stretch',
						    },
						    defaults: {
						    	xtype: "textfield",
						        anchor: '100%',
						        labelWidth: 130
						    },    	
							items : 
							[
								{
									fieldLabel : "XEN Desktop IP",
									id: 'text_xd_ip',
									name: 'xd_ip',
									value: "192.168.56.159",			
								},
								{
									fieldLabel : "SSH Port",
									id: 'text_xd_port',
									name: 'xd_port',
									value: "root",			
								},
								{
									fieldLabel : "User Name",
									id: 'text_xd_username',
									name: 'xd_username',
									value: "vagrant",			
								},
								{
									fieldLabel : "Password",
									id: 'text_xd_password',
									name: 'xd_password',
									value: "vagrant",	
									inputType:'password',		
								},

							]
						}										
		    		]

				}, 
		    ],


		    dockedItems: [
		        {
		            xtype: 'toolbar',
		            dock: 'bottom',
		            alias: 'widget.toolbar_window',
		            id: 'toolbar_window',
		            ui: 'footer',
		            //defaults: {minWidth: minButtonWidth},
		            items: [
		            	{ xtype: 'component', flex: 1 },
		                { xtype: 'button', id: 'btn_config_save', text: 'Save', },
		                { xtype: 'button', id: 'btn_config_close', text: 'Close', },
		            ]
		        }
		    ],
		    
    	});
		
    },

    show: function()
    {
		this.configWindow.show();
		this.loadConfig();
    },

    close: function()
    {
    	console.debug("Config.close : configWindow = " , this.configWindow);

    	this.configWindow.close();
    	Netra.manager.removeController(this);
    },


    loadConfig: function()
    {
    	Netra.helper.showWaitDialog("","",'btn_config_save');

    	Netra.task.getConfig(this, {} , function(_this, retcode, json) 
    	{
    		console.debug("ConfigWindow.loadConfig : json = ", json);

			Netra.helper.hideWaitDialog();

    		if (json.ret_code != 0) {
    			Ext.MessageBox.alert('Fatal Error', 'Loading configuration data failed, please check config file to go!!!');
    			return;
    		}

 			for (var key in json['data'])
 			{
            	var textfield = Netra.helper.getComponent('[name=' + key +']');
            	if (textfield) {
            		textfield.setValue(json['data'][key]);
            	}
        	}

    	});
    },

    validateForm: function()
    {
    	if (! Netra.helper.getComponent('#form_config_general').isValid()) {
    		return false;
    	}

    	if (! Netra.helper.getComponent('#form_config_ad').isValid()) {
    		return false;
    	}

    	if (! Netra.helper.getComponent('#form_config_xen').isValid()) {
    		return false;
    	}

		return true;    	
    },


    updateProgressbar: function(retcode)
    {
        var caption = "";
        if (retcode == 0) {
        	caption = "Bootsrapping Done Successfully!!!";
			this.doesRefreshStore = true;
        } else {
        	caption = "Bootstrapping Failed!!!";
        }

		this.getProgressBar().updateText(caption);
		this.getProgressBar().reset();
    },

    getConfigParameters: function()
    {
		var params = {};

		var forms = Ext.ComponentQuery.query('#form_config_general .textfield');
        for(var i= 0; i < forms.length; i++) {
            params[forms[i].name] = forms[i].getValue();
        }

		forms = Ext.ComponentQuery.query('#form_config_ad .textfield');
        for(var i= 0; i < forms.length; i++) {
            params[forms[i].name] = forms[i].getValue();
        }

		forms = Ext.ComponentQuery.query('#form_config_xen .textfield');
        for(var i= 0; i < forms.length; i++) {
            params[forms[i].name] = forms[i].getValue();
        }

        return params;
    },




    onButtonSaveClick: function() 
	{
		console.debug("ConfigWindow.onButtonSaveClick");
		
		if (! this.validateForm()) {
			Ext.MessageBox.alert('Error', 'Please fill out all columns!');
			return;
		}

		var params = this.getConfigParameters();
		Netra.task.updateConfig(this, params, this.onUpdateConfigComplete);
	},

    onCloseButtonClick: function() 
    {
    	console.debug("btn_close clicked");
    	this.close();
    },

    onBroadcastMessage: function(param)
    {
        console.debug("ConfigWindow.onBroadcastMessage!!!");
        console.debug(param);        

		this.updateProgressbar(param.retcode);
        this.getAreaLog().setValue(param.msg); 
    },

	onUpdateConfigComplete: function(_this, retcode, json)
	{
		console.debug("ConfigWindow.onUpdateConfigComplete!!!");

		if (json.ret_code == 0) {
			Ext.MessageBox.alert('Success', 'Configuration is updated successfully!!!');			
		}

	},

});
