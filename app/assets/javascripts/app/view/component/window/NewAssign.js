Ext.define("Netra.view.component.window.NewAssign", {
	extend: 'Ext.app.Controller',
	alias: 'controller.newassign',
	id: 'newassign',
	reference: 'newassign',

    requires: [ 
        'Ext.toolbar.*',
    ],
 
    refs: [
    	{ ref: 'formInput', selector: '#form_newassign' },
    ],


    models: [ ],    
    stores: [ ],    

    poolRecord: null,
    catalogRecord: null,
    groupRecord: null,
    hypervisorRecord: null,
    securityRecord: null,
	newAssignWindow: null,
	selectedNodeIndex: 0,


    init: function() 
    {
    	console.debug("newassignWindow.init");

		//this.callParent(arguments);

        this.control({
            '#tree_new_assign': {
                itemclick: this.onNodeClick,
            },

            '#grid_newassign_hypervisor': { 
                selectionchange: this.onHypervisorGridSelectionChange,
            },

            '#grid_newassign_catalog': { 
                selectionchange: this.onCatalogGridSelectionChange,
            },
            
            '#grid_newassign_group': { 
                selectionchange: this.onGroupGridSelectionChange,
            },

            '#combo_newassign_machine' : { 
            	select: this.onComboMachineSelect,
            	change: this.onComboMachineChanged,
            	keypress: this.onComboMachineKeypress, 
            },
            '#combo_newassign_pool': { 
            	select: this.onPoolSelect,
            	change: this.onPoolChanged 
            },     

            '#btn_newassign_prev': { click: this.onButtonPrevClick },
            '#btn_newassign_next': { click: this.onButtonNextClick },
            '#btn_newassign_close': { click: this.onButtonCloseClick },
            '#btn_newassign_run': { click: this.onButtonRunClick },            
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });

        this.cellEditing.on('edit', this.onEditComplete, this);

    	this.newAssignWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Assign Virtual Machine',
		    height: 600,
		    width: 800,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,
		    
		    id: 'window_newassign',
		    //reference: 'windowSecurityRule',
		    //animCollapse: true,
		    //collapseDirection: 'top',
			//animateTarget: Ext.getDoc(),

		    layout: {
		    	type: 'border',
		    },

		    modal: true,
			onEsc: function() {},
			closable: false,
		    
			items : 
			[
                {
                    xtype: 'treepanel',
                    id: 'tree_new_assign',
                    region: 'west',
                    title: 'Process',
                    width: 160,
                    rootVisible: false,
                    useArrows: true,
                    split: true,
                    root: {
                        text: 'Root',
                        children: 
                        [
                            { text: 'User & Machine', leaf: true },
                            { text: 'Hypervisor Conn', leaf: true } ,
                            { text: 'Machine Catalog', leaf: true } ,
                            { text: 'Delivery Group', leaf: true } ,
                            { text: 'Finish', leaf: true },
                        ]
                    }
                },
                {
                    xtype: 'panel',
                    id: 'panel_newassign_container',
                    region: 'center',
                    border: false,
                    //title: 'Input Forms',
                    //titleAlign: 'center',
                    layout: {
                        // layout-specific configs go here
                        type: 'card',
                        animate: true,        
                        //titleCollapse: false,
                        //activeOnTop: true
                    },
                    defaults: {
                        titleAlign: 'left',
                        autoScroll: false,
                        headerPosition: 'left'        
                    },

                    items:
                    [
                    	{
                            xtype: 'form',
                            id: 'form_newassign_user',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },
                            items:
                            [
								{
									xtype: 'component',
									frame: false,
									border: 0,
									html: Netra.helper.createFormTitle('Please select users & machines','You can select multiple users and machines')
								},
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Machine Pool",
                                    name : 'newassign_pool',
                                    id: 'combo_newassign_pool',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a pool to provision a new virtual machine',
                                    store: Netra.manager.getStore('storeClusters'),
                                    //invalidText: 'Please select a template to install openstack controller',
                                },								                 
				                {
				                    xtype: 'gridpanel',
				                    id: 'grid_newassign_user',
				                    flex: 3,
				                    //title: 'List',
				                    stripeRows: true,
				                    columnLines: true,
									
									margin: '20 0 0 0',

									plugins: [this.cellEditing],
				                    
				                    store: Netra.manager.getStore('storeAdUsers'),

				                    selType: 'checkboxmodel',
				                    selModel: {
				                        checkOnly: true,
				                        injectCheckbox: 0
				                    },

				                    columns: 
				                    [
				                        { text: 'Account',flex: 1, hidden: false, dataIndex: 'account' },
				                        { text: 'Name', flex:  1, align: 'center', dataIndex: 'name', hidden: true },
				                        { text: 'User ID', flex: 1, sortable: true, hidden: true, dataIndex: 'userprincipalname' },  
				                        {
				                            text: 'Oganization Unit', flex: 1, dataIndex: 'dn',
				                            renderer: function(value)
				                            {
				                                return Netra.helper.extractString(value,"OU=",",");
				                            }                    
				                        },                      
				                        { text: 'Domain', flex: 2, sortable: true, dataIndex: 'dn', hidden: true, },
				                        { text: 'Machine', flex: 2, dataIndex: 'machine_uuid',

			                        	    editor: new Ext.form.field.ComboBox({
						                    	id: 'combo_newassign_machine',
						                    	typeAhead: false,
						                    	typeAheadDelay: 90000,
						                    	triggerAction: 'all',
						                    	disableKeyFilter: true,
						                    	store: Netra.manager.getStore('storeXenServers'),
			                                    displayField: 'name',
			                                    valueField: 'uuid',
						                	})
				                        },
				                        { text: 'Computer Name', flex: 1, sortable: true, dataIndex: 'computer_account', 
				                        	field: {
				                        		xtype: 'textfield'
				                        	}
				                        },
				                    ],
				                }

                            ]
						}, 
                    	{
                            xtype: 'form',
                            id: 'form_newassign_hypervisor',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },
                            items:
                            [
								{
									xtype: 'component',
									frame: false,
									border: 0,
									html: Netra.helper.createFormTitle('Please Select Hypervisor Connection','')
								},	                    
				                {
				                    xtype: 'gridpanel',
				                    id: 'grid_newassign_hypervisor',
				                    flex: 3,
				                    //title: 'List',
				                    stripeRows: true,
				                    columnLines: true,

				                    store: Netra.manager.getStore('storeHypervisorConnections'),

				                    selType: 'checkboxmodel',
				                    selModel: {
				                        checkOnly: true,
				                        injectCheckbox: 0,
				                        mode: 'SINGLE'
				                    },

				                    columns: 
				                    [
				                        { text: 'Uid',flex: 1, hidden: false, dataIndex: 'Uid' },
				                        { text: 'Name', flex:  1, align: 'center', dataIndex: 'Name', },                                           
				                        { text: 'State', flex: 1, sortable: true, dataIndex: 'State' },
				                        { text: 'Is Ready', flex: 1, dataIndex: 'IsReady', },
				                    ],
				                }

                            ]
						},      						                   
                    	{
                            xtype: 'form',
                            id: 'form_newassign_catalog',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },
                            items:
                            [
								{
									xtype: 'component',
									frame: false,
									border: 0,
									html: Netra.helper.createFormTitle('Please Select Machine Catalog','')
								},	                    
				                {
				                    xtype: 'gridpanel',
				                    id: 'grid_newassign_catalog',
				                    flex: 3,
				                    //title: 'List',
				                    stripeRows: true,
				                    columnLines: true,

				                    store: Netra.manager.getStore('storeMachineCatalogs'),

				                    selType: 'checkboxmodel',
				                    selModel: {
				                        checkOnly: true,
				                        injectCheckbox: 0,
				                        mode: 'SINGLE'
				                    },

				                    columns: 
				                    [
				                        {
				                            text: 'Uid',flex: 1, hidden: false, dataIndex: 'Uid'
				                        },
				                        {
				                            text: 'Name', flex:  1, align: 'center', dataIndex: 'Name',
				                        },                                           
				                        {
				                            text: 'Description', flex: 1, sortable: true, dataIndex: 'Description'
				                        },  
				                        {
				                            text: 'Provisioning Type', flex: 1, dataIndex: 'ProvisioningType',
				                        },                      
				                        {
				                            text: 'Total Count', flex: 1, sortable: true, dataIndex: 'AssignedCount'
				                        },
				                        {
				                            text: 'Used Count', flex: 1, sortable: true, dataIndex: 'UsedCount'
				                        },
				                        {
				                            text: 'UUID', flex: 1, sortable: true, hidden:true, dataIndex: 'UUID'
				                        },
				                    ],
				                }

                            ]
						},                    
                    	{
                            xtype: 'form',
                            id: 'form_newassign_group',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },
                            items:
                            [
								{
									xtype: 'component',
									frame: false,
									border: 0,
									html: Netra.helper.createFormTitle('Please Select Delivery Group','')
								},	                    
				                {
				                    xtype: 'gridpanel',
				                    id: 'grid_newassign_group',
				                    flex: 3,
				                    //title: 'List',
				                    stripeRows: true,
				                    columnLines: true,

				                    store: Netra.manager.getStore('storeDeliveryGroups'),

				                    selType: 'checkboxmodel',
				                    selModel: {
				                        checkOnly: true,
				                        injectCheckbox: 0,
				                        mode: 'SINGLE'
				                    },

				                    columns: 
				                    [
				                        { text: 'Uid', flex:  1, align: 'center', dataIndex: 'Uid', },
				                        { text: 'Name', flex:  1, align: 'center', dataIndex: 'Name', },
				                        { text: 'Description', flex: 2, sortable: true, dataIndex: 'Description' },
				                        { text: 'Total Desktops', flex: 1, sortable: true, dataIndex: 'TotalDesktops' },
				                        { text: 'Desktops Available', flex: 1, sortable: true, dataIndex: 'DesktopsAvailable' },
				                        { text: 'Desktops In Use', flex: 1, sortable: true, dataIndex: 'DesktopsInUse' },
				                        { text: 'UUID', flex: 1, sortable: true, hidden:true, dataIndex: 'UUID' },
				                    ],
				                }

                            ]
						},       

				        {
				        	xtype: 'form',
				        	region: 'center',

				        	border: false,
		                    layout: {
		                    	type: 'vbox',
						    	pack: 'start',
						    	align: 'stretch',
								anchor: '100%',
		                    },
						    defaults: {
						    	xtype: "textfield",
						        align: 'stretch',
						        anchor: '100%',
						        labelWidth: 120,
						    },
							bodyPadding: '15 30 30 30',
							id: 'form_newassign',

		                    items: 
		                    [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Finish','Please checked your options, then click Create button to go!!!')
                                },  
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('VM Assignment Summary','')
                                },
                                {
                                    xtype: 'propertygrid',
                                    id: 'grid_newassign_finish',
                                    title: 'General',
                                    titleAlign: 'center',
                                    rowspan: 2,
                                    nameColumnWidth: 160,
                                    sortableColumns: false,
                                    source: {},
                                    sourceConfig: {
                                        borderWidth: {
                                            displayName: 'Border Width'
                                        },
                                        tested: {
                                            displayName: 'QA'
                                        }
                                    }                                            
                                },
		                    ]
		                },

                    ]
		    	}

			],

			dockedItems: [
			    {
			        xtype: 'toolbar',
			        dock: 'bottom',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            { xtype: 'component', flex: 1 },
			            { text: 'Run', id: 'btn_newassign_run' },
			            { text: 'Close', id: 'btn_newassign_close', margin: '0 30 0 0 0' },
                        { text: 'Prev', id: 'btn_newassign_prev' },
                        { text: 'Next', id: 'btn_newassign_next' },

			        ]
			    }
			],

		});

    },

    show: function(record)
    {
		this.securityRecord = record;
		this.newAssignWindow.show();

        Netra.helper.refreshData('storeAdUsers', '#grid_newassign_user', function() {
            //_this.filterInstanceStore(this.hypervisorRecord);
        });

		Netra.service.loadMachineCatalogData(function(retcode,json) {

		});

		Netra.service.loadDeliveryGroupData(function(retcode,json) {

		});

		Netra.service.loadHypervisorConnectionData(function(retcode,json) {

		});
    },

    close: function()
    {
    	console.debug("NewAssign.close : newAssignWindow = " , this.newAssignWindow);

    	this.newAssignWindow.close();
    	//Netra.app.getApplication().destroyController(this);
    	Netra.manager.removeController(this);
    	//Netra.manager.removeController('Netra.view.component.window.NewFlavor');
    },

    validateForm: function()
    {
        console.debug("NewAssign.validateForm");

		var selected = Netra.helper.getComponent('#grid_newassign_user').getSelectionModel().getSelection();
		if (selected.length == 0) {
			this.showView(0);
			return false;
		}

        if (! this.poolRecord) {
            this.showView(0);
            return false;
        }

        if (! this.hypervisorRecord) {
            this.showView(1);
            return false;
        }

        if (! this.catalogRecord) {
            this.showView(2);
            return false;
        }

        if (! this.groupRecord) {
            this.showView(2);
            return false;
        }

        return true;
    },

	getRunParameter: function()
	{
		console.debug('newassign.getRunParameter');

        var json = {};

        json['pool'] = this.poolRecord.data.name;
        json['catalog'] = this.catalogRecord.Name;
        json['catalog_uid'] = this.catalogRecord.Uid;
        json['group'] = this.groupRecord.Name;
        json['group_uid'] = this.groupRecord.Uid;
        json['hypervisor_uid'] = this.hypervisorRecord.Uid;
        
        var items = [];
        var selected = Netra.helper.getComponent('#grid_newassign_user').getSelectionModel().getSelection();        
        for (var index=0; index<selected.length; index++)
        {
        	var item = selected[index];    		
    		items.push({'dn': item.data.dn, 'account': item.data.userprincipalname, 'computer':item.data.computer_account, 'machine':item.data.machine_uuid});
        }        
        
        json['items'] = items;

        return json;
	},


    clearXenserverFilter: function()
    {
        Netra.manager.getStore('storeXenServers').clearFilter();
    },

    filterXenserverStore: function(value)
    {
        console.debug("newAssignWindow.filterUserStore : value = ", value);

        var store = Netra.manager.getStore('storeXenServers');
        
        this.clearXenserverFilter();
        
        store.filterBy(function(record, index) 
        {
            if ( (typeof(record.get('name')) !== "undefined") && (record.get('name') !== null) ) 
            {
	            console.debug("newAssignWindow.filterUserStore : name = " + value);

            	if (record.get('name').indexOf(value)>-1) {
                	return true;
                }
            }	 

            return false;
        });

    },

    updateNavigationButtons: function(increment)
    {
        console.debug("NewAssign.updateNavigationButtons");

        switch (this.selectedNodeIndex) 
        {
            case 0:
                Netra.helper.getComponent('#btn_newassign_run').setDisabled(true);
                Netra.helper.getComponent('#btn_newassign_prev').setDisabled(true);
                Netra.helper.getComponent('#btn_newassign_next').setDisabled(false);
                break;
            
            case (Netra.helper.getComponent('#tree_new_assign').getRootNode().childNodes.length - 1):
                Netra.helper.getComponent('#btn_newassign_run').setDisabled(false);
                Netra.helper.getComponent('#btn_newassign_prev').setDisabled(false);
                Netra.helper.getComponent('#btn_newassign_next').setDisabled(true);
                break;

            default:
                Netra.helper.getComponent('#btn_newassign_run').setDisabled(true);
                Netra.helper.getComponent('#btn_newassign_prev').setDisabled(false);
                Netra.helper.getComponent('#btn_newassign_next').setDisabled(false);
                break;            
        }

    },

    doPanelNavigation: function(ctl,increment)
    {

        var tree = Netra.helper.getComponent('#tree_new_assign');
        var page_count = tree.getRootNode().childNodes.length - 1;

        switch (ctl)
        {
            case 0: //if tree
                this.selectedNodeIndex = increment;
                break;

            case 1: //if button
                this.selectedNodeIndex = this.selectedNodeIndex + increment;    
                break;
        }
        

        if (this.selectedNodeIndex<0) {
            this.selectedNodeIndex = 0;
        } else if (this.selectedNodeIndex > page_count) {
            this.selectedNodeIndex = page_count;
        }

        Netra.helper.selectTreeNode(Netra.helper.getComponent('#tree_new_assign'),this.selectedNodeIndex);
        this.updateNavigationButtons();


        if ((Netra.helper.getComponent('#tree_new_assign').getRootNode().childNodes.length - 1) == this.selectedNodeIndex) {
            this.updateFinishForm();
        }   

        this.showView(this.selectedNodeIndex);

    },

    showView: function(index)
    {
    	this.selectedNodeIndex = index;
		Netra.helper.showCardView('panel_newassign_container',this.selectedNodeIndex,'','');
    },

    updateFinishForm: function()
    {
        console.debug("NewAssign.updateFinishForm");

        var params_general = {};

        if (this.poolRecord) {
            params_general['Pool'] = this.poolRecord.data.name;
        }
        
        if (this.hypervisorRecord) {
            params_general['Hypervisor Uid'] = this.hypervisorRecord.Uid;
        }

        if (this.catalogRecord) {
            params_general['Catalog Name'] = this.catalogRecord.Name;
            params_general['Catalog Description'] = this.catalogRecord.Description;
        }

        if (this.groupRecord) {
            params_general['Group Name'] = this.groupRecord.Name;
            params_general['Group Description'] = this.groupRecord.Description;
        }
        
        var selected = Netra.helper.getComponent('#grid_newassign_user').getSelectionModel().getSelection();        
        for (var index=0; index<selected.length; index++)
        {
        	var item = selected[index];

            console.debug("NewAssign.updateFinishForm : item = ", item);
    		
    		params_general['Newly VM No. '+index] = item.data.dn + ", Account="+item.data.account+", Computer="+item.data.computer_account+", Machine="+item.data.machine_uuid;
        }        
        
        Netra.helper.updatePropertyGrid('#grid_newassign_finish', params_general);

    },




    onStart: function()
    {
		console.debug("newAssignWindow.onStart");
    },


    onPoolChanged: function(combo, newValue, oldValue, eOpts )
    {
        console.debug("newAssignWindow.onPoolChanged : record = ", newValue);    	
/*    	
        var store = Netra.manager.getStore('storeClusters');

        var record = store.findRecord('id',newValue);
        
        if (record == null) return;

        this.poolRecord = record;

        Netra.service.loadXenServerData(this.poolRecord.data.name, this.onLoadXenServerComplete)
*/        
    },

	onPoolSelect: function(combo, records, eOpts)
    {        
        if (records.length == 0) return;

        console.debug("newAssignWindow.onPoolSelect : record = ", records[0]);

        this.poolRecord = records[0];

        Netra.helper.showWaitDialog("","",'combo_newassign_pool');
        Netra.service.loadXenServerData(this.poolRecord.data.name, this.onLoadXenServerComplete)
    },


    onComboMachineChanged: function(combo, newValue, oldValue, eOpts )
    {
        //var record = Netra.manager.getStore('storeXenServers').findRecord('id',newValue);
        
        if (newValue.trim() == "") return;

        console.debug("newAssignWindow.onComboMachineChanged : value = ", newValue);

        this.filterXenserverStore(newValue.trim());
    },

	onButtonCloseClick: function()
	{
		console.debug("newAssignWindow.onButtonCloseClick : controller = ", this);
		this.close();
	},

	onButtonRunClick: function()
	{
		console.debug("newAssignWindow.onButtonRunClick");
	
		if (! this.validateForm()) {
            Ext.MessageBox.alert('Error', 'Please fill out the form');
            return;
        }

		var json = this.getRunParameter();
		Netra.task.runAssignTask(this,json,this.onRunAssignComplete);
	},

	onButtonPrevClick: function()
	{
		console.debug("newAssignWindow.onButtonPrevClick");
		this.doPanelNavigation(1,-1);
	},

	onButtonNextClick: function()
	{
		console.debug("newAssignWindow.onButtonNextClick");
		this.doPanelNavigation(1,1);
	},

	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("NewAssign.onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        //console.debug(record);
        //this.doPanelNavigation(0,index);
        this.doPanelNavigation(0,index);
    },

    onEditComplete: function(editor, context) 
    {
    	console.debug("NewAssign.onEditComplete : context = ", context);   
		
    	if (context.field == "machine") {
			this.clearXenserverFilter();    		
    	}
    	
    	if (context.field == "computer_account") {
    	
    	}

    },

    onPoolLoadComplete: function(records, operation, success) 
    {
        var _this = this;

        console.log("NewAssign.onPoolLoadComplete : length = " + records.length);
        Netra.helper.getComponent('#combo_newassign_pool').bindStore(Netra.manager.getStore('storeClusters'));
    },

    onLoadXenServerComplete: function(ret_code,json)
    {
        console.debug("NewAssign.onLoadXenServerComplete");
     	Netra.helper.hideWaitDialog();
    },

	onComboMachineKeypress: function(combo, e, eOpts)
	{
        console.debug("NewAssign.onComboMachineKeypress : combo = ", combo);
	},

	onComboMachineSelect: function(combo, records, eOpts)
	{
		console.debug("NewAssign.onComboMachineSelect : combo = ", records);
	},

    onCatalogGridSelectionChange: function(grid, selected, eOpts)
    {
        console.debug("NewAssign.onCatalogGridSelectionChange : selected = ", selected);
        this.catalogRecord = selected[0].data;
    },

    onHypervisorGridSelectionChange: function(grid, selected, eOpts)
    {
        console.debug("NewAssign.onHypervisorGridSelectionChange : selected = ", selected);
        this.hypervisorRecord = selected[0].data;
    },

    onGroupGridSelectionChange: function(grid, selected, eOpts)
    {
        console.debug("NewAssign.onGroupGridSelectionChange : selected = ", selected);

        this.groupRecord = selected[0].data;
    },

	onRunAssignComplete: function(_this, retcode, json)
	{
		console.debug("NewAssign.onRunAssignComplete : json = ", json);
	},

});