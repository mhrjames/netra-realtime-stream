Ext.define("Netra.view.component.window.NewCluster", {
	extend: 'Ext.app.Controller',
	alias: 'controller.newcluster',
	id: 'newcluster',
	reference: 'newcluster',

    requires: [ 
        'Ext.toolbar.*',
    ],

    refs: [
    	{ ref: 'formInput', selector: '#form_newcluster' },
    	{ ref: 'formTitle', selector: '#form_newcluster_title' },
    ],

    models: [ ],    
    stores: [ ],    

	clusterWindow: null,
	clusterRecord: null,


    init: function() 
    {
    	console.debug("window.init");

		//this.callParent(arguments);

        this.control({
            '#btn_newcluster_window_close': { click: this.onButtonCloseClick },
            '#btn_newcluster_window_save': { click: this.onButtonSaveClick },
/*            
            '#combo_public_ip': { change: this.onHostChanged },
            '#combo_public_ip_multi': { change: this.onHostChanged },
            '#combo_compute_ip_multi': { change: this.onComputeChanged },
            '#tree_openstack': {
            	itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },
            '#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
*/            
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 


    	this.clusterWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Pool Dialog',
		    height: 400,
		    width: 600,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

		    id: 'window_newcluster',
		    reference: 'windowCluster',

		    layout: {
		    	type: 'fit',
		    	pack: 'start',
		    	align: 'stretch',
		    },

			onEsc: function() {},
			closable: false,

		    modal: true,
			items : 
			[
		        {
		        	xtype: 'form',
		        	border: false,
                    layout: {
                    	type: 'vbox',
                    	anchor: '100%',
                    	align: 'stretch'
                    },
				    defaults: {
				    	xtype: "textfield",
				        align: 'stretch',
				        anchor: '100%',
				        labelWidth: 160,
				    },
					bodyPadding: '10 30 10 30',
					id: 'form_newcluster',

                    items: 
                    [
						{
							xtype: 'component',
							frame: false,
							border: 0,
							id: 'form_newcluster_title',
							html: Netra.helper.createFormTitle('New Pool','')
						},	                    
                    	{
							fieldLabel : "ID",
							id: 'text_newcluster_id',
							name: 'id',
							value: "",			
							hidden: true,
                    	},
                    	{
							fieldLabel : "Name",
							id: 'text_newcluster_name',
							name: 'name',
							value: "cluster",			
                    	},
                    	{
							fieldLabel : "Master IP",
							id: 'text_newcluster_master_ip',
							name: 'master_ip',
							value: "221.147.129.137",			
                    	},                    	
                    	{
							xtype : 'textareafield',
							fieldLabel : "Description",
							id: 'text_newcluster_description',
							name: 'description',
							value: "description",			
                            grow: true,
                            allowBlank : true,          
                            height: 140,							
                    	},
                    ]
                },
			],

			dockedItems: [
			    {
			        xtype: 'toolbar',
			        dock: 'bottom',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            { xtype: 'component', flex: 1 },
			            { text: 'Save', id: 'btn_newcluster_window_save' },
			            { text: 'Close', id: 'btn_newcluster_window_close' },
			            //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
			            //{ xtype: 'button', id: 'btn_newcluster_window_close', text: 'Close' }
			        ]
			    }
			],

		});

    },

    selectClusterRecord: function(record)
    {
    	if (! record) {
    		return;
    	}

    	this.clusterRecord = record;
    	this.updateForm(this.clusterRecord);

    },

    updateForm: function(record)
    {
		console.debug('NewCluster.updateForm : record = ', record);

		var form = this.getFormInput();

		form.clearForm();

		this.getFormTitle().update(Netra.helper.createFormTitle('Update Pool',''));

        form.query('.textfield, .textareafield').forEach(function(component) 
        {
            component.setValue(record.data[component.name]);
        });

    },

    show: function(record)
    {
		this.clusterWindow.show();
		this.selectClusterRecord(record);
    },

    close: function()
    {
    	console.debug("NewCluster.close : clusterWindow = " , this.clusterWindow);

    	this.clusterWindow.close();
    	Netra.manager.removeController(this);
    },

	sendRequest: function(json)
	{
		console.debug("NewCluster.sendRequest");
		
		Ext.Ajax.request({
		    url: '/api/openstack/flavors/new',
		    method: 'POST',
		    jsonData: json,
		    success: function(response, opts) 
		    {
		        console.debug('sendParameters Response = ', response);
		        var json = JSON.parse(response.responseText);
		        if (json.retcode == 0) {
		        	Ext.MessageBox.alert('Success', 'Your Template is created!!!');
		        } else {
		        	Ext.MessageBox.alert('Error', json.msg);
		        }
		        	
		    },
		    failure: function(response, opts) 
		    {
		        console.debug('sendParameters : woops');
		    }
		});

	},

	getParameter: function()
	{
		console.debug('NewCluster.getParameter');

		var json = {};

		var form = this.getFormInput();

        form.query('.textfield').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
	},

    saveToServer: function()
    {
		var form = this.getFormInput();

		console.debug("clusterWindow.saveToServer : form = ", form);
		
		if (! this.clusterRecord) {
			Netra.service.addNewCluster(form.form.getFieldValues(),this.onNewClusterRequestComplete);
		} else {
			Netra.service.updateNewCluster(form.form.getFieldValues(),this.onNewClusterRequestComplete);
		}

    },






    onStart: function()
    {
		console.debug("clusterWindow.onStart");
    },


	onButtonCloseClick: function()
	{
		console.debug("clusterWindow.onButtonCloseClick : controller = ", this);

		this.close();
	},

	onButtonSaveClick: function()
	{
		console.debug("clusterWindow.onButtonSaveClick");
		this.saveToServer();
	},


	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

	onNewClusterRequestComplete: function(retcode, response)
	{
		console.debug("onNewClusterRequestComplete : ret = ", response);

		if (retcode == Netra.const.REQ_OK) {
			Ext.MessageBox.alert('Success', response.msg);
        } else if (retcode == Netra.const.REQ_FAIL) {
        	Ext.MessageBox.alert('Fail', response);
        } else {
        	Ext.MessageBox.alert('Fail', response.msg);
        }
	},

});