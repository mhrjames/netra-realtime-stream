Ext.define("Netra.view.component.window.NewHost", {
	extend: 'Ext.app.Controller',
	alias: 'controller.newhost',
	id: 'newhost',
	reference: 'newhost',

    requires: [ 
        'Ext.toolbar.*',
    ],

    refs: [
    	{ ref: 'formInput', selector: '#form_newhost' },
    	{ ref: 'formTitle', selector: '#form_newhost_title' },
    ],

    models: [ ],    
    stores: [ ],    

	newHostWindow: null,
	hostRecord: null,


    init: function() 
    {
    	console.debug("window.init");

		//this.callParent(arguments);

        this.control({
            '#btn_newhost_window_close': { click: this.onButtonCloseClick },
            '#btn_newhost_window_save': { click: this.onButtonSaveClick },
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 


    	this.newHostWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Host Dialog',
		    height: 420,
		    width: 600,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

		    id: 'window_newhost',
		    reference: 'windowNewHost',

		    layout: {
		    	type: 'fit',
		    	pack: 'start',
		    	align: 'stretch',
		    },

		    modal: true,
			onEsc: function() {},
			closable: false,
		    
			items : 
			[
		        {
		        	xtype: 'form',
		        	border: false,
                    layout: {
                    	type: 'vbox',
                    	anchor: '100%',
                    	align: 'stretch'
                    },
				    defaults: {
				    	xtype: "textfield",
				        align: 'stretch',
				        anchor: '100%',
				        labelWidth: 160,
				    },
					bodyPadding: '10 30 10 30',
					id: 'form_newhost',

                    items: [
						{
							xtype: 'component',
							id: 'form_newhost_title',
							frame: false,
							border: 0,
							html: Netra.helper.createFormTitle('New Host','')
						},	                    
                    	{
							fieldLabel : "ID",
							id: 'text_newhost_id',
							name: 'id',
							value: "",			
							hidden: true,
                    	},
                        {
                            xtype:'combo',
                            fieldLabel: 'Cluster',
                            name:'cluster',
                            valueField: 'name',
                            queryMode:'local',
                            store: {
                                type: 'clusters',
                                autoLoad: true,
                            },
                            displayField:'name',
                            editable: false,
                            autoSelect: true,
                            forceSelection: true,
                        },                    	
                    	{
							fieldLabel : "IP",
							id: 'text_newhost_ip',
							name: 'ip',
							emptyText: '192.168.10.67'
                    	},
                    	{
							fieldLabel : "Name",
							id: 'text_newhost_name',
							name: 'name',
							value: 'Test Server'
                    	},                    	
                    	{
							fieldLabel : "User ID",
							id: 'text_newhost_userid',
							name: 'userid',
							value: "root",			
                    	},
                    	{
							fieldLabel : "Password",
							id: 'text_newhost_password',
							name: 'password',
							value: "vagrant",	
							inputType:'password',
                    	},
                    	{
							fieldLabel : "SSH Port",
							id: 'text_ssh_port',
							name: 'ssh_port',
							value: "22",			
                    	},
                    	{
                            xtype : 'textareafield',                    		
							fieldLabel : "Description",
							id: 'text_newhost_description',
							name: 'description',
							value: 'description',
                            grow: true,
                            allowBlank : true,          
                            height: 60,
                    	},
                    ]
                },
			],

			dockedItems: [
			    {
			        xtype: 'toolbar',
			        dock: 'bottom',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            { xtype: 'component', flex: 1 },
			            { text: 'Save', id: 'btn_newhost_window_save' },
			            { text: 'Close', id: 'btn_newhost_window_close' },
			            //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
			            //{ xtype: 'button', id: 'btn_newhost_window_close', text: 'Close' }
			        ]
			    }
			],

		});

    },

    selectHostRecord: function(record)
    {
    	if (! record) {
    		return;
    	}

    	this.hostRecord = record;
    	this.updateForm(this.hostRecord);

    },

    updateForm: function(record)
    {
		console.debug('NewHost.updateForm : record = ', record);

		var form = this.getFormInput();
		form.clearForm();

		this.getFormTitle().update(Netra.helper.createFormTitle('Update Host',''));

        form.query('.textfield, .textareafield').forEach(function(component) 
        {
            component.setValue(record.data[component.name]);
            //json[component.name] = component.value;
        });

    },

    show: function(record)
    {
		this.newHostWindow.show();
		this.selectHostRecord(record);
    },

    close: function()
    {
    	console.debug("NewHost.close : newHostWindow = " , this.newHostWindow);

    	this.newHostWindow.close();
    	Netra.manager.removeController(this);
    },

	sendRequest: function(json)
	{
		console.debug("NewHost.sendRequest");
		
		Ext.Ajax.request({
		    url: '/api/openstack/flavors/new',
		    method: 'POST',
		    jsonData: json,
		    success: function(response, opts) 
		    {
		        console.debug('sendParameters Response = ', response);
		        var json = JSON.parse(response.responseText);
		        if (json.retcode == 0) {
		        	Ext.MessageBox.alert('Success', 'Your Template is created!!!');
		        } else {
		        	Ext.MessageBox.alert('Error', json.msg);
		        }
		        	
		    },
		    failure: function(response, opts) 
		    {
		        console.debug('sendParameters : woops');
		    }
		});

	},

	getParameter: function()
	{
		console.debug('NewHost.getParameter');

		var json = {};

		var form = this.getFormInput();

        form.query('.textfield').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
	},

    saveToServer: function()
    {
    	console.debug('NewHost.getParameter');

		var form = this.getFormInput();

		console.debug("newHostWindow.saveToServer : form = ", form);		
		console.debug("newHostWindow.saveToServer : rec = ", form.form.getFieldValues());

		if (! this.hostRecord) {
			Netra.service.addNewHost(form.form.getFieldValues(),this.onNewHostRequestComplete);
		} else {
			Netra.service.updateNewHost(form.form.getFieldValues(),this.onNewHostRequestComplete);
		}
    },






    onStart: function()
    {
		console.debug("newHostWindow.onStart");
    },


	onButtonCloseClick: function()
	{
		console.debug("newHostWindow.onButtonCloseClick : controller = ", this);

		this.close();
	},

	onButtonSaveClick: function()
	{
		console.debug("newHostWindow.onButtonSaveClick");
		this.saveToServer();
	},


	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

	onNewHostRequestComplete: function(retcode, response)
	{
		console.debug("onNewHostRequestComplete : ret = ", response);

		if (response.ret_code == Netra.const.REQ_OK) {
			Ext.MessageBox.alert('Success', response.msg);
        } else if (retcode == Netra.const.REQ_FAIL) {
        	Ext.MessageBox.alert('Fail', response);
        } else {
        	Ext.MessageBox.alert('Fail', response.msg);
        }
	},
	
});