Ext.define("Netra.view.component.window.NewImage", {
    extend: 'Ext.app.Controller',
    alias: 'controller.newimage',
    id: 'newimage',
    reference: 'newimage',

    models: [ ],    
    stores: [ ],    
    requires: [ 
        'Ext.layout.container.Accordion',
    ],

    refs: [
        { ref: 'formNewImage', selector: '#form_newimage' },
        { ref: 'panelTree', selector: '#tree_menu' },
        { ref: 'textName',selector: '#text_name' },
        { ref: 'textFile',selector: '#text_image' },
        { ref: 'textVlanRange',selector: '#text_network_vlan_ranges_multi' },
        { ref: 'areaDescription',selector: '#area_description' },        
        { ref: 'areaCompute',selector: '#area_compute_multi' },        
        { ref: 'comboHost',selector: '#combo_public_ip' },        
        { ref: 'comboHostMulti',selector: '#combo_public_ip_multi' },
        { ref: 'comboComputeMulti',selector: '#combo_compute_ip_multi' },
    ],

    imageRecord: null,
    imageWindow: null,

    init: function() 
    {
    	console.debug("NewImageWindow.init");

        this.control({
            '#btn_newimage_close': { click: this.onButtonCloseClick },
            '#btn_newimage_upload': { click: this.onButtonUploadClick },
            '#combo_public_ip': { change: this.onHostChanged },
        });

        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

    	this.imageWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Add New Image',
		    height: 400,
		    width: 600,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

            frame: false,
		    layout: {
		    	type: 'fit',
		    },
            defaults: {
                bodyPadding: '15 30 30 30',
                autoScroll: false,
            },

		    modal: true,
            onEsc: function() {},
            closable: false,
            
			items : 
            [
		        {
		        	xtype: 'form',
		        	id: 'form_newimage',
		        	border: false,
		        	//title: 'Input Forms',
		        	//titleAlign: 'center',
				    defaults: {
                        xtype: "textfield",
                        align: 'stretch',
                        anchor: '100%',
                        labelWidth: 140,
				    },

				    items: 
                    [
                        {
                            xtype: 'component',
                            frame: false,
                            border: 0,
                            html: Netra.helper.createFormTitle('New Image','')
                        },  
                        {
                            fieldLabel : "Name",
                            id: 'text_name',
                            name: 'image_name',
                            value: "Netra",         
                            vtype: 'alphanum'
                        },
                        {
                            xtype : 'textareafield',
                            fieldLabel : 'Description',
                            id: 'area_description_multi',
                            name: 'image_description',
                            grow: true,
                            allowBlank : true,          
                            height: 60,
                            value: "Please Description",            
                        },
                        {
                            fieldLabel : "Minimum Disk",
                            name : 'image_min_disk',
                            id: 'text_min_disk',
                            value: "0",
                            vtype: 'alphanum',
                            maskRe: /[0-9.]/,
                        },                                  
                        {
                            fieldLabel : "Minimum RAM",
                            id: 'text_min_ram',
                            name: 'image_min_ram',                   
                            value: "0",
                            vtype: 'alphanum',
                            maskRe: /[0-9.]/,
                        },      
/*                        
                        {
                            xtype: 'radiogroup',
                            fieldLabel : "Public",
                            name : 'image_is_public',
                            id: 'radio_is_public',
                            margin: '0 0 0 0',
                            items: [
                                { boxLabel: 'Yes', name: 'is_public', inputValue: 1 , checked: true, },
                                { boxLabel: 'No', name: 'is_public', inputValue: 2 },
                            ]
                        },
*/                        
                        {
                            xtype:'combo',
                            fieldLabel:'Disk Format',
                            name:'image_disk_format',
                            valueField: 'image_disk_format',
                            queryMode:'local',
                            store:
                            [   
                                'AKI - Amazon Kernel Image',
                                'AMI - Amazon Machine Image',
                                'ARI - Amazon Ramdisk Image', 
                                'ISO - Optical Disk Image', 
                                'QCOW2 - QEMU Emulator Image', 
                                'RAW - Raw Disk Image'
                            ],
                            displayField:'image_disk_format',
                            //readOnly: true,
                            autoSelect:true,
                            forceSelection:true,
                        },
                        {
                            xtype: 'filefield',
                            fieldLabel : "Image File",
                            id: 'text_image',
                            name: 'image_file',
                            value: "172.16.0.102",
                            buttonText: '...',
                            buttonConfig: {
                                iconCls: 'upload-icon'
                            }
                        },      

				    ],
		        },

			],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_newimage',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        { xtype: 'button', id: 'btn_newimage_upload', text: 'Upload' },
                        //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
                        { xtype: 'button', id: 'btn_newimage_close', text: 'Close' }
                    ]
                }
            ],


		});

		this.onStart();
    },

    show: function()
    {
		console.debug("window.popupWindow");

		this.imageWindow.show();		
	},

    close: function()
    {
        console.debug("NewImage.close : imageWindow = " , this.imageWindow);

        //this.getTextFile().destroy();

        this.imageWindow.close();
        Netra.manager.removeController(this);
    },

    loadHosts: function(params) 
    {
        console.log("NewOpenstack.loadHosts");

        var store = this.getStore("Hosts");

        store.load({
            callback: this.onHostLoaded,
            params: {},
            scope: this
        });

    },

    loadTemplateFromServer: function()
    {
        console.log("loadTemplateFromServer");

        var store = this.getStore("Templates");

        store.load({
            callback: this.onTemplateLoad,
            params: {},
            scope: this
        });
    },

    validateInput: function()
    {
        console.debug("validateInput");

        if (this.templateRecord == null) {
            Ext.MessageBox.alert('Error!!!', "Please select a template first!!!");
            return false;
        }
        
        if (this.getComboHost().getValue() == null) {
            Ext.MessageBox.alert('Error!!!', "Please select an openstack server");
            return false;
        }

        return true;
    },


    sendUploadRequest: function(json)
    {
        console.log("NewImageWindow.sendUploadRequest");

        var form = this.getFormNewImage();
        if (form.isValid())        
        {
            form.submit( {
                url: '/api/openstack/images/upload',
                waitMsg: 'Uploading Image File',
                success: function(response, operation) 
                {
                    console.debug("response = ", operation.result);

                    if (operation.result.retcode == 0) {
                        Ext.MessageBox.alert('Success', 'File "' + operation.result.data.file + '" has been uploaded successfully.');
                    } else {
                        Ext.MessageBox.alert('Error', 'Fail to upload ' + operation.result.file);
                    } 
                    
                },
                failure: function(response, operation)  {
                    console.debug("response = ", operation.result);
                    if (operation.result.retcode == 0) {
                        Ext.MessageBox.alert('Success', 'File "' + operation.result.data.file + '" has been uploaded successfully.');
                    } else {
                        Ext.MessageBox.alert('Error', 'Fail to upload ' + operation.result.file);
                    } 
                }
            } );
        }
/*        
        Ext.Ajax.request({
            url: '/api/openstack/images/upload',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('NewImageWindow.sendUploadRequest : success');
                Ext.MessageBox.alert('Success', 'Your stack is being deployed!!!');
            },
            failure: function(response, opts) 
            {
                console.debug('NewImageWindow.sendUploadRequest : woops');
            }
        });
*/

    },

    getParameter: function()
    {
        console.debug('NewImage.getParameter');

        var json = {};

        var form = this.getFormNewImage();

        form.query('.textfield, .radiogroup').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
    },

    saveToServer: function()
    {
        var form = this.getFormNewImage();

        console.debug("imageWindow.saveToServer : form = ", form);
        
        var json = this.getParameter();
        this.sendUploadRequest(json);
    },





    onStart: function()
    {
    	console.debug("Window.Openstack.onStart");
    	//this.loadTemplateFromServer();
    	//this.loadHosts();
    },

	onButtonCloseClick: function()
	{
		console.debug("NewImage.onButtonCloseClick");
		this.close();
	},

	onButtonUploadClick: function()
	{
		console.debug("NewImage.onButtonUploadClick");

        this.saveToServer();

/*
        var form = this.getFormNewImage();
        if (form.isValid())
        {
            form.submit(
            {
                url: 'file-upload.php',
                waitMsg: 'Uploading your photo...',
                success: function(fp, o) 
                {
                    msg('Success', 'Processed file "' + o.result.file + '" on the server');
                }
            });
        }
*/        
	},

	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

    onTemplateLoad: function(records, operation, success) 
    {
    	var _this = this;

        console.log("onTemplateLoad : length = " + records.length);
        //console.log(operation);
        
        //this.selectTemplate();
        //this.updateParameterForm();

        this.getStore("Templates").each(function(record,idx) 
        {
            _this.addTemplateNodeToTree(record.get("name"), record);            
        });

        this.getTreeMenu().getView().refresh();
        this.getTreeMenu().getRootNode().expand();
    },

    onHostLoaded: function()
    {
        var _this = this;

        console.debug("onHostLoaded");
        this.getComboHost().bindStore(this.getHostsStore());
        this.getComboHostMulti().bindStore(this.getHostsStore());
        this.getComboComputeMulti().bindStore(this.getHostsStore());
    },

    onHostChanged: function(combo, newValue, oldValue, eOpts )
    {

        var index = this.getHostsStore().find('ip_addr',newValue);
        
        console.debug("onHostChanged : index = " + index);
        console.debug(combo);

        if (index>-1) 
        {
            this.hostRecord = this.getHostsStore().getAt(index);
            console.debug(this.hostRecord);
        }

    },



});