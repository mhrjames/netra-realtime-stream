Ext.define("Netra.view.component.window.NewInstance", {
    extend: 'Ext.app.Controller',
    alias: 'controller.newinstance',
    id: 'newinstance',
    reference: 'newinstance',

    requires: [ 
        'Ext.layout.container.Accordion',
    ],

    refs: [
        { ref: 'treeMenu', selector: '#tree_openstack' },
        { ref: 'panelGrid', selector: 'panel_grid' },
        { ref: 'panelList', selector: '#panel_list' },
        { ref: 'panelContents', selector: '#panel_container' },
        { ref: 'panelTree', selector: '#tree_menu' },
        { ref: 'formInstance',selector: '#form_newinstance' },
        { ref: 'areaCompute',selector: '#area_compute_multi' },        
        { ref: 'comboFlavor',selector: '#combo_instance_template' },        
        { ref: 'comboImage',selector: '#combo_instance_image' },
        //{ ref: 'comboComputeMulti',selector: '#combo_compute_ip_multi' },
        //{ ref: 'radioNetworkModelMulti',selector: '#radio_tenant_network_model_multi' },
    ],

    instanceWindow: null,

    init: function() 
    {
    	console.debug("NewInstanceWindow.init");

        this.control({
            '#btn_new_close': { click: this.onCloseButtonClick },
            '#btn_new_create': { click: this.onButtonCreateClick },
            //'#combo_public_ip': { change: this.onHostChanged },
            //'#combo_public_ip_multi': { change: this.onHostChanged },
            //'#combo_compute_ip_multi': { change: this.onComputeChanged },
            //'#tree_platform': {
            //	itemclick: this.onNodeClick,
            //    render: this.onTreeMenuRendered
            //},
            //'#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

    	this.instanceWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Create a New Virtual Instance',
		    height: 400,
		    width: 600,
            
            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

		    layout: {
		    	type: 'fit',
		    	pack: 'start',
		    	align: 'stretch',
		    },

		    modal: true,
            onEsc: function() {},
            closable: false,
            
			items : 
            [
		        {
		        	xtype: 'panel',
		        	id: 'panel_container',
		        	border: false,
		        	//title: 'Input Forms',
		        	//titleAlign: 'center',
				    layout: {
				        // layout-specific configs go here
				        type: 'fit',
				        animate: true,        
				        //titleCollapse: false,
				        //activeOnTop: true
				    },
				    defaults: {
				        titleAlign: 'left',
				        autoScroll: false,
				        headerPosition: 'left'        
				    },

				    items: 
                    [
                        {
                            xtype: 'form',
                            id: 'form_newinstance',
                            border: false,
                            bodyPadding: '30 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('New Virtual Machine','')
                                },  
                                {
                                    fieldLabel : "Name",
                                    id: 'text_instance_name',
                                    name: 'instance_name',
                                    value: "Netra",         
                                },
                                {
                                    fieldLabel : "Instance Count",
                                    name : 'instance_count',
                                    id: 'text_instance_count',
                                    value: "1",
                                },                                
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Template",
                                    name : 'instance_template',
                                    id: 'combo_instance_template',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a template to install openstack controller',
                                },
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Machine Image",
                                    name : 'instance_image',
                                    id: 'combo_instance_image',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a template to install openstack controller',
                                },
                            ]
                        }
                    ],

		        },

			],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_new_window',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        { xtype: 'button', id: 'btn_new_create', text: 'Create' },
                        //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
                        { xtype: 'button', id: 'btn_new_close', text: 'Close' }
                    ]
                }
            ],

		});

		this.onStart();
    },

    show: function()
    {
        console.debug("NewInstance.show");
        this.instanceWindow.show();     
    },


    loadFlavors: function(params) 
    {
        console.log("NewOpenstack.loadFlavors");

        var store = Netra.manager.getStore('storeFlavors');

        store.load({
            callback: this.onFlavorLoaded,
            params: {},
            scope: this
        });

    },

    loadImages: function()
    {
        console.log("loadImages");

        var store = Netra.manager.getStore('storeImages');

        store.load({
            callback: this.onImageLoad,
            params: {},
            scope: this
        });
    },

    addTemplateNodeToTree: function(text,record)
    {
        var rootNode = this.getTreeMenu().getRootNode();
        var node = this.addNodeToTree(this.getTreeMenu(),rootNode, text, 'openstack', record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    updateTemplateRole:function()
    {
        resource = this.getTemplateResourceItem(this.ALL_IN_ONE_ROLE);
        if (resource == null) {
            console.log("Error!!! : " + node.data.text + " is not found!!!");
        }
        
        resource.Properties.fqdn = [];
        resource.Properties.fqdn.push(this.hostRecord.raw.name);

        console.debug("updateTemplateRole::json");
        console.debug(this.templateRecord.json.Resources);
    },


    sendNewInstanceRequest: function(jsonStr)
    {
        console.debug("NewInstanceWindow.sendNewInstanceRequest");
        
        Ext.Ajax.request({
            url: '/api/openstack/instances/new',
            method: 'POST',
            jsonData: jsonStr,
            success: function(response, opts) 
            {
                console.debug('sendNewInstanceRequest : success');
                Ext.MessageBox.alert('Success', 'Your Instance is being deployed!!!');
            },
            failure: function(response, opts) {
                console.debug('sendNewInstanceRequest : woops');
            }
        });

    },

 
    updateForm: function(record)
    {
        console.debug("updateForm");

        //var index = this.getStore("Templates").find('name','havana');
        //this.templateRecord = this.getStore("Templates").getAt(index);
        //console.debug('templateRecord');

    },





    onStart: function()
    {
    	console.debug("Window.Openstack.onStart");
    	
        this.loadImages();
    	this.loadFlavors();
    },

	onCloseButtonClick: function()
	{
        console.debug("NewInstance.close : instanceWindow = " , this.instanceWindow);

        //this.instanceWindow.hide();
        this.instanceWindow.close();
        Netra.manager.removeController(this);
	},

	onButtonCreateClick: function()
	{
		console.debug("onButtonCreateClick");
        
        var form = this.getFormInstance();
        if (! form.isValid()) 
        {
            Ext.MessageBox.alert('Error','Please fill out the form!!!');
            return;
        }

        var json = Netra.helper.getFormParameterInJson(form,'.textfield, .combo');
        this.sendNewInstanceRequest(json);
        
	},

	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

    onImageLoad: function(records, operation, success) 
    {
    	var _this = this;

        console.log("onImageLoad : length = " + records.length);
        this.getComboImage().bindStore(Netra.manager.getStore('storeImages'));
    },

    onFlavorLoaded: function()
    {
        var _this = this;

        console.debug("onFlavorLoaded");
        this.getComboFlavor().bindStore(Netra.manager.getStore('storeFlavors'));
    },

    onHostChanged: function(combo, newValue, oldValue, eOpts )
    {

        var index = this.getHostsStore().find('ip_addr',newValue);
        
        console.debug("onHostChanged : index = " + index);
        console.debug(combo);

        if (index>-1) 
        {
            this.hostRecord = this.getHostsStore().getAt(index);
            console.debug(this.hostRecord);
        }

    },



 	onNodeClick: function(view, record, item, index, e) 
    {
		console.debug("onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        console.debug(record);
        
        this.providerRecord = record;
        
        this.getPanelContents().items.items[index].expand();
 	},

    onTenantNetworkChange : function(thisFormField, newValue, oldValue, eOpts) 
    {
        console.debug("onTenantNetworkChange");
        console.debug(thisFormField);
        console.debug(newValue.network_model_multi);

        this.getTextVlanRange().enable();    
        if (newValue.network_model_multi == 1) {
            this.getTextVlanRange().disable();    
        }
        
    },

 	onTreeMenuRendered: function() 
 	{
        console.debug("onTreeMenuRendered");
        //this.selectFirstNode();
 	},


});