Ext.define("Netra.view.component.window.NewInstanceExt", {
    extend: 'Ext.app.Controller',
    alias: 'controller.newinstance',
    id: 'newinstance',
    reference: 'newinstance',

    requires: [ 
        'Ext.layout.container.Accordion',
    ],

    refs: [
        { ref: 'treeMenu', selector: '#tree_openstack' },
        { ref: 'panelGrid', selector: 'panel_grid' },
        { ref: 'panelList', selector: '#panel_list' },
        { ref: 'panelContents', selector: '#panel_container' },
        { ref: 'panelTree', selector: '#tree_menu' },
        { ref: 'formInstance',selector: '#form_newinstance_template' },
        { ref: 'formVolume',selector: '#form_newinstance_disk' },
        { ref: 'areaCompute',selector: '#area_compute_multi' },        
        { ref: 'comboFlavor',selector: '#combo_instance_template' },        
        { ref: 'comboImage',selector: '#combo_instance_image' },
        { ref: 'comboSecurity',selector: '#combo_security_group' },
        { ref: 'textFlavorCpu',selector: '#text_flavor_cpu' },
        { ref: 'textFlavorMemory',selector: '#text_flavor_memory' },
        { ref: 'textFlavorDisk',selector: '#text_flavor_disk' },
        { ref: 'pbarCpu',selector: '#pbar_instance_usage_cpu' },
        { ref: 'pbarMemory',selector: '#pbar_instance_usage_ram' },
        { ref: 'pbarInstance',selector: '#pbar_instance_usage_instance' },
        { ref: 'pbarVolumeSize',selector: '#pbar_volume_usage_size' },
        { ref: 'pbarVolumeCount',selector: '#pbar_volume_usage_count' },
        { ref: 'pbarFloating',selector: '#pbar_floating_usage' },
        { ref: 'textVolumeType',selector: '#text_volume_type' },
        { ref: 'textVolumeSize',selector: '#text_volume_size' },
        { ref: 'textVolumeZone',selector: '#text_volume_zone' },
        { ref: 'textImageName',selector: '#text_image_name' },
        { ref: 'textImageFormat',selector: '#text_image_format' },
        { ref: 'textImageSize',selector: '#text_image_size' },
        { ref: 'textImageMinSize',selector: '#text_image_min_size' },
        { ref: 'textImageMinRam',selector: '#text_image_min_ram' },
        { ref: 'textImageCreated',selector: '#text_image_created' },
        { ref: 'checkImageProtected',selector: '#check_image_protected' },
    ],


    instanceWindow: null,


    init: function() 
    {
    	console.debug("NewInstanceWindow.init");

        this.control({
            '#btn_new_close': { click: this.onButtonCloseClick },
            //'#btn_new_create': { click: this.onButtonCreateClick },
            '#btn_new_save': { click: this.onButtonSaveClick },
            '#combo_instance_template': { change: this.onFlavorChanged },
            '#combo_instance_image': { change: this.onImageChanged },
            '#combo_instance_volume': { change: this.onVolumeChanged },
            '#combo_security_group': { change: this.onSecurityChanged },
            '#tree_new_instance': {
            	itemclick: this.onNodeClick,
            //    render: this.onTreeMenuRendered
            },
            //'#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

    	this.instanceWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Create a New Virtual Instance',
		    height: 600,
		    width: 800,
            
            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

		    layout: {
		    	type: 'border',
		    	//pack: 'start',
		    	//align: 'stretch',
		    },

		    modal: true,
            
            onEsc: function() {},
            closable: false,
            
			items : 
            [
                {
                    xtype: 'treepanel',
                    id: 'tree_new_instance',
                    region: 'west',
                    title: 'Items',
                    width: 180,
                    rootVisible: false,
                    useArrows: true,
                    split: true,
                    root: {
                        text: 'Root',
                        children: 
                        [
                            //{ text: 'Task', leaf: true },                        
                            { text: 'Template', leaf: true } ,
                            { text: 'Image', leaf: true } ,
                            { text: 'Disk', leaf: true },
                            { text: 'Network', leaf: true },
                        ]
                    }
                },

		        {
		        	xtype: 'panel',
		        	id: 'panel_container',
                    region: 'center',
		        	border: false,
		        	//title: 'Input Forms',
		        	//titleAlign: 'center',
				    layout: {
				        // layout-specific configs go here
				        type: 'fit',
				        animate: true,        
				        //titleCollapse: false,
				        //activeOnTop: true
				    },
				    defaults: {
				        titleAlign: 'left',
				        autoScroll: false,
				        headerPosition: 'left'        
				    },

				    items: 
                    [
/*                    
                        {
                            xtype: 'form',
                            id: 'form_newinstance_task',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Task Information','')
                                },    
                                {
                                    fieldLabel : "Task Name",
                                    id: 'text_task_name',
                                    name: 'task_name',
                                    //readOnly: true,
                                    value: "",         
                                },                                
                                {
                                    xtype : 'textareafield',
                                    fieldLabel : 'Description',
                                    id: 'area_task_description',
                                    name: 'area_task_description',
                                    grow: true,
                                    allowBlank : true,          
                                    height: 260,
                                    value: "",
                                }
                            ]
                        },
*/
                        {
                            xtype: 'form',
                            id: 'form_newinstance_template',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('New Virtual Machine','')
                                },  
                                {
                                    fieldLabel : "Instance Name",
                                    id: 'text_instance_name',
                                    name: 'instance_name',
                                    value: "Netra",   
                                    allowBlank: false,      
                                    invalidText: 'Please give an instance name!!!',
                                },
                                {
                                    xtype: 'numberfield',                                    
                                    fieldLabel : "Instance Count",
                                    name : 'instance_count',
                                    id: 'text_instance_count',
                                    value: 1,
                                    minValue: 1,
                                    maxValue: 99,
                                    allowBlank: false,
                                    invalidText: 'Please type intance count',
                                },                                
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Template",
                                    name : 'instance_template',
                                    id: 'combo_instance_template',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a template to install openstack controller',
                                    invalidText: 'Please select a template to install openstack controller',
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Template Details','')
                                },
                                {
                                    fieldLabel : "CPU",
                                    id: 'text_flavor_cpu',
                                    name: 'instance_cpu',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Memory (MB)",
                                    id: 'text_flavor_memory',
                                    name: 'instance_memory',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Root Disk (GB)",
                                    id: 'text_flavor_disk',
                                    name: 'instance_disk',
                                    readOnly: true,
                                    value: "",         
                                },
/*                                
                                {
                                    fieldLabel : "Ephemeral Disk",
                                    id: 'text_instance_ephemeral',
                                    name: 'instance_ephemeral',
                                    readOnly: true,
                                    value: "",         
                                },
*/                                
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Current Usage','')
                                },
                                {
                                    xtype: 'progressbar',
                                    id: 'pbar_instance_usage_instance',
                                    name: 'pbar_instance_usage_cpu',
                                    text: 'Total Used Instance',
                                    margin: '10 0 0 0',
                                    //formBind: true,
                                },                                
                                {
                                    xtype: 'progressbar',
                                    id: 'pbar_instance_usage_cpu',
                                    name: 'pbar_instance_usage_cpu',
                                    text: 'Total Used CPUs',
                                    margin: '10 0 0 0',
                                    //formBind: true,
                                },
                                {
                                    xtype: 'progressbar',
                                    id: 'pbar_instance_usage_ram',
                                    name: 'pbar_instance_usage_cpu',
                                    text: 'Total Used Memory',
                                    margin: '10 0 0 0',
                                    //formBind: true,
                                },
                            ]
                        },
                        {
                            xtype: 'form',
                            id: 'form_newinstance_image',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Virtual Machine Image','')
                                },    
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Machine Image",
                                    name : 'instance_image',
                                    id: 'combo_instance_image',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a template to install openstack controller',
                                    invalidText: 'Please select a template to install openstack controller',
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Image Details','')
                                },
                                {
                                    fieldLabel : "Image Name",
                                    id: 'text_image_name',
                                    name: 'image_name',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Disk Format",
                                    id: 'text_image_format',
                                    name: 'image_format',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Image File Size",
                                    id: 'text_image_size',
                                    name: 'image_size',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    xtype: 'checkbox',
                                    fieldLabel : "Protected Image",
                                    id: 'check_image_protected',
                                    name: 'image_protected',
                                    readOnly: true,
                                    value: "",         
                                },                                
                                {
                                    fieldLabel : "Minimum Disk Size",
                                    id: 'text_image_min_size',
                                    name: 'image_min_size',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Minimum Memory Size",
                                    id: 'text_image_min_ram',
                                    name: 'image_min_ram',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Created At",
                                    id: 'text_image_created',
                                    name: 'image_created',
                                    readOnly: true,
                                    value: "",         
                                },

                            ]
                        },
                        {
                            xtype: 'form',
                            id: 'form_newinstance_disk',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('New Volume','')
                                },    
/*                                
                                {
                                    fieldLabel : "Disk Name",
                                    id: 'text_volume_name',
                                    name: 'volume_name',
                                    value: "",         
                                },
*/                                
                                {
                                    xtype:'combo',
                                    fieldLabel:'Select a Volume',
                                    id:'combo_instance_volume',
                                    name:'volume_type',
                                    valueField: 'id',
                                    displayField:'display_name',
                                    queryMode:'local',
                                    store: {
                                        type: 'volumes',
                                        autoLoad: true,
                                    },
                                    allowBlank: false,
                                    editable: false,
                                    autoSelect:true,
                                    emptyText: 'Please select a volume to attach'
                                },                                                      
                                {
                                    fieldLabel : "Volume Path",
                                    id: 'text_volume_path',
                                    name: 'volume_path',
                                    value: "",        
                                    emptyText: '/dev/sdb',
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Volume Details','')
                                },
                                {
                                    fieldLabel : "Disk Size(GB)",
                                    id: 'text_volume_size',
                                    name: 'volume_size',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Availability Zone",
                                    id: 'text_volume_zone',
                                    name: 'volume_zone',
                                    readOnly: true,
                                    value: "",         
                                },
                                {
                                    fieldLabel : "Volume Type",
                                    id: 'text_volume_type',
                                    name: 'volume_type',
                                    readOnly: true,
                                    value: "",         
                                },

                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Current Volume Usage','')
                                },
                                {
                                    xtype: 'progressbar',
                                    id: 'pbar_volume_usage_size',
                                    name: 'pbar_instance_usage_cpu',
                                    text: 'Total Used Instance',
                                    margin: '10 0 0 0',
                                },                                
                                {
                                    xtype: 'progressbar',
                                    id: 'pbar_volume_usage_count',
                                    name: 'pbar_instance_usage_cpu',
                                    text: 'Total Used CPUs',
                                    margin: '10 0 0 0',
                                    //formBind: true,
                                },
                            ]
                        },                       

                        {
                            xtype: 'form',
                            id: 'form_newinstance_network',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('New Network','')
                                },  
/*
                                {
                                    xtype: 'checkbox',
                                    fieldLabel : "Assign Floating IP",
                                    id: 'check_floating_assign',
                                    name: 'floating_size',
                                    value: "",         
                                },
*/                                
                                {
                                    fieldLabel : "Fixed IP",
                                    id: 'text_fixed_ip',
                                    name: 'fixed_ip',
                                    value: "",         
                                },
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Security Group",
                                    name : 'instance_security',
                                    id: 'combo_security_group',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    store: {
                                        type: 'securitys',
                                    },
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'name',
                                    emptyText: 'Please select a security group',
                                    invalidText: 'Please select a security group',
                                },

                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Current Floating IP Usage','')
                                },
                                {
                                    fieldLabel : "CIDR",
                                    id: 'text_network_cidr',
                                    name: 'network_cidr',
                                    value: Netra.helper.getCurrentNetworkData('cidr'),  
                                    readOnly: true,  
                                },
                                {
                                    fieldLabel : "DHCP Start",
                                    id: 'text_network_dhcp_start',
                                    name: 'network_dhcp_start',
                                    value: Netra.helper.getCurrentNetworkData('dhcp_start'),  
                                    readOnly: true,
                                },
                                {
                                    xtype: 'progressbar',
                                    id: 'pbar_floating_usage',
                                    name: 'pbar_instance_usage_cpu',
                                    text: 'Total Used Instance',
                                    margin: '10 0 0 0',
                                },                                
                            ]
                        },

                    ],

		        },

			],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_new_window',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        //{ xtype: 'button', id: 'btn_new_create', text: 'Create' },
                        { xtype: 'button', id: 'btn_new_save', text: 'Save' },
                        { xtype: 'button', id: 'btn_new_close', text: 'Close' }
                    ]
                }
            ],

		});

		this.onStart();
    },

    show: function()
    {
        console.debug("NewInstance.show");
        this.instanceWindow.show();     
    },


    loadFlavors: function(params) 
    {
        console.log("NewOpenstack.loadFlavors");

        var store = Netra.manager.getStore('storeFlavors');

        store.load({
            callback: this.onFlavorLoaded,
            params: {},
            scope: this
        });

    },

    loadImages: function()
    {
        console.log("loadImages");

        var store = Netra.manager.getStore('storeImages');

        store.load({
            callback: this.onImageLoad,
            params: {},
            scope: this
        });
    },

    addTemplateNodeToTree: function(text,record)
    {
        var rootNode = this.getTreeMenu().getRootNode();
        var node = this.addNodeToTree(this.getTreeMenu(),rootNode, text, 'openstack', record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    updateTemplateRole:function()
    {
        resource = this.getTemplateResourceItem(this.ALL_IN_ONE_ROLE);
        if (resource == null) {
            console.log("Error!!! : " + node.data.text + " is not found!!!");
        }
        
        resource.Properties.fqdn = [];
        resource.Properties.fqdn.push(this.hostRecord.raw.name);

        console.debug("updateTemplateRole::json");
        console.debug(this.templateRecord.json.Resources);
    },



    updateProgressBar: function(pbar,max,used,item)
    {
        pbar.updateProgress(used/max, 'Total ' + item + ' ' + used + '  of ' + max + ' Used');
    },

    updateUsageForm: function(record)
    {
        var _this = this;

        console.debug("NewInstanceExt.updateUsageForm");

        var store = Netra.manager.getStore('storeLimits');
        
        store.removeAll();
        store.load( { 
            scope: this,
            callback: function(records, operation, success) 
            {
                if (! success) return;

                var record = records[0];

                _this.updateProgressBar(_this.getPbarInstance(),record.data.maxTotalInstances,record.data.totalInstancesUsed,'Instance');
                _this.updateProgressBar(_this.getPbarCpu(),record.data.maxTotalCores,record.data.totalCoresUsed,'CPU');
                _this.updateProgressBar(_this.getPbarMemory(),record.data.maxTotalRAMSize,record.data.totalRAMUsed,'Memory');

                //to floating ips
                _this.updateProgressBar(_this.getPbarFloating(),record.data.maxTotalFloatingIps,record.data.totalFloatingIpsUsed,'Floating IP');

            }
        } );

        //this.templateRecord = this.getStore("Templates").getAt(index);
        //console.debug('templateRecord');

    },

    setActiveForm: function(form_index)
    {
        var panel = this.getPanelContents();
        console.debug("NewInstanceExt.setActiveForm : form id = " + form_index + " , panel = ", panel);
        
        for (var i=0; panel.items.length > i; i++) 
        {
            panel.items.items[i].hide();
        }

        panel.items.items[form_index].show();
        //panel.getLayout().setActiveItem(form_index);
    },

    loadVolumeLimit: function()
    {
        var _this = this;

        console.debug("NewInstanceExt.loadVolumeLimit");

        Netra.task.getVolumeLimit(function(json) 
        {
            console.debug('loadVolumeLimit.Success : json = ' , json);
            
            _this.updateVolumeLimit(json);

        }, function(json) {
            console.debug('loadVolumeLimit.Failure : json = ' , json);
            //Ext.MessageBox.alert('Error', msg);
        });

    },

    updateVolumeLimit: function(json)
    {
        console.debug("NewInstanceExt.updateVolumeLimit : json = " , json[0]);

        this.updateProgressBar(this.getPbarVolumeSize(),json[0].maxTotalVolumeGigabytes,json[0].maxTotalVolumes,'Size');
        this.updateProgressBar(this.getPbarVolumeCount(),json[0].maxTotalVolumeGigabytes,json[0].maxTotalVolumes,'Count');
    },

    updateFloatingForm: function()
    {
        console.debug("NewInstanceExt.updateFloatingForm");
        
        var store = Netra.manager.getStore('storeLimits');

        //this.updateProgressBar(this.getPbarFloating(),record,,'Floating IP');
    },

    validateForm: function()
    {
        console.debug("NewInstanceExt.validateForm");

        var form = Netra.helper.getComponent('#form_newinstance_template');
        if (! form.isValid()) {
            this.setActiveForm(0);
            return false;
        }

        form = Netra.helper.getComponent('#form_newinstance_image');
        if (! form.isValid()) {
            this.setActiveForm(1);
            return false;
        }

        form = Netra.helper.getComponent('#form_newinstance_disk');
        if (Netra.helper.getComponent('#combo_instance_volume').getValue()) {
            if (! Netra.helper.getComponent('#text_volume_path').getValue()) {
                this.setActiveForm(2);
                return false;
            }
        }

        form = Netra.helper.getComponent('#form_newinstance_network');
        if (! form.isValid()) {
            this.setActiveForm(3);
            return false;
        }

        return true;
    },

    getNewInstanceParameterInJson: function()
    {
        console.debug("getNewInstanceParameterInJson ");

        var json = {};
        json['name'] = Netra.helper.getComponent('#text_instance_name').getValue();
        json['count'] = Netra.helper.getComponent('#text_instance_count').getValue();
        json['flavor_id'] = Netra.helper.getComponent('#combo_instance_template').getValue();
        json['image_id'] = Netra.helper.getComponent('#combo_instance_image').getValue();

        //json['assign_floating'] = Netra.helper.getComponent('#check_floating_assign').getValue();
        
        json['options'] = {} 

        json['options']['security_groups'] = [];
        json['options']['security_groups'].push(Netra.helper.getComponent('#combo_security_group').getValue());

        if (Netra.helper.getComponent('#text_fixed_ip').getValue().length>0)
        {
            var network_json = {}
            network_json['net_id'] = Netra.helper.getCurrentNetworkData('id');
            network_json['v4_fixed_ip'] = Netra.helper.getComponent('#text_fixed_ip').getValue();
            network_json['port_id'] = null;            

            json['options']['nics'] = []
            json['options']['nics'].push(network_json);            
        }

        if (Netra.helper.getComponent('#combo_instance_volume').getValue()) {
            json['options']['block_device_mapping'] = {}
            json['options']['block_device_mapping']['volume_id'] = Netra.helper.getComponent('#combo_instance_volume').getValue();
            json['options']['block_device_mapping']['volume_size'] = null;
            json['options']['block_device_mapping']['delete_on_termination'] = null;
            json['options']['block_device_mapping']['device_name'] = Netra.helper.getComponent('#text_volume_path').getValue();
        }

        return json;
    },








    onStart: function()
    {
    	console.debug("Window.NewInstanceExt.onStart");
    	
        this.loadImages();
    	this.loadFlavors();
        this.updateUsageForm();
        this.loadVolumeLimit();
    },

	onButtonCloseClick: function()
	{
        console.debug("NewInstance.close : instanceWindow = " , this.instanceWindow);

        //this.instanceWindow.hide();
        this.instanceWindow.close();
        Netra.manager.removeController(this);
	},

    onButtonSaveClick: function()
    {
        console.debug("NewInstanceExt.onButtonSaveClick : instanceWindow = " , this.instanceWindow);
        if (! this.validateForm()) {
            Ext.MessageBox.alert('Error', 'Please fill out the form');
            return;
        }

        var json = this.getNewInstanceParameterInJson();
        console.debug('onButtonSaveClick : json = ', json);

        Netra.task.sendNewInstanceRequest(json);
    },


	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

    onImageLoad: function(records, operation, success) 
    {
    	var _this = this;

        console.log("onImageLoad : length = " + records.length);
        this.getComboImage().bindStore(Netra.manager.getStore('storeImages'));
    },

    onFlavorLoaded: function()
    {
        var _this = this;

        console.debug("onFlavorLoaded");
        this.getComboFlavor().bindStore(Netra.manager.getStore('storeFlavors'));
    },

    onFlavorChanged: function(combo, newValue, oldValue, eOpts )
    {

        var store = Netra.manager.getStore('storeFlavors');

        var record = store.findRecord('id',newValue);
        
        console.debug("onFlavorChanged : record = ", record);
        
        if (record == null) return;

        this.getTextFlavorCpu().setValue(record.data.vcpus);
        this.getTextFlavorMemory().setValue(record.data.ram);
        this.getTextFlavorDisk().setValue(record.data.disk);

    },

    onImageChanged: function(combo, newValue, oldValue, eOpts )
    {

        var store = Netra.manager.getStore('storeImages');

        var record = store.findRecord('id',newValue);
        
        console.debug("onImageChanged : record = ", record);
        
        if (record == null) return;

        this.getTextImageName().setValue(record.data.name);
        this.getTextImageFormat().setValue(record.data.disk_format);
        this.getTextImageSize().setValue(record.data.size);
        this.getTextImageCreated().setValue(record.data.created_at);
        this.getTextImageMinSize().setValue(record.data.min_disk);
        this.getTextImageMinRam().setValue(record.data.min_ram);
        this.getCheckImageProtected().setValue(record.data.protected);
    },

    onVolumeChanged: function(combo, newValue, oldValue, eOpts )
    {
        var store = Netra.manager.getStore('storeVolumes');
        var record = store.findRecord('id',newValue);
        
        console.debug("onVolumeChanged : record = ", record);
        
        if (record == null) return;

        this.getTextVolumeSize().setValue(record.data.size);
        this.getTextVolumeZone().setValue(record.data.availability_zone);
        this.getTextVolumeType().setValue(record.data.volume_type);

    },

    onSecurityChanged: function(combo, newValue, oldValue, eOpts )
    {
        var store = Netra.manager.getStore('storeSecuritys');
        var record = store.findRecord('id',newValue);
        
        console.debug("onSecurityChanged : record = ", record);
        
        if (record == null) return;
/*
        this.getTextVolumeSize().setValue(record.data.size);
        this.getTextVolumeZone().setValue(record.data.availability_zone);
        this.getTextVolumeType().setValue(record.data.volume_type);
*/        
    },

 	onNodeClick: function(view, record, item, index, e) 
    {
		console.debug("onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        console.debug(record);
        
        this.setActiveForm(index);
        //this.providerRecord = record;
        
        //this.getPanelContents().items.items[index].expand();
 	},

    onTenantNetworkChange : function(thisFormField, newValue, oldValue, eOpts) 
    {
        console.debug("onTenantNetworkChange");
        console.debug(thisFormField);
        console.debug(newValue.network_model_multi);

        this.getTextVlanRange().enable();    
        if (newValue.network_model_multi == 1) {
            this.getTextVlanRange().disable();    
        }
        
    },

 	onTreeMenuRendered: function() 
 	{
        console.debug("onTreeMenuRendered");
        //this.selectFirstNode();
 	},


});