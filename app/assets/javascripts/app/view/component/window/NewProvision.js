Ext.define("Netra.view.component.window.NewProvision", {
    extend: 'Ext.app.Controller',
    alias: 'controller.newprovision',
    id: 'newprovision',
    reference: 'newprovision',

    requires: [ 
        //'Ext.layout.container.Accordion',
    ],

    refs: [
        { ref: 'panelContents', selector: '#panel_container' },
        { ref: 'formInstance',selector: '#form_newprovision_template' },
        { ref: 'formVolume',selector: '#form_newinstance_xenhost' },
        { ref: 'areaCompute',selector: '#area_compute_multi' },        
        { ref: 'comboFlavor',selector: '#combo_instance_template' },        
        { ref: 'comboImage',selector: '#combo_instance_image' },
        { ref: 'checkImageProtected',selector: '#check_image_protected' },
    ],


    instanceWindow: null,


    init: function() 
    {
        console.debug("NewProvisionWindow.init");

        this.control({
            '#newprovision': {
                activate: this.onWindowActivate,
            },

            '#tree_new_provision': {
                itemclick: this.onNodeClick,
                render: this.onProvisionTreeRendered
            },

            '#grid_newprovision_template': { 
                itemdblclick: this.onGridColumnDoubleClick,
                selectionchange: this.onTemplateGridSelectionChange,
            },

            '#grid_newprovision_storage': { 
                selectionchange: this.onStorageGridSelectionChange,
            },

            '#grid_newprovision_xenhost': { 
                selectionchange: this.onXenhostGridSelectionChange,
            },


            '#btn_new_close': { click: this.onButtonCloseClick },
            '#btn_new_create': { click: this.onButtonCreateClick },

            '#btn_new_prev': { click: this.onButtonPrevClick },
            '#btn_new_next': { click: this.onButtonNextClick },

            '#combo_newprovision_pool': { change: this.onPoolChanged },
            //'#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

        this.instanceWindow = Ext.create('Ext.window.Toast', 
        {
            title: 'Create a New Provisioning Task',
            height: 600,
            width: 800,
            
            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

            layout: {
                type: 'border',
                //pack: 'start',
                //align: 'stretch',
            },

            modal: true,
            
            onEsc: function() {},
            closable: false,
            

            poolRecord: null,
            templateRecord: null,
            xenhostRecord: null,
            storageRecord: null,
            selectedNodeIndex: 0,

            items : 
            [
                {
                    xtype: 'treepanel',
                    id: 'tree_new_provision',
                    region: 'west',
                    title: 'Process',
                    width: 160,
                    rootVisible: false,
                    useArrows: true,
                    split: true,
                    root: {
                        text: 'Root',
                        children: 
                        [
                            { text: 'Task', leaf: true },
                            { text: 'Template', leaf: true } ,
                            { text: 'Storage', leaf: true } ,
                            { text: 'XenHost', leaf: true },
                            { text: 'VM', leaf: true },
                            { text: 'Finish', leaf: true },
                        ]
                    }
                },

                {
                    xtype: 'panel',
                    id: 'panel_container',
                    region: 'center',
                    border: false,
                    //title: 'Input Forms',
                    //titleAlign: 'center',
                    layout: {
                        // layout-specific configs go here
                        type: 'fit',
                        animate: true,        
                        //titleCollapse: false,
                        //activeOnTop: true
                    },
                    defaults: {
                        titleAlign: 'left',
                        autoScroll: false,
                        headerPosition: 'left'        
                    },

                    items: 
                    [
                        {
                            xtype: 'form',
                            id: 'form_provision_task',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Task Info','Please provide task information')
                                },

                                {
                                    fieldLabel : "Task Name",
                                    id: 'text_provision_task_name',
                                    name: 'provision_task_name',
                                    value: "Netra",   
                                    allowBlank: false,      
                                    invalidText: 'Please give an task name!!!',
                                },
                                {
                                    xtype : 'textareafield',
                                    fieldLabel : 'Task Description',
                                    id: 'text_provision_task_description',
                                    name: 'text_provision_task_description',
                                    grow: true,
                                    allowBlank : true,          
                                    height: 200,
                                    value: "",
                                },                                
                            ]
                        },

                        {
                            xtype: 'form',
                            id: 'form_newprovision_template',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Select Pool & Provisioning Template','')
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a pool','')
                                },                                
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Pool",
                                    name : 'instance_pool',
                                    id: 'combo_newprovision_pool',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a pool to provision a new virtual machine',
                                    //invalidText: 'Please select a template to install openstack controller',
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a template','')
                                },

                                {
                                    xtype: 'gridpanel',
                                    id: 'grid_newprovision_template',
                                    //reference: 'grid_template',
                                    //title: 'Template List',
                                    stripeRows: true,
                                    columnLines: true,
                                    height: 200,

                                    store: Netra.manager.getStore('storeTemplates'),

                                    selType: 'checkboxmodel',
                                    selModel: {
                                        checkOnly: true,
                                        injectCheckbox: 0,
                                        mode: 'SINGLE'
                                    },

                                    columns: [
                                        {
                                            text     : 'Name',
                                            flex     :  2,
                                            dataIndex: 'name'
                                        },
                                        {
                                            text     : 'CPU',
                                            flex     :  1,
                                            dataIndex: 'vcpus_max'
                                        },
                                        {
                                            text     : 'Memory',
                                            flex     :  1,
                                            dataIndex: 'memory_max',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }
                                        },
                                        {
                                            text     : 'Storage Size',
                                            flex     :  1,
                                            dataIndex: 'storage',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.getTemplateTotalStorageSize(value);
                                            }

                                        },                  
                                        {
                                            text     : 'Description',
                                            flex     :  3,
                                            dataIndex: 'description'
                                        },                
                                    ],
                                },
                            ]
                        },
                        {
                            xtype: 'form',
                            id: 'form_newprovision_storage',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Storage','')
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a storage to provision','')
                                },                                
                                {
                                    xtype: 'gridpanel',
                                    id: 'grid_newprovision_storage',
                                    //title: 'Template List',
                                    stripeRows: true,
                                    columnLines: true,
                                    height: 200,

                                    store: Netra.manager.getStore('storeStorageRepositories'),

                                    selType: 'checkboxmodel',
                                    selModel: {
                                        checkOnly: true,
                                        injectCheckbox: 0,
                                        mode: 'SINGLE'
                                    },

                                    columns: [
                                        {
                                            text     : 'Name',
                                            flex     :  2,
                                            dataIndex: 'name'
                                        },
                                        {
                                            text     : 'Description',
                                            flex     :  2,
                                            dataIndex: 'description'
                                        },                                        
                                        {
                                            text     : 'Total Size',
                                            flex     :  1,
                                            dataIndex: 'physical_size',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }                                            
                                        },
                                        {
                                            text     : 'Used Size',
                                            flex     :  1,
                                            dataIndex: 'physical_utilisation',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }
                                        },
                                        {
                                            text     : 'Virtual Used Size',
                                            flex     :  1,
                                            dataIndex: 'virtual_allocation',
                                        },
                                        {
                                            text     : 'Shared',
                                            flex     :  1,
                                            dataIndex: 'shared',
                                        },                                        
                                        {
                                            text     : 'UUID',
                                            flex     :  1,
                                            dataIndex: 'uuid'
                                        },
                                        {
                                            text     : 'Reference',
                                            flex     :  1,
                                            dataIndex: 'reference'
                                        },                                                        
                                    ],
                                },
                            ]
                        },
                        {
                            xtype: 'form',
                            id: 'form_newinstance_xenhost',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Xen Host','')
                                },    
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a xen host for VM provision','')
                                },

                                {
                                    xtype: 'gridpanel',
                                    id: 'grid_newprovision_xenhost',
                                    //reference: 'grid_host_instance',
                                    //title: 'Host List',
                                    height: 300,
                                    stripeRows: true,
                                    columnLines: true,

                                    store: Netra.manager.getStore('storeXenHosts'),

                                    selType: 'checkboxmodel',
                                    selModel: {
                                        checkOnly: true,
                                        injectCheckbox: 0,
                                        mode: 'SINGLE'
                                    },

                                    columns: 
                                    [
                                        {
                                            text     : 'Name',
                                            flex     : 1,
                                            sortable : true,
                                            dataIndex: 'name'
                                        },                                    
                                        {
                                            text     : 'IP',
                                            flex     : 2,
                                            sortable : true,
                                            dataIndex: 'ip'
                                        },
                                        {
                                            text     : 'CPU',
                                            flex     : 1,
                                            dataIndex: 'cpu_count',
                                        },
                                        {
                                            text     : 'VM Count',
                                            flex     : 1,
                                            dataIndex: 'xenserver',
                                        },                                        
                                        {
                                            text     : 'Total Memory',
                                            flex     : 1,
                                            dataIndex: 'total_memory',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }                                            
                                        },
                                        {
                                            text     : 'Free Memory',
                                            flex     : 1,
                                            dataIndex: 'free_memory',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }                                            

                                        },
                                    ],

                                }

                            ]
                        },                       

                        {
                            xtype: 'form',
                            id: 'form_newprovision_vm',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Virtual Machine Info','')
                                },    
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please provide basic VM information','')
                                },                                
                                {
                                    fieldLabel : "VM Name",
                                    id: 'text_newvm_name',
                                    name: 'instance_name',
                                    value: "Netra",   
                                    allowBlank: false,      
                                    invalidText: 'Please give a VM name!!!',
                                },
                                {
                                    xtype: 'numberfield',                                    
                                    fieldLabel : "VM Count",
                                    name : 'instance_count',
                                    id: 'text_newvm_count',
                                    value: 1,
                                    minValue: 1,
                                    maxValue: 99,
                                    allowBlank: false,
                                    invalidText: 'Please type VM count',
                                },
                                {
                                    xtype: 'numberfield',                                    
                                    fieldLabel : "Start Index",
                                    name : 'newvm_start_index',
                                    id: 'text_newvm_start_index',
                                    value: 1,
                                    minValue: 1,
                                    maxValue: 99,
                                    allowBlank: false,
                                    invalidText: 'Please type VM postfix index starting number',
                                },                                
                                {
                                    xtype : 'textareafield',
                                    fieldLabel : 'Description',
                                    id: 'text_newvm_description',
                                    name: 'text_newvm_description',
                                    grow: true,
                                    allowBlank : true,          
                                    height: 200,
                                    value: "",
                                }
                            ]
                        },

                        {
                            xtype: 'form',
                            id: 'form_newinstance_finish',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Finish','Please checked your options, then click Create button to go!!!')
                                },  
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('VM Provisioning Parameters','')
                                },
                                {
                                    xtype: 'propertygrid',
                                    id: 'grid_newvm_finish',
                                    title: 'General',
                                    titleAlign: 'center',
                                    rowspan: 2,
                                    nameColumnWidth: 160,
                                    sortableColumns: false,
                                    source: {},
                                    sourceConfig: {
                                        borderWidth: {
                                            displayName: 'Border Width'
                                        },
                                        tested: {
                                            displayName: 'QA'
                                        }
                                    }                                            
                                },
                            ]
                        },

                    ],

                },

            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_new_window',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        { xtype: 'button', id: 'btn_new_create', text: 'Create', disabled: true},
                        { xtype: 'button', id: 'btn_new_close', text: 'Close' },
                        { xtype: 'button', id: 'btn_new_prev', text: 'Prev', margin: '0 7 0 20 0' },
                        { xtype: 'button', id: 'btn_new_next', text: 'Next' },
                    ]
                }
            ],

        });

        this.onStart();
    },

    show: function()
    {
        console.debug("NewInstance.show");
        this.instanceWindow.show();     
    },


    loadPools: function()
    {
        console.log("NewVM.loadPools");

        var store = Netra.manager.getStore('storeClusters');

        store.load({
            callback: this.onPoolLoadComplete,
            params: {},
            scope: this
        });
    },

    addTemplateNodeToTree: function(text,record)
    {
        var rootNode = this.getTreeMenu().getRootNode();
        var node = this.addNodeToTree(this.getTreeMenu(),rootNode, text, 'openstack', record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    updateProgressBar: function(pbar,max,used,item)
    {
        pbar.updateProgress(used/max, 'Total ' + item + ' ' + used + '  of ' + max + ' Used');
    },

    updateFinishForm: function()
    {
        console.debug("NewVM.updateFinishForm : templateRecord = ", this.templateRecord);

        var params_general = {};

        params_general['Task Name'] = Netra.helper.getComponent('#text_provision_task_name').getValue();
        params_general['Task Description'] = Netra.helper.getComponent('#text_provision_task_description').getValue();
        
        if (this.poolRecord) {
            params_general['Pool'] = this.poolRecord.data.name;
        }
        
        if (this.templateRecord) {
            params_general['Template Name'] = this.templateRecord.name;
            params_general['vCPU'] = this.templateRecord.vcpus_max;
            params_general['Memory'] = Netra.helper.formatByteNumber(this.templateRecord.memory_max);
            params_general['Template Size'] = Netra.helper.getTemplateTotalStorageSize(this.templateRecord.storage);
        }

        if (this.storageRecord) {
            params_general['Storage Name'] = this.storageRecord.sr;
            params_general['Storage Total Size'] = Netra.helper.formatByteNumber(this.storageRecord.sr_size);
            params_general['Storage Used Size'] = Netra.helper.formatByteNumber(this.storageRecord.sr_used_size);
        }
        
        if (this.xenhostRecord) {
            params_general['XenHost'] = this.xenhostRecord.name;
            params_general['Host Total Memory'] = Netra.helper.formatByteNumber(this.xenhostRecord.total_memory);
            params_general['Host Free Memory'] = Netra.helper.formatByteNumber(this.xenhostRecord.free_memory);
        }

        params_general['VM Name'] = Netra.helper.getComponent('#text_newvm_name').getValue();
        params_general['VM Count'] = Netra.helper.getComponent('#text_newvm_count').getValue();
        params_general['VM Name Index'] = Netra.helper.getComponent('#text_newvm_start_index').getValue();
        params_general['VM Description'] = Netra.helper.getComponent('#text_newvm_description').getValue();
        
        Netra.helper.updatePropertyGrid('#grid_newvm_finish', params_general);

    },

    setActiveForm: function(form_index)
    {
        var panel = this.getPanelContents();
        console.debug("NewVM.setActiveForm : form id = " + form_index + " , panel = ", panel);
        
        this.selectedNodeIndex = form_index;

        for (var i=0; panel.items.length > i; i++) 
        {
            panel.items.items[i].hide();
        }

        if ((Netra.helper.getComponent('#tree_new_provision').getRootNode().childNodes.length - 1) == form_index) {
            this.updateFinishForm();
        }   

        panel.items.items[form_index].show();
    },

  
    validateForm: function()
    {
        console.debug("NewVM.validateForm");

        form = Netra.helper.getComponent('#form_provision_task');
        if (! form.isValid()) {
            this.setActiveForm(0);
            return false;
        }

        if (! this.poolRecord) {
            this.setActiveForm(1);
            return false;
        }

        if (! this.templateRecord) {
            this.setActiveForm(1);
            return false;
        }

        if (! this.storageRecord) {
            this.setActiveForm(2);
            return false;
        }

        if (! this.xenhostRecord) {
            this.setActiveForm(3);
            return false;
        }

        form = Netra.helper.getComponent('#form_newprovision_vm');
        if (! form.isValid()) {
            this.setActiveForm(4);
            return false;
        }


        return true;
    },

    getNewProvisionParameterInJson: function()
    {
        console.debug("NewVM.getNewProvisionParameterInJson ");

        var json = {};

        json['task_name'] = Netra.helper.getComponent('#text_provision_task_name').getValue();
        json['task_description'] = Netra.helper.getComponent('#text_provision_task_description').getValue();
        json['pool'] = this.poolRecord.data.name;
        json['vm_name'] = Netra.helper.getComponent('#text_newvm_name').getValue();
        json['vm_count'] = Netra.helper.getComponent('#text_newvm_count').getValue();
        json['vm_start_index'] = Netra.helper.getComponent('#text_newvm_start_index').getValue();
        json['vm_description'] = Netra.helper.getComponent('#text_newvm_description').getValue();
        json['template_ref'] = this.templateRecord.reference;
        json['host_ref'] = this.xenhostRecord.reference;
        json['storage_ref'] = this.storageRecord.reference;

        return json;
    },

    updateNavigationButtons: function(increment)
    {
        console.debug("NewProvision.updateNavigationButtons");

        switch (this.selectedNodeIndex) 
        {
            case 0:
                Netra.helper.getComponent('#btn_new_create').setDisabled(true);
                Netra.helper.getComponent('#btn_new_prev').setDisabled(true);
                Netra.helper.getComponent('#btn_new_next').setDisabled(false);
                break;
            
            case (Netra.helper.getComponent('#tree_new_provision').getRootNode().childNodes.length - 1):
                Netra.helper.getComponent('#btn_new_create').setDisabled(false);
                Netra.helper.getComponent('#btn_new_prev').setDisabled(false);
                Netra.helper.getComponent('#btn_new_next').setDisabled(true);
                break;

            default:
                Netra.helper.getComponent('#btn_new_create').setDisabled(true);
                Netra.helper.getComponent('#btn_new_prev').setDisabled(false);
                Netra.helper.getComponent('#btn_new_next').setDisabled(false);
                break;            
        }

    },

    doPanelNavigation: function(ctl,increment)
    {

        var tree = Netra.helper.getComponent('#tree_new_provision');
        var page_count = tree.getRootNode().childNodes.length - 1;

        switch (ctl)
        {
            case 0: //if tree
                this.selectedNodeIndex = increment;
                break;

            case 1: //if button
                this.selectedNodeIndex = this.selectedNodeIndex + increment;    
                break;
        }
        

        if (this.selectedNodeIndex<0) {
            this.selectedNodeIndex = 0;
        } else if (this.selectedNodeIndex > page_count) {
            this.selectedNodeIndex = page_count;
        }

        Netra.helper.selectTreeNode(Netra.helper.getComponent('#tree_new_provision'),this.selectedNodeIndex);
        this.setActiveForm(this.selectedNodeIndex);
        this.updateNavigationButtons();
    },




    onStart: function()
    {
        console.debug("Window.NewVM.onStart");

        this.loadPools();
        //this.loadFlavors();
        //this.updateUsageForm();
        //this.loadVolumeLimit();
    },

    onButtonPrevClick: function()
    {
        console.debug("NewInstance.onButtonPrevClick ");
        this.doPanelNavigation(1,-1);

    },

    onButtonNextClick: function()
    {
        console.debug("NewInstance.onButtonPrevClick ");
        this.doPanelNavigation(1,1);
    },

    onButtonCloseClick: function()
    {
        console.debug("NewInstance.close : instanceWindow = " , this.instanceWindow);

        //this.instanceWindow.hide();
        this.instanceWindow.close();
        Netra.manager.removeController(this);
    },

    onButtonCreateClick: function()
    {
        console.debug("NewVM.onButtonCreateClick : instanceWindow = " , this.instanceWindow);

        if (! this.validateForm()) {
            Ext.MessageBox.alert('Error', 'Please fill out the form');
            return;
        }

        var json = this.getNewProvisionParameterInJson();
        console.debug('NewVM.onButtonCreateClick : json = ', json);

        Netra.task.sendVMProvisionTask(json, this.onVMProvisionComplete);
    },


    onBroadcastMessage: function(param)
    {
        console.debug("onBroadcastMessage");
    },

    onPoolLoadComplete: function(records, operation, success) 
    {
        var _this = this;

        console.log("NewVM.onPoolLoadComplete : length = " + records.length);
        Netra.helper.getComponent('#combo_newprovision_pool').bindStore(Netra.manager.getStore('storeClusters'));
    },

    onLoadTemplateComplete: function(ret_code,json)
    {
        console.debug("NewVM.onLoadTemplateComplete");
        //Netra.helper.getComponent('#grid_template').bindStore(Netra.manager.getStore('storeTemplates'));
    },

    onLoadXenHostComplete: function(ret_code,json)
    {
        console.debug("NewVM.onLoadXenHostComplete");
        //Netra.helper.getComponent('#grid_template').bindStore(Netra.manager.getStore('storeTemplates'));
    },

    onLoadStorageRepositoryComplete: function(ret_code,json)
    {
        console.debug("NewVM.onLoadStorageRepositoryComplete : json = ",json);
        //Netra.helper.getComponent('#grid_template').bindStore(Netra.manager.getStore('storeTemplates'));
    },

    onPoolChanged: function(combo, newValue, oldValue, eOpts )
    {
        var store = Netra.manager.getStore('storeClusters');

        var record = store.findRecord('id',newValue);
        
        console.debug("NewVM.onPoolChanged : record = ", record);
        
        if (record == null) return;

        this.poolRecord = record;

        Netra.service.loadBaseTemplateData(this.poolRecord.data.name, this.onLoadTemplateComplete)
        Netra.service.loadXenHostData(this.poolRecord.data.name, this.onLoadXenHostComplete)
        Netra.service.loadStorageRepositoryData(this.poolRecord.data.name, this.onLoadStorageRepositoryComplete)
    },

    onVolumeChanged: function(combo, newValue, oldValue, eOpts )
    {
        var store = Netra.manager.getStore('storeVolumes');
        var record = store.findRecord('id',newValue);
        
        console.debug("NewVM.onVolumeChanged : record = ", record);
        
        if (record == null) return;

        this.getTextVolumeSize().setValue(record.data.size);
        this.getTextVolumeZone().setValue(record.data.availability_zone);
        this.getTextVolumeType().setValue(record.data.volume_type);

    },

    onGridColumnDoubleClick: function(grid, record, item, index, e, eopts) 
    {
        console.debug("NewVM.onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));
    },

    onXenHostGridColumnDoubleClick: function(grid, record, item, index, e, eopts) 
    {
        console.debug("NewVM.onXenHostGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.xenhostRecord = record;
        grid.getSelectionModel().select(index);
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("NewVM.onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        //console.debug(record);
        this.doPanelNavigation(0,index);
        //this.setActiveForm(index);
    },

    onTenantNetworkChange : function(thisFormField, newValue, oldValue, eOpts) 
    {
        console.debug("NewVM.onTenantNetworkChange");
        console.debug(thisFormField);
        console.debug(newValue.network_model_multi);

        this.getTextVlanRange().enable();    
        if (newValue.network_model_multi == 1) {
            this.getTextVlanRange().disable();    
        }
        
    },

    onProvisionTreeRendered: function() 
    {
        console.debug("NewVM.onProvisionTreeRendered");
    },

    onVMProvisionComplete: function(retcode, json)
    {
        console.debug("NewVM.onVMProvisionComplete");
        if (json.ret_code == 0) {
            Ext.MessageBox.alert('Success', 'New VM has created and started!!');
        } else {
            Ext.MessageBox.alert('Error', json['msg']);
        }
    },

    onTemplateGridSelectionChange: function(grid, selected, eOpts)
    {
        console.debug("NewVM.onTemplateGridCheckChange : selected = ", selected);

        this.templateRecord = selected[0].data;
        //grid.getSelectionModel().select(index);
    },

    onStorageGridSelectionChange: function(grid, selected, eOpts)
    {
        console.debug("NewVM.onStorageGridSelectionChange : selected = ", selected);

        this.storageRecord = selected[0].data;
        //grid.getSelectionModel().select(index);
    },

    onXenhostGridSelectionChange: function(grid, selected, eOpts)
    {
        console.debug("NewVM.onStorageGridSelectionChange : selected = ", selected);

        this.xenhostRecord = selected[0].data;
        //grid.getSelectionModel().select(index);
    },

    onWindowActivate: function(window, eOpts)
    {
        console.debug("NewVM.onWindowActivate");
        Netra.helper.selectTreeNode(Netra.helper.getComponent('#tree_new_provision'),0);
    },
});