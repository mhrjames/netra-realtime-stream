Ext.define("Netra.view.component.window.NewTask", {
	extend: 'Ext.app.Controller',
	alias: 'controller.newtask',
	id: 'newtask',
	reference: 'newtask',

    requires: [ 
        'Ext.toolbar.*',
        'Plugin.codemirror.CodeMirror',
    ],

    refs: [
    	{ ref: 'formInput', selector: '#form_newtask_script' },
    	{ ref: 'formTitle', selector: '#form_title' },
    ],

    models: [],    
    stores: [],    

	taskWindow: null,
	taskRecord: null,


    init: function() 
    {
    	console.debug("NewTask.init");

		//this.callParent(arguments);

        this.control({
            '#window_newtask': { beforeclose: this.onWindowBeforeClose },
            '#btn_newtask_window_close': { click: this.onButtonCloseClick },
            '#btn_newtask_window_save': { click: this.onButtonSaveClick },
            '#btn_newtask_window_syntax': { click: this.onButtonSyntaxClick },
/*            
            '#combo_public_ip': { change: this.onHostChanged },
            '#combo_public_ip_multi': { change: this.onHostChanged },
            '#combo_compute_ip_multi': { change: this.onComputeChanged },
            '#tree_openstack': {
            	itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },
            '#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
*/            
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 


    	this.taskWindow = Ext.create('Ext.window.Toast', 
    	{
		    height: 400,
		    width: 600,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

		    title: 'Add New Task',
		    id: 'window_newtask',
		    reference: 'windowTask',
		    modal: true,

		    layout: {
		    	type: 'vbox',
				align: 'stretch',		    	
		    },

			onEsc: function() {},
			closable: false,

			items : 
			[
		        {
		        	xtype: 'form',
		        	border: false,
					bodyPadding: '0 0 0 0',
					flex: 2,
					autoScroll: true,
					layout: {
						type: 'vbox',
						align: 'stretch',
					},
					id: 'form_newtask_script',

                    items: 
                    [
            			{
				            xtype         : 'codemirror',
				            name          : 'script',
				            //id            : 'code_script',
				            flex          : 2,
				            padding       : '0 0 0 0',
				            anchor        : '100%',
				            layout        : 'fit',
				            pathModes     : '/assets/lib/codemirror/mode',
				            pathExtensions: '/assets/lib/codemirror/util',
				            lineNumbers   : true,
				            matchBrackets : true,
				            indentUnit    : 2,
				            //mode          : "text/css",
				            showModes     : false,
				            showLineNumbers: false,
            			},
            			{
            				xtype: 'textfield',
            				name: 'name',
            				id: 'text_script_name',
            				hidden: true,
            				value: "",
            			},
            			{
            				xtype: 'textfield',
            				name: 'id',
            				id: 'text_script_id',
            				hidden: true,
            				value: "",
            			}            			
                    ]
                },

                {
                	xtype: 'form',
                	layout: 'fit',
                	flex: 1,
                	items: 
                	[
                    	{
                            xtype : 'textareafield',
                            anchor: '100%',
                            fieldLabel : '',
                            id: 'area_newtask_output',
                            name: 'output',
                            readOnly: true,
                            grow: true,
                            allowBlank : true,          
                            //height: 130,
                            value: "",
                    	},
                	]
                }

			],


			dockedItems: 
			[
			    {
			        xtype: 'toolbar',
			        dock: 'top',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            //{ text: 'Run', id: 'btn_newtask_window_run' },
			            { text: 'Syntax Check', id: 'btn_newtask_window_syntax' },
			        ]
			    },

			    {
			        xtype: 'toolbar',
			        dock: 'bottom',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            { xtype: 'component', flex: 1 },
			            { text: 'Save', id: 'btn_newtask_window_save' },
			            { text: 'Close', id: 'btn_newtask_window_close' },
			        ]
			    }
			],
		});
    },

    show: function(record)
    {
		this.taskWindow.show();

		this.taskRecord = record;
		this.updateForm(this.taskRecord);
    },

    close: function()
    {
    	console.debug("NewTaskWindow.close : taskWindow = " , this.taskWindow);

    	this.taskWindow.close();
    	Netra.manager.removeController(this);
    },

    isUpdateMode: function()
	{
		return (this.taskRecord != null);
	},


	sendRequest: function(action,json,callback)
	{
		console.debug("NewTasks.sendRequest");
		
		Ext.Ajax.request({
		    url: '/api/task/'+action,
		    method: 'POST',
		    jsonData: json,
		    success: function(response, opts) 
		    {
		        console.debug('sendParameters Response = ', response);
		        
		        var json = JSON.parse(response.responseText);

		        callback(json.retcode,json);

		    },
		    failure: function(response, opts) 
		    {
		        console.debug('sendParameters : woops');
		        callback(-1,response);
		    }
		});

	},

	getParameter: function()
	{
		console.debug('NewTaskWindow.getParameter');

		var json = {};

        json['id'] = Netra.helper.getComponent("#text_script_id").value;
        json['name'] = Netra.helper.getComponent("#text_script_name").value;
        json['script'] = Netra.helper.getComponent("codemirror[name=script]").getValue();

        return json;
	},

    saveToServer: function()
    {
		var form = this.getFormInput();

		console.debug("TaskWindow.saveToServer : form = ", form);
		
		this.sendRequest('update',this.getParameter() ,function (result, response) 
		{	
			if (result == 0 ) 
			{
				Ext.MessageBox.alert('Success', 'Your Task is saved!!!');

			}

		});
    },

    syntaxCheckScriptToServer: function()
    {
		console.debug("TaskWindow.syntaxCheckScriptToServer");

		this.sendRequest('syntax',this.getParameter() ,function (result, response) 
		{	

			if (result == 0 ) 
			{
				console.debug("TaskWindow.syntaxCheckScriptToServer : result = " + result + " , json = ",response);
				Netra.helper.getComponent("#area_newtask_output").setValue("No Error Found!!!");
			} else {
				Netra.helper.getComponent("#area_newtask_output").setValue(response.data.msg);
			}

		});

    },

    updateForm: function(record)
    {
		console.debug("NewTask.updateForm : record = " , record);

		Netra.helper.getComponent("codemirror[name=script]").setValue("");

		if (! record) {
			//this.getFormTitle().update(Netra.helper.createFormTitle('New Tenant',''));
			return;
		}

		Netra.helper.getComponent("#text_script_id").setValue(record.data.id);
		Netra.helper.getComponent("#text_script_name").setValue(record.data.name);
		Netra.helper.getComponent("codemirror[name=script]").setValue(record.data.script);

    },







    onStart: function()
    {
		console.debug("taskWindow.onStart");
    },


	onButtonCloseClick: function()
	{
		console.debug("taskWindow.onButtonCloseClick : controller = ", this);

		//var controller = Netra.app.getApplication().getController('Netra.view.component.window.NewTaskWindow');
		//var controller = Netra.manager.getController('Netra.view.component.window.NewFlavor');
		//console.debug(controller);
		this.close();
	},

	onButtonSaveClick: function()
	{
		var _this = this;

		console.debug("taskWindow.onButtonSaveClick");

		if (this.isUpdateMode()) 
		{
	        _this.saveToServer();
			
			return;			
		}

		Ext.Msg.prompt('Netra', 'Please enter script name:', function(btn, text) 
		{
		    if(btn == 'ok') 
		    {
		        if (Ext.isEmpty(text)) {
		        	Ext.Msg.alert("Error", "Please type script name!!!");
		        	return;
		        }

		        var text_ctrl = Netra.helper.getComponent("#text_script_name");
		        text_ctrl.setValue(text);

		        _this.saveToServer();
		        
		    }
		});

	},

	onButtonSyntaxClick: function()
	{
		console.debug("onButtonSyntaxClick");
		this.syntaxCheckScriptToServer();
	},

	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},

	onWindowBeforeClose: function()
	{
		console.debug("onWindowBeforeClose");
		//return false;
	},


});