Ext.define("Netra.view.component.window.NewUser", {
	extend: 'Ext.app.Controller',
	alias: 'controller.newuser',
	id: 'newuser',
	reference: 'newuser',

    requires: [ 
        'Ext.toolbar.*',
    ],

    refs: [
    	{ ref: 'formInput', selector: '#form_newuser' },
    	{ ref: 'formTitle', selector: '#form_title' },
    ],

    models: [ ],    
    stores: [ ],    

	userWindow: null,
	userRecord: null,


    init: function() 
    {
    	console.debug("NewUserWindow.init");

		//this.callParent(arguments);

        this.control({
            '#btn_user_window_close': { click: this.onButtonCloseClick },
            '#btn_user_window_save': { click: this.onButtonSaveClick },
/*            
            '#combo_public_ip': { change: this.onHostChanged },
            '#combo_public_ip_multi': { change: this.onHostChanged },
            '#combo_compute_ip_multi': { change: this.onComputeChanged },
            '#tree_openstack': {
            	itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },
            '#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
*/            
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 


    	this.userWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'User Window',
		    height: 400,
		    width: 600,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,
		    
		    id: 'window_user',
		    reference: 'windowUser',
		    //animCollapse: true,
		    //collapseDirection: 'top',
			//animateTarget: Ext.getDoc(),

		    layout: {
		    	type: 'fit',
		    },

		    modal: true,
			onEsc: function() {},
			closable: false,
		    
			items : 
			[
		        {
		        	xtype: 'form',
		        	border: false,
                    layout: {
                    	type: 'vbox',
				    	pack: 'start',
				    	align: 'stretch',
						anchor: '100%',
                    },
				    defaults: {
				    	xtype: "textfield",
				        align: 'stretch',
				        anchor: '100%',
				        labelWidth: 120,
				    },
					bodyPadding: '15 30 30 30',
					id: 'form_newuser',

                    items: 
                    [
						{
							xtype: 'component',
							id: 'form_title',
							frame: false,
							border: 0,
							html: Netra.helper.createFormTitle('New User','')
						},	 
/*						                   
                    	{
							fieldLabel : "User Name",
							id: 'text_user_username',
							name: 'username',
							value: "",			
                    	},
*/                    	
                    	{
							fieldLabel : "Name",
							id: 'text_user_name',
							name: 'name',
							value: "",			
                    	},
                    	{
							fieldLabel : "Password",
							id: 'text_user_password',
							name: 'password',
							inputType: 'password', 
							value: "",			
                    	},
                        {
                            xtype: 'combo',
                            fieldLabel : "Enabled",
                            name : 'enabled',
                            id: 'combo_user_enabled',
                            queryMode: 'local',
                            triggerAction: 'all', 
                            editable: false,
                            store: [ 'True', 'False' ],
                            value: "",
						},                    	
                    	{
							fieldLabel : "EMail",
							id: 'text_user_email',
							name: 'email',
							value: "",			
                    	},
                        {
                            xtype: 'combo',
                            fieldLabel : "Tenant",
                            name : 'tenantId',
                            id: 'combo_user_tenant',
                            queryMode: 'local',
                            triggerAction: 'all', 
                            editable: false,
                            store: {
                            	type: 'tenants',
                            	autoLoad: true,
                            },
                            value: "",
                            forceSelection: true,
                            allowBlank: false,
                            displayField: 'name',
                            valueField: 'id',
                            emptyText: 'Please select a template to install openstack controller',
                        },
                    	{
							fieldLabel : 'id',
							hidden: true,
							name: 'id',
                    	},

                    ]
                },
			],

			dockedItems: [
			    {
			        xtype: 'toolbar',
			        dock: 'bottom',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            { xtype: 'component', flex: 1 },
			            { text: 'Save', id: 'btn_user_window_save' },
			            { text: 'Close', id: 'btn_user_window_close' },
			            //{ xtype: 'button', id: 'btn_new_save', text: 'Save' },
			            //{ xtype: 'button', id: 'btn_flavor_window_close', text: 'Close' }
			        ]
			    }
			],

		});

    },

    show: function(record)
    {
		this.userWindow.show();

    	this.userRecord = record;
    	this.updateForm(this.userRecord);
    },

    close: function()
    {
    	console.debug("NewUser.close : userWindow = " , this.userWindow);

    	this.userWindow.close();
    	//Netra.app.getApplication().destroyController(this);
    	Netra.manager.removeController(this);
    	//Netra.manager.removeController('Netra.view.component.window.NewUser');
    },

    isNewMode: function()
    {
    	return (this.userRecord == null);
    },

    updateForm: function(record)
    {
		console.debug("NewUser.updateForm : record = " , record);

		this.getFormInput().clearForm();

		if (! record) {
			this.getFormTitle().update(Netra.helper.createFormTitle('New User',''));
			return;
		}

		this.getFormTitle().update(Netra.helper.createFormTitle('Edit User',''));

        this.getFormInput().query('.textfield').forEach(function(component) 
        {
            component.setValue(record.data[component.name]);
            //json[component.name] = component.value;
        });

    },

	sendRequest: function(json)
	{
		console.debug("NewUser.sendRequest");
		
		Ext.Ajax.request({
		    url: '/api/openstack/users/update',
		    method: 'POST',
		    jsonData: json,
		    success: function(response, opts) 
		    {
		        console.debug('sendParameters Response = ', response);
		        var json = JSON.parse(response.responseText);
		        if (json.retcode == 0) {

		        	var msg = 'User Account is updated!!!'
		        	if (_this.isNewMode()) {
		        		msg = 'User Account is created!!!';
		        	}

		        	Ext.MessageBox.alert('Success', msg);
		        } else {
		        	Ext.MessageBox.alert('Error', json.msg);
		        }
		        	
		    },
		    failure: function(response, opts) 
		    {
		        console.debug('sendParameters : woops');
		        Ext.MessageBox.alert('Error', "Something is wrong!!!");
		    }
		});

	},

	getParameter: function()
	{
		console.debug('NewUser.getParameter');

		var json = {};

		var form = this.getFormInput();

        form.query('.textfield').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
	},

    saveToServer: function()
    {
		var form = this.getFormInput();

		console.debug("userWindow.saveToServer : form = ", form);
		
		var json = this.getParameter();
		this.sendRequest(json);
    },






    onStart: function()
    {
		console.debug("userWindow.onStart");
    },


	onButtonCloseClick: function()
	{
		console.debug("userWindow.onButtonCloseClick : controller = ", this);

		//var controller = Netra.app.getApplication().getController('Netra.view.component.window.NewUser');
		//var controller = Netra.manager.getController('Netra.view.component.window.NewUser');
		//console.debug(controller);
		this.close();
	},

	onButtonSaveClick: function()
	{
		console.debug("userWindow.onButtonSaveClick");
		this.saveToServer();
	},


	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},


});