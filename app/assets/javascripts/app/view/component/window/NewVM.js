Ext.define("Netra.view.component.window.NewVM", {
    extend: 'Ext.app.Controller',
    alias: 'controller.newvm',
    id: 'newvm',
    reference: 'newvm',

    requires: [ 
        //'Ext.layout.container.Accordion',
    ],

    refs: [
        { ref: 'treeMenu', selector: '#tree_openstack' },
        { ref: 'panelGrid', selector: 'panel_grid' },
        { ref: 'panelList', selector: '#panel_list' },
        { ref: 'panelContents', selector: '#panel_container' },
        { ref: 'panelTree', selector: '#tree_menu' },
        { ref: 'formInstance',selector: '#form_newinstance_template' },
        { ref: 'formVolume',selector: '#form_newinstance_xenhost' },
        { ref: 'areaCompute',selector: '#area_compute_multi' },        
        { ref: 'comboFlavor',selector: '#combo_instance_template' },        
        { ref: 'comboImage',selector: '#combo_instance_image' },
        { ref: 'checkImageProtected',selector: '#check_image_protected' },
    ],


    instanceWindow: null,


    init: function() 
    {
        console.debug("NewInstanceWindow.init");

        this.control({
            '#grid_template': { 
                itemdblclick: this.onGridColumnDoubleClick,
            },

            '#grid_newvm_xenhost': { 
                itemdblclick: this.onXenHostGridColumnDoubleClick,
            },


            '#btn_new_close': { click: this.onButtonCloseClick },
            //'#btn_new_create': { click: this.onButtonCreateClick },
            '#btn_new_create': { click: this.onButtonCreateClick },
            '#combo_instance_pool': { change: this.onPoolChanged },
            '#tree_new_instance': {
                itemclick: this.onNodeClick,
            //    render: this.onTreeMenuRendered
            },
            //'#radio_tenant_network_model_multi' : { change: this.onTenantNetworkChange },
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

        this.instanceWindow = Ext.create('Ext.window.Toast', 
        {
            title: 'Create a New Virtual Instance',
            height: 600,
            width: 800,
            
            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,

            layout: {
                type: 'border',
                //pack: 'start',
                //align: 'stretch',
            },

            modal: true,
            
            onEsc: function() {},
            closable: false,
            

            poolRecord: null,
            templateRecord: null,
            xenhostRecord: null,


            items : 
            [
                {
                    xtype: 'treepanel',
                    id: 'tree_new_instance',
                    region: 'west',
                    title: 'Items',
                    width: 180,
                    rootVisible: false,
                    useArrows: true,
                    split: true,
                    root: {
                        text: 'Root',
                        children: 
                        [
                            { text: 'Template', leaf: true } ,
                            { text: 'Name', leaf: true } ,
                            { text: 'XenHost', leaf: true },
                            { text: 'Finish', leaf: true },
                        ]
                    }
                },

                {
                    xtype: 'panel',
                    id: 'panel_container',
                    region: 'center',
                    border: false,
                    //title: 'Input Forms',
                    //titleAlign: 'center',
                    layout: {
                        // layout-specific configs go here
                        type: 'fit',
                        animate: true,        
                        //titleCollapse: false,
                        //activeOnTop: true
                    },
                    defaults: {
                        titleAlign: 'left',
                        autoScroll: false,
                        headerPosition: 'left'        
                    },

                    items: 
                    [

                        {
                            xtype: 'form',
                            id: 'form_newinstance_template',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Select Pool & Provisioning Template','')
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a pool','')
                                },                                
                                {
                                    xtype: 'combo',
                                    fieldLabel : "Pool",
                                    name : 'instance_pool',
                                    id: 'combo_instance_pool',
                                    queryMode: 'local',
                                    triggerAction: 'all', 
                                    editable: false,
                                    value: "",
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'name',
                                    valueField: 'id',
                                    emptyText: 'Please select a pool to provision a new virtual machine',
                                    //invalidText: 'Please select a template to install openstack controller',
                                },
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '30 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a template','')
                                },

                                {
                                    xtype: 'gridpanel',
                                    id: 'grid_template',
                                    reference: 'grid_template',
                                    //title: 'Template List',
                                    stripeRows: true,
                                    columnLines: true,
                                    height: 200,

                                    store: Netra.manager.getStore('storeTemplates'),

                                    selType: 'checkboxmodel',
                                    selModel: {
                                        checkOnly: true,
                                        injectCheckbox: 0
                                    },

                                    columns: [
                                        {
                                            text     : 'Name',
                                            flex     :  2,
                                            dataIndex: 'name'
                                        },
                                        {
                                            text     : 'CPU',
                                            flex     :  1,
                                            dataIndex: 'vcpus_max'
                                        },
                                        {
                                            text     : 'Memory',
                                            flex     :  1,
                                            dataIndex: 'memory_max',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }
                                        },
                                        {
                                            text     : 'Storage Size',
                                            flex     :  1,
                                            dataIndex: 'storage',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.getTemplateTotalStorageSize(value);
                                            }

                                        },                  
                                        {
                                            text     : 'Description',
                                            flex     :  3,
                                            dataIndex: 'description'
                                        },                
                                    ],
                                },
                            ]
                        },

                        {
                            xtype: 'form',
                            id: 'form_newinstance_name',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Virtual Machine Name','')
                                },    
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please provide basic VM information','')
                                },                                
                                {
                                    fieldLabel : "Instance Name",
                                    id: 'text_instance_name',
                                    name: 'instance_name',
                                    value: "Netra",   
                                    allowBlank: false,      
                                    invalidText: 'Please give an instance name!!!',
                                },
                                {
                                    xtype: 'numberfield',                                    
                                    fieldLabel : "Instance Count",
                                    name : 'instance_count',
                                    id: 'text_instance_count',
                                    value: 1,
                                    minValue: 1,
                                    maxValue: 99,
                                    allowBlank: false,
                                    invalidText: 'Please type intance count',
                                },
                                {
                                    xtype : 'textareafield',
                                    fieldLabel : 'Description',
                                    id: 'text_instance_description',
                                    name: 'text_instance_description',
                                    grow: true,
                                    allowBlank : true,          
                                    height: 200,
                                    value: "",
                                }
                            ]
                        },

                        {
                            xtype: 'form',
                            id: 'form_newinstance_xenhost',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Xen Host','')
                                },    
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('Please select a xen host for VM provision','')
                                },

                                {
                                    xtype: 'gridpanel',
                                    id: 'grid_newvm_xenhost',
                                    //reference: 'grid_host_instance',
                                    //title: 'Host List',
                                    height: 300,
                                    stripeRows: true,
                                    columnLines: true,

                                    store: Netra.manager.getStore('storeXenHosts'),

                                    selType: 'checkboxmodel',
                                    selModel: {
                                        checkOnly: true,
                                        injectCheckbox: 0
                                    },

                                    columns: 
                                    [
                                        {
                                            text     : 'Name',
                                            flex     : 1,
                                            sortable : true,
                                            dataIndex: 'name'
                                        },                                    
                                        {
                                            text     : 'IP',
                                            flex     : 2,
                                            sortable : true,
                                            dataIndex: 'ip'
                                        },
                                        {
                                            text     : 'CPU',
                                            flex     : 1,
                                            dataIndex: 'cpu_count',
                                        },
                                        {
                                            text     : 'Total Memory',
                                            flex     : 1,
                                            dataIndex: 'total_memory',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }                                            
                                        },
                                        {
                                            text     : 'Free Memory',
                                            flex     : 1,
                                            dataIndex: 'free_memory',
                                            renderer : function(value) 
                                            {
                                                return Netra.helper.formatByteNumber(value);
                                            }                                            

                                        },
                                    ],

                                }

                            ]
                        },                       

                        {
                            xtype: 'form',
                            id: 'form_newinstance_finish',
                            border: false,
                            autoScroll: true,
                            bodyPadding: '20 30 30 30',

                            defaults: {
                                xtype: "textfield",
                                //align: 'stretch',
                                anchor: '100%',
                                labelWidth: 140,
                            },

                            items: 
                            [
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    html: Netra.helper.createFormTitle('Finish','')
                                },  
                                {
                                    xtype: 'component',
                                    frame: false,
                                    border: 0,
                                    margin: '0 0 0 0',
                                    html: Netra.helper.createFormSubTitle('VM Provisioning Parameters','')
                                },
                                {
                                    xtype: 'propertygrid',
                                    id: 'grid_newvm_finish',
                                    title: 'General',
                                    titleAlign: 'center',
                                    rowspan: 2,
                                    nameColumnWidth: 140,
                                    source: {},
                                    sourceConfig: {
                                        borderWidth: {
                                            displayName: 'Border Width'
                                        },
                                        tested: {
                                            displayName: 'QA'
                                        }
                                    }                                            
                                },


                            ]
                        },

                    ],

                },

            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    id: 'toolbar_new_window',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { xtype: 'component', flex: 1 },
                        //{ xtype: 'button', id: 'btn_new_create', text: 'Create' },
                        { xtype: 'button', id: 'btn_new_create', text: 'Create' },
                        { xtype: 'button', id: 'btn_new_close', text: 'Close' }
                    ]
                }
            ],

        });

        this.onStart();
    },

    show: function()
    {
        console.debug("NewInstance.show");
        this.instanceWindow.show();     
    },


    loadPools: function()
    {
        console.log("NewVM.loadPools");

        var store = Netra.manager.getStore('storeClusters');

        store.load({
            callback: this.onPoolLoadComplete,
            params: {},
            scope: this
        });
    },

    addTemplateNodeToTree: function(text,record)
    {
        var rootNode = this.getTreeMenu().getRootNode();
        var node = this.addNodeToTree(this.getTreeMenu(),rootNode, text, 'openstack', record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    updateProgressBar: function(pbar,max,used,item)
    {
        pbar.updateProgress(used/max, 'Total ' + item + ' ' + used + '  of ' + max + ' Used');
    },

    updateFinishForm: function()
    {
        console.debug("NewVM.updateFinishForm");

        var params_general = {};
        
        if (this.poolRecord) {
            params_general['Pool'] = this.poolRecord.data.name;
        }
        
        if (this.templateRecord) {
            params_general['Template Name'] = this.templateRecord.data.name;
            params_general['vCPU'] = this.templateRecord.data.vcpus_max;
            params_general['Memory'] = Netra.helper.formatByteNumber(this.templateRecord.data.memory_max);
            params_general['Storage'] = Netra.helper.getTemplateTotalStorageSize(this.templateRecord.data.storage);
        }
        
        if (this.xenhostRecord) {
            params_general['XenHost'] = this.xenhostRecord.data.name;
            params_general['Host Total Memory'] = Netra.helper.formatByteNumber(this.xenhostRecord.data.total_memory);
            params_general['Host Used Memory'] = Netra.helper.formatByteNumber(this.xenhostRecord.data.used_memory);
        }

        params_general['VM Name'] = Netra.helper.getComponent('#text_instance_name').getValue();
        params_general['Description'] = Netra.helper.getComponent('#text_instance_description').getValue();
        
        Netra.helper.updatePropertyGrid('#grid_newvm_finish', params_general);

    },

    setActiveForm: function(form_index)
    {
        var panel = this.getPanelContents();
        console.debug("NewVM.setActiveForm : form id = " + form_index + " , panel = ", panel);
        
        for (var i=0; panel.items.length > i; i++) 
        {
            panel.items.items[i].hide();
        }

        panel.items.items[form_index].show();
        //panel.getLayout().setActiveItem(form_index);

        if (form_index == 3) {
            this.updateFinishForm();
        }
    },

  
    validateForm: function()
    {
        console.debug("NewVM.validateForm");

        if (! this.poolRecord) {
            this.setActiveForm(0);
            return false;
        }

        if (! this.templateRecord) {
            this.setActiveForm(0);
            return false;
        }

        form = Netra.helper.getComponent('#form_newinstance_name');
        if (! form.isValid()) {
            this.setActiveForm(1);
            return false;
        }

        if (! this.xenhostRecord) {
            this.setActiveForm(2);
            return false;
        }

        return true;
    },

    getNewInstanceParameterInJson: function()
    {
        console.debug("NewVM.getNewInstanceParameterInJson ");

        var json = {};

        json['pool'] = this.poolRecord.data.name;
        json['vm_name'] = Netra.helper.getComponent('#text_instance_name').getValue();
        json['description'] = Netra.helper.getComponent('#text_instance_description').getValue();
        json['template_ref'] = this.templateRecord.data.reference;
        json['host_ref'] = this.xenhostRecord.data.reference;

        return json;
    },








    onStart: function()
    {
        console.debug("Window.NewVM.onStart");
        
        this.loadPools();
        //this.loadFlavors();
        //this.updateUsageForm();
        //this.loadVolumeLimit();
    },

    onButtonCloseClick: function()
    {
        console.debug("NewInstance.close : instanceWindow = " , this.instanceWindow);

        //this.instanceWindow.hide();
        this.instanceWindow.close();
        Netra.manager.removeController(this);
    },

    onButtonCreateClick: function()
    {
        console.debug("NewVM.onButtonCreateClick : instanceWindow = " , this.instanceWindow);

        if (! this.validateForm()) {
            Ext.MessageBox.alert('Error', 'Please fill out the form');
            return;
        }

        var json = this.getNewInstanceParameterInJson();
        console.debug('NewVM.onButtonCreateClick : json = ', json);

        Netra.task.sendVMProvision(json, this.onVMProvisionComplete);
    },


    onBroadcastMessage: function(param)
    {
        console.debug("onBroadcastMessage");
    },

    onPoolLoadComplete: function(records, operation, success) 
    {
        var _this = this;

        console.log("NewVM.onPoolLoadComplete : length = " + records.length);
        Netra.helper.getComponent('#combo_instance_pool').bindStore(Netra.manager.getStore('storeClusters'));
    },

    onLoadTemplateComplete: function(ret_code,json)
    {
        console.debug("NewVM.onLoadTemplateComplete");
        //Netra.helper.getComponent('#grid_template').bindStore(Netra.manager.getStore('storeTemplates'));
    },

    onLoadXenHostComplete: function(ret_code,json)
    {
        console.debug("NewVM.onLoadXenHostComplete");
        //Netra.helper.getComponent('#grid_template').bindStore(Netra.manager.getStore('storeTemplates'));
    },

    onPoolChanged: function(combo, newValue, oldValue, eOpts )
    {
        var store = Netra.manager.getStore('storeClusters');

        var record = store.findRecord('id',newValue);
        
        console.debug("NewVM.onPoolChanged : record = ", record);
        
        if (record == null) return;

        this.poolRecord = record;

        Netra.service.loadBaseTemplateData(this.poolRecord.data.name, this.onLoadTemplateComplete)
        Netra.service.loadXenHostData(this.poolRecord.data.name, this.onLoadXenHostComplete)
    },

    onVolumeChanged: function(combo, newValue, oldValue, eOpts )
    {
        var store = Netra.manager.getStore('storeVolumes');
        var record = store.findRecord('id',newValue);
        
        console.debug("NewVM.onVolumeChanged : record = ", record);
        
        if (record == null) return;

        this.getTextVolumeSize().setValue(record.data.size);
        this.getTextVolumeZone().setValue(record.data.availability_zone);
        this.getTextVolumeType().setValue(record.data.volume_type);

    },

    onGridColumnDoubleClick: function(grid, record, item, index, e, eopts) 
    {
        console.debug("NewVM.onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.templateRecord = record;
        grid.getSelectionModel().select(index);
    },

    onXenHostGridColumnDoubleClick: function(grid, record, item, index, e, eopts) 
    {
        console.debug("NewVM.onXenHostGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.xenhostRecord = record;
        grid.getSelectionModel().select(index);
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("NewVM.onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        console.debug(record);
        
        this.setActiveForm(index);
        //this.providerRecord = record;
        
        //this.getPanelContents().items.items[index].expand();
    },

    onTenantNetworkChange : function(thisFormField, newValue, oldValue, eOpts) 
    {
        console.debug("NewVM.onTenantNetworkChange");
        console.debug(thisFormField);
        console.debug(newValue.network_model_multi);

        this.getTextVlanRange().enable();    
        if (newValue.network_model_multi == 1) {
            this.getTextVlanRange().disable();    
        }
        
    },

    onTreeMenuRendered: function() 
    {
        console.debug("NewVM.onTreeMenuRendered");
        //this.selectFirstNode();
    },

    onVMProvisionComplete: function(retcode, json)
    {
        console.debug("NewVM.onVMProvisionComplete");
        if (json.ret_code == 0) {
            Ext.MessageBox.alert('Success', 'New VM has created and started!!');
        } else {
            Ext.MessageBox.alert('Error', json['msg']);
        }
    },

});