Ext.define("Netra.view.component.window.NewVolume", {
	extend: 'Ext.app.Controller',
	alias: 'controller.newvolume',
	id: 'newvolume',
	reference: 'newvolume',

    requires: [ 
        'Ext.toolbar.*',
    ],

    refs: [
    	{ ref: 'formInput', selector: '#form_newvolume' },
    	{ ref: 'formTitle', selector: '#form_title' },
		{ ref: 'buttonCreate', selector: '#btn_volume_window_create' },
		{ ref: 'textRecord', selector: '#text_newvolume_record' },    	
		{ ref: 'comboZone', selector: '#combo_availability_zone' },    	
		{ ref: 'comboType', selector: '#combo_volume_type' },    			
    ],



    models: [ ],    
    stores: [ ],    

	volumeWindow: null,
	volumeRecord: null,


    init: function() 
    {
    	console.debug("NewVolume.init");

		//this.callParent(arguments);

        this.control({
            '#btn_volume_window_close': { click: this.onButtonCloseClick },
            '#btn_volume_window_create': { click: this.onButtonCreateClick },

/*            
            '#combo_public_ip': { change: this.onHostChanged },
            '#combo_public_ip_multi': { change: this.onHostChanged },
            '#combo_compute_ip_multi': { change: this.onComputeChanged },
            '#tree_openstack': {
            	itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },
*/            
        });


        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 


    	this.volumeWindow = Ext.create('Ext.window.Toast', 
    	{
		    title: 'Add New Volume',
		    height: 450,
		    width: 600,

            align: 't',
            autoClose: false,
            autoDestroy: false,
            slideInDuration: 500,
            slideBackDuration: 500,
		    
		    id: 'window_security',
		    reference: 'windowSecurity',
		    //animCollapse: true,
		    //collapseDirection: 'top',
			//animateTarget: Ext.getDoc(),

		    layout: {
		    	type: 'fit',
		    	pack: 'start',
		    	align: 'stretch',
		    },

		    modal: true,
			onEsc: function() {},
			closable: false,
		    
			items : [
		        {
		        	xtype: 'form',
		        	border: false,
                    layout: {
                    	type: 'vbox',
				        align: 'stretch',
				        anchor: '100%',                    	
                    },
				    defaults: {
				    	xtype: "textfield",
				        align: 'stretch',
				        anchor: '100%',
				        labelWidth: 160,
				    },
					bodyPadding: '15 30 30 30',
					id: 'form_newvolume',

                    items: [
						{
							xtype: 'component',
							id: 'form_title',
							frame: false,
							border: 0,
							html: Netra.helper.createFormTitle('New Volume','')
						},	                    
                    	{
							fieldLabel : "ID",
							name: 'id',
							value: "",
							hidden: false,
                    	},
                    	{
							//fieldLabel : "Record",
							xtype: 'hiddenfield',
							id: 'text_newvolume_record',
							name: 'record',
							value: "",
							//hidden: false,
                    	},						                    	
                    	{
							fieldLabel : "Name",
							id: 'text_tenant_name',
							name: 'display_name',
							value: "",			
                    	},
                    	{
                            xtype : 'textareafield',
                            fieldLabel : 'Description',
                            id: 'area_description',
                            name: 'display_description',
                            grow: true,
                            allowBlank : true,          
                            height: 60,
                            value: "Please Description",            
                    	},
                        {
                            xtype:'combo',
                            fieldLabel:'Volume Type',
                            id:'combo_volume_type',
                            name:'volume_type',
                            valueField: 'name',
                            displayField:'name',
                            queryMode:'local',
                            store: {
                            	type: 'volumetypes',
                            	autoLoad: true,
                            },
                            editable: false,
                            autoSelect:true,
                            forceSelection:true,
                        },
                    	{
							fieldLabel : "Size (GB)",
							xtype: 'numberfield',
							id: 'text_volume_size',
							name: 'size',
							value: "",	
							maskRe: /[0-9.]/,		
                    	},
                        {
                            xtype:'combo',
                            fieldLabel:'Availability Zone',
                            id:'combo_availability_zone',
                            name:'availability_zone',
                            valueField: 'zoneName',
                            displayField:'zoneName',
                            queryMode:'local',
                            store: {
                            	type: 'zones',
                            	autoLoad: true,
                            },
                            editable: false,
                            autoSelect:true,
                            forceSelection:true,
                        },
                    ]
                },

			],

			dockedItems: 
			[
			    {
			        xtype: 'toolbar',
			        dock: 'bottom',
			        ui: 'footer',
			        //defaults: {minWidth: minButtonWidth},
			        items: [                    
			            { xtype: 'component', flex: 1 },
			            { text: 'Create', id: 'btn_volume_window_create', reference: 'btn_volume_window_create' },
			            { text: 'Close', id: 'btn_volume_window_close' },
			        ]
			    }
			],

		});

    },

    show: function(record)
    {
		this.volumeWindow.show();
		this.volumeRecord = record;

		if (this.isUpdateMode())
		{
			this.getComboType().setDisabled(true);
			this.getComboZone().setDisabled(true);
			this.getButtonCreate().setText('Update');
			this.getFormTitle().update(Netra.helper.createFormTitle('Edit Volume',''));
			this.updateForm(this.volumeRecord);
			return;
		}

		this.getComboType().setDisabled(false);
		this.getComboZone().setDisabled(false);
		this.getButtonCreate().setText('Create');		
		this.getFormTitle().update(Netra.helper.createFormTitle('New Volume',''));
    },

    close: function()
    {
    	console.debug("NewvolumeWindow.close : volumeWindow = " , this.volumeWindow);

    	this.volumeWindow.close();
    	Netra.manager.removeController(this);
    },

    isUpdateMode: function()
    {
    	if (this.volumeRecord) {
    		return true;
    	}
    	return false;
    },

	sendRequest: function(url,json)
	{
		var _this = this;

		console.debug("NewVolumeWindow.sendRequest");
		
		Ext.Ajax.request({
		    url: url,
		    method: 'POST',
		    jsonData: json,
		    success: function(response, opts) 
		    {
		        console.debug('sendParameters Response = ', response);
		        var json = JSON.parse(response.responseText);
		        if (json.retcode == 0) 
		        {
		        	var mgs = "Your Volume is created!!!";
		        	if (_this.isUpdateMode()) {
						mgs = "Your Volume is updated!!!";
		        	}

		        	Ext.MessageBox.alert('Success', msg);

		        } else {
		        	Ext.MessageBox.alert('Error', json.msg);
		        }
		        	
		    },
		    failure: function(response, opts) 
		    {
		        console.debug('sendParameters : woops');
		    }
		});

	},

	getParameter: function()
	{
		console.debug('NewVolumeWindow.getParameter');

		var json = {};

		var form = this.getFormInput();

        form.query('.textfield, .hiddenfield').forEach(function(component) 
        {
            json[component.name] = component.value;
        });

        return json;
	},

    saveToServer: function()
    {
		var form = this.getFormInput();

		console.debug("volumeWindow.saveToServer : form = ", form);
		
		var json = this.getParameter();

		var url = "/api/openstack/volumes/new";
		if (this.volumeRecord) {
			url = "/api/openstack/volumes/update";
		}

		this.sendRequest(url,json);
    },

    updateForm: function(record)
    {
		console.debug('NewVolume.updateForm : record = ', record);

		var form = this.getFormInput();

        form.query('.textfield').forEach(function(component) 
        {
            if (record.data.hasOwnProperty(component.name)) {
            	component.setValue(record.data[component.name]);
            }
        });

        this.getTextRecord().setValue(JSON.stringify(record.data));
    },




    onStart: function()
    {
		console.debug("volumeWindow.onStart");
    },


	onButtonCloseClick: function()
	{
		console.debug("volumeWindow.onButtonCloseClick : controller = ", this);
		this.close();
	},

	onButtonCreateClick: function()
	{
		console.debug("volumeWindow.onButtonCreateClick");
		this.saveToServer();
	},


	onBroadcastMessage: function(param)
	{
		console.debug("onBroadcastMessage");
	},


});