Ext.define('Netra.window.Config', {
    extend: 'Ext.app.Controller',
    id: 'addHost',

    refs: [
        { ref: 'progressBar', selector: '#pbar_progress' },    
        { ref: 'textIp', selector: '#text_ip' },
        { ref: 'textUserid', selector: '#text_userid' },
        { ref: 'textPassword', selector: '#text_password' },
        { ref: 'areaLog', selector: '#area_output' },
        { ref: 'btnAddHost', selector: '#btn_addhost' },
    ],

    hostWindow: null,
    doesRefreshStore: false,


    init: function() 
    {
    	console.debug("window.init");

        this.control({
            '#btn_close': { click: this.onCloseButtonClick },
            '#btn_addhost': { click: this.onAddhostButtonClick },
        });

        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
            }
        }); 

    },

    popupWindow: function()
    {
		console.debug("window.popupWindow");

    	this.hostWindow = Ext.create('Ext.window.Window', {
		    title: 'Add New Physical Host',
		    layout: {
		    	type: 'vbox',
		    	align: 'stretch',
		    },
		    height: 600,
		    width: 600,
		    modal: true,
		    
		    items: [{
		    	xtype: 'form',
			    bodyBorder: false,
			    border: false,
				padding: '20 20 20 20',

			    layout: {
			        type: 'vbox',
			        align: 'stretch',
			    },
			    defaults: {
			    	xtype: "textfield",
			        anchor: '100%',
			        labelWidth: 130
			    },    	
				items : [
					{
						xtype: 'panel',
						frame: false,
						border: 0,
						html: Netra.Helper.createFormTitle('Add New Physical Host','Please fill out following columns')
					},	
					{
						fieldLabel : "Proxy URL",
						id: 'text_ip',
						name: 'ip',
						value: "192.168.56.159",			
					},
					{
						fieldLabel : "Netra Server URL",
						id: 'text_userid',
						name: 'userid',
						value: "root",			
					},
					{
						fieldLabel : "Test",
						id: 'text_password',
						name: 'password',
						value: "vagrant",			
					},

				]
				}, 
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
						pack: 'center',
						align: 'middle'
					},
					defaults: {
						margin: '20 5 0 0'
					},

					items: [
						{
							xtype: 'button',
							text: 'Add Host',
							id: 'btn_addhost',
							iconCls: Icons.btnValidate,
						},
					]
				},
		    ],


		    dockedItems: [
		        {
		            xtype: 'toolbar',
		            dock: 'bottom',
		            alias: 'widget.toolbar_window',
		            id: 'toolbar_window',
		            ui: 'footer',
		            //defaults: {minWidth: minButtonWidth},
		            items: [
		            	{ xtype: 'component', flex: 1 },
		                { 
		                    xtype: 'button', id: 'btn_close', text: 'Close',
		                }                
		            ]
		        }
		    ],
		    
    	});
		
		this.hostWindow.show();
    },

    updateProgressbar:function(retcode)
    {
        var caption = "";
        if (retcode == 0) {
        	caption = "Bootsrapping Done Successfully!!!";
			this.doesRefreshStore = true;
        } else {
        	caption = "Bootstrapping Failed!!!";
        }

		this.getProgressBar().updateText(caption);
		this.getProgressBar().reset();
    },


    onAddhostButtonClick: function() 
	{
		console.debug("add host button clicked");
		
		this.getProgressBar().updateText('Bootstrapping in progress');
		this.getProgressBar().wait();

		Ext.Ajax.request({
		    url: '/api/resource/bootstrap',
		    method: 'POST',
		    params: { 
		    	ip:  this.getTextIp().getValue(),
		    	userid: this.getTextUserid().getValue(),
		    	password: this.getTextPassword().getValue()
		    },
		    success: function(response, opts) 
		    {
		        console.log('sendParameters : success');
		        //Ext.MessageBox.alert('Success', 'The node is successfully bootstrapped');
		    },
		    failure: function(response, opts) 
		    {
		        console.log('sendParameters : woops');
		    }
		});
	},

    onCloseButtonClick: function() 
    {
    	console.debug("btn_close clicked");

    	if (this.doesRefreshStore) {
    		Ext.globalEvents.fireEvent('refreshHostStore');
    	}
    	this.hostWindow.close();
    },

    onBroadcastMessage: function(param)
    {
        console.debug("onBroadcastMessage!!!");
        console.debug(param);        

		this.updateProgressbar(param.retcode);
        this.getAreaLog().setValue(param.msg); 
    },

});
