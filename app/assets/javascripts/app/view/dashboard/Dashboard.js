Ext.define('Netra.view.dashboard.Dashboard', {
    extend: 'Ext.panel.Panel',
    id: 'panel_dashboard',

    requires: [
        'Chart.ux.Highcharts',
        'Chart.ux.Highcharts.Serie',
        'Chart.ux.Highcharts.BarSerie',
        'Chart.ux.Highcharts.ColumnSerie',
        'Chart.ux.Highcharts.GaugeSerie',
        'Chart.ux.Highcharts.SolidGaugeSerie',
        'Chart.ux.Highcharts.LineSerie',
        'Chart.ux.Highcharts.PieSerie',

        'Netra.view.dashboard.DashboardController',
    ],

    layout: {
        type: 'vbox',
        align : 'stretch',
        pack  : 'start'                                
    },
    
    controller: 'dashboard',

    autoScroll: false,
    
    items: 
    [
        {
            xtype: 'panel',
            title: 'Dashboard',
            titleAlign: 'center',

            //height: 210,
            
            layout: {
                type: 'hbox',
                align : 'stretch',
                pack  : 'start'                        
            },
            defaults: {
                margin: '10 10 10 10',
                flex: 1,
            },
            items: [
                { xtype: 'numberlabel', title: 'Total VDI', number: '', id: 'number_dashboard_total_vdi', icon: '', },            
                { xtype: 'numberlabel', title: 'Running VDI', number: '', id: 'number_dashboard_running_vdi', icon: '', },            
                { xtype: 'numberlabel', title: 'Total VM', number: '', id: 'number_dashboard_total_vm', icon: '',},            
                { xtype: 'numberlabel', title: 'Total Host', number: '', id: 'number_dashboard_total_host', icon: '', },            
            ]
        },    
        {
            xtype: 'panel',
            //title: 'VDI Dashboard',
            titleAlign: 'center',
            border: false,
            flex: 2,
            id: 'panel_chart_resource',
            layout: {
                type: 'hbox',
                align : 'stretch',
                pack  : 'start'                        
            },
            defaults: {
                margin: '10 10 10 10',
            },
        },        
        {
            xtype: 'panel',
            title: 'Resource Usage',
            titleAlign: 'center',

            layout: {
                type: 'hbox',
                align : 'stretch',
                pack  : 'start'                        
            },
            defaults: {
                margin: '10 10 10 10',
                flex: 1,
            },
            items: [
                { xtype: 'numberlabel', title: 'Total Cores', number: '', id: 'number_dashboard_cpu', icon: '', },            
                { xtype: 'numberlabel', title: 'Total Memory', number: '', id: 'number_dashboard_memory', icon: '',},            
                { xtype: 'numberlabel', title: 'Total Disk', number: '', id: 'number_dashboard_disk', icon: '',},            
            ]
        },            
        
/*        
        {
            xtype: 'panel',
            id: 'panel_dashboard_list',
            title: '',
            titleAlign: 'center',
            border: false,
            //flex: 1,

            layout: {
                type: 'table',
                columns: 2,
                tableAttrs: { 
                    style: { 
                        width: '100%',
                        height: '100%',
                        //padding: '20 20 20 20',
                    } 
                }, 
                tdAttrs: { 
                    style: 'vertical-align: top; width:50%;padding-right: 20px;' 
                }
            },

            items: 
            [
                {
                    xtype: 'gridpanel',
                    id: 'grid_dashboard_cluster',
                    title: 'Cluster List',

                    //layout: 'fit',
                    //width: '50%',
                    //stripeRows: true,
                    //colspan: 1,

                    header: {
                        titlePosition: 0,
                        items: [
                            {
                                xtype: 'button',
                                text: 'Detail',
                                id: 'btn_dashboard_cluster_detail',
                            }
                        ]
                    },


                    store: Netra.manager.getStore('storeClusters'),

                    columns: [
                        {
                            text     : 'id',
                            flex     :  1,
                            sortable : false,
                            hidden   : false,
                            dataIndex: 'id'
                        },
                        {
                            text     : 'Name',
                            flex     :  3,
                            sortable : true,
                            //renderer : 'usMoney',
                            dataIndex: 'name'
                        },        
                        {
                            text     : 'Owner',
                            flex     :  2,
                            sortable : true,
                            //renderer : 'usMoney',
                            dataIndex: 'creator'
                        },                        
                        {
                            text     : 'Description',
                            flex     :  2,
                            sortable : true,
                            //renderer : this.pctChange,
                            dataIndex: 'description'    
                        }
                    ],
                },

                {
                    xtype: 'gridpanel',
                    id: 'grid_dashboard_host',
                    title: 'Host List',
                    //layout: 'fit',
                    //width: '50%',
                    //stripeRows: true,
                    //columnLines: true,

                    header: {
                        titlePosition: 0,
                        items: [
                            {
                                xtype: 'button',
                                text: 'Detail',
                                id: 'btn_dashboard_host_detail',
                            }
                        ]
                    },

                    store: Netra.manager.getStore('storeHosts'),

                    columns: 
                    [
                        {
                            text     : 'id',
                            flex     : 1,
                            sortable : false,
                            hidden   : false,
                            dataIndex: 'id'
                        },
                        {
                            text     : 'IP',
                            flex     : 2,
                            sortable : true,
                            dataIndex: 'ip'
                        },
                        {
                            text     : 'Name',
                            flex     : 2,
                            sortable : true,
                            dataIndex: 'name'
                        },
                        {
                            text     : 'OS',
                            flex     : 2,
                            sortable : true,
                            dataIndex: 'os'
                        }
                    ],
                },

            ]
        }
*/        
    ]

});
