Ext.define('Netra.view.dashboard.DashboardController', 
{
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.dashboard',

    require: [
        //'Netra.store.HypervisorStats',
    ],

    refs: [
        { ref: 'panelContent', selector: '#panel_container' },
    ],

    storeCpu: null,
    storeMemory: null,
    storeDisk: null,
    storeInstance: null,
    storeSecurity: null,
    storeFloating: null,

    chartCpu: null,
    chartDisk: null,
    chartRam: null,
    chartInstance: null,
    chartFloating: null,
    chartSecurity: null,


    init: function() 
    {
        console.debug("MainController.init");

        this.control({
            '#btn_flavor': { click: this.onBtnFlavorClick },
            '#btn_instance': { click: this.onBtnInstanceClick },
            '#btn_image': { click: this.onBtnImageClick },
            '#btn_report': { click: this.onBtnReportClick },
            '#btn_config': { click: this.onBtnConfigClick },
            '#btn_dashboard': { click: this.onBtnDashboardClick },
            '#btn_dashboard_cluster_detail': { click: this.onBtnDashboardClusterClick },
            '#btn_dashboard_host_detail': { click: this.onBtnDashboardHostClick },
            '#panel_dashboard': { beforedestroy: this.onDestroy }, 
        });

        this.onStart();
    },

    createDataStore: function()
    {
        this.storeCpu = new Ext.data.Store({
            fields: [ { name: 'label' } , { name: 'value' } ],
            storeId: 'storeCpu',
            autoDestroy: false,
        });

        this.storeRam = new Ext.data.Store({
            fields: [ { name: 'label' } , { name: 'value' } ],
            storeId: 'storeRam',
            autoDestroy: false,
        });

        this.storeDisk = new Ext.data.Store({
            fields: [ { name: 'label' } , { name: 'value' } ],
            storeId: 'storeDisk',
            autoDestroy: false,
        });

        this.storeInstance = new Ext.data.Store({
            fields: [ { name: 'label' } , { name: 'value' } ],
            storeId: 'storeInstance',
            autoDestroy: false,
        });

        this.storeFloating = new Ext.data.Store({
            fields: [ { name: 'label' } , { name: 'value' } ],
            storeId: 'storeFloating',
            autoDestroy: false,
        });

        this.storeSecurity = new Ext.data.Store({
            fields: [ { name: 'label' } , { name: 'value' } ],
            storeId: 'storeSecurity',
            autoDestroy: false,
        });

    },

    loadHypervisorStatData: function()
    {
        console.debug("DashboardController.loadHypervisorStatData");

        var store = Netra.manager.getStore('storeHypervisorStats');

        store.load({
            callback: this.onHypervisorStatLoad,
            params: {},
            scope: this
        });

    },

    loadLimitsData: function()
    {
        console.debug("DashboardController.loadLimitsData");

        var store = Netra.manager.getStore('storeLimits');

        store.load({
            callback: this.onLimitsLoad,
            params: {},
            scope: this
        });

    },

    drawChart: function(store, data, chart_ref)
    {
        store.loadRawData( data );

        //console.debug("drawChart : chart ref = " + chart_ref + " , count = " + store.getTotalCount());
        //console.debug("drawChart : record = ", store.getAt(0));

        var chart = Netra.helper.getComponent('#'+chart_ref);
        chart.bindStore(store);
    },

    drawChartWithData: function(data, chart_ref)
    {
        console.debug("drawChart : chart ref = " + chart_ref + " , data = ", data);

        var chart = this.getReference(chart_ref);
        var series = chart.series[0];

        console.debug("drawChartWithData : series.data = " , series.data);

        chart.setData(0,data);
    },

    createPiePanelChart: function(title,chart_ref)
    {
        var panel = Ext.create('Ext.panel.Panel', 
        {
            title: title,
            titleAlign: 'center',
            layout: 'fit',
            flex: 1,
        });

        var chart = Ext.create('Chart.ux.Highcharts', 
        {
            reference: chart_ref,
            initAnimAfterLoad: false,
            layout: 'fit',
            id: chart_ref,
            debug: false,
            flex: 1,

            chartConfig: {
              chart: {
                //showAxes: true,
              },
              legend: { enabled: false },

              title: {
                text: ''
              },
  
              xAxis: {},
              yAxis: {},
                credits: {
                    enabled: false
                },

            },
            series: 
            [
                {
                    type: 'pie',
                    categorieField: 'label',
                    dataField: 'value',
                    size: '80%',
                    totalDataField: true,
                }
            ],
            
        });

        panel.add(chart);

        return panel;
    },

    createGaugePanelChart: function(title,chart_ref)
    {
        var panel = Ext.create('Ext.panel.Panel', 
        {
            title: title,
            titleAlign: 'center',
            layout: 'fit',
            flex: 1,
        });

        var chart = Ext.create('Chart.ux.Highcharts', 
        {
            reference: chart_ref,
            initAnimAfterLoad: false,
            layout: 'fit',
            id: chart_ref,
            debug: false,
            flex: 1,

            chartConfig: 
            {
                chart: {
                    type: 'bar',
                },

                title: title,

                xAxis: {
                    categories: [ 
                        'label',
                    ]
                },
                yAxis: {
                    min: 0,
                },
                credits: {
                    enabled: false
                },
                legend: {
                    reversed: true
                },

                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
   
            },
      
            series: 
            [
                {
                    //categorieField: 'label',
                    //dataField: 'value'
                }
            ]
        });

        panel.add(chart);

        return panel;
    },


    addChartToPanel: function(panel_ref,chart)
    {
        Netra.helper.getComponent('#'+panel_ref).add(chart);
        //this.getReference(panel_ref).add(chart);
    },

    createSummaryChart: function()
    {   
        console.debug("DashboardController.createSummaryChart");

        this.chartCpu = this.createPiePanelChart('CPU', 'chart_cpu');
        this.addChartToPanel('panel_chart_resource',this.chartCpu);

        this.chartRam = this.createPiePanelChart('Memory', 'chart_ram');
        this.addChartToPanel('panel_chart_resource',this.chartRam);

        this.chartDisk = this.createPiePanelChart('Disk', 'chart_disk');
        this.addChartToPanel('panel_chart_resource',this.chartDisk);
    },

 
    createLimitChart: function()
    {   
        console.debug("DashboardController.createLimitChart");

        this.chartInstance = this.createPiePanelChart('Instance', 'chart_instance');
        this.addChartToPanel('panel_chart_limit',this.chartInstance);

        this.chartFloating = this.createPiePanelChart('Floating IPs', 'chart_floating');
        this.addChartToPanel('panel_chart_limit',this.chartFloating);

        this.chartSecurity = this.createPiePanelChart('Security Group', 'chart_security');
        this.addChartToPanel('panel_chart_limit',this.chartSecurity);

    },


    displayLimitChart: function(record)
    {
        console.debug("DashboardController.displayLimitChart");

        var instanceData = [
            { "label" : "Total Instance", "value" : record.data.maxTotalInstances },
            { "label" : "Used Instance", "value" : record.data.totalInstancesUsed }
        ];
        this.drawChart(this.storeInstance, instanceData, 'chart_instance');

        var floatingData = [
            { "label" : "Total Floating IPs", "value" : record.data.maxTotalFloatingIps },
            { "label" : "Used Floating IPs", "value" : record.data.totalFloatingIpsUsed }
        ];
        this.drawChart(this.storeFloating, floatingData, 'chart_floating');

        var securityData = [
            { "label" : "Total Security Group", "value" : record.data.maxSecurityGroupRules },
            { "label" : "Used Security Group", "value" : record.data.totalSecurityGroupsUsed }
        ];
        this.drawChart(this.storeSecurity, securityData, 'chart_security');

    },


    updateNumberPanel: function()
    {
        var _this = this;

        console.debug("DashboardController.updateNumberPanel");

        Netra.service.loadXenHostData('pool', function(retocde,json) {

            Netra.service.loadXenServerData('pool', function(retcode, json) {
                
                Netra.service.loadXenVDIData(function(retcode, json) {

                    console.debug("DashboardController.updateNumberPanel : loading store are done!!!");

                    var store_xenhost = Netra.manager.getStore('storeXenHosts');
                    var store_xenserver = Netra.manager.getStore('storeXenServers');
                    var store_vdi = Netra.manager.getStore('storeXenVDIs');

                    var running_vdi = 0;
                    store_vdi.each(function(record)
                    {
                        if (record.data.PowerState=="On") {
                            running_vdi += 1;
                        }
                    });

                    Netra.helper.getComponent('#number_dashboard_total_vdi').updateValue("Total VDI",store_vdi.getCount(),"");
                    Netra.helper.getComponent('#number_dashboard_running_vdi').updateValue("Running VDI",running_vdi,"");
                    Netra.helper.getComponent('#number_dashboard_total_vm').updateValue("Total VM",store_xenserver.getCount(),"");
                    Netra.helper.getComponent('#number_dashboard_total_host').updateValue("Total Host",store_xenhost.getCount(),"");

                    _this.updateResourceUsageChart();

                    Netra.helper.hideWaitDialog();
                });

            });

        });
    },

    updateResourceUsageChart: function()
    {
        console.debug("DashboardController.updateResourceUsageChart");

        var store_xenhost = Netra.manager.getStore('storeXenHosts');
        var store_xenserver = Netra.manager.getStore('storeXenServers');
        var store_vdi = Netra.manager.getStore('storeXenVDIs');

        var total_memory = 0, free_memory = 0, used_memory = 0;
        var total_storage = 0, used_storage = 0; 
        store_xenhost.each(function(record)
        {
            total_memory += parseInt(record.data.total_memory);
            free_memory += parseInt(record.data.free_memory);

            var disk_size = Netra.helper.getXenHostStorageSize(record.data.storage);

            //console.debug("DashboardController.updateResourceUsageChart : disk_size record = ",disk_size);
            //console.debug("DashboardController.updateResourceUsageChart : disk_size = ",disk_size);

            total_storage += parseInt(disk_size.total);
            used_storage += parseInt(disk_size.used);
        });
        used_memory = total_memory - free_memory;

        var total_cpu = 0, used_cpu = 0;
        store_xenserver.each(function(record)
        {
            total_cpu += parseInt(record.data.vcpus_max);
            total_memory += parseInt(record.data.memory_max);

            if (record.data.power_state.toLowerCase()=="running") {
                used_cpu += parseInt(record.data.vcpus_max);
            }
        });



        var cpuData = [
            { "label" : "VCPU", "value" : total_cpu },
            { "label" : "Used", "value" : used_cpu }
        ];

        this.drawChart(this.storeCpu, cpuData, 'chart_cpu');

        var ramData = [
            { "label" : "Memory (GB)", "value" : total_memory },
            { "label" : "Used (GB)", "value" : used_memory }
        ];
        //console.debug("DashboardController.updateResourceUsageChart : ramData = ",ramData);
        this.drawChart(this.storeRam, ramData, 'chart_ram');


        var diskData = [
            { "label" : "Disk (GB)", "value" : total_storage },
            { "label" : "Used (GB)", "value" : used_storage }
        ];
        //console.debug("DashboardController.updateResourceUsageChart : diskData = ",diskData);        
        this.drawChart(this.storeDisk, diskData, 'chart_disk');


        Netra.helper.getComponent('#number_dashboard_cpu').updateValue("Total Cores",total_cpu,"");
        Netra.helper.getComponent('#number_dashboard_memory').updateValue("Total Memory",Netra.helper.formatByteNumber(total_memory,0),"");
        Netra.helper.getComponent('#number_dashboard_disk').updateValue("Total Disk",Netra.helper.formatByteNumber(total_storage,0),"");

    },





    onStart: function()
    {
        console.debug("DashboardController.onStart");

        this.createDataStore();  


        Netra.helper.showWaitDialog("","",'btn_xen_refresh');

        this.updateNumberPanel();
        this.createSummaryChart();

        //this.loadDataStores();
/*        

        this.createLimitChart();

        this.loadHypervisorStatData();
        this.loadLimitsData();
*/
    },

    onLimitsLoad: function()
    {
        var store = Netra.manager.getStore('storeLimits');
        
        //console.debug("DashboardController.onHypervisorStatLoad : store = ", store);

        var record = store.getAt(0);
        console.debug("DashboardController.onLimitsLoad : record = ", record);

        this.displayLimitChart(record);
    },

    onHypervisorStatLoad: function()
    {
        var store = Netra.manager.getStore('storeHypervisorStats');

        var record = store.getAt(0);
        console.debug("DashboardController.onHypervisorStatLoad : record = ", record);

        this.displaySummaryChart(record);
    },

    onBtnFlavorClick: function() 
    {
        console.debug("onBtnFlavorClick");
        this.showView("Netra.view.flavor.Flavors");
    },

    onBtnInstanceClick: function()
    {
        console.debug("onBtnInstanceClick");
        this.showView("Netra.view.instance.Instances");
    },

    onBtnImageClick: function()
    {
        console.debug("onBtnImageClick");
        this.showView("Netra.view.image.Images");
    },

    onBtnReportClick: function()
    {
        console.debug("onBtnReportClick");
        this.showView("Netra.view.report.Report");
    },

    onBtnConfigClick: function()
    {
        console.debug("onBtnConfigClick");

        this.configWindowController = Netra.manager.createController("Netra.view.component.window.Config");
        this.configWindowController.show();        

    },

    onBtnDashboardClick: function()
    {
        console.debug("MainController.onBtnDashboardClick");
        this.showView("Netra.view.dashboard.Dashboard");
    },

    onBtnDashboardClusterClick: function()
    {
        console.debug("MainController.onBtnDashboardClusterClick");

        Ext.GlobalEvents.fireEvent('onRequestNewView', { view: 'Netra.view.cluster.Clusters' } );
    },

    onBtnDashboardHostClick: function()
    {
        console.debug("MainController.onBtnDashboardHostClick");

        Ext.GlobalEvents.fireEvent('onRequestNewView', { view: 'Netra.view.host.Hosts' } );
    },

    onDestroy: function()
    {
        console.debug("DashboardController.onDestroy");
    },

});
