Ext.define('Netra.view.host.Hosts', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.hosts',

    referenceHolder: true,
    //animCollapse: true,
    //collapsible: true,
    //collapseDirection: 'left',
    title: 'Host Manager',
    titleAlign: 'center',

    requires: [
        //'Ext.layout.container.Accordion',        
        'Netra.view.host.HostsController',        
    ],

    controller: 'hosts',
    id: 'container_host',
    
    layout: {
        type: 'card',
        //vertical: false,
        //animate: true,        
    },

    defaults: {
        titleAlign: 'center',
        autoScroll: true,
        //headerPosition: 'left',
        anchor: '100%',
        align: 'stretch',
    },
/*
    listeners: {
        'beforedestroy': function() {
            console.debug("Hosts.beforedestroy : control = ", this.controller);
            //this.removeAll();
            this.controller.destroy();

            //Netra.manager.removeController('Netra.view.host.HostsController');
        }        
    },
*/
    items: 
    [
        {
            xtype: 'gridpanel',
            id: 'grid_host_instance',
            title: '',
            flex: 1,
            //stripeRows: true,
            //columnLines: true,

            store: Netra.manager.getStore('storeHosts'),

            selType: 'checkboxmodel',
            selModel: {
                checkOnly: true,
                injectCheckbox: 0
            },
            
            ui: 'header',
            header: {
                //titlePosition: 1,
                margin: '5 5 5 5',
                items: 
                [
                    {
                        xtype: 'button', 
                        id: 'btn_host_new', 
                        text: 'New',
                    },        
                    { 
                        xtype: 'button', 
                        id: 'btn_host_refresh', 
                        text: 'Refresh'
                    },                                
                ]
            },

            columns: 
            [
                {
                    text     : 'id',
                    width     : 45,
                    sortable : false,
                    hidden   : false,
                    dataIndex: 'id'
                },
                {
                    text     : 'IP',
                    width    : 125,
                    sortable : true,
                    dataIndex: 'ip'
                },
                {
                    text     : 'Name',
                    width    : 125,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'name'
                },
                {
                    text     : 'Pool Name',
                    width    : 125,
                    dataIndex: 'cluster_name',
                },
                {
                    text     : 'User ID',
                    width    : 125,
                    dataIndex: 'userid',
                },
/*                
                {
                    text     : 'Password',
                    width    : 125,
                    dataIndex: 'password',
                },
*/
                {
                    text     : 'Updated At',
                    width    : 175,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'updated_at'
                },            
                {
                    text     : 'OS',
                    flex     : 1,
                    sortable : true,
                    //renderer : this.pctChange,
                    dataIndex: 'os'
                }
            ],
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: Netra.manager.getStore('storeHosts'),
                displayInfo: true,
                plugins: new Ext.ux.ProgressBarPager()
            },

        },
        {
            xtype: 'hostdetail',
            title: 'Host Detail',

            ui: 'header',
            header: {
                padding: '5 5 5 5',
                titlePosition: 1,
                items: [
                    { 
                        xtype: 'button', 
                        id: 'btn_host_form_back', 
                        text: 'Back',
                    },
                    { 
                        xtype: 'button', 
                        id: 'btn_host_edit', 
                        text: 'Edit',
                    },
/*                    
                    { 
                        xtype: 'button', 
                        id: 'btn_host_software', 
                        text: 'Software',
                    },
*/                    
                ]
            },

        },
    ],


    initComponent: function()
    {
        this.callParent(arguments);
        //console.debug("initComponent");
    }


});
