Ext.define('Netra.view.host.HostsController', {
    extend: 'Netra.controller.BaseController',
    alias: 'controller.hosts',

    refs: [
        { ref: 'panelHost', selector: '#panel_host' },
        { ref: 'textStart', selector: '#text_report_start' },
        { ref: 'textEnd', selector: '#text_report_end' },        
    ],


    hostRecord: null,
    gridPopupMenu: null,

    newSoftwareWindowController: null,
    newHostWindowController: null,


    init: function() 
    {
        console.debug("HostController.init");

        this.control({
            '#grid_host_instance': { 
                itemdblclick: this.onGridColumnDoubleClick,
                rowcontextmenu: this.onGridContextMenuClick,
            },
            '#btn_report_graph': { click: this.onButtonGraphClick },
            '#btn_host_new': { click: this.onButtonNewClick },
            '#btn_host_edit': { click: this.onButtonEditClick },            
            '#btn_host_form_back': { click: this.onButtonBackFormClick },
            '#btn_host_refresh': { click: this.onButtonRefreshClick },
            '#btn_host_software': { click: this.onButtonSoftwareClick },
            '#panel_host': { beforedestroy: this.onDestroy },
        });

        //this.getReference('chart_monitoring').bindStore( Netra.manager.getStore('storeMeterStats') );

        this.onStart();
    },

    loadHypervisorsData: function() 
    {
        console.debug("HostController.loadHypervisorsData");

        var store = Netra.manager.getStore("storeHypervisors");
        store.removeAll();

        store.load({
            callback: this.onHypervisorsLoaded,
            scope: this
        });

    },

    loadMetersData: function() 
    {
        console.debug("HostController.loadMetersData");

        Netra.helper.showPanelLoadingMask(this.getReference('tree_meter'),true);

        var store = Netra.manager.getStore("storeMeters");
        store.removeAll();

        store.load({
            callback: this.onMetersLoaded,
            scope: this
        });

    },

    getRequestParameter: function(meter_id)
    {
        var cur_date = new Date();

        var json = {};

        json['tenant_id'] = this.adminTenant.id;
        json['group_by'] = "";
        json['period'] = 60;
        json['meter_id'] = meter_id;
        json['start'] = Ext.Date.format(cur_date,'Y-m-d');
        json['end'] = Ext.Date.format(cur_date,'Y-m-d');

        return json;
    },

    sendUsageRequest: function(json)
    {
        var _this = this;

        console.debug("HostController.sendUsageRequest");

        Netra.helper.showPanelLoadingMask(this.getReference('chart_monitoring'),true);

        Ext.Ajax.request({
            url: '/api/openstack/ceilometer/stat',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendUsageRequest Response = ', response);
                var json = JSON.parse(response.responseText);

                _this.drawGraph(json);

                Netra.helper.showPanelLoadingMask(_this.getReference('chart_monitoring'),false);
            },
            failure: function(response, opts) 
            {
                console.debug('sendUsageRequest : woops');
                Netra.helper.showPanelLoadingMask(_this.getReference('chart_monitoring'),false);
            }
        });

    },

    getAdminTenant: function()
    {
        var store = Netra.manager.getStore("storeTenants");
        this.adminTenant = store.findRecord('name','admin');
        if (! this.adminTenant) {
            console.debug("HostController.getAdminTenant : Error!!! fail to find admin tenants");
        }

        console.debug("HostController.getAdminTenant : adminTenant = " , this.adminTenant);
    },

    drawGraph: function(json)
    {
        console.debug("HostController.drawGraph : json = " , json);

        var store = Netra.manager.getStore("storeMeterStats");
        store.loadRawData(json);

        console.debug("HostController.drawGraph : total count = " , store.getTotalCount());

        //console.debug("HostController.drawGraph : record = " , store.getAt(0));

        this.getReference('chart_monitoring').bindStore(store);

    },

    addRootNodeToTree: function(text)
    {
        var rootNode = this.getReference('tree_host').getRootNode();
        var node = this.addNodeToTree(this.getReference('tree_host'),rootNode, text, 'openstack', false, null);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    addItemNodeToTree: function(text,record)
    {
        var rootNode = this.getReference('tree_host').getRootNode().childNodes[0];

        //console.debug("addItemNodeToTree : node = " , rootNode);

        var node = this.addNodeToTree(this.getReference('tree_host'),rootNode, text, 'openstack', true, record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    selectHost: function(record)
    {
        console.debug("HostController.selectHost : record = ",record);

        //var index = this.getStore("Templates").find('name','havana');
        //this.templateRecord = this.getStore("Templates").getAt(index);
        //console.debug('templateRecord');

        this.hostRecord = record;
    },

    selectHypervisor: function(record)
    {
        console.debug("HostController.selectHypervisor : record = ",record);

        this.instanceHypervisor = record;
    },

    filterInstanceStore: function(record)
    {
        console.debug("HostsController.filterInstanceStore : record = ", record);

        var store = Netra.manager.getStore('storeInstances');
        
        store.clearFilter();
        
        if (record != null) 
        {
            var text = record.data.hypervisor_hostname;

            store.filterBy(function(record, index) 
            {
                console.debug("HostsController.filterInstanceStore : text = " + text + " , record = ", record);

                if (record.get('os_ext_srv_attr_hypervisor_hostname').toUpperCase() == text.toUpperCase()) {
                    return true;
                } 
                return false;
            });
        }        
    },

    updateHostDetailView: function(record)
    {
        console.debug("HostController.updateHostDetailView : record = ",record );
     
        Netra.service.getHostDetail(record.data.cluster_name,record.data.ip, function(retcode,response) {
            
            console.debug("HostController.updateHostDetailView : response = ",response);

            var disk_size = Netra.helper.getXenHostStorageSize(response.data.storage);
            
            Netra.helper.getComponent('#hostdetail_number_cpu').updateValue('CPU Speed',response.data.cpu_speed,'');
            Netra.helper.getComponent('#hostdetail_number_cpu_count').updateValue('CPU Count',response.data.cpu_count,'');
            Netra.helper.getComponent('#hostdetail_number_memory').updateValue('Memory',Netra.helper.formatByteNumber(response.data.total_memory),'');
            Netra.helper.getComponent('#hostdetail_number_disk').updateValue('Disk',Netra.helper.formatByteNumber(disk_size['total']),'');
            Netra.helper.getComponent('#hostdetail_number_xen_version').updateValue('XEN Ver.',response.data.xenserver_version,'');


            var params_general = { 'Host Name': response.data.hostname,
                            'IP': response.data.ip,
                            'CPU Model': response.data.cpu_model,
                            'CPU Count': response.data.cpu_count,
                            'CPU Speed': response.data.cpu_speed,
                            'Kernel': response.data.linux,
                            'Total Memory': Netra.helper.formatByteNumber(response.data.total_memory),
                            'Free Memory': Netra.helper.formatByteNumber(response.data.free_memory),
                            'Memory Overhead': Netra.helper.formatByteNumber(response.data.memory_overhead),
                            'Boot Time': Netra.helper.utcToDate(response.data.boottime),
                            'CPU Flags': response.data.cpu_flags,
                            'iSCSI iqn': response.data.iscsi_iqn,
                            'BIOS Version': response.data.bios_version,
                            'XEN Version': response.data.xenserver_version,
                            'XCP': response.data.xcp,
                            'System Manufacturer': response.data.system_manufacturer,
                            'System Serial': response.data.system_serial,
                            'Description': response.data.Description,                            
            }

            Netra.helper.updatePropertyGrid('#grid_general', params_general);

            var params_network = {
                'NIC Count': response.data.network.length,
                'Network Backend': response.data.network_backend,
            }

            for (var index=0; index < response.data.network.length; index++) 
            {
                name_prefix = 'NIC'+index.toString();
                params_network[name_prefix+" Device"] = response.data.network[index].device;
                params_network[name_prefix+" DNS"] = response.data.network[index].dns;
                params_network[name_prefix+" Gateway"] = response.data.network[index].gateway;
                params_network[name_prefix+" IP"] = response.data.network[index].ip;
                params_network[name_prefix+" MAC"] = response.data.network[index].mac;
                params_network[name_prefix+" Netmask"] = response.data.network[index].netmask;
            }

            Netra.helper.updatePropertyGrid('#grid_network', params_network);


            var params_disk = {
                'Storage Count': response.data.storage.length,
                'Total Size': Netra.helper.formatByteNumber(disk_size['total']),
                'Used Size': Netra.helper.formatByteNumber(disk_size['used'])
            }

            var name_prefix = "";
            for (var index=0; index < response.data.storage.length; index++) 
            {
                //console.debug("updateHostDetailView : record = ",response.data.storage[index]);

                name_prefix = 'storage'+index.toString();
                params_disk[name_prefix+" Name"] = response.data.storage[index].name;
                params_disk[name_prefix+" Type"] = response.data.storage[index].type;
                params_disk[name_prefix+" Description"] = response.data.storage[index].description;
                params_disk[name_prefix+" Size"] = Netra.helper.formatByteNumber(response.data.storage[index].physical_size);
                params_disk[name_prefix+" Used Size"] = Netra.helper.formatByteNumber(response.data.storage[index].physical_utilisation);
            }

            Netra.helper.updatePropertyGrid('#grid_disk', params_disk);


        });


    },

    getSelectedIds: function()
    {
        console.debug("HostController.getSelectedIds");
        
        var json = Netra.helper.getSelectedIdsInJson(this.getReference('grid_host_instance'));

        return json;
    },

    refreshData: function()
    {
        var _this = this;

        Netra.helper.refreshData('storeHosts', '#grid_host_instance', function() {
            //_this.filterInstanceStore(this.hypervisorRecord);
        });

    },

    showNewHostWindow: function(hostRecord) 
    {
        console.debug("showNewHostWindow");

        this.newHostWindowController = Netra.manager.createController("Netra.view.component.window.NewHost");
        this.newHostWindowController.show(hostRecord);
    },

    showNewSoftwareWindow: function(hostRecord) 
    {
        console.debug("HostController.showNewSoftwareWindow");

        this.newSoftwareWindowController = Netra.manager.createController("Netra.view.component.window.NewSoftware");
        this.newSoftwareWindowController.show(hostRecord);
    },

    updateConsoleLog: function(record)
    {
        var _this = this;

        console.debug("HostController.updateConsoleLog");

        Netra.helper.showPanelLoadingMask(this.getReference('panel_instance_log'),true);

        Ext.Ajax.request({
            url: '/api/openstack/instances/output',
            method: 'POST',
            jsonData: { "instance_id": record.data.id },
            success: function(response, opts) 
            {
                var json = JSON.parse(response.responseText);

                console.debug("updateConsoleLog : ", json);

                if (json.retcode == 0) {

                } else {

                }
                console.debug("updateConsoleLog : area = " , _this.getReference('area_instance_log'));
                _this.getReference('area_instance_log').setValue(json.data.output);

                Netra.helper.showPanelLoadingMask(_this.getReference('panel_instance_log'),false);

            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                
                Netra.helper.showPanelLoadingMask(_this.getReference('panel_instance_log'),false);

                Ext.MessageBox.alert('Error', 'Fail to get console log from server');
            }            
        });

    },

    loadInstanceAuditsData: function(instance_id) 
    {
        var store = Netra.manager.getStore("storeInstanceAudits");

        console.debug("HostController.loadInstanceAuditsData : id = " + instance_id + " , store = ", store);

        store.removeAll();

        var end_date = new Date();
        var start_date = end_date - 30;

        //console.debug("HostsController.loadInstanceAuditsData : start = " + Netra.helper.formatDate(start_date) + " , end = " + end_date);

        store.load({
            params: {
                'date_start': Ext.Date.format(start_date,'Y-m-d'), 'date_end': Ext.Date.format(end_date,'Y-m-d'), 'instance_id': instance_id
            },
            callback: this.onInstanceAuditsLoaded,
            scope: this
        });

    },

    showGridPopupMenu: function(grid, index, record, event) 
    {
        console.debug("HostController.showGridPopupMenu");

        var _this = this;

        event.stopEvent();

        if (! this.gridPopupMenu) 
        {
            this.gridPopupMenu = new Ext.menu.Menu({
                items: 
                [
                    {
                        text: 'Edit',
                        handler: function() 
                        {
                            _this.showNewHostWindow(_this.hostRecord);
                        }
                    }, 
                    {
                        text: 'Delete',
                        handler: function() 
                        {
                            _this.deleteHostRecord(record);
                        }
                    },
                    {
                        text: 'Bootstrap',
                        handler: function() 
                        {
                            Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(result) {
                                if (result == 'yes') {
                                    _this.bootstrapHost(new Array(record));
                                }
                            });
                        }
                    }

                ]
            });
        }
        
        this.gridPopupMenu.showAt(event.getXY());
    },

    deleteHostRecord:function(record)
    {
        console.debug("HostController.deleteHostRecord : Record Jons = ", record);

        Netra.service.deleteHostRecord(record.data,this.onHostRequestComplete);
    },

    bootstrapHost: function(host_record)
    {
        console.debug("HostController.deleteHostRecord : Record Arrays = ", host_record);
    },


    onStart: function()
    {
        console.debug("HostController.onStart");
        
        this.refreshData();
        //this.loadHypervisorsData();
    },

    onHypervisorsLoaded: function()
    {
        var _this = this;

        console.debug("HostController.onHypervisorsLoaded");

        this.clearTree(this.getReference('tree_host'));
        this.addRootNodeToTree("Hosts");

        Netra.manager.getStore("storeHypervisors").each(function(record,idx) 
        {
            _this.addItemNodeToTree(record.get("hypervisor_hostname"), record);            
        });

        this.getReference('tree_host').getView().refresh();
        this.getReference('tree_host').getRootNode().expand();

        Netra.helper.showPanelLoadingMask(this.getReference('tree_host'),false);

    },

    onFormRender: function(form, opts)
    {
        console.debug("HostController.onFormRender");
    },

    onInstanceAuditsLoaded: function()
    {
        console.debug("HostController.onInstanceAuditsLoaded");
    },

    onButtonGraphClick: function()
    {
        console.debug("onButtonGraphClick");

        var meter = this.getReference('combo_report_meter').getRawValue();
        var start = this.getReference('text_report_start').getRawValue();
        var end = this.getReference('text_report_end').getValue();

        console.debug("onButtonGraphClick : start = " + start);

        var json = this.getRequestParameter();
        this.sendUsageRequest(json);
    },

    onDestroy: function()
    {
        console.debug("HostController.onDestroy");
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick");

        this.selectHost(record);

        this.updateHostDetailView(this.hostRecord);
        Netra.helper.showCardView('container_host',1,'','');
    },

    onButtonNewClick: function()
    {
        console.debug("HostsController.onButtonNewClick");
        this.showNewHostWindow(null);
    },


    onButtonRefreshClick: function()
    {
        console.debug("HostsController.onButtonRefreshClick");
        this.refreshData();
    },

    onButtonBackFormClick: function()
    {
        console.debug("HostsController.onButtonBackFormClick");
        Netra.helper.showCardView('container_host',0,'','');
    },

    onButtonBackLogClick: function()
    {
        console.debug("HostsController.onButtonBackLogClick");
        Netra.helper.setActivePanelItem(this.getReference('panel_instance_contents'),0);
    },

    onButtonEditClick: function()
    {
        console.debug("HostsController.onButtonEditClick");
        this.showNewHostWindow(this.hostRecord);
    },

    onButtonBootstrapClick: function()
    {
        console.debug("HostsController.onButtonBootstrapClick");
    },

    onButtonSoftwareClick: function()
    {
        console.debug("HostsController.onButtonSoftwareClick");
        this.showNewSoftwareWindow(this.hostRecord);
    },

    onGridContextMenuClick: function(grid, record, tr, index, event, opts)     
    {
        console.debug("HostsController.onGridContextMenuClick : event = ",event);
        
        this.selectHost(record);
        this.showGridPopupMenu(grid, index, record, event);
    },

    onHostRequestComplete: function(retcode, response)
    {
        console.debug("onHostRequestComplete : ret = ", response);

        if (retcode == Netra.const.REQ_OK) {
            Ext.MessageBox.alert('Success', response.msg);
        } else if (retcode == Netra.const.REQ_FAIL) {
            Ext.MessageBox.alert('Fail', response);
        } else {
            Ext.MessageBox.alert('Fail', response.msg);
        }

    },

});
