Ext.define('Netra.view.image.Images', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.images',
    title: 'Virtual Machine Image',
    titleAlign: 'center',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',

    requires: [
        'Ext.layout.container.Accordion',
        'Netra.model.Image',
        'Netra.store.Images',
        'Netra.view.image.ImagesController',        
    ],

    layout: {
        type: 'accordion',
        animate: true,        
        vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: true,
        headerPosition: 'left',
        titleAlign: 'center',
    },

    controller: 'images',

    items: 
    [
        {
            xtype: 'gridpanel',
            id: 'grid_image',
            reference: 'grid_image',
            title: 'List',
            stripeRows: true,
            columnLines: true,

            store: Netra.manager.getStore('storeImages'),

            selType: 'checkboxmodel',
            selModel: {
                checkOnly: true,
                injectCheckbox: 0
            },

            columns: [
                {
                    text     : 'id',
                    flex     :  1,
                    sortable : false,
                    hidden   : false,
                    dataIndex: 'id'
                },
                {
                    text     : 'Public',
                    width    :  50,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'is_public'
                },        
                {
                    text     : 'Status',
                    width    :  50,
                    dataIndex: 'status',
                    align    : 'center',
                    renderer: function(value, metaData, record, row, col, store, gridView)
                    {
                        console.debug("Image.grid : value = ",  value); // + " , record = ", record);

                        return Netra.helper.createActiveIcon(value.toString().toLowerCase() == 'active');
                    }                                                            
                },                        
                {
                    text     : 'Name',
                    flex     :  3,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'name'
                },                        
                {
                    text     : 'Disk Format',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'size'
                },                
                {
                    text     : 'Image Size',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'size'
                },
                {
                    text     : 'Created',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'created_at'
                },
                {
                    text     : 'Minimum Disk Size',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'min_disk'
                },

                {
                    text     : 'Minimum RAM',
                    flex     :  2,
                    sortable : true,
                    //renderer : this.change,
                    dataIndex: 'min_ram'
                },
            ],
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: Netra.manager.getStore('storeImages'),
                displayInfo: true,
                plugins: new Ext.ux.ProgressBarPager()
            },

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    alias: 'widget.toolbar_image',
                    id: 'toolbar_image',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_image_new', 
                            text: 'New'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_image_delete', 
                            text: 'Delete',
                        },           
                        { 
                            xtype: 'button', 
                            id: 'btn_image_refresh', 
                            text: 'Refresh',
                        },                                  
                    ]
                }
            ],        
            
        },
 
        {
            xtype: 'panel',
            title: 'Details',
            items: 
            [
                {
                    xtype: 'form',
                    reference: 'form_image',                    
                    id: 'form_image',
                    bodyPadding: '30 60 30 60',
                    frame: false,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 180,
                    },

                    items: [],

                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_image2',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_image_back', 
                            text: 'Back'
                        },
                    ]
                }
            ],  
        }
    ],




    initComponent: function() 
    {
        console.debug("Image.initComponent");
        this.callParent(arguments);
    },


});
