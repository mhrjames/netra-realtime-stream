Ext.define('Netra.view.image.ImagesController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.images',

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridImage', selector: '#grid_image' },
    ],

    imageRecord: null,
    imageWindowController: null,


    init: function() 
    {
        console.debug("ImagesController.init");

        this.control({
            '#grid_image': { itemdblclick: this.onGridColumnDoubleClick },
            '#form_image': { render: this.onFormRender },
            '#btn_image_new': { click: this.onButtonNewClick },
            '#btn_image_edit': { click: this.onButtonEditClick },
            '#btn_image_delete': { click: this.onButtonDeleteClick },
            '#btn_image_refresh': { click: this.onButtonRefreshClick },
            '#btn_image_back': { click: this.onButtonBackClick },
        });

        //this.createStoreFlavor();
        this.onStart();
    },


    selectImage: function(record)
    {
        this.imageRecord = record;
    },

    filloutForm: function(record)
    {

        console.debug("ImageController.filloutForm : record = ", record);

        var form = this.getReference('form_image');
        var store = Netra.manager.getStore('storeImages');

        Netra.builder.buildForm(form,store,record.data,'Virtual Machine Image Details','');

/*        
        var form = this.getReference('form_image');
        var tpl = new Ext.XTemplate(
            '<h2>hello</h2>'
        );

        console.debug(record);
        console.debug(form);

        form.update({ image: record.data });

        //form.doComponentLayout();
*/
    },

    showImageWindow: function() 
    {
        console.debug("showImageWindow");

        this.imageWindowController = Netra.manager.createController("Netra.view.component.window.NewImage");
        this.imageWindowController.show();        
    },

    sendDeleteRequest: function(json)
    {
        var _this = this;

        console.debug("ImagesController.sendDeleteRequest");

        Netra.helper.showPanelLoadingMask(this.getReference('grid_image'),true);

        Ext.Ajax.request({
            url: '/api/openstack/images/delete',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendDeleteRequest Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'Selected Image is deleted!!!');
                    _this.refreshData();
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }
                
                Netra.helper.hideWaitDialog();
            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Netra.helper.hideWaitDialog();
            }
        });

    },

    refreshData: function()
    {
        var _this = this;

        Netra.helper.refreshData('storeImages','#grid_image', function() {
            _this.filterImageData();
            Netra.helper.hideWaitDialog();
        });
    },

    filterImageData: function()
    {
        var store = Netra.manager.getStore('storeImages');

        console.debug("ImagesController.filterImageData : store = ", store);

        store.clearFilter();

        store.filterBy(function(record) 
        {

            var property = record.get('properties');

            console.debug("property : " , property);

            if (property.hasOwnProperty('image_type')) {
                if (property.image_type.toLowerCase() == "snapshot") {
                    return false;
                }
            } 
            return true;
        });

    },


    onStart: function()
    {
        console.debug("onStart");

        this.flavorRecord = null;
        this.refreshData();
        
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.selectImage(record);
        this.filloutForm(this.imageRecord);
        Netra.helper.setActivePanelItem(this.getView(),1);

        //var comps = Ext.ComponentManager.getAll();
        //console.debug(comps);
        //Netra.helper.dumpComponents(null);
    },

    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
        //console.debug(form.body);
/*
        var html = "<h3>World!!!</h3>";

        var panel = form.up("panel");
        //var tpl = Ext.create('Ext.Template', 'World!!!');

        tpl.overwrite(panel.body, null);
        //form.setConfig('html', html);
*/
    },


    onButtonNewClick: function()
    {
        console.debug("onButtonNewClick");
        this.showImageWindow();
    },

    onButtonDeleteClick: function()
    {
        var _this = this;

        console.debug("ImagesController.onButtonDeleteClick");

        var selected = this.getReference('grid_image').getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select image to delete!');
            return;
        }

        var json = {};
        json['id'] = Netra.helper.getSelectedIds(this.getReference('grid_image'));

        Netra.helper.showWaitDialog("","",'btn_image_delete');

        this.sendDeleteRequest(json);
    },

    onButtonEditClick: function()
    {
        console.debug("onButtonEditClick");
        console.debug("this", this);
    },

    onButtonRefreshClick: function()
    {
        console.debug("ImageController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_image_refresh');
        this.refreshData();
    },

    onButtonBackClick: function()
    {
        console.debug("ImageController.onButtonBackClick");
        Netra.helper.setActivePanelItem(this.getView(),0);        
    },

});
