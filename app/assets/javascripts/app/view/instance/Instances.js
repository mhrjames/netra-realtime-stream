Ext.define('Netra.view.instance.Instances', {
    extend: 'Ext.panel.Panel',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',
    title: 'Virtual Machine',
    titleAlign: 'center',

    requires: [
        'Ext.layout.container.Accordion',    
        'Netra.model.Instance',
        'Netra.store.Instances',        
        'Netra.view.instance.InstancesController',        
    ],

    controller: 'instances',

    layout: {
        type: 'accordion',
        vertical: false,
        animate: true,        
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: false,
        headerPosition: 'left',
        titleAlign: 'center',
        //bodyPadding: '20 20 20 20',
    },

    items: 
    [
        {
            xtype: 'gridpanel',
            id: 'grid_instance',
            reference: 'grid_instance',
            title: 'List',

            stripeRows: true,
            columnLines: true,

            store: Netra.manager.getStore('storeInstances'),

            selType: 'checkboxmodel',
            selModel: {
                checkOnly: true,
                injectCheckbox: 0
            },

            columns: 
            [
                {
                    text     : 'id',
                    width     : 45,
                    sortable : false,
                    hidden   : false,
                    dataIndex: 'id'
                },
                {
                    text     : 'State',
                    width    : 115,
                    sortable : true,
                    //renderer : this.change,
                    dataIndex: 'state'
                },
                {
                    text     : 'VM State',
                    width    : 115,
                    sortable : true,
                    //renderer : this.change,
                    dataIndex: 'os_ext_sts_vm_state'
                },
                {
                    text     : 'Name',
                    width    : 125,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'name'
                },
                {
                    text     : 'Network',
                    width    : 155,
                    //sortable : true,
                    dataIndex: 'addresses',
                    renderer : function(value) 
                    {
                        console.debug("Instance.GridInstance : value = ", value);
                        
                        var keys = Object.keys(value);
                        var item = null , label = "";
                        for (var i=0; i < value[keys[0]].length; i++)
                        {
                            item = value[keys[0]][i];
                            label = label + item['OS-EXT-IPS:type'] + " : " + item.addr + "<br>";
                            console.debug("item = " , item);
                        }
                        return label;
                    }
                },                        
                {
                    text     : 'Created At',
                    width    : 175,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'created'
                },
                {
                    text     : 'Launched At',
                    width    : 175,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'OS-SRV-USG:launched_at'
                },
                {
                    text     : 'Host Machine',
                    width    : 95,
                    sortable : true,
                    //renderer : this.pctChange,
                    dataIndex: 'os_ext_srv_attr_hypervisor_hostname'
                },                
                {
                    text     : 'Zone',
                    width    : 95,
                    sortable : true,
                    //renderer : this.pctChange,
                    dataIndex: 'availability_zone'
                }
            ],
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: Netra.manager.getStore('storeInstances'),
                displayInfo: true,
                plugins: new Ext.ux.ProgressBarPager()
            },

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_instance',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_new', 
                            text: 'New',
                        },                
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_start', 
                            text: 'Start',
                        },                
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_stop', 
                            text: 'Stop',
                        },                
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_suspend', 
                            text: 'Suspend',
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_resume', 
                            text: 'Resume',
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_reboot', 
                            text: 'Reboot'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_snapshot', 
                            text: 'Snapshot'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_terminate', 
                            text: 'Terminate',
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_resize', 
                            text: 'Resize'
                        },                                                        
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_refresh', 
                            text: 'Refresh'
                        },                                
                    ]
                }
            ],    

        },
        {
            xtype: 'panel',
            title: 'Details',
            items: 
            [
                {
                    xtype: 'form',
                    reference: 'form_instance',                    
                    id: 'form_instance',
                    bodyPadding: '30 60 30 60',
                    autoScroll: true,
                    frame: false,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 180,
                    },

                    items: [],

                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_instance2',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_back_form', 
                            text: 'Back',
                        },        
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_console', 
                            text: 'Console',
                        },                                                             
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_log', 
                            text: 'Log',
                        },                                      
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            title: 'Log',
            reference: 'panel_instance_log',
            layout: {
                type: 'vbox',
                anchor: '100%',
                align: 'stretch',
            },
            bodyPadding: '30 30 30 30',

            items: 
            [
                {
                    xtype: 'component',
                    frame: false,
                    border: 0,
                    html: Netra.helper.createFormTitle('Virtual Machine Log Messages','')
                },  

                {
                    xtype : 'textareafield',
                    fieldCls: 'netra-log-box',
                    id: 'area_instance_log',
                    reference: 'area_instance_log',
                    name: 'description',
                    grow: true,
                    readOnly: true,
                    value: "", 
                    flex: 2,           
                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_instance3',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [                    
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_back_log', 
                            text: 'Back',
                        },         
                        { 
                            xtype: 'button', 
                            id: 'btn_instance_log_refresh', 
                            text: 'Refresh',
                        },                                     
                    ]
                }
            ]

        }
    ],


});
