Ext.define('Netra.view.instance.InstancesController', {
    extend: 'Netra.controller.BaseController',
    alias: 'controller.instances',

    refs: [
        { ref: 'areaInstanceLog', selector: '#area_instance_log' },
    ],
    
    instanceRecord: null,
    instanceWindowController: null,




    init: function() 
    {
        console.debug("InstancesController.init");

        this.control({
            '#grid_instance': { itemdblclick: this.onGridColumnDoubleClick },
            '#btn_instance_new': { click: this.onButtonNewClick },
            '#btn_instance_start': { click: this.onButtonStartClick },
            '#btn_instance_refresh': { click: this.onButtonRefreshClick },
            '#btn_instance_stop': { click: this.onButtonStopClick },
            '#btn_instance_suspend': { click: this.onButtonSuspendClick },
            '#btn_instance_resume': { click: this.onButtonResumeClick },
            '#btn_instance_reboot': { click: this.onButtonRebootClick },
            '#btn_instance_terminate': { click: this.onButtonTerminateClick },
            '#btn_instance_snapshot': { click: this.onButtonSnapshotClick },
            '#btn_instance_resize': { click: this.onButtonResizeClick },
            '#btn_instance_console': { click: this.onButtonConsoleClick },
            '#btn_instance_back_form': { click: this.onButtonBackFormClick },
            '#btn_instance_back_log': { click: this.onButtonBackLogClick },
            '#btn_instance_log': { click: this.onButtonLogClick },
            '#btn_instance_log_refresh': { click: this.onButtonLogRefreshClick },            
        });

        this.instanceRecord = null;

        this.onStart();
    },

    loadInstanceData: function() 
    {
        console.debug("loadInstanceData");

        var store = this.getStore("Instances");
        store.removeAll();

        store.load({
            callback: this.onInstancesLoaded,
            scope: this
        });

    },

    showInstanceWindow: function() 
    {
        console.debug("showInstanceWindow");

        this.instanceWindowController = Netra.manager.createController("Netra.view.component.window.NewInstance");
        this.instanceWindowController.show();        
    },

    selectInstance: function(record)
    {
        this.instanceRecord = record;
    },

    filloutForm: function(record)
    {
        console.debug("InstanceController.filloutForm : record = ",record );
        
        var form = this.getReference('form_instance');
        var store = Netra.manager.getStore('storeInstances');

        Netra.builder.buildForm(form,store,record.data,'Instance Details','');
/*
        var tpl = new Ext.XTemplate(
            '<h2>hello</h2>'
        );

        console.debug(record);
        console.debug(form);

        var json = this.convertInstanceToJson(record);
        form.update({ instance: json });
*/
        //form.doComponentLayout();

    },

    convertInstanceToJson: function(record)
    {
        console.debug("convertInstanceToJson : record = ", record);

        var json = {};

        json.id = record.data.id;
        json.availability_zone = record.data.availability_zone;
        json.user_id = record.data.user_id;
        json.tenant_id = record.data.tenant_id;
        json.power_state = record.data.os_ext_sts_power_state;
        json.state = record.data.state;
        json.name = record.data.name;
        json.created = record.data.created;
        json.updated = record.data.updated;
        json.host_id = record.data.host_id;
        json.host = record.data.os_ext_srv_attr_host;
        json.launched = record.data['OS-SRV-USG:launched_at'];
        json.key_name = record.data.key_name;
        json.hypervisor_hostname = record.data.os_ext_srv_attr_hypervisor_hostname;
        json.vm_state = record.data.os_ext_sts_vm_state;
        json.instance_name = record.data.os_ext_srv_attr_instance_name;
        json.flavor_id = record.data.flavor.id;
        json.image_id = record.data.image.id;

        var keys = Object.keys(record.data.addresses);
        var item = null , label = record.data.addresses[keys[0]] + "<br>";
        for (var i=0; i < record.data.addresses[keys[0]].length; i++)
        {
            item = record.data.addresses[keys[0]][i];
            label = label + item['OS-EXT-IPS:type'] + " : " + item.addr + "<br>";
            label = label + item[''];
            console.debug("item = " , item);
        }

        json.address = JSON.stringify(record.data.addresses);
        json.volumes_attached = JSON.stringify(record.data['os-extended-volumes:volumes_attached']);

        return json;
    },

    sendServerActionRequest: function(action,json,success_callback,failure_callback)
    {
        var _this = this;
        
        console.debug("InstanceController.sendRequest");

        Netra.helper.showPanelLoadingMask(this.getReference('grid_instance'),true);

        Ext.Ajax.request({
            url: '/api/openstack/instances/' + action,
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                var json = JSON.parse(response.responseText);

                if (json.retcode == 0) {
                    success_callback('ok');
                } else {
                    failure_callback(json.msg);
                }            

                Netra.helper.showPanelLoadingMask(_this.getReference('grid_instance'),false);
            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                
                Netra.helper.showPanelLoadingMask(_this.getReference('grid_instance'),false);

                Ext.MessageBox.alert('Error', 'Fail to send request to server');
            }            
        });
    },

    sendStartRequest: function(json)
    {
        console.debug("InstanceController.sendStartRequest");

        this.sendServerActionRequest('start',json, function(msg) {
            console.debug('sendStartRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Started!!!');
        }, function(msg) {
            console.debug('sendStartRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });
    },

    sendStopRequest: function(json)
    {
        console.debug("InstanceController.sendStopRequest");

        this.sendServerActionRequest('stop',json, function(msg) {
            console.debug('sendStopRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Stopped!!!');
        }, function(msg) {
            console.debug('sendStopRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    sendSuspendRequest: function(json)
    {
        console.debug("InstanceController.sendSuspendRequest");

        this.sendServerActionRequest('suspend',json, function(msg) {
            console.debug('sendSuspendRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Suspended!!!');
        }, function(msg) {
            console.debug('sendSuspendRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    sendResumeRequest: function(json)
    {
        console.debug("InstanceController.sendResumeRequest");

        this.sendServerActionRequest('resume',json, function(msg) {
            console.debug('sendResumeRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Resumed!!!');
        }, function(msg) {
            console.debug('sendResumeRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    sendRebootRequest: function(json)
    {
        console.debug("InstanceController.sendRebootRequest");

        this.sendServerActionRequest('reboot',json, function(msg) {
            console.debug('sendRebootRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Rebooted!!!');
        }, function(msg) {
            console.debug('sendRebootRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    sendTerminateRequest: function(json)
    {
        console.debug("InstanceController.sendTerminateRequest");

        var _this = this;

        this.sendServerActionRequest('terminate',json, function(msg) {
            console.debug('sendTerminateRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Terminated!!!');
            _this.refreshData();
        }, function(msg) {
            console.debug('sendTerminateRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    sendSnapshotRequest: function(json)
    {
        console.debug("InstanceController.sendSnapshotRequest");

        var _this = this;

        this.sendServerActionRequest('snapshot',json, function(msg) {
            console.debug('sendSnapshotRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Snapshotted!!!');
            _this.refreshData();
        }, function(msg) {
            console.debug('sendSnapshotRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    sendResizeRequest: function(json)
    {
        console.debug("InstanceController.sendResizeRequest");

        var _this = this;

        this.sendServerActionRequest('resize',json, function(msg) {
            console.debug('sendResizeRequest.Success : Msg = ' + msg);
            Ext.MessageBox.alert('Success', 'Server Resized!!!');
            _this.refreshData();
        }, function(msg) {
            console.debug('sendResizeRequest.Failure : Msg = ' + msg);            
            Ext.MessageBox.alert('Error', msg);
        });

    },

    refreshData: function()
    {
        Netra.helper.refreshData('storeInstances','#grid_instance',function() {
            Netra.helper.hideWaitDialog();
        });
    },

    getSelectedIds: function()
    {
        console.debug("InstanceController.handleServerAction");
        
        var json = {};
        json['id'] = Netra.helper.getSelectedIds(this.getReference('grid_instance'));

        return json;
    },

    updateConsoleLog: function(record)
    {
        var _this = this;

        console.debug("InstanceController.updateConsoleLog");

        Ext.Ajax.request({
            url: '/api/openstack/instances/output',
            method: 'POST',
            jsonData: { "instance_id": record.data.id },
            success: function(response, opts) 
            {
                var json = JSON.parse(response.responseText);

                console.debug("updateConsoleLog : ", json);

                if (json.retcode == 0) {

                } else {

                }
                console.debug("updateConsoleLog : area = " , _this.getReference('area_instance_log'));
                _this.getReference('area_instance_log').setValue(json.data.output);

                Netra.helper.hideWaitDialog();

            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                
                Ext.MessageBox.alert('Error', 'Fail to get console log from server');
                Netra.helper.hideWaitDialog();

            }            
        });

    },

    getVncConsole: function(record)
    {
        var _this = this;

        console.debug("InstanceController.getVncConsole");

        Ext.Ajax.request({
            url: '/api/openstack/instances/console',
            method: 'POST',
            jsonData: { "instance_id": record.data.id },
            success: function(response, opts) 
            {
                var json = JSON.parse(response.responseText);

                console.debug("getVncConsole : ", json);

                if (json.retcode == 0) {

                } else {

                }

                window.open(json.data.console.url);
                
                //console.debug("updateConsoleLog : area = " , _this.getReference('area_instance_log'));
                //_this.getReference('area_instance_log').setValue(json.data.output);

            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Ext.MessageBox.alert('Error', 'Fail to get console window from server');
            }            
        });

    },





    onStart: function() 
    {
        console.debug("onStart");
        //this.initView();

        this.refreshData();
        
        //this.loadInstanceData();
        //this.loadTemplateFromServer();
        //this.loadHosts();
        //this.createOpenstackWindowController();
    },

    onInstancesLoaded: function()
    {
        console.debug("InstancesController.onInstancesLoaded");
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick");

        this.selectInstance(record);
        this.filloutForm(this.instanceRecord);
        Netra.helper.setActivePanelItem(this.getView(),1);
    },

    onButtonNewClick: function()
    {
        console.debug("InstanceController.onButtonNewClick");
        this.showInstanceWindow();
    },

    onButtonStartClick: function()
    {
        console.debug("InstanceController.onButtonStartClick");
        
        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }
        
        this.sendStartRequest(ids);
    },

    onButtonStopClick: function()
    {
        console.debug("InstanceController.onButtonStopClick");

        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }
        
        this.sendStopRequest(ids);
        
    },

    onButtonSuspendClick: function()
    {
        console.debug("InstanceController.onButtonSuspendClick");

        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }
        
        this.sendSuspendRequest(ids);

    },

    onButtonResumeClick: function()
    {
        console.debug("InstanceController.onButtonResumeClick");

        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }
        
        this.sendResumeRequest(ids);

    },

    onButtonRebootClick: function()
    {
        console.debug("InstanceController.onButtonRebootClick");

        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }
        
        this.sendRebootRequest(ids);
    },

    onButtonTerminateClick: function()
    {
        console.debug("InstanceController.onButtonTerminateClick");

        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }
        
        this.sendTerminateRequest(ids);
    },

    onButtonRefreshClick: function()
    {
        console.debug("InstanceController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_instance_refresh');
        this.refreshData();
    },

    onButtonSnapshotClick: function()
    {
        console.debug("InstanceController.onButtonSnapshotClick");

        var grid = this.getReference('grid_instance');
        var selected = grid.getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select instances to create snapshot!!!');
            return;
        }

        var instance_arr = [];
        Ext.each(selected, function (item) 
        {
            var instance = {}
            instance['id']  = item.data.id;
            instance['name'] = item.data.name;

            instance_arr.push(instance);
        });        

        this.sendSnapshotRequest(instance_arr)
    },

    onButtonResizeClick: function()
    {
        var _this = this;

        console.debug("InstanceController.onButtonResizeClick");

        var ids = this.getSelectedIds();
        if (! ids['id']) {
            Ext.MessageBox.alert('Error', 'Please Select Items');
            return;
        }

        var msgbox = Ext.create('Netra.helper.ComboMessageBox');
        msgbox.buildMessageBox('flavors','name','id',true);

        msgbox.prompt('Resize Virtual Instances', 'Please select teamplate to resize', function (btn, text) 
        {
            console.debug("onButtonResizeClick : text = " + text);

            if (btn == 'ok') 
            {
                ids['flavor_id'] = text;
                _this.sendResizeRequest(ids);
            }
        });

    },

    onButtonConsoleClick: function()
    {
        console.debug("InstanceController.onButtonConsoleClick");        
        //if (this.instanceRecord.)
        this.getVncConsole(this.instanceRecord);
    },

    onButtonBackFormClick: function()
    {
        console.debug("InstanceController.onButtonBackFormClick");
        Netra.helper.setActivePanelItem(this.getView(),0);
    },

    onButtonBackLogClick: function()
    {
        console.debug("InstanceController.onButtonBackLogClick");
        Netra.helper.setActivePanelItem(this.getView(),1);
    },

    onButtonLogClick: function()
    {
        console.debug("InstanceController.onButtonLogClick");
        this.updateConsoleLog(this.instanceRecord);        
        Netra.helper.setActivePanelItem(this.getView(),2);
    },

    onButtonLogRefreshClick: function()
    {
        console.debug("InstanceController.onButtonLogRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_instance_log_refresh');
        this.updateConsoleLog(this.instanceRecord);        
    },

});
