Ext.define('Netra.view.login.Login', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.login',
    id: 'view_login',
    title: 'Netra Realtime-Stream Login',
    titleAlign: 'center',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',

    requires: [
        'Netra.view.login.LoginController',
    ],

    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {},
    
    //style: 'background-image: url(square.gif)',

    controller: 'logins',

        
    items:
    [
        {
            xtype: 'window',
            autoShow: true,
            defaultFocus : 'text_userid',
            title: 'Login to Netra Realtime-Stream Edition',
            layout: 'fit',
            height: 270,
            width: 400,

            items: 
            [
                {
                   xtype : 'form',
                   frame: false,
                   defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 110,
                   },
                   bodyPadding: '25 30 30 30',
                   items: 
                   [
                        {
                            xtype: 'component',
                            frame: false,
                            border: 0,
                            html: Netra.helper.createFormTitle('Realtime-Stream Login','')
                        },                      
                   
                        {
                            fieldLabel: 'User ID',
                            name: 'user',
                            id: 'text_userid',
                            //itemId: 'userid',
                            name: 'userid',
                            hasfocus:true,
                        },
                        {
                            fieldLabel: 'Password',
                            inputType: 'password',
                            id: 'text_password',
                            name: 'password'
                        },
                        {
                            xtype: 'hidden',
                            id: 'hidden_login_count',
                            name: 'login_count',
                            value: '0',
                        }           
                   ]
                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    ui: 'footer',
                    dock: 'bottom',
                    items: 
                    [
                        { xtype: 'component', flex: 1 },                
                        { xtype: 'button', text: 'Login', id: 'btn_login' },
                        //{ xtype: 'button', text: "Cancel", id: 'btn_cancel' }
                    ]
                }
            ],

        }

    ],


    initComponent: function()
    {
        this.callParent(arguments);
        //console.debug("initComponent");
        //Netra.helper.getComponent("#text_userid").focus(false,100);
    }    

});
