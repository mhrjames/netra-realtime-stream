Ext.define('Netra.view.login.LoginController', 
{
    extend: 'Netra.controller.BaseController',
    alias: 'controller.logins',

    require: [],

    refs: [
        { ref: 'panelContent',selector: '#panel_container' },
        { ref: 'treeMenu',selector: '#tree_menu' },
        { ref: 'nodeKickStart',selector: '#node_kickstart' },
        { ref: 'textKickstart', selector: '#text_kickstart' },
    ],
    

    loginCount: 0,


    init: function() 
    {
        console.debug("MainController.init");

        this.control({
            '#btn_login': { click: this.onBtnLoginClick },
            '#btn_cancel': { click: this.onBtnCancelClick },
        });

        this.onStart();
    },

    sendRequest: function(json,callback)
    {
        console.debug("NewTasks.sendRequest");
        
        Ext.Ajax.request({
            url: '/api/login',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendParameters Response = ', response);
                
                var json = JSON.parse(response.responseText);

                callback(json.retcode,json);

            },
            failure: function(response, opts) 
            {
                console.debug('sendParameters : woops');
                callback(-1,response);
            }
        });

    },

    getParameter: function()
    {
        console.debug('NewTaskWindow.getParameter');

        var json = {};

        json['userid'] = Netra.helper.getComponent("#text_userid").value;
        json['password'] = Netra.helper.getComponent("#text_password").value;
        json['login_count'] = this.loginCount;

        return json;
    },

    increaseLoginCount: function()
    {
        this.loginCount = this.loginCount + 1;
    },

    doLogin: function()
    {
        var _this = this;

        console.debug('NewTaskWindow.doLogin');

        this.increaseLoginCount();

        Ext.MessageBox.show({
            msg: 'Login to Netra, please wait...',
            progressText: 'working...',
            width:300,
            wait: {
                interval:200
            },
            animateTarget: 'btn_login'
        });

        this.sendRequest(this.getParameter(), function (retcode, json) 
        {
            console.debug("doLogin : json = ", json);

            Ext.MessageBox.hide();

            if (json.retcode == 0) 
            {
                Netra.user.login(json.data.user);

                Netra.helper.getComponent('#view_login').destroy();
                Ext.create('Netra.view.main.Main');
                
                return;
            }

            Ext.MessageBox.alert('Error!!!', "사용자계정 혹은 비밀번호가 일치하지 않습니다.");
        });
    },

    validate: function()
    {
        console.debug("MainController.validate");

        if (Netra.helper.getComponent("#text_userid").value.length == 0 ) {
            Ext.MessageBox.alert('Error!!!', "Please input userid");
            return false;
        }

        if (Netra.helper.getComponent("#text_password").value.length == 0 ) {
            Ext.MessageBox.alert('Error!!!', "Please type password");
            return false;
        }

        return true;
    },






    onStart: function()
    {
        console.debug("MainController.onStart");
        
    },

    onBtnLoginClick: function()
    {
        console.debug("MainController.onBtnLoginClick");

        if (! this.validate()) return;

        this.doLogin();
    },

    onBtnCancelClick: function()
    {
        console.debug("MainController.onBtnCancelClick");
    },

        
});
