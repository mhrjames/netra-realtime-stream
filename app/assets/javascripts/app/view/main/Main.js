/**
 * A sample portal layout application class.
 */
Ext.define('Netra.view.main.Main', {
    extend: 'Ext.container.Viewport',

    requires: [
        'Chart.ux.Highcharts',
        'Chart.ux.Highcharts.Serie',
        'Chart.ux.Highcharts.BarSerie',
        'Chart.ux.Highcharts.ColumnSerie',
        'Chart.ux.Highcharts.GaugeSerie',
        'Chart.ux.Highcharts.SolidGaugeSerie',
        'Chart.ux.Highcharts.LineSerie',
        'Chart.ux.Highcharts.PieSerie',
    
        'Ext.layout.container.Border',
        'Netra.controller.BaseController',
        'Netra.view.component.Header',
        'Netra.view.component.window.Config',
        //'Netra.view.component.window.NewUser',
        //'Netra.view.component.window.NewFlavor',        
        //'Netra.view.component.window.NewTenant',        
        //'Netra.view.component.window.NewInstance',
        //'Netra.view.component.window.NewImage',
        //'Netra.view.component.window.NewSecurityGroup',
        //'Netra.view.component.window.NewSecurityGroupRule',
        'Netra.view.main.MainController',
    ],

    referenceHolder: true,
    
    layout: {
        type: 'border'
    },

    controller: 'main',

    items: [
    {
        id: 'app-header',
        xtype: 'app-header',
        //xtype: 'panel',
        region: 'north',
        title: 'hello'
    },
    {
        //id: 'app-options',
        xtype: 'panel',
        region: 'west',
        width: 48,
        cls: 'netra-icon-panel',
        border: false,
        
        bodyStyle: {
            background: '#2a3f5d',
        },

        layout:{
            type: 'vbox',
            align: 'stretch',
        },

        defaults: {
            border: false,
            cls: 'netra-icon',
            width:48,
            height:48,
            iconAlign: 'top',
            scale: 'large',
            //margin: "0 0 10 0",
            //componentCls: 'bottom-border',
        },

        items:
        [
            {
                xtype: 'button',
                id: 'btn_dashboard',
                iconCls: 'x-btn-glyph fontawesome-dashboard',
                tooltip: 'Dashboard',
                //glyph:'fa-dashboard@FontAwesome',                
            },
            {
                xtype: 'button',
                id: 'btn_xen',
                iconCls: 'x-btn-glyph fontawesome-tasks',
                tooltip: 'VM Manager'
            },
            {
                xtype: 'button',
                id: 'btn_user',
                iconCls: 'x-btn-glyph fontawesome-group',
                tooltip: 'User Manager'
            },
            {
                xtype: 'button',
                id: 'btn_provision',
                iconCls: 'x-btn-glyph fontawesome-cogs',
                tooltip: 'Provision'
            },               
            {
                xtype: 'button',
                id: 'btn_host',
                iconCls: 'x-btn-glyph fontawesome-desktop',
                tooltip: 'Host Manager'
            },
/*            
            {
                xtype: 'button',
                id: 'btn_cluster',
                iconCls: 'x-btn-glyph fontawesome-sitemap',
                tooltip: 'Pool Manager'
            },                            
            {
                xtype: 'button',
                id: 'btn_template',
                iconCls: 'x-btn-glyph fontawesome-file',
                tooltip: 'Template'
            },                            
*/            
            {
                xtype: 'button',
                id: 'btn_report',
                iconCls: 'x-btn-glyph fontawesome-book',
                tooltip: 'Report'
            },                      
            {
                xtype: 'button',
                id: 'btn_monitoring',
                iconCls: 'x-btn-glyph fontawesome-bar-chart',
                tooltip: 'Monitoring'
            }
        ]
    },
    {
        xtype: 'dashboard',
        id: 'panel_main_dashboard',
        region: 'center',
        stateful: false,
        layout: 'fit',
        autoScroll: false,
        border: false,
        //padding: '10 10 10 10',
        //bodyStyle: { background: '#ffffff',},

        items: []
    },

    ],



    initComponent: function()
    {
        this.callParent(arguments);
        //console.debug("initComponent");
    }

});
