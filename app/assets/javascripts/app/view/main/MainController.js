Ext.define('Netra.view.main.MainController', 
{
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.main',

    require: [
        'Netra.view.repository.Repository',
    ],

    refs: [
        { ref: 'panelContent',selector: '#panel_container' },
        { ref: 'treeMenu',selector: '#tree_menu' },
        { ref: 'nodeKickStart',selector: '#node_kickstart' },
        { ref: 'textKickstart', selector: '#text_kickstart' },
    ],

    configWindowController: null,
    

    init: function() 
    {
        console.debug("MainController.init");

        this.control({
            '#btn_header_config': { click: this.onBtnHeaderConfigClick },
            '#btn_header_logout': { click: this.onBtnHeaderLogoutClick },

            '#btn_instance': { click: this.onBtnInstanceClick },
            '#btn_image': { click: this.onBtnImageClick },
            '#btn_report': { click: this.onBtnReportClick },
            '#btn_config': { click: this.onBtnConfigClick },
            '#btn_dashboard': { click: this.onBtnDashboardClick },
            '#btn_snapshot': { click: this.onBtnSnapshotClick },
            '#btn_template': { click: this.onBtnTemplateClick },
            '#btn_cluster': { click: this.onBtnClusterClick },
            '#btn_ips': { click: this.onBtnIpsClick },
            '#btn_user': { click: this.onBtnUserClick },
            '#btn_monitoring': { click: this.onBtnMonitoringClick },
            '#btn_xen': { click: this.onBtnXenClick },            
            '#btn_host': { click: this.onBtnHostClick },
            '#btn_provision': { click: this.onBtnProvisionClick },
        });

        this.listen({ 
            global: {
                 broadcast : this.onBroadcastMessage,
                'onRequestNewView':this.onRequestNewView, 
                'onInstanceStart':this.onInstanceStart,
                'onInstanceStop':this.onInstanceStop,
                'onInstanceSuspend':this.onInstanceSuspend,
                'onInstanceResume':this.onInstanceResume,
                'onInstanceReboot':this.onInstanceReboot,
                'onInstanceSnapshot':this.onInstanceSnapshot,
                'onInstanceTerminate':this.onInstanceTerminate,
                'onInstanceResize':this.onInstanceResize,

            }
        }); 

        this.onStart();
    },

    clearContentsView: function() 
    {
        var dashboard = Netra.helper.getComponent('#panel_main_dashboard');
        dashboard.removeAll();
    },

    showView: function(view)
    {
        var dashboard = Netra.helper.getComponent('#panel_main_dashboard');
        
        if (dashboard.items.length>0) {
            var controller = dashboard.items.items[0].controller;
            console.debug("MainController.showView : to_destroy = ", controller);
            Netra.manager.removeController(controller);
        }

        this.clearContentsView();
        
        //Netra.app.getApplication().clearListeners();

        //console.debug("MainController.showView : to_destroy = ", controller);


        var new_view = Ext.create(view);
        //Netra.manager.removeController(new_view);

        dashboard.add(new_view);

        console.debug("MainController.showView : view = " + view + " , child = ", new_view.controller);

        //var view_instance = this.getView(view).create();
        //dashboard.add(view_instance);
    },

    updateHeaderLoginPanel: function()
    {

        if (! Netra.user.isLogin()) return;

        var panel = Netra.helper.getComponent('#header_user_info');
        //console.debug("MainController.updateHeaderLoginPanel : panel = ", panel);
        //console.debug("MainController.updateHeaderLoginPanel : user = ",Netra.user.getCurrentUser().name);

        var html = Netra.user.getCurrentUser().name + " , " + Netra.user.getCurrentUser().email ;

        panel.html = html;

    },


    onStart: function()
    {
        console.debug("MainController.onStart");

        this.updateHeaderLoginPanel();
        
        this.showView("Netra.view.dashboard.Dashboard");
        //this.showView("Netra.view.host.Hosts");
        //this.showView("Netra.view.cluster.Clusters");
        //this.showView("Netra.view.repository.Repository");
        //this.showView("Netra.view.xen.Xen");
        //this.showView("Netra.view.template.Template");
        //this.showView("Netra.view.monitoring.Monitoring");
        //this.showView("Netra.view.report.Report");
        //this.showView("Netra.view.provision.Provision");
        //this.showView("Netra.view.user.Users");
    },


    onBroadcastMessage: function(param)
    {
        console.debug("onBroadcastMessage!!!");
        console.debug(param);        

        //this.updateProgressbar(param.retcode);
        //this.getAreaLog().setValue(param.msg); 
    },


    onRequestNewView: function(param)
    {
        console.debug("MainController.onRequestNewView", param);

        this.showView(param['view']);
    },

    onInstanceStart: function()
    {
        console.debug("MainController.onInstanceStart");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Started!!!   ');
    },

    onInstanceStop: function()
    {
        console.debug("MainController.onInstanceStop");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Stopped!!!   ');
    },

    onInstanceSuspend: function()
    {
        console.debug("MainController.onInstanceSuspend");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Suspended!!!   ');
    },

    onInstanceResume: function()
    {
        console.debug("MainController.onInstanceResume");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Resumed!!!   ');
    },

    onInstanceReboot: function()
    {
        console.debug("MainController.onInstanceReboot");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Rebooted!!!   ');

    },
    
    onInstanceSnapshot: function()
    {
        console.debug("MainController.onInstanceSnapshot");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Snapshoted!!!   ');
    },

    onInstanceTerminate: function()
    {
        console.debug("MainController.onInstanceTerminate");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Terminated!!!   ');
    },

    onInstanceResize: function()
    {
        console.debug("MainController.onInstanceResize");
        Netra.helper.showToast(Netra.helper.getComponent('#panel_main_dashboard'),'Info','   Virtual Instance Resized!!!   ');
    },




    onBtnFlavorClick: function() 
    {
        console.debug("onBtnFlavorClick");
        this.showView("Netra.view.flavor.Flavors");
    },

    onBtnInstanceClick: function()
    {
        console.debug("onBtnInstanceClick");
        this.showView("Netra.view.instance.Instances");
    },

    onBtnImageClick: function()
    {
        console.debug("onBtnImageClick");
        this.showView("Netra.view.image.Images");
    },

    onBtnReportClick: function()
    {
        console.debug("onBtnReportClick");
        this.showView("Netra.view.report.Report");
    },

    onBtnConfigClick: function()
    {

    },

    onBtnDashboardClick: function()
    {
        console.debug("MainController.onBtnDashboardClick");
        this.showView("Netra.view.dashboard.Dashboard");
    },

    onBtnSnapshotClick: function()
    {
        console.debug("MainController.onBtnSnapshotClick");
        this.showView("Netra.view.snapshot.Snapshots");
    },

    onBtnTemplateClick: function()
    {
        console.debug("MainController.onBtnTemplateClick");
        this.showView("Netra.view.template.Template");
    },

    onBtnClusterClick: function()
    {
        console.debug("MainController.onBtnClusterClick");
        this.showView("Netra.view.cluster.Clusters");
    },

    onBtnSecurityGroupClick: function()
    {
        console.debug("MainController.onBtnSecurityGroupClick");
        this.showView("Netra.view.security.Securitys");
    },

    onBtnIpsClick: function()
    {
        console.debug("MainController.onBtnIpsClick");
        this.showView("Netra.view.network.Floatings");
    },

    onBtnUserClick: function()
    {
        console.debug("MainController.onBtnUserClick");
        this.showView("Netra.view.user.Users");
    },

    onBtnMonitoringClick: function()
    {
        console.debug("MainController.onBtnMonitoringClick");
        this.showView("Netra.view.monitoring.Monitoring");

    },

    onBtnXenClick: function()
    {
        console.debug("MainController.onBtnXenClick");
        this.showView("Netra.view.xen.Xen");
    },

    onBtnHostClick: function()
    {
        console.debug("MainController.onBtnHostClick");
        this.showView("Netra.view.host.Hosts");
    },

    onBtnProvisionClick: function()
    {
        console.debug("MainController.onBtnProvisionClick");
        this.showView("Netra.view.provision.Provision");
    },

    onBtnHeaderLogoutClick: function()
    {
        console.debug("MainController.onBtnHeaderLogoutClick");

        Netra.user.logout();
        
        this.showView("Netra.view.provision.Provision");
        Netra.helper.reloadPage();
    },

    onBtnHeaderConfigClick: function()
    {
        console.debug("MainController.onBtnConfigClick");

        this.configWindowController = Netra.manager.createController("Netra.view.component.window.Config");
        this.configWindowController.show();        
    },

});
