Ext.define('Netra.view.monitoring.Monitoring', {
    extend: 'Ext.panel.Panel',
    referenceHolder: true,
    //animCollapse: true,
    //collapsible: true,
    //collapseDirection: 'left',
    title: 'XEN Monitoring',
    titleAlign: 'center',

    requires: [
        'Netra.view.monitoring.MonitoringController',        
    ],

    controller: 'monitoring',
    id: 'panel_monitoring',
    
    layout: {
        type: 'border',
        //vertical: false,
        //animate: true,        
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: true,
        //headerPosition: 'left',
        titleAlign: 'center',
        anchor: '100%',
        align: 'stretch',
    },

    items: 
    [
        {
            xtype: 'xentree',
            id: 'xentree',
            region: 'west',
            //title: 'XEN',
            width: 200,
            rootVisible: false,
            useArrows: true,
            split: true,
            root: {
                text: 'Root',
                children: []
            }
        },

        {
            xtype: 'panel',
            region: 'center',            
            //title: 'Monitoring Manager',
            titleAlign: 'center',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_user',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: 
                    [              
                        { 
                            xtype:'combo',
                            fieldLabel:'Monitoring Interval',
                            name: 'monitoring_interval',
                            id: 'combo_monitoring_interval',
                            //displayField:'name',
                            //valueField: 'meter_id',
                            queryMode:'local',

                            store: ['10초','30초','1분','5분'],

                            editable: false,
                            autoSelect: true,
                            forceSelection: true,
                            labelWidth: 120,
                            width: 210,                            
                        },
                        { 
                            xtype: 'checkbox', 
                            id: 'checkbox_monitoring_reload', 
                            boxLabel: 'Auto Reload Chart',
                            inputValue: 'true',
                            margin: '0 0 0 30',
                        },                                  
                    ]
                }
            ],                    

            items:
            [
                {
                    xtype: 'multichart',
                    initAnimAfterLoad: false,
                    layout: 'fit',
                    id: 'chart_cpu',
                    debug: true,
                    flex: 1,

                    chartConfig: {
                        chart: {
                        //showAxes: true,
                        },
                        title: {
                            text: 'CPU'
                        },
                        legend: { enabled: true },
                        xAxis: {},
                        yAxis: [ {} ]
                    },

                    series: [],          
                },
                {
                    xtype: 'multichart',
                    initAnimAfterLoad: false,
                    layout: 'fit',
                    id: 'chart_memory',
                    debug: true,
                    flex: 1,

                    chartConfig: {
                        chart: {
                        //showAxes: true,
                        },
                        title: {
                            text: 'Memory'
                        },
                        legend: { enabled: true },
                        xAxis: {},
                        yAxis: [ {} ]
                    },

                    series: [],          
                },
                {
                    xtype: 'multichart',
                    initAnimAfterLoad: false,
                    layout: 'fit',
                    id: 'chart_network',
                    debug: true,
                    flex: 1,

                    chartConfig: {
                        chart: {
                        //showAxes: true,
                        },
                        title: {
                            text: 'Network'
                        },
                        legend: { enabled: true },
                        xAxis: {},
                        yAxis: [ {} ]
                    },

                    series: [],          
                },

            ]            

        }
    ],


    initComponent: function()
    {
        this.callParent(arguments);
        //console.debug("initComponent");
    }


});
