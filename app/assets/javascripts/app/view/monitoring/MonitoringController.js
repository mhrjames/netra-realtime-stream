Ext.define('Netra.view.monitoring.MonitoringController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.monitoring',

    refs: [],


    intervalRecord: null,
    graphReloader: null,
    taskRunner: null,


    init: function() 
    {
        console.debug("MonitoringController.init");

        this.control({
            '#btn_report_graph': { click: this.onButtonGraphClick },
            '#xentree': {
                itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },

            
            '#checkbox_monitoring_reload': {
                change: this.onAutoReloadChange
            },

            '#combo_monitoring_interval': {
                select: this.onMonitoringIntervalSelect,
                change: this.onMonitoringIntervalChange
            },


            '#panel_monitoring': { beforedestroy: this.onDestroy },
        });


        //this.getReference('chart_monitoring').bindStore( Netra.manager.getStore('storeMeterStats') );

        this.onStart();
    },



    getRequestParameter: function(meter_id)
    {
        var cur_date = new Date();

        var json = {};

        json['tenant_id'] = this.adminTenant.id;
        json['group_by'] = "";
        json['period'] = 60;
        json['meter_id'] = meter_id;
        json['start'] = Ext.Date.format(cur_date,'Y-m-d');
        json['end'] = Ext.Date.format(cur_date,'Y-m-d');

        return json;
    },

    drawGraph: function(json)
    {
        console.debug("MonitoringController.drawGraph : json = " , json);

        var store = Netra.manager.getStore("storeMeterStats");
        store.loadRawData(json);

        console.debug("MonitoringController.drawGraph : total count = " , store.getTotalCount());

        //console.debug("MonitoringController.drawGraph : record = " , store.getAt(0));

        this.getReference('chart_monitoring').bindStore(store);

    },

    clearTree: function(tree)
    {
        tree.getRootNode().removeAll();
    },

    addNodeToTree: function(tree,parent_node,text,node_type,record)
    {
        var node = parent_node.appendChild({
                leaf: true,
                text: text,
                nodeType: node_type,
                nodeRecord: record
        });

        return node;
    },

    addItemNodeToTree: function(tree,rootNode,text,record)
    {
        //var rootNode = this.getReference('tree_meter').getRootNode();
        var node = this.addNodeToTree(tree,rootNode, text, 'openstack', record);
        node.set("icon",Icons.nodeOpenstack);

        return node;
    },

    getComboSelectedIndex: function(combo_id)
    {
        var combo = Netra.helper.getComponent('#'+combo_id);
        if (! combo.getValue()) {
            return -1;
        }

        var record = combo.findRecord(combo.valueField || combobox.displayField, combo.getValue());
        return combo.store.indexOf(record);
    },

    getMonitoringInterval: function()
    {
        var new_interval = 0;
        
        switch (this.getComboSelectedIndex('combo_monitoring_interval')) {
            case -1:
                new_interval = -1;
                break;

            case 0:
                new_interval = 1000 * 10;
                break;

            case 1:
                new_interval = 1000 * 30;
                break;

            case 2:
                new_interval = 1000 * 60;
                break;

            case 3:
                new_interval = 1000 * 60 * 5;
                break;
        }

        return new_interval;
    },

    doConfigMonitoringInterval: function()
    {
        console.debug("MonitoringController.doConfigMonitoringInterval");

        
        var checkbox = Netra.helper.getComponent('#checkbox_monitoring_reload');

        if (! checkbox.getValue()) 
        {
            if (this.graphReloader) {
                console.debug("Monitoring.onAutoReloadChange : destroy");        
                this.graphReloader.stop();
                this.graphReloader.destroy();
            }
            return;
        }

        //store: ['10초','30초','1분','5분'],
        var new_interval = this.getMonitoringInterval();
        if (new_interval == -1) {
            Ext.MessageBox.alert('Error', 'Monitoring Interval을 선택하세요.');
            return;
        }

        console.debug("Monitoring.onAutoReloadChange : interval = ", new_interval);

        var _this = this;

        this.graphReloader = this.taskRunner.newTask({
            interval: new_interval,
            run: function () 
            {
                var date = new Date();
                console.debug("graphReloader.run : time = ", Netra.helper.formatDateTime(date));
            },
        });

        this.graphReloader.start();
    },




    onStart: function()
    {
        console.debug("MonitoringController.onStart");
        //this.loadTenantsData();
        //this.loadMetersData();
        var tree = Netra.helper.getComponent('#xentree');
        if (! tree) {
            console.debug("MonitoringController.onStart : tree is null");
        }
        console.debug("MonitoringController.onStart : tree = ",tree);

        //Netra.helper.showPanelLoadingMask(Netra.helper.getComponent('#xentree'),true);

        tree.updateTree();
        //tree.addPoolNodeToTree('asd','asd');
        //Netra.helper.getComponent('#xentree');

        this.taskRunner = new Ext.util.TaskRunner();

    },


    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
    },

    onButtonGraphClick: function()
    {
        console.debug("onButtonGraphClick");

        var meter = this.getReference('combo_report_meter').getRawValue();
        var start = this.getReference('text_report_start').getRawValue();
        var end = this.getReference('text_report_end').getValue();

        console.debug("onButtonGraphClick : start = " + start);

        var json = this.getRequestParameter();
        this.sendUsageRequest(json);
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());
        console.debug(record);
        
        if (record.data.nodeType == Netra.const.NODE_HOST) 
        {
            console.debug("MonitoringController.onNodeClick : host graph");
            var pool = record.parentNode.data.text;    
            Netra.service.getLatestHostPerfData(pool,record.data.nodeRecord.data.reference, this.onGetLatestHostPerfDataComplete);
        }

        if (record.data.nodeType == Netra.const.NODE_SERVER) 
        {
            console.debug("MonitoringController.onNodeClick : server graph");

            var host_node = record.parentNode;
            var pool_node = host_node.parentNode;

            var pool = pool_node.data.text;    
            Netra.service.getLatestVMPerfData(pool,host_node.data.nodeRecord.data.reference, record.data.nodeRecord.data.reference, this.onGetLatestServerPerfDataComplete);
        }

        //this.selectMeter(record.data.nodeRecord);
        //var json = this.getRequestParameter(this.meterRecord.data.name);
        //this.sendUsageRequest(json);

    },

    onTreeMenuRendered: function() 
    {
        console.debug("onTreeMenuRendered");
        //this.selectFirstNode();
    },

    onDestroy: function()
    {
        console.debug("MonitoringController.onDestroy");
    },
    
    onGetLatestHostPerfDataComplete: function(retcode,json)
    {
        console.debug("MonitoringController.onGetLatestHostPerfDataComplete : json = ",json);
        
        var chart_cpu = Netra.helper.getComponent('#chart_cpu');
        var chart_memory = Netra.helper.getComponent('#chart_memory');
        var chart_network = Netra.helper.getComponent('#chart_network');

        chart_cpu.clear();
        chart_memory.clear();
        chart_network.clear();
        
        var chart = null;

        for (var metric in json.data.metrics) 
        {
            //console.debug("onGetLatestHostPerfDataComplete : metric = " + metric);

            var data = Netra.helper.metricToArrayData(json.data.metrics[metric]);

            chart = null;
            if (Netra.helper.doesContainString(['cpu','loadavg'], metric)) 
            {
                chart = chart_cpu;
            } else if (Netra.helper.doesContainString(['memory_total','memory_free'], metric)) 
            {
                chart = chart_memory;
            } else if (Netra.helper.doesContainString(['pif_aggr'], metric)) 
            {
                chart = chart_network;
            }

            if (chart) { 
                chart.addGraph("line", metric, data);                
            }            
        }

    },

    onGetLatestServerPerfDataComplete: function(retcode,json)
    {
        console.debug("MonitoringController.onGetLatestServerPerfDataComplete : json = ",json);
        
        var chart_cpu = Netra.helper.getComponent('#chart_cpu');
        var chart_memory = Netra.helper.getComponent('#chart_memory');
        var chart_network = Netra.helper.getComponent('#chart_network');
        
        chart_cpu.clear();
        chart_memory.clear();
        chart_network.clear();

        var chart = null;

        for (var metric in json.data.metrics) 
        {
            var data = Netra.helper.metricToArrayData(json.data.metrics[metric]);

            chart = null;
            if (Netra.helper.doesContainString(['cpu','loadavg'], metric)) 
            {
                chart = chart_cpu;
            } else if (Netra.helper.doesContainString(['memory'], metric)) 
            {
                chart = chart_memory;
            } else if (Netra.helper.doesContainString(['pif_aggr'], metric)) 
            {
                chart = chart_network;
            }

            if (chart) { 
                chart.addGraph("line", metric, data);                
            }
        }

    },


    onAutoReloadChange: function(checkbox, newValue, oldValue, eOpts )
    {
        console.debug("Monitoring.onAutoReloadChange : value = ", newValue);

        this.doConfigMonitoringInterval();    
    },


    onMonitoringIntervalChange: function(checkbox, newValue, oldValue, eOpts )
    {
        //console.debug("Monitoring.onMonitoringIntervalChange : value = ", newValue);
    },


    onMonitoringIntervalSelect: function(combo, records)
    {
        console.debug("Monitoring.onMonitoringIntervalSelect : records = ", records);
        
        this.doConfigMonitoringInterval();
    },



});
