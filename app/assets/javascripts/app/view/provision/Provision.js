Ext.define('Netra.view.provision.Provision', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.provision',
    title: 'Provision Manager',
    titleAlign: 'center',
    //referenceHolder: true,
    //animCollapse: true,
    //collapsible: false,
    //collapseDirection: 'left',
    //id: 'container_provision',
    autoScroll: false,

    requires: [
        //'Plugin.codemirror.CodeMirror',
        'Netra.view.provision.ProvisionController',
    ],

    layout: {
        type: 'border',
    },

    controller: 'provisions',

    items: 
    [
/*
        {
            xtype: 'treepanel',
            id: 'provisiontree',
            title: 'Task',
            titleAlign: 'center',
            width: 200,
            rootVisible: false,
            useArrows: true,
            region: 'west',

            root: {
                text: 'Root',
                expanded: true,
                children: 
                [
                    {
                        text: 'Machine Catalog',
                        nodeType: Netra.const.NODE_CATALOG,
                    },
                    {
                        text: 'Delivery Group',
                        nodeType: Netra.const.NODE_DELIVERY,
                    },                    
                ]
            }
        },
*/        
        {
            xtype: 'panel',
            region: 'center',
            id: 'container_provision',
            frame: false,
            border: false,
            layout: {
                type: 'vbox',
                align: 'stretch',
            },
            
            items:
            [
                {
                    xtype: 'gridpanel',
                    id: 'grid_provision_task',
                    flex: 1,

                    store: Netra.manager.getStore('storeTasks'),

                    ui: 'footer',
                    header: {
                        margin: '5 5 5 5',
                        //titlePosition: 0,
                        items: [
                            { xtype: 'button', id: 'btn_task_new', text: 'New Provisioning Task', },

                            { xtype: 'button', id: 'btn_task_run', text: 'Run', margin: '0 0 0 10'},
                            { xtype: 'button', id: 'btn_task_delete', text: 'Delete'},
                            { xtype: 'button', id: 'btn_task_refresh', text: 'Refresh', margin: '0 0 0 10' },                            
                        ]
                    },

                    columns: 
                    [
                        {
                            text     : 'id',
                            width    : 50,
                            sortable : false,
                            hidden   : false,
                            dataIndex: 'id'
                        },
                        {
                            text     : 'Name',
                            flex     : 1,
                            dataIndex: 'name'
                        },        
/*
                        {
                            text     : 'Progress',
                            width    : 130,
                            dataIndex: 'progress'
                        },                              
                        {
                            text     : 'Status',
                            width    : 130,
                            dataIndex: 'status'
                        },
*/                        
                        {
                            text     : 'Created At',
                            flex     : 1,
                            dataIndex: 'created_at'
                        },        
                        {
                            text     : 'Executed At',
                            flex     : 1,
                            dataIndex: 'started_at'
                        },        
                        {
                            text     : 'Description',
                            flex     : 2,
                            dataIndex: 'description'
                        },                  
                    ],
                },

                {
                    xtype: 'gridpanel',
                    id: 'grid_provision_subtask',
                    flex: 1,

                    ui: 'footer',
                    header: {
                        margin: '5 5 5 5',
                        //titlePosition: 0,
                        items: [
                            { xtype: 'button', id: 'btn_provision_update', text: 'Update', },
                        ]
                    },

                    store: Netra.manager.getStore('storeSubtasks'),

                    columns: 
                    [
                        {
                            text     : 'id',
                            width    : 50,
                            sortable : false,
                            hidden   : false,
                            dataIndex: 'id'
                        },
                        {
                            text     : 'Command',
                            flex     : 1,
                            dataIndex: 'command'
                        },        
                        {
                            text     : 'Started At',
                            flex     : 1,
                            dataIndex: 'created_at'
                        },
                        {
                            text     : 'Finished At',
                            flex     : 1,
                            dataIndex: 'finished_at'
                        },                                          
                        {
                            text     : 'Status',
                            flex     : 1,
                            dataIndex: 'status'
                        },                              
                        {
                            text     : 'Progress',
                            flex     : 3,
                            dataIndex: 'progress',
                            renderer : function(value, meta) 
                            {
/*                                
                                if(parseInt(value) == 1) {
                                    meta.style = "background-color:green;";
                                } else {
                                    meta.style = "background-color:red;";
                                }
*/                                
                                //meta.attr = 'style="background-color:#ffaaaa !important;"';                                
                                return value;
                            }                            
                        },
                        {
                            text     : 'Task Ref',
                            flex     : 1,
                            dataIndex: 'task_ref',
                            hidden : true,
                        },                                                                                    
                        {
                            text     : 'Response',
                            flex     : 2,
                            dataIndex: 'result',
                            hidden : true,
                        },                               
                    ],
                },


            ],

        },
 
    ],





    initComponent: function() 
    {
        console.debug("Provision.initComponent");
        this.callParent(arguments);
    },


});
