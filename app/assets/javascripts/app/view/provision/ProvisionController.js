Ext.define('Netra.view.provision.ProvisionController', {
    extend: 'Netra.controller.BaseController',
    alias: 'controller.provisions',

    requires: [],

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridTask', selector: '#grid_task' },
    ],

    taskRecord: null,
    subtaskRecord: null,
    taskWindowController: null,
    assignTaskWindowController: null,


    init: function() 
    {
        console.debug("ProvisionController.init");

        this.control({
            '#grid_provision_task': { 
                itemclick: this.onProvisionGridColumnClick ,
                itemdblclick: this.onProvisionGridColumnDoubleClick ,
                rowcontextmenu: this.onProvisionGridContextMenuClick,
            },

            '#btn_task_new': { click: this.onButtonNewClick },
            '#btn_task_run': { click: this.onButtonRunClick },
            '#btn_task_delete': { click: this.onButtonDeleteClick },

            '#btn_task_refresh': { click: this.onButtonRefreshClick },
            '#btn_provision_update': { click: this.onButtonProvisionUpdateClick },
        });

        this.onStart();
    },

    showTaskWindow: function(record) 
    {
        console.debug("ProvisionController.showTaskWindow");

        this.taskWindowController = Netra.manager.createController("Netra.view.component.window.NewProvision");
        this.taskWindowController.show(record);        
    },


    selectTask: function(record)
    {
        this.taskRecord = record;
    },

    updateTaskView: function(record)
    {
        console.debug("ProvisionController.updateTaskView : record = ", record);

        Netra.service.loadSubtaskData(record.data.id, this.onLoadSubtaskComplete)
    },

    updateTaskLog: function(msg)
    {
        var logs = "", line = "";
        var json = JSON.parse(msg);

        console.debug("updateTaskLog : json length = ",json.length + " , json = ",json);

        for (var i=0;i<json.length;i++) {
            line = json[i].datetime + " [" + json[i].type + "] " + json[i].msg;
            logs = logs + line + "\n";
        }

        Netra.helper.getComponent('#area_task_log').setValue(logs);
    },

    runProvisionTask: function(record)
    {
        var _this = this;
        var pool = this.extractPoolFromTaskRecord(record);
        
        Ext.MessageBox.confirm('Confirm', 'Do you want to run the selected task?', function(result) {
            if (result == 'yes') {
                Netra.task.runProvisionTask(_this, {'pool': pool, 'task_id': record.data.id}, _this.onRunProvisionTaskComplete);
            }
        });
    },

    showProvisionGridPopupMenu: function(grid, index, record, event) 
    {
        console.debug("ProvisionController.showGridPopupMenu");

        var _this = this;

        event.stopEvent();

        if (! this.gridPopupMenu) 
        {
            this.gridPopupMenu = new Ext.menu.Menu({
                items: 
                [
                    {
                        text: 'Run',
                        handler: function() 
                        {
                            _this.runProvisionTask(record);
                        }
                    }, 
                    {
                        text: 'Delete',
                        handler: function() 
                        {
                            _this.deleteTaskRecord(record);
                        }
                    }

                ]
            });
        }
        
        this.gridPopupMenu.showAt(event.getXY());
    },

    deleteTaskRecord: function(record)
    {
        var _this = this;

        console.debug("ProvisionController.deleteTaskRecord");

        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(result) {
            if (result == 'yes') {
                Netra.task.deleteTaskRecord(_this,_this.extractPoolFromTaskRecord(record),record.data.id,_this.onDeleteTaskComplete);
            }
        });

    },

    extractPoolFromTaskRecord: function(record)
    {
        var pool = Netra.helper.extractString(record.data.parameters,'pool:','\n');
        return pool.trim();
    },

    loadTaskData: function()
    {
        Netra.service.loadTaskData(this.onLoadTaskComplete);
    },






    onStart: function()
    {
        console.debug("onStart");

        this.taskRecord = null;

        //this.createFlavorWindowController();
    },

    onProvisionGridColumnClick: function(grid, record) 
    {
        console.debug("onProvisionGridColumnClick : record = ", record);

        this.selectTask(record);
        this.updateTaskView(this.taskRecord);

        //this.selectTask(record);
        //this.updateTaskView(this.taskRecord);
        //Netra.helper.setActivePanelItem(this.getView(),1);
    },

    onProvisionGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onProvisionGridColumnDoubleClick : record = ", record);

        //this.selectTask(record);
        //this.updateTaskView(this.taskRecord);
        //Netra.helper.setActivePanelItem(this.getView(),1);
    },


    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
    },


    onButtonNewClick: function()
    {
        console.debug("ProvisionController.onButtonNewClick");
        this.showTaskWindow(null);
    },

    onButtonNewAssignClick: function()
    {
        console.debug("ProvisionController.onButtonNewAssignClick");
        this.showTaskWindow(null);
    },

    onButtonRunClick: function()
    {
        console.debug("ProvisionController.onButtonRunClick");

        if (! this.taskRecord) {
            Ext.MessageBox.alert('Error', 'Please select a task to run!');
            return;
        }

        this.runProvisionTask(this.taskRecord);
    },

    onButtonDeleteClick: function()
    {
        console.debug("ProvisionController.onButtonDeleteClick");
        if (! this.taskRecord) {
            Ext.MessageBox.alert('Error', 'Please select a task to delete!');
            return;
        }

        this.deleteTaskRecord(this.taskRecord);
    },

    onButtonRefreshClick: function()
    {
        console.debug("ProvisionController.onButtonRefreshClick");
        this.loadTaskData();
    },


    onButtonProvisionUpdateClick: function()
    {
        console.debug("ProvisionController.onButtonProvisionUpdateClick");

        if ( (this.taskRecord) && (Netra.manager.getStore("storeSubtasks").getTotalCount() > 0))
        {
            Netra.helper.showWaitDialog('Loading data from server','work in progress','btn_provision_update');
            Netra.task.getTaskStatus(this, this.extractPoolFromTaskRecord(this.taskRecord), this.taskRecord.data.id, this.onProvisionUpdateComplete);            
        }

    },

    onProvisionGridContextMenuClick: function(grid, record, tr, index, event, opts)     
    {
        console.debug("ProvisionController.onProvisionGridContextMenuClick : event = ",event);
        
        this.selectTask(record);
        this.showProvisionGridPopupMenu(grid, index, record, event);
    },

    onLoadSubtaskComplete: function(retcode, json)
    {
        console.debug("ProvisionController.onLoadSubtaskComplete");
        Netra.helper.hideWaitDialog();
    },

    onProvisionUpdateComplete: function(_this, retcode, json)
    {
        console.debug("ProvisionController.onProvisionUpdateComplete : json = ", json);
        _this.updateTaskView(_this.taskRecord);
    },

    onRunProvisionTaskComplete: function(_this, retcode, json)
    {
        console.debug("ProvisionController.onRunProvisionTaskComplete : json = ", json);  
        Ext.MessageBox.alert('Success', 'The selected task is running now, please click update button to get latest status!');
    },

    onDeleteTaskComplete: function(_this, retcode, json)
    {
        console.debug("ProvisionController.onDeleteTaskComplete : json = ", json);  
        _this.loadTaskData();
    },

    onLoadTaskComplete: function(retcode, json)
    {
        console.debug("ProvisionController.onLoadTaskComplete : json = ", json);
    },

});
