Ext.define('Netra.view.report.Report', {
    extend: 'Ext.panel.Panel',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',
    title: 'Usage Report',
    titleAlign: 'center',

    requires: [
        'Netra.view.report.ReportController',        
    ],

    controller: 'report',
    id: 'panel_report',
    
    layout: {
        type: 'border',
        //vertical: false,
        //animate: true,        
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: true,
        //headerPosition: 'left',
        titleAlign: 'center',
        anchor: '100%',
        align: 'stretch',
    },

    items: 
    [
        {
            xtype: 'panel',
            region: 'north',
            layout: 'vbox',
            bodyPadding: '20 30 30 30',

            items: 
            [
                {
                    xtype: 'component',
                    frame: false,
                    border: 0,
                    margin: '0 0 0 0',
                    html: Netra.helper.createFormSubTitle('Please select dates','')
                },     
                {
                    xtype: 'form',
                    layout: 'hbox',
                    frame: false,
                    border: false,

                    defaults: {
                        align: 'stretch',
                        //anchor: '100%',
                        labelWidth: 75,
                        margin: '0 10 0 10',
                        //labelAlign: 'top',
                        frame: false,
                        boder: false,      
                    },

                    items:
                    [
/*                    
                        {
                            xtype:'combo',
                            fieldLabel: 'Pool',
                            name:'cluster',
                            id: 'combo_report_pool',
                            valueField: 'name',
                            queryMode:'local',
                            store: {
                                type: 'clusters',
                                autoLoad: true,
                            },
                            displayField:'name',
                            editable: false,
                            autoSelect: true,
                            forceSelection: true,
                        },     
                        {
                            xtype:'combo',
                            fieldLabel:'Report',
                            name: 'report_meter',
                            id: 'combo_report_type',
                            displayField:'name',
                            valueField: 'meter_id',
                            queryMode:'local',

                            store: ['일간보고서','주간보고서','월간보고서'],

                            editable: false,
                            autoSelect: true,
                            forceSelection: true,
                            width: 180,                            
                        },
*/                        
                        {
                            fieldLabel : "Start Date",
                            xtype: 'datefield',
                            id: 'text_report_start',
                            name: 'report_start',
                            format: 'Y-m-d',
                            editable: false,
                            //width: 200,                            
                        },
                        {
                            fieldLabel : "End Date",
                            xtype: 'datefield',

                            id: 'text_report_end',
                            name: 'report_end',
                            format: 'Y-m-d',
                            editable: false,
                            //width: 180,
                        },
                        {
                            xtype: 'button',
                            text: 'Report',
                            id: 'btn_report_report',
                            margin: '0 0 0 30',
                        },
                        {
                            xtype: 'button',
                            text: 'Excel',
                            id: 'btn_report_excel',
                            margin: '0 10 0 0',
                        }                        
                    ]
                },

            ],
        },
        {
            xtype: 'tabpanel',
            region: 'center',
            id: 'report_tab',
            //title: 'Report',

            items: 
            [
                {
                    xtype: 'panel',
                    title: 'Overall Chart',
                    autoScroll: true,

                    items:
                    [
                        {
                            xtype: 'multichart',
                            initAnimAfterLoad: false,
                            layout: 'fit',
                            title: 'Chart',
                            id: 'chart_report_overall_cpu',
                            debug: true,
                            flex: 1,

                            chartConfig: {
                                chart: {
                                //showAxes: true,
                                },
                                title: {
                                    text: 'CPU'
                                },
                                legend: { enabled: true },
                                xAxis: {},
                                yAxis: [ {} ]
                            },

                            series: [],          
                        },
                        {
                            xtype: 'multichart',
                            initAnimAfterLoad: false,
                            layout: 'fit',
                            title: 'Chart',
                            id: 'chart_report_overall_memory',
                            debug: true,
                            flex: 1,

                            chartConfig: {
                                chart: {
                                //showAxes: true,
                                },
                                title: {
                                    text: 'Memory'
                                },
                                legend: { enabled: true },
                                xAxis: {},
                                yAxis: [ {} ]
                            },

                            series: [],          
                        },
                        {
                            xtype: 'multichart',
                            initAnimAfterLoad: false,
                            layout: 'fit',
                            title: 'Chart',
                            id: 'chart_report_overall_network',
                            debug: true,
                            flex: 1,

                            chartConfig: {
                                chart: {
                                //showAxes: true,
                                },
                                title: {
                                    text: 'Network'
                                },
                                legend: { enabled: true },
                                xAxis: {},
                                yAxis: [ {} ]
                            },

                            series: [],          
                        }

                    ]

                },
                {
                    xtype: 'gridpanel',
                    id: 'grid_report_host_all',
                    flex: 1,
                    title: 'Host Overall',
                    titleAlign: 'center',
                    stripeRows: true,
                    columnLines: true,

                    store: Netra.manager.getStore('storeHostStats'),

                    columns: [
                        {
                            text     : 'IP',
                            flex     :  1,
                            sortable : true,
                            dataIndex: 'ip'
                        },        
                        {
                            text     : 'CPU Avg',
                            flex     :  1,
                            sortable : true,
                            dataIndex: 'cpu_avg'
                        },                        
                        {
                            text     : 'Average Load',
                            flex     :  1,
                            sortable : true,
                            dataIndex: 'loadavg'
                        },
                        {
                            text     : 'Total Memory',
                            flex     :  1,
                            dataIndex: 'memory_total_kib'    
                        },
                        {
                            text     : 'Used Memory',
                            flex     :  1,
                            dataIndex: 'memory_free_kib'    
                        },
                        {
                            text     : 'Network Sent',
                            flex     :  1,
                            dataIndex: 'pif_aggr_tx'    
                        },
                        {
                            text     : 'Network Received',
                            flex     :  1,
                            dataIndex: 'pif_aggr_rx'    
                        }
                    ],
                },
                {
                    xtype: 'container',
                    title: 'Host Detail',

                    items:
                    [
                       {
                            xtype: 'multichart',
                            initAnimAfterLoad: false,
                            layout: 'fit',
                            title: 'Chart',
                            id: 'chart_report_host_cpu',
                            debug: true,
                            flex: 1,

                            chartConfig: {
                                chart: {
                                //showAxes: true,
                                },
                                title: {
                                    text: 'CPU'
                                },
                                legend: { enabled: true },
                                xAxis: {},
                                yAxis: [ {} ]
                            },

                            series: [],          
                        },
                        {
                            xtype: 'multichart',
                            initAnimAfterLoad: false,
                            layout: 'fit',
                            title: 'Chart',
                            id: 'chart_report_host_memory',
                            debug: true,
                            flex: 1,

                            chartConfig: {
                                chart: {
                                //showAxes: true,
                                },
                                title: {
                                    text: 'Memory'
                                },
                                legend: { enabled: true },
                                xAxis: {},
                                yAxis: [ {} ]
                            },

                            series: [],          
                        },
                        {
                            xtype: 'multichart',
                            initAnimAfterLoad: false,
                            layout: 'fit',
                            title: 'Chart',
                            id: 'chart_report_host_network',
                            debug: true,
                            flex: 1,

                            chartConfig: {
                                chart: {
                                //showAxes: true,
                                },
                                title: {
                                    text: 'Network'
                                },
                                legend: { enabled: true },
                                xAxis: {},
                                yAxis: [ {} ]
                            },

                            series: [],          
                        },

                        {
                            xtype: 'gridpanel',
                            id: 'grid_report_host_detail',
                            flex: 1,
                            title: 'Host Detail',
                            titleAlign: 'center',
                            //stripeRows: true,
                            //columnLines: true,

                            store: Netra.manager.getStore('storeHostStatDetails'),

                            columns: [
                                {
                                    text     : 'Date/Time',
                                    flex     :  1,
                                    sortable : true,
                                    dataIndex: 'date',
                                    renderer : function(value) 
                                    {
                                        return Netra.helper.utcToDate(value);
                                    }

                                },        
                                {
                                    text     : 'CPU Avg',
                                    flex     :  1,
                                    sortable : true,
                                    dataIndex: 'cpu_avg'
                                },                        
                                {
                                    text     : 'Average Load',
                                    flex     :  1,
                                    sortable : true,
                                    dataIndex: 'loadavg'
                                },
                                {
                                    text     : 'Total Memory',
                                    flex     :  1,
                                    dataIndex: 'memory_total_kib'    
                                },
                                {
                                    text     : 'Used Memory',
                                    flex     :  1,
                                    dataIndex: 'memory_free_kib'    
                                },
                                {
                                    text     : 'Network Sent',
                                    flex     :  1,
                                    dataIndex: 'pif_aggr_tx'    
                                },
                                {
                                    text     : 'Network Received',
                                    flex     :  1,
                                    dataIndex: 'pif_aggr_rx'    
                                }
                            ],

                        }
                    ]

                },                
            ]
        }
    ],


    initComponent: function()
    {
        this.callParent(arguments);
        //console.debug("initComponent");
    }


});
