Ext.define('Netra.view.report.ReportController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.report',

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'textStart', selector: '#text_report_start' },
        { ref: 'textEnd', selector: '#text_report_end' },        
    ],


    hostStatData: null,



    init: function() 
    {
        console.debug("ReportController.init");

        this.control({
            '#grid_report_host_all': { 
                itemdblclick: this.onHostOverviewGridColumnDoubleClick,
            },

            '#btn_report_report': { click: this.onButtonReportClick },
            '#btn_report_excel': { click: this.onButtonExcelClick },
            '#panel_report': { beforedestroy: this.onDestroy },
        });


        //this.getReference('chart_report').bindStore( Netra.manager.getStore('storeMeterStats') );


        this.onStart();
    },


    sendUsageRequest: function(json)
    {
        var _this = this;

        console.debug("ReportController.sendUsageRequest");
        
        Netra.helper.showWaitDialog('Loading data from server','work in progress','btn_report_graph');

        Ext.Ajax.request({
            url: '/api/openstack/ceilometer/stat',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendUsageRequest Response = ', response);
                var json = JSON.parse(response.responseText);

                _this.drawGraph(json);

                //Netra.helper.showPanelLoadingMask(_this.getReference('panel_report'),false);
                Netra.helper.hideWaitDialog();
            },
            failure: function(response, opts) 
            {
                console.debug('sendUsageRequest : woops');
                //Netra.helper.showPanelLoadingMask(_this.getReference('panel_report'),false);                
                Netra.helper.hideWaitDialog();
            }
        });

    },

    setHostStatData: function(json)
    {
        this.hostStatData = json;
    },

    showHostOverallDataOnGrid: function(json)
    {
        var store = Netra.manager.getStore("storeHostStats");
        store.data.removeAll();  

        for (var i=0;i<json.data.length;i++) 
        {
            var host = json.data[i];
            var record = Ext.create('Netra.model.HostStat');
            
            record.data.ip = host.ip;
            record.data.loadavg = host.total_avg.loadavg;
            record.data.cpu_avg = host.total_avg.cpu_avg;
            record.data.memory_free_kib = host.total_avg.memory_free_kib;
            record.data.memory_total_kib = host.total_avg.memory_total_kib;
            record.data.pif_aggr_rx = host.total_avg.pif_aggr_rx;
            record.data.pif_aggr_tx = host.total_avg.pif_aggr_tx;

            store.add(record);
        }

    },

    drawHostOverallChart: function(json)
    {
        var chart_cpu = Netra.helper.getComponent('#chart_report_overall_cpu');
        var chart_memory = Netra.helper.getComponent('#chart_report_overall_memory');
        var chart_network = Netra.helper.getComponent('#chart_report_overall_network');

        chart_cpu.clear();
        chart_memory.clear();
        chart_network.clear();
    
        for (var index = 0; index < json.data.length; index++) 
        {
            chart_cpu.addGraph("bar", json.data[index].ip, [json.data[index].total_avg.loadavg]);
            chart_memory.addGraph("bar", json.data[index].ip, [json.data[index].total_avg.memory_free_kib]);
            chart_network.addGraph("bar", json.data[index].ip, [json.data[index].total_avg.pif_aggr_rx]);
        }
    },

    showHostDetailDataOnGrid: function(host_ip)
    {
        var store = Netra.manager.getStore("storeHostStatDetails");
        store.data.removeAll();  

        var json = this.hostStatData;
        var host, record = null;

        for (var i=0;i<json.data.length;i++) 
        {
            host = json.data[i];
            
            if (host.ip != host_ip) {
                continue;
            }    

            for (var index=0;index<host.avg.cpu_avg.items.length;index++)
            {
                record = Ext.create('Netra.model.HostStatDetail');
                
                record.data.date = host.avg.loadavg.items[index].last_updated;
                record.data.loadavg = Netra.helper.sanitizeValue(host.avg.loadavg.items[index].value);
                record.data.cpu_avg = Netra.helper.sanitizeValue(host.avg.cpu_avg.items[index].value);
                record.data.memory_free_kib = Netra.helper.sanitizeValue(host.avg.memory_free_kib.items[index].value);
                record.data.memory_total_kib = Netra.helper.sanitizeValue(host.avg.memory_total_kib.items[index].value);
                record.data.pif_aggr_rx = Netra.helper.sanitizeValue(host.avg.pif_aggr_rx.items[index].value);
                record.data.pif_aggr_tx = Netra.helper.sanitizeValue(host.avg.pif_aggr_tx.items[index].value);

                store.add(record);                
            }

        }

    },

    drawHostDetailChart: function(host_ip)
    {
        var chart_cpu = Netra.helper.getComponent('#chart_report_host_cpu');
        var chart_memory = Netra.helper.getComponent('#chart_report_host_memory');
        var chart_network = Netra.helper.getComponent('#chart_report_host_network');

        chart_cpu.clear();
        chart_memory.clear();
        chart_network.clear();
    
        var json = this.hostStatData;

        for (var index = 0; index < json.data.length; index++) 
        {
            host = json.data[index];
            
            if (host.ip != host_ip) {
                continue;
            }    

            var data_loadavg = Netra.helper.metricToArrayData(host.avg.loadavg.items);
            var data_cpu_avg = Netra.helper.metricToArrayData(host.avg.cpu_avg.items);

            chart_cpu.addGraph("line", 'CPU Average', data_cpu_avg);
            chart_cpu.addGraph("line", 'Load Average', data_loadavg);

            var data_memory_free = Netra.helper.metricToArrayData(host.avg.memory_free_kib.items);
            var data_memory_total = Netra.helper.metricToArrayData(host.avg.memory_total_kib.items);

            chart_memory.addGraph("line", "Free Memory", data_memory_free);
            chart_memory.addGraph("line", "Total Memory", data_memory_total);
            
            var data_network_rx = Netra.helper.metricToArrayData(host.avg.pif_aggr_rx.items);
            var data_network_tx = Netra.helper.metricToArrayData(host.avg.pif_aggr_tx.items);

            chart_network.addGraph("line", "Network Sent", data_network_tx);
            chart_network.addGraph("line", "Network Received", data_network_rx);
        }
    },



    onStart: function()
    {
        console.debug("ReportController.onStart");
    },

    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
    },

    onButtonReportClick: function()
    {
        console.debug("ReportController.onButtonReportClick");

/*
        var pool = Netra.helper.getComponent('#combo_report_pool').getRawValue();
        var meter = Netra.helper.getComponent('#combo_report_type').getRawValue();
        if (pool.length == 0 ) {
            Ext.MessageBox.alert('Error', 'Please select a pool to report!!!');
            return;
        }

        if (meter.length == 0 ) {
            Ext.MessageBox.alert('Error', 'Please select a usage meter to report!!!');
            return;
        }
*/

        var start = Netra.helper.getComponent('#text_report_start').getRawValue();
        var end = Netra.helper.getComponent('#text_report_end').getValue();

        if (start.length == 0 ) {
            Ext.MessageBox.alert('Error', 'Please select start date to report!!!');
            return;
        }

        if (end.length == 0 ) {
            Ext.MessageBox.alert('Error', 'Please select end date to report!!!');
            return;
        }

        console.debug("onButtonReportClick : start = " + start);

        Netra.helper.showWaitDialog('Loading data from server','Work in progress','btn_report_report');

        Netra.task.requestDailyAllHostData(this, 'pool', start, end, this.onRequestDailyAllHostDataComplete);
    },

    onButtonExcelClick: function()
    {
        console.debug("ReportController.onButtonExcelClick");

        var start = Netra.helper.getComponent('#text_report_start').getRawValue();
        var end = Netra.helper.getComponent('#text_report_end').getValue();

        if (start.length == 0 ) {
            Ext.MessageBox.alert('Error', 'Please select start date to report!!!');
            return;
        }

        if (end.length == 0 ) {
            Ext.MessageBox.alert('Error', 'Please select end date to report!!!');
            return;
        }

        Netra.helper.showWaitDialog('Loading data from server','Work in progress','btn_report_report');

        Netra.task.requestDailyAllHostDataInExcel(this, 'pool', start, end, this.onRequestDailyAllHostDataComplete);
    },


    onDestroy: function()
    {
        console.debug("ReportController.onDestroy");
    },

    onRequestDailyAllHostDataComplete: function(controller, retcode, json)
    {
        var _this = controller;

        console.debug("ReportController.onRequestDailyAllHostDataComplete : json = ", json);

        Netra.helper.hideWaitDialog();
        
        _this.setHostStatData(json);
        _this.showHostOverallDataOnGrid(json);
        _this.drawHostOverallChart(json);



/*
        for (var index = 0; index < json.data.length; index++) 
        {
            //console.debug("onRequestDailyAllHostDataComplete : metric = " , json.data[index].avg.loadavg);

            var data_loadavg = Netra.helper.metricToArrayData(json.data[index].avg.loadavg.items);
            var data_cpu_avg = Netra.helper.metricToArrayData(json.data[index].avg.cpu_avg.items);

            console.debug("onRequestDailyAllHostDataComplete : data = " , data_loadavg);

            chart.addGraph("bar", json.data[index].ip, data_loadavg);
        }
*/

    },

    onHostOverviewGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("ReportController.onHostOverviewGridColumnDoubleClick : record = ",record);

        this.showHostDetailDataOnGrid(record.data.ip);
        this.drawHostDetailChart(record.data.ip);

        Netra.helper.setActiveTab('report_tab',2);
    },

});
