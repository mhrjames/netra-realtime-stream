Ext.define('Netra.view.snapshot.Snapshots', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.snapshots',
    title: 'Virtual Machine Snapshot',
    titleAlign: 'center',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',

    requires: [
        'Ext.layout.container.Accordion',
        'Netra.view.snapshot.SnapshotsController',        
    ],

    layout: {
        type: 'accordion',
        animate: true,        
        vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: false,
        headerPosition: 'left',
        titleAlign: 'center',
    },

    controller: 'snapshots',

    items: 
    [
        {
            xtype: 'gridpanel',
            id: 'grid_snapshot',
            reference: 'grid_snapshot',
            title: 'List',
            stripeRows: true,
            columnLines: true,

            store: Netra.manager.getStore('storeSnapshots'),

            selType: 'checkboxmodel',
            selModel: {
                checkOnly: true,
                injectCheckbox: 0
            },

            columns: [
                {
                    text     : 'id',
                    flex     :  1,
                    sortable : false,
                    hidden   : false,
                    dataIndex: 'id'
                },
                {
                    text     : 'Status',
                    width    : 50, 
                    dataIndex: 'status',
                    align    : 'center',
                    renderer: function(value, metaData, record, row, col, store, gridView)
                    {
                        console.debug("Snapshot.grid : value = ",  value); // + " , record = ", record);
                        return Netra.helper.createActiveIcon(value.toString().toLowerCase() == 'available');
                    }                                                                                
                },                        
                {
                    text     : 'Name',
                    flex     :  3,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'display_name'
                },        
                {
                    text     : 'Project ID',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'os-extended-snapshot-attributes:project_id'
                },
                {
                    text     : 'Created',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'created_at'
                },
                {
                    text     : 'Size(GB)',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'size'
                },                                
                {
                    text     : 'Progress',
                    flex     :  2,
                    sortable : true,
                    //renderer : 'usMoney',
                    dataIndex: 'os-extended-snapshot-attributes:progress'
                },
                {
                    text     : 'Description',
                    flex     :  2,
                    sortable : true,
                    //renderer : this.change,
                    dataIndex: 'display_description'
                },
            ],
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: Netra.manager.getStore('storeSnapshots'),
                displayInfo: true,
                plugins: new Ext.ux.ProgressBarPager()
            },

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_snapshot',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_snapshot_delete', 
                            text: 'Delete',
                        },           
                        { 
                            xtype: 'button', 
                            id: 'btn_snapshot_refresh', 
                            text: 'Refresh',
                        },                                  
                    ]
                }
            ],   

        },
 
        {
            xtype: 'panel',
            title: 'Details',
            items: 
            [
                {

                    xtype: 'form',
                    reference: 'form_snapshot',                    
                    id: 'form_snapshot',
                    bodyPadding: '30 60 30 60',
                    autoScroll: true,
                    frame: false,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 180,
                    },

                    items: [],

                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_snapshot2',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_snapshot_back', 
                            text: 'Back',
                        },           
                    ]
                }
            ],   

        }
    ],     



    initComponent: function() 
    {
        console.debug("Image.initComponent");
        this.callParent(arguments);
    },


});
