Ext.define('Netra.view.snapshot.SnapshotsController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.snapshots',

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridSnapshot', selector: '#grid_snapshot' },
    ],

    snapshotRecord: null,
    imageWindowController: null,


    init: function() 
    {
        console.debug("SnapshotsController.init");

        this.control({
            '#grid_snapshot': { itemdblclick: this.onGridColumnDoubleClick },
            '#form_image': { render: this.onFormRender },
            '#btn_snapshot_delete': { click: this.onButtonDeleteClick },
            '#btn_snapshot_refresh': { click: this.onButtonRefreshClick },
            '#btn_snapshot_back': { click: this.onButtonBackClick },
        });

        //this.createStoreFlavor();
        this.onStart();
    },


    selectImage: function(record)
    {
        this.snapshotRecord = record;
    },

    filloutForm: function(record)
    {

        console.debug("SnapshotController.filloutForm : record = ", record);

        var form = this.getReference('form_snapshot');
        var store = Netra.manager.getStore('storeImages');

        Netra.builder.buildForm(form,store,record.data,'Virtual Machine Snapshot Details','');

/*
        var form = this.getReference('form_image');
        var tpl = new Ext.XTemplate(
            '<h2>hello</h2>'
        );

        console.debug(record);
        console.debug(form);

        form.update({ image: record.data });

        //form.doComponentLayout();
*/
    },

    showImageWindow: function() 
    {
        console.debug("showFlavorWindow");

        //Ext.create("Netra.view.component.window.NewFlavor").show();

        //this.flavorWindowController = Ext.app.Application.createController("Netra.view.component.window.NewFlavor");
        //this.flavorWindowController = Netra.app.getApplication().createController("Netra.view.component.window.NewFlavor");
        //this.flavorWindowController = Netra.manager.createController("Netra.view.component.window.NewFlavor");
        this.imageWindowController = Netra.manager.createController("Netra.view.component.window.NewImage");
        this.imageWindowController.show();        
        //this.flavorWindowController.init();

    },

    sendDeleteRequest: function(json)
    {
        var _this = this;
        
        console.debug("Snapshots.sendRequest");

        Ext.Ajax.request({
            url: '/api/openstack/snapshots/delete',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendDeleteRequest Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'Selected Template is deleted!!!');
                    _this.refreshData();
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }
                Netra.helper.hideWaitDialog();
            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Netra.helper.hideWaitDialog();
            }
        });

    },

    refreshData: function()
    {
        var _this = this;

        Netra.helper.refreshData('storeSnapshots','#grid_snapshot',function() {
            //_this.filterImageData();
            Netra.helper.hideWaitDialog();
        });
    },

    filterImageData: function()
    {
        var store = Netra.manager.getStore('storeImages');

        console.debug("ImagesController.filterImageData : store = ", store);

        store.clearFilter();

        store.filterBy(function(record) 
        {

            var property = record.get('properties');

            console.debug("property : " , property);

            if (property.hasOwnProperty('image_type')) {
                if (property.image_type.toLowerCase() == "snapshot") {
                    return true;
                }
            } 
            return false;
        });

    },





    onStart: function()
    {
        console.debug("onStart");

        this.snapshotRecord = null;
        this.refreshData();

        //this.createFlavorWindowController();
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.selectImage(record);
        this.filloutForm(this.snapshotRecord);
        Netra.helper.setActivePanelItem(this.getView(),1);

        //var comps = Ext.ComponentManager.getAll();
        //console.debug(comps);
        //Netra.helper.dumpComponents(null);
    },

    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
        //console.debug(form.body);
/*
        var html = "<h3>World!!!</h3>";

        var panel = form.up("panel");
        //var tpl = Ext.create('Ext.Template', 'World!!!');

        tpl.overwrite(panel.body, null);
        //form.setConfig('html', html);
*/
    },


    onButtonNewClick: function()
    {
        console.debug("onButtonNewClick");
        this.showImageWindow();
    },

    onButtonDeleteClick: function()
    {
        var _this = this;

        console.debug("ImagesController.onButtonDeleteClick");

        //var selected = this.getReGridFlavor().getSelectionModel().getSelection();
        var selected = this.getReference('grid_snapshot').getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select snapshot to delete!');
            return;
        }

        console.debug(selected);

        var json = {};
        json['id'] = Netra.helper.getSelectedIds(this.getReference('grid_snapshot'));

        Netra.helper.showWaitDialog("","",'btn_snapshot_delete');
        this.sendDeleteRequest(json);
    },

    onButtonEditClick: function()
    {
        console.debug("onButtonEditClick");
        console.debug("this", this);
    },

    onButtonRefreshClick: function()
    {
        console.debug("SnapshotController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_snapshot_refresh');
        this.refreshData();
    },

    onButtonBackClick: function()
    {
        console.debug("SnapshotController.onButtonBackClick");
        Netra.helper.setActivePanelItem(this.getView(),0);        
    },

});
