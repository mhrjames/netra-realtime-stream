Ext.define('Netra.view.template.Template', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.template',
    //id: 'container_template',
    title: 'Template Manager',
    titleAlign: 'center',
    referenceHolder: true,
    controller: 'template',

    requires: [
        'Netra.model.Template',
        'Netra.view.template.TemplateController',
    ],

    layout: {
        type: 'hbox',
        align: 'stretch',
        //animate: true,        
        //vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        autoScroll: true,
        //headerPosition: 'left',
        titleAlign: 'center',
    },



    items: 
    [
        {
            xtype: 'treepanel',
            id: 'xentemplate',
            //title: 'Xen Templates',
            width: 200,
            rootVisible: false,
            useArrows: true,

            root: {
                text: 'Root',
                expanded: true,
                children: []
            }
        },    
        {
            xtype: 'panel',
            id: 'container_template',
            flex: 1,
            layout: {
                type: 'card',
            },

            items: [
            {
                xtype: 'gridpanel',
                id: 'grid_template',
                reference: 'grid_template',
                //title: 'Template List',
                //stripeRows: true,
                //columnLines: true,

                store: Netra.manager.getStore('storeTemplates'),

                selType: 'checkboxmodel',
                selModel: {
                    checkOnly: true,
                    injectCheckbox: 0
                },

                ui: 'footer',
                header: {
                    margin: '5 5 5 0',
                    //titlePosition: 1,
                    items: [
                        { 
                            xtype: 'button', 
                            id: 'btn_template_refresh', 
                            text: 'Refresh',
                        },
                    ]
                },

                columns: [
                    {
                        text     : 'Name',
                        flex     :  2,
                        dataIndex: 'name'
                    },
                    {
                        text     : 'Reference',
                        flex     :  1,
                        dataIndex: 'reference'
                    },                    
                    {
                        text     : 'CPU',
                        flex     :  1,
                        dataIndex: 'vcpus_max'
                    },
                    {
                        text     : 'Memory',
                        flex     :  1,
                        dataIndex: 'memory_max',
                        renderer : function(value) 
                        {
                            return Netra.helper.formatByteNumber(value);
                        }
                    },
                    {
                        text     : 'Storage Size',
                        flex     :  1,
                        dataIndex: 'storage',
                        renderer : function(value) 
                        {
                            //console.debug("Template.GridInstance : value = ", value);
                            var storage_size = 0;
                            value.forEach(function(record) {
                                //console.debug("Template.GridInstance : record = ", record['size']);    
                                storage_size += parseInt(record['size']);
                            });

                            return Netra.helper.formatByteNumber(storage_size);
                        }

                    },
                    {
                        text     : 'Network',
                        flex     :  1,
                        dataIndex: 'network',
                        renderer : function(value) 
                        {
                            //console.debug("Template.GridInstance : value = ", value);
                            
                            var eths = "";
                            value.forEach(function(record) {
                                if (eths.length > 0) {
                                    eths += ", ";
                                }
                                eths += "eth" + record['device'];
                            });
                            return eths;
                        }                        
                    },                        
                    {
                        text     : 'Description',
                        flex     :  3,
                        dataIndex: 'description'
                    },                
                ],
                bbar: {
                    xtype: 'pagingtoolbar',
                    pageSize: 10,
                    store: Netra.manager.getStore('storeTemplates'),
                    displayInfo: true,
                    plugins: new Ext.ux.ProgressBarPager()
                },
            },
            {
                xtype: 'panel',
                title: 'Package Details',
                layout: 'fit',  
                ui: 'footer',
                header: {
                    titlePosition: 1,
                    items: [
                        { 
                            xtype: 'button', 
                            id: 'btn_repository_back', 
                            text: 'Back'
                        },

                    ]
                },

                items: [
                    {
                        xtype: 'softwaredetail',
                    }
                ],
            }

            ]

        },

    ],




    initComponent: function() 
    {
        console.debug("Repository.initComponent");
        this.callParent(arguments);
    },


});
