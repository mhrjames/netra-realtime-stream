Ext.define('Netra.view.template.TemplateController', {
    extend: 'Netra.controller.BaseController',
    alias: 'controller.template',

    requires: [
    ],

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridFloating', selector: '#grid_template' },
    ],

    poolRecord: null,
    templateRecord: null,
    floatingDialogController: null,


    init: function() 
    {
        console.debug("TemplateController.init");

        this.control({
            '#xentemplate': {
                itemclick: this.onNodeClick,
            },            

            '#grid_template': { 
                itemdblclick: this.onGridColumnDoubleClick,
                rowcontextmenu: this.onGridContextMenuClick 
            },

            '#form_repository': { render: this.onFormRender },
            '#btn_repository_new': { click: this.onButtonNewClick },
            '#btn_repository_delete': { click: this.onButtonDeleteClick },
            '#btn_template_refresh': { click: this.onButtonRefreshClick },
            '#btn_repository_back': { click: this.onButtonBackClick },
        });

        //this.createStoreFlavor();
        this.onStart();
    },


    selectPool: function(record)
    {
        this.poolRecord = record;
    },

    selectTemplate: function(record)
    {
        this.templateRecord = record;
    },

    filloutForm: function(record)
    {

        console.debug("TemplateController.filloutForm");

        var form = this.getReference('form_repository');
        var store = Netra.manager.getStore('storeRepositorys');

        Netra.builder.buildForm(form,store,record.data,'Module Details','');

/*
        var form = this.getReference('form_repository');
        var tpl = new Ext.XTemplate(
            '<h2>hello</h2>'
        );

        console.debug(record);
        console.debug(form);

        form.update({ image: record.data });

        //form.doComponentLayout();
*/

    },

    showFloatingDialog: function() 
    {
        console.debug("TemplateController.showFloatingDialog");

        this.floatingDialogController = Netra.manager.createController("Netra.view.component.window.NewFloatingIp");
        this.floatingDialogController.show();        

    },

    sendDeleteRequest: function(json)
    {
        var _this = this;
        
        console.debug("TemplateController.sendRequest");
        
        Ext.Ajax.request({
            url: '/api/openstack/floating_ips/delete',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendDeleteRequest Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'Selected Template is deleted!!!');
                    _this.refreshData();
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }

                Netra.helper.hideWaitDialog();
            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Netra.helper.hideWaitDialog();
            }
        });

    },

    refreshData: function()
    {
        Netra.helper.refreshData('storeTemplates','#grid_template', function() {
            Netra.helper.hideWaitDialog();
        });
    },


    addPoolNodeToTree: function(text,record)
    {
        //var subnetNode = this.getTreeRole().getStore().getNodeById('node_subnet');
        var tree = Netra.helper.getComponent('#xentemplate');
        var node = this.addNodeToTree(tree, tree.getRootNode(), text, Netra.const.NODE_HOST, record);
        //node.set("icon",Icons.nodeRole);

        tree.getView().refresh();

        return node;
    },

    loadPoolData: function()
    {
        var _this = this;

        Netra.service.loadAllTemplates( function(retcode, json) {
            console.debug("TemplateController.loadPoolData : json = ",json);
            _this.onLoadPoolDataComplete(json['data']);
        });

    },

    updateTree: function(poolRecords)
    {
        var tree = Netra.helper.getComponent('#xentemplate');
        this.clearTree(tree);

        for (var key in poolRecords) 
        {
            var data = poolRecords[key];
            this.addPoolNodeToTree(key,data);
        }
    },

    loadTemplateData: function(pool)
    {
        var template_store = Netra.manager.getStore('storeTemplates');
        template_store.removeAll();

        template_store.load({
            params: {
                'pool': pool
            },
            callback: this.onLoadTemplateDataComplete,
            scope: this
        });

    },

    deleteTemplate: function(record)
    {
        var parameters_in_json = {};
        parameters_in_json['pool'] = this.poolRecord.data.text;
        parameters_in_json['vm_ref'] = record.data.reference;

        console.debug("TemplateController.deleteTemplate : params = ", parameters_in_json);
        
        Netra.service.deleteTemplate(parameters_in_json, this.onDeleteTemplateComplete);
    },

    makeCachedTemplate: function(record)
    {
        var parameters_in_json = {};
        parameters_in_json['pool'] = this.poolRecord.data.text;
        parameters_in_json['template_ref'] = record.data.reference;

        console.debug("TemplateController.makeCachedTemplate : params = ", parameters_in_json);
        
        Netra.service.makeCachedTemplate(parameters_in_json, this.onMakeCachedTemplateComplete);
    },

    showGridPopupMenu: function(grid, index, record, event) 
    {
        console.debug("TemplateController.showGridPopupMenu");

        var _this = this;

        event.stopEvent();

        if (! this.gridPopupMenu) 
        {
            this.gridPopupMenu = new Ext.menu.Menu({
                items: 
                [
                    {
                        text: 'Delete',
                        handler: function() 
                        {
                            _this.deleteTemplate(record);
                        }
                    },
                    {
                        text: 'Make Cached Template',
                        handler: function() 
                        {
                            _this.makeCachedTemplate(record);
                        }
                    },                    
                ]
            });
        }
        
        this.gridPopupMenu.showAt(event.getXY());
    },





    onStart: function()
    {
        console.debug("TemplateController.onStart");

        this.poolRecord = null;
        this.templateRecord = null;

        //this.refreshData();
        //this.updateTree();

        Netra.helper.showWaitDialog('Loading data from server','Work in progress','btn_report_report');
        this.loadPoolData();
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("TemplateController.onNodeClick : index = " + index + " , name = " + record.data.text.toLowerCase());
        console.debug("TemplateController.onNodeClick : item = ", record);

        this.selectPool(record);

        this.loadTemplateData(record.data.text);

        if (record.data.nodeType == Netra.const.NODE_HOST) 
        {
            //this.selectHost(record.data.nodeRecord);
            //this.updateHostPanel(this.hostRecord);
        }
        
        //this.filterInstanceStore(this.hypervisorRecord);

        //var json = this.getRequestParameter(this.meterRecord.data.name);
        //this.sendUsageRequest(json);

    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("TemplateController.onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.selectTemplate(record);
        //Netra.helper.showCardView('container_template',1,'','');
    },

    onGridContextMenuClick: function(grid, record, tr, index, event, opts)     
    {
        console.debug("TemplateController.onGridContextMenuClick : event = ",event);
        
        this.selectTemplate(record);
        this.showGridPopupMenu(grid, index, record, event);
    },

    onFormRender: function(form, opts)
    {

    },


    onButtonNewClick: function()
    {
        console.debug("TemplateController.onButtonNewClick");

        this.showFloatingDialog();
    },

    onButtonDeleteClick: function()
    {
        var _this = this;

        console.debug("TemplateController.onButtonDeleteClick");

        //var selected = this.getReGridFlavor().getSelectionModel().getSelection();
        var selected = this.getReference('grid_template').getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select software to delete!');
            return;
        }

        console.debug(selected);

        var json = {};
        json['id'] = Netra.helper.getSelectedIds(this.getReference('grid_template'));

        Netra.helper.showWaitDialog("","",'btn_repository_delete');
        this.sendDeleteRequest(json);
    },

    onButtonEditClick: function()
    {
        console.debug("onButtonEditClick");
        console.debug("this", this);
    },

    onButtonRefreshClick: function()
    {
        console.debug("TemplateController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_template_refresh');
        this.loadPoolData();
    },

    onButtonBackClick: function()
    {
        console.debug("TemplateController.onButtonBackClick");
        Netra.helper.showCardView('container_repository',0,'','');
    },

    onLoadPoolDataComplete: function(poolRecords)
    {
        Netra.helper.hideWaitDialog();
        this.updateTree(poolRecords);
    },

    onLoadTemplateDataComplete: function(retcode, response)
    {
        console.debug("TemplateController.onLoadTemplateDataComplete : response = ", response);

        Netra.helper.hideWaitDialog();
    },

    onDeleteTemplateComplete: function(retcode, response)
    {
        console.debug("TemplateController.onDeleteTemplateComplete : response = ", response);  
    },

    onMakeCachedTemplateComplete: function(retcode, response)
    {
        console.debug("TemplateController.onMakeCachedTemplateComplete : response = ", response);  
    },
});
