Ext.define('Netra.view.tenant.Tenants', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.tenants',
    title: 'Tenants',
    titleAlign: 'center',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',

    requires: [
        'Netra.model.Tenant',
        //'Netra.store.Users',    
        'Netra.view.tenant.TenantsController',
    ],

    layout: {
        type: 'accordion',
        animate: true,        
        vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: false,
        headerPosition: 'left',
        titleAlign: 'center',
    },

    controller: 'tenants',

    items: 
    [
        {

            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items:
            [
   
                {
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },

                    items: 
                    [
                        {
                            xtype: 'component',
                            flex: 1,
                            id: 'number_tenant_total',
                            html: Netra.builder.createNumberWidget("hello","432",""),
                        },
                    ]

                },

                {
                    xtype: 'gridpanel',
                    id: 'grid_tenant',
                    reference: 'grid_tenant',
                    title: 'List',
                    flex: 1,                    
                    stripeRows: true,
                    columnLines: true,
                    store: Netra.manager.getStore('storeTenants'),

                    selType: 'checkboxmodel',
                    selModel: {
                        checkOnly: true,
                        injectCheckbox: 0
                    },

                    columns: 
                    [
                        {
                            text     : 'id',
                            width    : 250,
                            dataIndex: 'id'
                        },
                        {
                            text     : 'Enabled',
                            flex     :  1,
                            dataIndex: 'enabled',
                            align: 'center',
                            renderer: function(value, metaData, record, row, col, store, gridView)
                            {
                                console.debug("Tenants.grid.renderer : value = ",value, " , record = ", record);
                                return Netra.helper.createActiveIcon(value.toString().toLowerCase() == 'true');
                            }                    
                        },                      
                        {
                            text     : 'Name',
                            flex     :  2,
                            sortable : true,
                            //renderer : 'usMoney',
                            dataIndex: 'name'
                        },                  
                        {
                            text     : 'Description',
                            flex     :  5,
                            sortable : true,
                            //renderer : 'usMoney',
                            dataIndex: 'description'
                        },

                    ],
                    bbar: {
                        xtype: 'pagingtoolbar',
                        pageSize: 10,
                        store: Netra.manager.getStore('storeTenants'),
                        displayInfo: true,
                        plugins: new Ext.ux.ProgressBarPager()
                    },

                }

            ],


            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_tenant',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_new', 
                            text: 'New'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_delete', 
                            text: 'Delete',
                        },           
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_refresh', 
                            text: 'Refresh',
                        },                                  
                    ]
                }
            ],        

        },
 
        {
            xtype: 'panel',
            title: 'Details',
            layout: 'fit',

            items: 
            [
                {

                    xtype: 'form',
                    reference: 'form_tenant',                    
                    id: 'form_tenant',
                    bodyPadding: '30 60 30 60',
                    autoScroll: true,
                    frame: false,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 180,
                    },

                    items: [],

                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_tenant2',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: 
                    [              
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_form_back', 
                            text: 'Back'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_edit', 
                            text: 'Edit',
                        },                                        
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_quota', 
                            text: 'Quota'
                        },

                    ]
                }
            ],        

        },

       {
            xtype: 'panel',
            title: 'Quota',
            layout: 'fit',
            reference: 'panel_tenant_quota',

            items: 
            [
                {
                    xtype: 'form',
                    reference: 'form_tenant_quota',                    
                    id: 'form_tenant_quota',
                    bodyPadding: '30 60 30 60',
                    autoScroll: true,
                    frame: false,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 180,
                    },

                    items: 
                    [
                       {
                            xtype: 'slider',
                            fieldLabel : "VM Instance",
                            id: 'slider_form_quota_instance',
                            name: 'instances',
                            margin: '10 0 0 0',
                            formBind: true,
                        },   
                        {
                            xtype: 'slider',
                            fieldLabel : "Virtual CPU",
                            id: 'slider_form_quota_vcpu',
                            name: 'cores',
                            margin: '10 0 0 0',
                            //formBind: true,
                        },
                        {
                            xtype: 'slider',
                            fieldLabel : "Virtual RAM",                            
                            id: 'slider_form_quota_vram',
                            name: 'ram',
                            increment: 1024,
                            margin: '10 0 0 0',
                            //formBind: true,
                        },         
                        {
                            xtype: 'slider',
                            fieldLabel : "Floating IPs",
                            id: 'slider_form_quota_floating',
                            name: 'floating_ips',
                            margin: '10 0 0 0',
                            //formBind: true,
                        }, 
                        {
                            xtype: 'slider',
                            fieldLabel : "Security Group",
                            id: 'slider_form_quota_security',
                            name: 'security_groups',
                            margin: '10 0 0 0',
                            //formBind: true,
                        }, 
                    
                    ],

                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_tenant3',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_quota_back', 
                            text: 'Back'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_tenant_quota_update', 
                            text: 'Update'
                        },

                    ]
                }
            ],        

        },

    ],





    initComponent: function() 
    {
        console.debug("Image.initComponent");
        this.callParent(arguments);
    },


});
