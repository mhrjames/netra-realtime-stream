Ext.define('Netra.view.tenant.TenantsController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.tenants',

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridTenant', selector: '#grid_tenant' },
    ],

    tenantRecord: null,
    limitRecord: null,
    tenantWindowController: null,
    tenantQuotaWindowController: null,


    init: function() 
    {
        console.debug("UsersController.init");

        this.control({
            '#grid_tenant': { itemdblclick: this.onGridColumnDoubleClick },
            '#form_tenant': { render: this.onFormRender },
            '#btn_tenant_new': { click: this.onButtonNewClick },
            '#btn_tenant_edit': { click: this.onButtonEditClick },
            '#btn_tenant_delete': { click: this.onButtonDeleteClick },
            '#btn_tenant_refresh': { click: this.onButtonRefreshClick },
            '#btn_tenant_quota': { click: this.onButtonQuotaClick },
            '#btn_tenant_form_back': { click: this.onButtonFormBackClick },
            '#btn_tenant_quota_back': { click: this.onButtonQuotaBackClick },
            '#btn_tenant_quota_update': { click: this.onButtonQuotaUpdateClick },
        });

        //this.createStoreFlavor();
        this.onStart();
    },

    selectTenant: function(record)
    {
        this.tenantRecord = record;
    },

    filloutForm: function(record)
    {

        console.debug("filloutForm");

        var form = this.getReference('form_tenant');
        var store = Netra.manager.getStore('storeTenants');

        Netra.builder.buildForm(form,store,record.data,'Tenants Details','');

    },

    filloutQuotaForm: function(record)
    {
        console.debug("TenantsController.filloutQuotaForm : record = " , record);

        var form = this.getReference('form_tenant_quota');

        Netra.builder.buildFormExt(form, record,'Tenants Quota','');
    },

    showWindow: function(record) 
    {
        console.debug("TenantController.showWindow");

        //Ext.create("Netra.view.component.window.NewFlavor").show();

        //this.flavorWindowController = Ext.app.Application.createController("Netra.view.component.window.NewFlavor");
        //this.flavorWindowController = Netra.app.getApplication().createController("Netra.view.component.window.NewFlavor");
        //this.flavorWindowController = Netra.manager.createController("Netra.view.component.window.NewFlavor");
        this.tenantWindowController = Netra.manager.createController("Netra.view.component.window.NewTenant");
        this.tenantWindowController.show(record);        
        //this.flavorWindowController.init();

    },

    showQuotaUpdateWindow: function(record) 
    {
        console.debug("TenantController.showQuotaUpdateWindow");

        this.tenantQuotaWindowController = Netra.manager.createController("Netra.view.component.window.UpdateTenantQuota");
        this.tenantQuotaWindowController.show(record);        
        //this.flavorWindowController.init();

    },

    sendDeleteRequest: function(json)
    {
        var _this = this;

        console.debug("TenantsController.sendRequest");
        
        Ext.Ajax.request({
            url: '/api/openstack/tenants/delete',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendDeleteRequest Response = ', response);

                Netra.helper.hideWaitDialog();

                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.Msg.alert('Success', 'Selected Tenant is deleted!!!');
                    _this.refreshData();
                } else {
                    Ext.Msg.alert('Error', json.msg);
                }
                    
            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Netra.helper.hideWaitDialog();
                Ext.MessageBox.alert('Error', "Something is wrong!!!");
            }
        });

    },

    sendGetLimitRequest: function(tenant_id)
    {
        var _this = this;

        console.debug("TenantsController.sendGetLimitRequest : id = " + tenant_id);
        

        Netra.service.getTenantQuota(tenant_id, function(success, json) {
            console.debug('sendGetLimitRequest json = ', json);

            if (! success) return;

            _this.limitRecord = _this.jsonToModel(json);

            _this.filloutQuotaForm(_this.limitRecord);

            Netra.helper.hideWaitDialog();            
            Netra.helper.setActivePanelItem(_this.getView(),2);
        });

    },

    sendGetLimitRequest2: function(tenant_id)
    {
        var _this = this;

        console.debug("TenantsController.sendGetLimitRequest : id = " + tenant_id);
        
        Netra.helper.showPanelLoadingMask(this.getReference('panel_tenant_quota'),true);

        Ext.Ajax.request({
            url: '/api/openstack/quotas',
            method: 'POST',
            jsonData: {'tenant_id': tenant_id},
            success: function(response, opts) 
            {
                var json = JSON.parse(response.responseText);
                console.debug('sendGetLimitRequest : Response = ', json);

                _this.limitRecord = _this.jsonToModel(json);

                _this.filloutQuotaForm(_this.limitRecord);
                
                Netra.helper.showPanelLoadingMask(_this.getReference('panel_tenant_quota'),false);

            },
            failure: function(response, opts) 
            {
                console.debug('sendGetLimitRequest : woops');
                Netra.helper.showPanelLoadingMask(_this.getReference('panel_tenant_quota'),false);                
            }
        });

    },

    refreshData: function()
    {

        Netra.helper.refreshData('storeTenants','#grid_tenant',function() {
            Netra.helper.hideWaitDialog();
        });
    },

    loadTenantSummaryData: function()
    {
        var end_date = new Date();
        var start_date = end_date;

        Netra.service.getTenantSumary(Ext.Date.format(start_date,'Y-m-d'),Ext.Date.format(end_date,'Y-m-d'), function(response) {
            
            var json = JSON.parse(response.responseText);            
            console.debug("tenants.loadTenantSummaryData : response = ",json);

        });
    },

    jsonToModel: function(json)
    {
        var model = Ext.create('Netra.model.Quota');
        for (var prop in json) 
        {
            model.set(prop, json[prop]);
            //console.debug("jsonData : item = ",item);
        }

        console.debug("jsonToModel : model = ",model );

        return model;
    },





    onStart: function()
    {
        console.debug("onStart");

        this.tenantRecord = null;
        this.limitRecord = null;
        
        this.refreshData();
        //this.loadTenantSummaryData();
        //this.createFlavorWindowController();
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick : record = ", record);

        this.selectTenant(record);
        this.filloutForm(this.tenantRecord);
        Netra.helper.setActivePanelItem(this.getView(),1);
    },

    onFormRender: function(form, opts)
    {
        console.debug("onFormRender");
    },


    onButtonNewClick: function()
    {
        console.debug("onButtonNewClick");
        this.showWindow(null);
    },

    onButtonDeleteClick: function()
    {
        var _this = this;

        console.debug("TenantsController.onButtonDeleteClick");

        var selected = this.getReference('grid_tenant').getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select snapshot to delete!');
            return;
        }

        console.debug(selected);

        var json = {};
        json['id'] = Netra.helper.getSelectedIds(this.getReference('grid_tenant'));

        this.sendDeleteRequest(json);

    },

    onButtonEditClick: function()
    {
        console.debug("onButtonEditClick");
        this.showWindow(this.tenantRecord);
    },

    onButtonRefreshClick: function()
    {
        console.debug("TenantsController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_tenant_delete');
        this.refreshData();
    },

    onButtonQuotaClick: function()
    {
        console.debug("TenantsController.onButtonQuotaClick");
        
        Netra.helper.showWaitDialog("Work in progress...","Work in progress...",'btn_tenant_quota');
        this.sendGetLimitRequest(this.tenantRecord.data.id);
    },

    onButtonFormBackClick: function()
    {
        console.debug("TenantsController.onButtonFormBackClick");
        Netra.helper.setActivePanelItem(this.getView(),0);
    },

    onButtonQuotaBackClick: function()
    {
        console.debug("TenantsController.onButtonQuotaBackClick");
        Netra.helper.setActivePanelItem(this.getView(),1);
    },

    onButtonQuotaUpdateClick: function()
    {
        console.debug("TenantsController.onButtonQuotaUpdateClick");
        this.showQuotaUpdateWindow(this.tenantRecord);
    },

});
