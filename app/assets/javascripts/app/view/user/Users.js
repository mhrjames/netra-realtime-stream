Ext.define('Netra.view.user.Users', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.users',
    title: 'User Manager',
    titleAlign: 'center',


    requires: [
        'Netra.model.AdUser',
        'Netra.view.user.UsersController',
    ],

    layout: {
        type: 'border',
        //animate: true,        
        //vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        titleAlign: 'center',
        autoScroll: false,
        //headerPosition: 'left',
        //titleAlign: 'center',
    },

    controller: 'users',

    items: 
    [
        {
            xtype: 'treepanel',
            id: 'tree_user',
            region: 'west',
            //title: 'Users',
            width: 170,
            rootVisible: false,
            useArrows: true,
            split: true,
            root: {
                text: 'Root',
                children: []
            }
        },
        {
            xtype: 'panel',
            id: 'container_user',
            region: 'center',
            layout: {
                type: 'card',
                //align: 'stretch',
            },
            
            items: 
            [

                {
                    xtype: 'panel',
                    region: 'center',

                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },

                    defaults: {
                        titleAlign: 'left',
                        autoScroll: true,
                        headerPosition: 'left',
                        titleAlign: 'center',
                    },

                    items:
                    [
                            
                        {
                            xtype: 'gridpanel',
                            id: 'grid_user',
                            flex: 3,
                            //title: 'List',
                            stripeRows: true,
                            columnLines: true,

                            store: Netra.manager.getStore('storeAdUsers'),

        /*
                            selType: 'checkboxmodel',
                            selModel: {
                                checkOnly: true,
                                injectCheckbox: 0
                            },
        */

                            columns: 
                            [
                                {
                                    text     : 'Account',
                                    flex     : 1,
                                    hidden   : true,
                                    dataIndex: 'account'
                                },
                                {
                                    text     : 'Name',
                                    flex     :  1.5,
                                    //align    : 'center',
                                    dataIndex: 'name',
                                },                                           
                                {
                                    text     : 'User ID',
                                    flex     :  2,
                                    sortable : true,
                                    dataIndex: 'userprincipalname'
                                },  
                                {
                                    text     : 'Oganization Unit',
                                    flex     :  2,
                                    dataIndex: 'dn',
                                    renderer: function(value)
                                    {
                                        return Netra.helper.extractString(value,"OU=",",");
                                    }                    
                                },                      
                                {
                                    text     : 'Domain',
                                    flex     : 2,
                                    sortable : true,
                                    hidden: true,
                                    dataIndex: 'dn'
                                },
                                {
                                    text     : 'Last Logon',
                                    flex     :  2,
                                    sortable : true,
                                    dataIndex: 'last_logon'
                                },
                                {
                                    text     : 'Last Logoff',
                                    flex     :  2,
                                    sortable : true,
                                    dataIndex: 'last_logoff'
                                },
                            ],

                            dockedItems: 
                            [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    id: 'toolbar_user',
                                    ui: 'footer',
                                    //defaults: {minWidth: minButtonWidth},
                                    items: [              
                                        { 
                                            xtype: 'button', 
                                            id: 'btn_user_assign', 
                                            text: 'Assign VMs'
                                        },
                                        { 
                                            xtype: 'button', 
                                            id: 'btn_user_refresh', 
                                            text: 'Refresh',
                                        },                                  
                                    ]
                                }
                            ],                    
                        },

               
                        {
                            xtype: 'tabpanel',
                            flex: 3,
                            items:
                            [
                                {
                                    title: 'Virtual Machine',
                                    xtype: 'panel',

                                    bodyBorder: false,
                                    border: false,
                                    bodyPadding: '10 20 0 20',

                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch',
                                    },

                                    items: 
                                    [
                                        {
                                            xtype: 'component',
                                            frame: false,
                                            border: 0,
                                            margin: '0 0 0 0',
                                            html: Netra.helper.createFormSubTitle('Assigned VM Info','')
                                        },                                

                                        {
                                            xtype: 'form',
                                            id: 'form_user_vm',

                                            bodyBorder: false,
                                            border: false,
                                            margin: '-20 0 0 0',

                                            layout: {
                                                type: 'hbox',
                                                //align: 'stretch',
                                            },
                                            defaults: {
                                                xtype: "textfield",
                                                //anchor: '100%',
                                                labelWidth: 70,
                                                labelAlign: 'top',
                                                readOnly: true,
                                                //disabled: true,                                                
                                            },      

                                            items : 
                                            [
                                                {
                                                    fieldLabel : "Power State",
                                                    id: 'text_user_vm_power',
                                                    name: 'gateway',
                                                    value: "vagrant", 
                                                    width: 90,          
                                                },
                                                {
                                                    fieldLabel : "Name",
                                                    id: 'text_user_vm_name',
                                                    name: 'name',
                                                    value: "192.168.56.159",            
                                                },                                        
                                                {
                                                    fieldLabel : "CPU",
                                                    id: 'text_user_vm_cpu',
                                                    name: 'hostname',
                                                    value: "192.168.56.159",
                                                    width: 70,
                                                },
                                                {
                                                    fieldLabel : "Memory",
                                                    id: 'text_user_vm_memory',
                                                    name: 'ip',
                                                    value: "root",
                                                    width: 70,
                                                },
                                                {
                                                    fieldLabel : "Disk",
                                                    id: 'text_user_vm_disk',
                                                    name: 'netmask',
                                                    value: "vagrant",
                                                    width: 70,
                                                },
                                                {
                                                    fieldLabel : "Template",
                                                    id: 'text_user_vm_template',
                                                    name: 'template',
                                                    value: "vagrant",
                                                    hidden: true,
                                                },
                                                {
                                                    fieldLabel : "Machine Catalog",
                                                    id: 'text_user_vm_catalog',
                                                    name: 'catalog',
                                                    value: "vagrant",
                                                },                                                                                        
                                                {
                                                    fieldLabel : "Delivery Group",
                                                    id: 'text_user_vm_group',
                                                    name: 'group',
                                                    value: "vagrant",
                                                },                                        

                                            ]

                                        },
                                        {
                                            xtype: 'component',
                                            frame: false,
                                            border: 0,
                                            margin: '10 0 0 0',
                                            html: Netra.helper.createFormSubTitle('IP Configuration','')
                                        },                                

                                        {
                                            xtype: 'form',
                                            id: 'form_user_static',

                                            bodyBorder: false,
                                            border: false,
                                            margin: '-20 0 0 0',

                                            layout: {
                                                type: 'hbox',
                                                //align: 'stretch',
                                            },
                                            defaults: {
                                                xtype: "textfield",
                                                anchor: '100%',
                                                labelWidth: 70,
                                                labelAlign: 'top',
                                                width: 145,
                                            },      

                                            items : 
                                            [
/*                                            
                                                {
                                                    xtype: 'checkboxfield',
                                                    fieldLabel : "Static",
                                                    id: 'check_user_bootproto',
                                                    name: 'bootproto',
                                                    inputValue: "static",
                                                    checked: true,
                                                },
*/                                                
                                                {
                                                    fieldLabel : "NIC ID",
                                                    id: 'text_user_nic_id',
                                                    name: 'nic_id',
                                                    value: "0",
                                                    width: 90,
                                                },                                            
                                                {
                                                    fieldLabel : "Host Name",
                                                    id: 'text_user_host',
                                                    name: 'hostname',
                                                    value: "192.168.56.159",            
                                                },
                                                {
                                                    fieldLabel : "IP",
                                                    id: 'text_user_ip',
                                                    name: 'ip',
                                                    value: "root",
                                                },
                                                {
                                                    fieldLabel : "Netmask",
                                                    id: 'text_user_netmask',
                                                    name: 'netmask',
                                                    value: "vagrant",
                                                },
                                                {
                                                    fieldLabel : "Gateway",
                                                    id: 'text_user_gateway',
                                                    name: 'gateway',
                                                    value: "vagrant",           
                                                },
                                                {
                                                    fieldLabel : "DNS",
                                                    id: 'text_user_dns1',
                                                    name: 'dns1',
                                                    value: "vagrant",           
                                                },
                                            ]

                                        },
                                    
                                    ],

                                    dockedItems: 
                                    [
                                        {
                                            xtype: 'toolbar',
                                            dock: 'top',
                                            id: 'toolbar_user_vm_window',
                                            ui: 'footer',
                                            //defaults: {minWidth: minButtonWidth},
                                            items: 
                                            [
                                                { xtype: 'component', flex: 1 },
                                                { xtype: 'button', id: 'btn_user_vm_info', text: 'VM Info', margin: '0 30 0 0'},
                                                { xtype: 'button', id: 'btn_user_unassign', text: 'Unassign VM', margin: '0 30 0 0'},
                                                { xtype: 'button', id: 'btn_user_vm_save', text: 'Save', },
                                                { xtype: 'button', id: 'btn_user_vm_delete', text: 'Delete', margin: '0 30 0 0' },
                                                { xtype: 'button', id: 'btn_user_vm_start', text: 'Start VM' },
                                                { xtype: 'button', id: 'btn_user_vm_reboot', text: 'Reboot VM', },                                      
                                                { xtype: 'button', id: 'btn_user_vm_shutdown', text: 'Shutdown VM',},
                                                { xtype: 'button', id: 'btn_user_vm_force_shutdown', text: 'Force Shutdown VM',},
                                            ]
                                        }
                                    ],

                                }                        

                            ]
                        }

                    ]
                },

                {
                    xtype: 'serverdetail',
                    title: 'Server Detail',
                    id: 'serverdetail_user',
                    
                    height: 1500,

                    ui: 'header',
                    header: {
                        padding: '5 5 5 5',
                        titlePosition: 1,
                        items: 
                        [
                            { 
                                xtype: 'button', 
                                id: 'btn_user_vm_back', 
                                text: 'Back',
                            },
                        ]
                    },

                }
            ]            
        }
    ],





    initComponent: function() 
    {
        console.debug("UsersController.initComponent");

        this.callParent(arguments);
    },


});
