Ext.define('Netra.view.user.UsersController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.users',

    refs: [],

    userRecord: null,
    poolRecord: null,
    vdiRecord: null,
    userinfoRecord: null,
    xenserverRecord: null,
    assignWindowController: null,


    init: function() 
    {
        console.debug("UsersController.init");

        this.control({
            '#tree_user': {
                itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            }, 

            '#grid_user': { 
                itemclick: this.onUserGridColumnClick ,
                itemdblclick: this.onGridColumnDoubleClick 
            },

            '#btn_user_assign': { click: this.onButtonAssignClick },
            '#btn_user_refresh': { click: this.onButtonRefreshClick },
            '#btn_user_unassign': { click: this.onButtonUnassignClick },


            '#btn_user_vm_info': { click: this.onButtonVMInfoClick },
            '#btn_user_vm_save': { click: this.onButtonVMSaveClick },
            '#btn_user_vm_start': { click: this.onButtonStartClick },
            '#btn_user_vm_reboot': { click: this.onButtonRebootClick },
            '#btn_user_vm_shutdown': { click: this.onButtonShutdownClick },
            '#btn_user_vm_force_shutdown': { click: this.onButtonForceShutdownClick },
            '#btn_user_vm_delete': { click: this.onButtonDeleteClick },

            '#btn_user_vm_back': { click: this.onButtonVMBackClick },
        });

        //this.createStoreFlavor();
        this.onStart();
    },


    selectUser: function(record)
    {
        this.userRecord = record;
    },

    showAssignWindow: function(record) 
    {
        console.debug("UserController.showAssignWindow");
        this.assignWindowController = Netra.manager.createController("Netra.view.component.window.NewAssign");
        this.assignWindowController.show(record);
    },

    refreshData: function()
    {
        Netra.helper.refreshData('storeAdUsers','#grid_user',function() {
            Netra.helper.hideWaitDialog();
        });
    },

    updateUserTree: function()
    {
        console.debug("UserController.updateUserTree");

        var store = Netra.manager.getStore("storeOus");
        store.removeAll();

        store.load({
            callback: this.onOULoaded,
            scope: this
        });

    },

    filterUserStore: function(record)
    {
        console.debug("UserController.filterUserStore : record = ", record);

        var store = Netra.manager.getStore('storeAdUsers');
        
        store.clearFilter();
        
        if (record != null) 
        {
            var key_ou = Netra.helper.extractString(record.data.dn,"OU=",",");

            store.filterBy(function(a_record, index) 
            {
                console.debug("UserController.filterUserStore : a_record = ", a_record);

                if (a_record.data.dn != null) 
                {
                    var ou = Netra.helper.extractString(a_record.data.dn,"OU=",",");

                    if (ou.toUpperCase() == key_ou.toUpperCase()) {
                        return true;
                    }                     
                }
                return false;
            });
        }        
    },

    getSelectedIds: function()
    {
        var grid = Netra.helper.getComponent('#grid_user');
        var selected = grid.getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select user to delete!');
            return null;
        }

        console.debug("getSelectedIds : selected = ", selected);

        var json = {};
        json['id'] = Netra.helper.getSelectedIds(grid);

        return json;
    },

    clearVMPanel: function()
    {
        Netra.helper.getComponent('#form_user_vm').clearForm();
        Netra.helper.getComponent('#form_user_static').clearForm();
    },

    updateVMPanel: function(record)
    {
        //this.clearVMPanel();
        console.debug("UserController.updateVMPanel : record = ", record);

        this.clearVMPanel();

        this.userinfoRecord = Netra.manager.getStore("storeUserInfos").findRecord('principalusername', record.data.userprincipalname);
        if (this.userinfoRecord)
        {
            Netra.helper.getComponent('#text_user_nic_id').setValue(this.userinfoRecord.data.device_id);
            Netra.helper.getComponent('#text_user_host').setValue(this.userinfoRecord.data.hostname);
            Netra.helper.getComponent('#text_user_ip').setValue(this.userinfoRecord.data.ip);
            Netra.helper.getComponent('#text_user_netmask').setValue(this.userinfoRecord.data.netmask);
            Netra.helper.getComponent('#text_user_gateway').setValue(this.userinfoRecord.data.gateway);
            Netra.helper.getComponent('#text_user_dns1').setValue(this.userinfoRecord.data.dns1);
        }

        var key_name = "{" + record.data.userprincipalname + "}";
        this.vdiRecord = Netra.manager.getStore("storeXenVDIs").findRecord('AssociatedUserUPNs', key_name);

        if (! this.vdiRecord) return;

        Netra.helper.getComponent('#text_user_vm_catalog').setValue(this.vdiRecord.data.CatalogName);
        Netra.helper.getComponent('#text_user_vm_group').setValue(this.vdiRecord.data.DesktopGroupName);

        this.xenserverRecord = Netra.manager.getStore("storeXenServers").findRecord('uuid', this.vdiRecord.data.HostedMachineId);
        if (this.xenserverRecord)
        {
            //console.debug("UserController.updateVMPanel : server_record = ", server_record);
            Netra.helper.getComponent('#text_user_vm_power').setValue(this.xenserverRecord.data.power_state);
            Netra.helper.getComponent('#text_user_vm_name').setValue(this.xenserverRecord.data.name);
            Netra.helper.getComponent('#text_user_vm_cpu').setValue(this.xenserverRecord.data.vcpus_max);
            Netra.helper.getComponent('#text_user_vm_memory').setValue(Netra.helper.formatByteNumber(this.xenserverRecord.data.memory_max));
            Netra.helper.getComponent('#text_user_vm_disk').setValue(Netra.helper.getTemplateTotalStorageSize(this.xenserverRecord.data.storage));
            Netra.helper.getComponent('#text_user_vm_template').setValue(this.xenserverRecord.data.template_name);            
        }

    },






    onStart: function()
    {
        console.debug("UserController.onStart");

        this.userRecord = null;

        this.clearVMPanel();

        this.updateUserTree();
        this.refreshData();

        Netra.service.loadUserInfoData(this.onLoadUserInfoComplete);
        Netra.service.loadXenVDIData(this.onLoadXenVDIComplete);

        var _this = this;
        Netra.service.loadPoolData(this, function(records, operation, success) 
        {
            _this.poolRecord = Netra.manager.getStore("storeClusters").getAt(0);
            Netra.service.loadXenServerData(_this.poolRecord.data.name, _this.onLoadXenServerComplete);
        });

    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("onGridColumnDoubleClick");

    },

    onButtonVMSaveClick: function()
    {
        if (! this.userRecord) {
            Ext.MessageBox.alert('Error', 'Please select user first');
            return;
        }
        
        if (! Netra.helper.getComponent('#form_user_static').isValid()) {
            Ext.MessageBox.alert('Error', 'Please fill out the form');
            return;
        }
        
        
        var params = {};
        params['device_id'] = Netra.helper.getComponent('#text_user_nic_id').getValue();
        params['hostname'] = Netra.helper.getComponent('#text_user_host').getValue();
        params['ip'] = Netra.helper.getComponent('#text_user_ip').getValue();
        params['netmask'] = Netra.helper.getComponent('#text_user_netmask').getValue();
        params['gateway'] = Netra.helper.getComponent('#text_user_gateway').getValue();
        params['dns1'] = Netra.helper.getComponent('#text_user_dns1').getValue();
        params['account'] = this.userRecord.data.account;
        params['userprincipalname'] = this.userRecord.data.userprincipalname;
        params['dn'] = this.userRecord.data.dn;
        if (this.vdiRecord) {
            params['machine_uuid'] = this.vdiRecord.data.HostedMachineId;
        }
        if (this.xenserverRecord) {
            params['machine_reference'] = this.xenserverRecord.data.reference;
        }

        Netra.task.updateUserinfo(this, params, this.onUpdateUserinfoComplete);

        console.debug("UserController.onButtonVMSaveClick : params = ", params);     
    },

    onButtonVMInfoClick: function()
    {
        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }
        
        //updateServerDetailPanel: function(poolRecord, xenserverRecord, container_name, index)
        
        Netra.helper.showWaitDialog("","",'btn_user_vm_info');

        var vmInfo = Netra.helper.getComponent('#serverdetail_user');
        vmInfo.updateServerDetailPanel(this.poolRecord, this.xenserverRecord, 'container_user',1);
    },

    onButtonAssignClick: function()
    {
        console.debug("UserController.onButtonAssignClick");
        this.showAssignWindow(null);
    },

    onButtonUnassignClick: function()
    {
        console.debug("UsersController.onButtonUnassignClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }
        

        var params = {};
        params['machine_uid'] = this.vdiRecord.data.Uid;
        params['machine_name'] = this.vdiRecord.data.MachineName;
        params['group_uid'] = this.vdiRecord.data.DesktopGroupUid;

        Netra.helper.showWaitDialog("","",'btn_user_unassign');
        Netra.task.unassignVM(this, params, function(_this,retcode,json) {
            if (json.ret_code == 0) {            
                Ext.MessageBox.alert('Success', 'Selected VM has been unassigned successfully!!!');
            }

            Netra.helper.hideWaitDialog();            
        });
    },

    onButtonStartClick: function()
    {
        console.debug("UserController.onButtonStartClick : xenserverRecord = ", this.xenserverRecord, " , pool = ", this.poolRecord);

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        Netra.helper.showWaitDialog("","",'btn_user_vm_start');

        Netra.task.startVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("UserController.onButtonStartClick : response = ", json);
            if (json.ret_code == 0) {
                _this.xenserverRecord.data.power_state = "Running";
                Netra.helper.getComponent('#text_user_vm_power').setValue(_this.xenserverRecord.data.power_state);
            
                Ext.MessageBox.alert('Success', 'Selected VM has started successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });
    },

    onButtonRefreshClick: function()
    {
        console.debug("UserController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_user_refresh');
        this.refreshData();
    },

    onButtonShutdownClick: function()
    {
        console.debug("UserController.onButtonShutdownClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        Netra.helper.showWaitDialog("","",'btn_user_vm_shutdown');
        Netra.task.shutdownVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("UserController.onButtonShutdownClick : response = ", json);
            if (json.ret_code == 0) {
                _this.xenserverRecord.data.power_state = "Halted";
                Netra.helper.getComponent('#text_user_vm_power').setValue(_this.xenserverRecord.data.power_state);
                
                Ext.MessageBox.alert('Success', 'Selected VM has shutdowned successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });

    },

    onButtonForceShutdownClick: function()
    {
        console.debug("UserController.onButtonForceShutdownClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        Netra.helper.showWaitDialog("","",'btn_user_vm_force_shutdown');
        Netra.task.brutalShutdownVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("UserController.onButtonForceShutdownClick : response = ", json);
            if (json.ret_code == 0) {
                _this.xenserverRecord.data.power_state = "Halted";
                Netra.helper.getComponent('#text_user_vm_power').setValue(_this.xenserverRecord.data.power_state);
                
                Ext.MessageBox.alert('Success', 'Selected VM has been shutdowned successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });

    },

    onButtonRebootClick: function()
    {
        console.debug("UserController.onButtonRebootClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        Netra.helper.showWaitDialog("","",'btn_user_vm_reboot');
        Netra.task.rebootVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("UserController.onButtonRebootClick : response = ", json);
            if (json.ret_code == 0) {
                _this.xenserverRecord.data.power_state = "Running";
                Netra.helper.getComponent('#text_user_vm_power').setValue(_this.xenserverRecord.data.power_state);
                
                Ext.MessageBox.alert('Success', 'Selected VM has rebooted successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });
        

    },

    onButtonVMBackClick: function()
    {
        console.debug("UserController.onButtonVMBackClick");
        Netra.helper.showCardView('container_user',0,'','');
    },

    onButtonDeleteClick: function()
    {
        console.debug("UserController.onButtonDeleteClick");
        if (! this.userRecord) {
            Ext.MessageBox.alert('Error', 'Please select user first');
            return;
        }

        if (! this.userinfoRecord) {
            Ext.MessageBox.alert('Error', 'No data to delete');
            return;
        }

        Netra.task.deleteUserinfo(this, { 'id': this.userinfoRecord.data.id }, this.onDeleteUserinfoComplete );
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        console.debug("UserController.onNodeClick : record = " ,record);
        //console.debug("UserController.onNodeClick : index = " + index + " , item = " + record.data.text.toLowerCase());

        if (record.data.nodeType == Netra.const.NODE_USER_ALL) 
        {
            this.filterUserStore(null);
        } 

        if (record.data.nodeType == Netra.const.NODE_USER_OU) {
           this.filterUserStore(record.data.nodeRecord);
        } 

    },

    onTreeMenuRendered: function() 
    {
        console.debug("onTreeMenuRendered");
        //this.selectFirstNode();
    },

    onOULoaded: function(records, operation, success)
    {
        var _this = this;

        console.debug("UserController.onOULoaded : records = ", records);

        var tree = Netra.helper.getComponent('#tree_user');

        //addNodeToTree: function(tree,parent_node,text,node_type,record,leaf)

        Netra.helper.clearTree(tree);

        var rootNode = tree.getRootNode();
        var all_node = this.addNodeToTree(tree,rootNode,"ALL",Netra.const.NODE_USER_ALL,null,false);

        records.forEach(function(record,idx) 
        {
            _this.addNodeToTree(tree,all_node,record.get("name"), Netra.const.NODE_USER_OU, record,true);
        });


        tree.getView().refresh();
        tree.getRootNode().expand();

        Netra.helper.showPanelLoadingMask(tree,false);
    },

    onUserGridColumnClick: function(grid, record) 
    {
        console.debug("UserController.onUserGridColumnClick : record = ", record);

        this.selectUser(record);
        this.updateVMPanel(this.userRecord);
    },

    onLoadXenServerComplete: function(retcode, json)
    {
        console.debug("UserController.onLoadXenServerComplete : json = ", json);
    },

    onLoadXenVDIComplete: function(retcode, json)
    {
        console.debug("UserController.onLoadXenVDIComplete : json = ", json);
    },

    onLoadDeliveryComplete: function(retcode, json)
    {
        console.debug("UserController.onLoadDeliveryComplete : json = ", json);
    },

    onLoadUserInfoComplete: function(retcode, json)
    {
        console.debug("UserController.onLoadUserInfoComplete : json = ", json);
    },
    
    onUpdateUserinfoComplete: function(_this, retcode, json)
    {
        console.debug("UserController.onUpdateUserinfoComplete : json = ", json);
        Ext.MessageBox.alert('Success', 'IP Configuration is updated!!!');
    },

    onDeleteUserinfoComplete: function(_this, retcode, json)
    {
        console.debug("UserController.onDeleteUserinfoComplete : json = ", json);
        if (json.ret_code == 0)
        {
            Netra.helper.getComponent('#form_user_static').clearForm();
            Ext.MessageBox.alert('Success', 'IP Configuration is deleted!!!');
        }

    },


});