Ext.define('Netra.view.volume.Volumes', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.volumes',
    title: 'Volumes',
    titleAlign: 'center',
    referenceHolder: true,
    animCollapse: true,
    collapsible: true,
    collapseDirection: 'left',

    requires: [
        'Netra.model.Volume',
        'Netra.view.volume.VolumesController',
    ],

    layout: {
        type: 'accordion',
        animate: true,        
        vertical: false,
        //titleCollapse: false,
        //activeOnTop: true
    },

    defaults: {
        titleAlign: 'left',
        autoScroll: false,
        headerPosition: 'left',
        titleAlign: 'center',
    },

    controller: 'volumes',

    items: 
    [
        {
            xtype: 'gridpanel',
            id: 'grid_volume',
            reference: 'grid_volume',
            title: 'List',
            stripeRows: true,
            columnLines: true,

            store: Netra.manager.getStore('storeVolumes'),

            selType: 'checkboxmodel',
            selModel: {
                checkOnly: true,
                injectCheckbox: 0
            },

            columns: [
                {
                    text     : 'id',
                    flex     :  1,
                    hidden   : false,
                    dataIndex: 'id'
                },
                {
                    text     : 'Status',
                    width    : 50, 
                    dataIndex: 'status',
                    align    : 'center',
                    renderer: function(value, metaData, record, row, col, store, gridView)
                    {
                        console.debug("Image.grid : value = ",  value); // + " , record = ", record);
                     
                        return Netra.helper.createActiveIcon(value.toString().toLowerCase() == 'available');
                    }                                                                                
                },                
                {
                    text     : 'Name',
                    flex     :  3,
                    dataIndex: 'display_name'
                },        
                {
                    text     : 'Bootable',
                    flex     :  2,
                    dataIndex: 'bootable'
                },                                
                {
                    text     : 'Encrypted',
                    flex     :  2,
                    dataIndex: 'encrypted'
                },                                
                {
                    text     : 'Size(GB)',
                    flex     :  2,
                    dataIndex: 'size'
                },                
                {
                    text     : 'Volume Type',
                    flex     :  2,
                    dataIndex: 'volume_type'
                },                                
                {
                    text     : 'Availability Zone',
                    flex     :  2,
                    dataIndex: 'availability_zone'
                },                                
                {
                    text     : 'Host',
                    flex     :  2,
                    dataIndex: 'os-vol-host-attr:host'
                },                                
                {
                    text     : 'Created At',
                    flex     :  2,
                    dataIndex: 'created_at'
                },                                
                {
                    text     : 'Description',
                    flex     :  5,
                    dataIndex: 'display_description'
                },

            ],
            bbar: {
                xtype: 'pagingtoolbar',
                pageSize: 10,
                store: Netra.manager.getStore('storeVolumes'),
                displayInfo: true,
                plugins: new Ext.ux.ProgressBarPager()
            },

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    alias: 'widget.toolbar_image',
                    id: 'toolbar_image',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: [              
                        { 
                            xtype: 'button', 
                            id: 'btn_volume_new', 
                            text: 'New'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_volume_delete', 
                            text: 'Delete',
                        },           
                        { 
                            xtype: 'button', 
                            id: 'btn_volume_refresh', 
                            text: 'Refresh',
                        },                                  
                    ]
                }
            ],        

        },
 
        {
            xtype: 'panel',
            title: 'Details',
            items: 
            [
                {
                    xtype: 'form',
                    reference: 'form_volume',                    
                    id: 'form_volume',
                    bodyPadding: '30 60 60 60',
                    autoScroll: true,
                    frame: false,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 180,
                    },

                    items: [],

                }
            ],

            dockedItems: 
            [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'toolbar_volume2',
                    ui: 'footer',
                    //defaults: {minWidth: minButtonWidth},
                    items: 
                    [              
                        { 
                            xtype: 'button', 
                            id: 'btn_volume_back', 
                            text: 'Back'
                        },
                        { 
                            xtype: 'button', 
                            id: 'btn_volume_edit', 
                            text: 'Edit',
                        },                                        
                    ]
                }
            ],       
        }
    ],



    initComponent: function() 
    {
        console.debug("Image.initComponent");
        this.callParent(arguments);
    },


});
