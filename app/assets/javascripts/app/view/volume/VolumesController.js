Ext.define('Netra.view.volume.VolumesController', {
    extend: 'Netra.controller.BaseController',
    alias: 'controller.volumes',

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridTenant', selector: '#grid_volume' },
    ],

    volumeRecord: null,
    volumeWindowController: null,


    init: function() 
    {
        console.debug("UsersController.init");

        this.control({
            '#grid_volume': { itemdblclick: this.onGridColumnDoubleClick },
            '#form_volume': { render: this.onFormRender },
            '#btn_volume_new': { click: this.onButtonNewClick },
            '#btn_volume_edit': { click: this.onButtonEditClick },
            '#btn_volume_delete': { click: this.onButtonDeleteClick },
            '#btn_volume_refresh': { click: this.onButtonRefreshClick },
            '#btn_volume_back': { click: this.onButtonBackClick },
        });

        //this.createStoreFlavor();
        this.onStart();
    },


    selectVolume: function(record)
    {
        this.volumeRecord = record;
    },

    filloutForm: function(record)
    {
        console.debug("VolumeController.filloutForm : record = ",record);

        var form = this.getReference('form_volume');
        var store = Netra.manager.getStore('storeVolumes');

        Netra.builder.buildForm(form,store,record.data,'Virtual Machine Volume Details','');

/*
        
        var form = this.getReference('form_image');
        var tpl = new Ext.XTemplate(
            '<h2>hello</h2>'
        );

        form.update({ image: record.data });

        //form.doComponentLayout();
*/
    },

    showWindow: function(record) 
    {
        console.debug("VolumesController.showWindow");

        this.volumeWindowController = Netra.manager.createController("Netra.view.component.window.NewVolume");
        this.volumeWindowController.show(record);        
    },

    sendDeleteRequest: function(json)
    {
        var _this = this;

        console.debug("VolumesController.sendRequest");
     
        //Netra.helper.showPanelLoadingMask(this.getReference('grid_volume'),true);   

        Ext.Ajax.request({
            url: '/api/openstack/volumes/delete',
            method: 'POST',
            jsonData: json,
            success: function(response, opts) 
            {
                console.debug('sendDeleteRequest Response = ', response);
                var json = JSON.parse(response.responseText);
                if (json.retcode == 0) {
                    Ext.MessageBox.alert('Success', 'Selected Volume is deleted!!!');
                    _this.refreshData();
                } else {
                    Ext.MessageBox.alert('Error', json.msg);
                }
                
                Netra.helper.hideWaitDialog();
            },
            failure: function(response, opts) 
            {
                console.debug('sendDeleteRequest : woops');
                Netra.helper.hideWaitDialog();
            }
        });

    },

    refreshData: function()
    {
        Netra.helper.refreshData('storeVolumes','#grid_volume',function() {
            Netra.helper.hideWaitDialog();
        });
    },





    onStart: function()
    {
        console.debug("onStart");

        this.volumeRecord = null;
        this.refreshData();
    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("VolumesController.onGridColumnDoubleClick");
        //this.collapseListPanel(this.getReference('panel_flavors'));

        this.selectVolume(record);
        this.filloutForm(this.volumeRecord);
        Netra.helper.setActivePanelItem(this.getView(),1);

        //var comps = Ext.ComponentManager.getAll();
        //console.debug(comps);
        //Netra.helper.dumpComponents(null);
    },

    onFormRender: function(form, opts)
    {
        console.debug("VolumesController.onFormRender");
    },


    onButtonNewClick: function()
    {
        console.debug("VolumesController.onButtonNewClick");
        this.showWindow(null);
    },

    onButtonDeleteClick: function()
    {
        var _this = this;

        console.debug("VolumesController.onButtonDeleteClick");

        var selected = this.getReference('grid_volume').getSelectionModel().getSelection();
        
        if (selected.length == 0) {
            Ext.MessageBox.alert('Error', 'Please select snapshot to delete!');
            return;
        }

        console.debug(selected);

        var json = {};
        json['id'] = Netra.helper.getSelectedIds(this.getReference('grid_volume'));

        Netra.helper.showWaitDialog("","",'btn_volume_delete');
        this.sendDeleteRequest(json);

    },

    onButtonEditClick: function()
    {
        console.debug("VolumesController.onButtonEditClick");
        this.showWindow(this.volumeRecord);
    },

    onButtonRefreshClick: function()
    {
        console.debug("VolumesController.onButtonRefreshClick");

        Netra.helper.showWaitDialog("","",'btn_volume_refresh');
        this.refreshData();
    },

    onButtonBackClick: function()
    {
        console.debug("VolumesController.onButtonBackClick");
        Netra.helper.setActivePanelItem(this.getView(),0);        
    },

});
