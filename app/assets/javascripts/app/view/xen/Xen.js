Ext.define('Netra.view.xen.Xen', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.xen',
    title: 'VM Manager',
    titleAlign: 'center',
    animCollapse: true,
    //collapsible: true,
    //collapseDirection: 'left',

    requires: [
        'Netra.view.xen.XenController',
    ],

    layout: {
        type: 'border',
        align: 'stretch',
        pack: 'start',
    },

    defaults: {
        autoScroll: true,
        titleAlign: 'center',
    },

    controller: 'xen',

    items: 
    [
        {
            xtype: 'treepanel',
            id: 'xentree',
            //title: 'Xen',
            width: 200,
            rootVisible: false,
            useArrows: true,
            region: 'west',

            root: {
                text: 'Root',
                expanded: true,
                children: []
            }
        },
        {
            xtype: 'panel',
            id: 'container_xen',
            region: 'center',
            layout: {
                type: 'card',
                //align: 'stretch',
            },
            
            items: 
            [
                {
                    xtype: 'gridxenserver',
                    //layout: 'fit',
                    id: 'grid_xenserver',
                    ui: 'header',
                    header: {
                        padding: '5 5 5 5',
                        titlePosition: 1,
                        items: 
                        [
                            { xtype: 'button', id: 'btn_xen_refresh', text: 'Refresh', },
                        ]
                    },

                },
                {
                    xtype: 'serverdetail',
                    title: 'Server Detail',

                    height: 1500,

                    ui: 'header',
                    header: {
                        padding: '5 5 5 5',
                        titlePosition: 1,
                        items: 
                        [
                            { xtype: 'button', id: 'btn_server_form_back', text: 'Back', margin: '0 30 0 0'},
                            { xtype: 'button', id: 'btn_server_vm_start', text: 'Start VM' },
                            { xtype: 'button', id: 'btn_server_vm_reboot', text: 'Reboot VM', },                                      
                            { xtype: 'button', id: 'btn_server_vm_shutdown', text: 'Shutdown VM',},
                            { xtype: 'button', id: 'btn_server_vm_force_shutdown', text: 'Force Shutdown VM',},
                        ]
                    },
                },

            ],

        },
 
        
    ],
/*
    dockedItems: 
    [
        {
            xtype: 'toolbar',
            dock: 'top',
            alias: 'widget.toolbar_instance',
            id: 'toolbar_instance',
            //ui: 'footer',
            //defaults: {minWidth: minButtonWidth},
            items: 
            [   
                {
                    //xtype: 'button',
                    text: 'Add New Host',
                },
                {
                    //xtype: 'button',
                    id: 'btn_xen_newvm',
                    text: 'New VM',
                },
                {
                    //xtype: 'button',
                    id: 'btn_xen_test',
                    text: 'Test',
                },
            ]
        }
    ],
*/


    initComponent: function() 
    {
        console.debug("Image.initComponent");
        this.callParent(arguments);
    },


});
