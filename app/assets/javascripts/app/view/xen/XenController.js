Ext.define('Netra.view.xen.XenController', {
    //extend: 'Ext.app.ViewController',
    extend: 'Netra.controller.BaseController',
    alias: 'controller.xen',

    refs: [
        { ref: 'panelImage', selector: '#panel_image' },
        { ref: 'gridSecurity', selector: '#grid_security' },
    ],

    poolRecord: null,
    xenhostRecord: null,
    xenserverRecord: null,
    newVMWindowController: null,
    gridPopupMenu: null,


    init: function() 
    {
        console.debug("XenController.init");


        this.control({
            '#xentree': {
                itemclick: this.onNodeClick,
                render: this.onTreeMenuRendered
            },     

            '#component_grid_xenserver': { 
                itemdblclick: this.onGridColumnDoubleClick,
                rowcontextmenu: this.onGridContextMenuClick,
            },

            '#btn_server_form_back': { click: this.onButtonServerBackClick },
            '#btn_server_vm_start': { click: this.onButtonStartClick },
            '#btn_server_vm_reboot': { click: this.onButtonRebootClick },
            '#btn_server_vm_shutdown': { click: this.onButtonShutdownClick },
            '#btn_server_vm_force_shutdown': { click: this.onButtonForceShutdownClick },
            
            '#btn_xen_refresh': { click: this.onButtonRefreshClick },
            '#btn_xen_newvm': { click: this.onButtonNewVmClick },
            '#btn_xen_test': { click: this.onButtonTestClick },

        });

        //this.createStoreFlavor();
        this.onStart();
    },


    selectHost: function(record)
    {
        this.hostRecord = record;
    },

    selectXenserverRecord: function(record)
    {
        this.xenserverRecord = record;
    },

    showNewVMWindow: function() 
    {
        console.debug("XenController.showNewVMWindow");
        this.newVMWindowController = Netra.manager.createController("Netra.view.component.window.NewVM");
        this.newVMWindowController.show();        

    },



    refreshData: function()
    {
        Netra.helper.showWaitDialog("","",'btn_xen_refresh');

        Netra.helper.refreshData('storeXenServers','#grid_xenserver',function() {
            Netra.helper.hideWaitDialog();
        });
    },


    loadXenHostData: function() 
    {
        console.debug("XenController.loadXenHostData");

        var store = Netra.manager.getStore('storeXenHosts');
        store.data.removeAll();

        store.load({
            scope: this,
            callback: this.onXenHostsLoaded,
        });

    },

    loadXenserverData: function(pool) 
    {
        console.debug("XenController.loadXenserverData : pool = "+pool);

        var store = Netra.manager.getStore('storeXenServers');
        store.data.removeAll();

        store.load({
            params: {
                'pool': pool
            },
            scope: this,
            callback: this.onXenServersLoaded,
        });

    },

    deleteSecurityRuleStoreData: function(json)
    {
        console.debug("deleteSecurityRuleStoreData : json = ",json);
        
        var store = Netra.manager.getStore('storeSecurityRules');

        var items = json.id.split(",");
        
        items.forEach( function(item) {
            if (item) {
                console.debug("XenController.deleteSecurityRuleStoreData : item = ", item);
                store.removeAt(item);
            }
        });

        //store.reload();
        this.getReference('grid_security_rule').getView().refresh();
    },

    clearXenHostTree: function() 
    {
        Netra.helper.getComponent('#xentree').getRootNode().removeAll();
    },

    addPoolNodeToTree: function(text,record)
    {
        //var subnetNode = this.getTreeRole().getStore().getNodeById('node_subnet');
        var tree = Netra.helper.getComponent('#xentree');
        var node = this.addNodeToTree(tree, tree.getRootNode(), text, Netra.const.NODE_POOL, record);
        //node.set("icon",Icons.nodeRole);

        tree.getView().refresh();

        return node;
    },

    addXenHostNodeToTree: function(parent,text,record)
    {
        //var subnetNode = this.getTreeRole().getStore().getNodeById('node_subnet');
        var tree = Netra.helper.getComponent('#xentree');
        var node = this.addNodeToTree(tree, parent, text, Netra.const.NODE_HOST, record);
        //node.set("icon",Icons.nodeRole);

        tree.getView().refresh();

        return node;
    },

    addXenServerNodeToTree: function(parent,text,record)
    {
        //var subnetNode = this.getTreeRole().getStore().getNodeById('node_subnet');
        var tree = Netra.helper.getComponent('#xentree');
        var node = this.addNodeToTree(tree, parent, text, Netra.const.NODE_SERVER, record);
        //node.set("icon",Icons.nodeRole);

        tree.getView().refresh();  

        return node;      
    },

    updateXenHostTree: function(records)
    {
        console.debug("XenController.updateXenHostTree : records = ", records);

        var _this = this;

        var store_host = Netra.manager.getStore("storeXenHosts");
        var store_server = Netra.manager.getStore("storeXenServers");
        var host_node, server_records = null;

        this.clearXenHostTree();

        store_host.each(function(record_host) 
        {
            record_host.data.vm_count = 0;
            record_host.data.vm_running = 0;

            host_node = _this.addXenHostNodeToTree(record_host.data.name,record_host);
            
            server_records = store_server.query('affinity', record_host.data.reference);
            console.debug("updateXenHostTree : host.uuid = ", record_host.data.uuid ,"record = " , server_records);

            server_records.items.forEach(function(record_server)
            {
                _this.addXenServerNodeToTree(host_node, record_server.data.name, record_server);

                if (record_server.data.power_state == Netra.const.VM_RUNNING) {
                    record_host.data.vm_running++;
                }
                record_host.data.vm_count++;
            });
        });

    },

    updateXenHostTree2: function()
    {
        console.debug("XenController.updateXenHostTree2");

        var _this = this;

        var store_pool = Netra.manager.getStore("storeClusters");
        var store_host = Netra.manager.getStore("storeXenHosts");
        //var store_server = Netra.manager.getStore("storeXenServers");
        
        var pool_node, xenhost_node, server_records = null;

        this.clearXenHostTree();

        store_host.data.removeAll();

        Netra.service.loadPoolData(this, function(pool_records, operation, success) {
            pool_records.forEach(function(record_pool) {

                console.debug("XenController.updateXenHostTree2 : record = ",record_pool);
                
                pool_node = _this.addPoolNodeToTree(record_pool.data.name, record_pool);

                Netra.service.loadXenHostData(record_pool.data.name, function(xenhost_records, operation, success) {
                    
                    xenhost_records.forEach(function(record_xenhost) {

                        xenhost_node = _this.addXenHostNodeToTree(pool_node,record_xenhost.data.name, record_xenhost);

                    });

                });
            });
        });

    },

    updateHostPanel:function(host_record)
    {
        Netra.helper.getComponent('#number_host_cpu').updateValue("CPU Count", host_record.data.cpu_count);
        Netra.helper.getComponent('#number_host_memory').updateValue("Memory", Netra.helper.formatByteNumber(host_record.data.total_memory,0));
        Netra.helper.getComponent('#number_host_disk').updateValue("Disk", Netra.helper.formatByteNumber(host_record.data.total_size,2));
        Netra.helper.getComponent('#number_host_vm').updateValue("VM", host_record.data.vm_count);

        var memory_ratio = ((host_record.data.total_memory-host_record.data.free_memory)/host_record.data.total_memory);
        var disk_ratio = (host_record.data.used_size/host_record.data.total_size);
        var vm_ratio = (host_record.data.vm_running/host_record.data.vm_count);

        Netra.helper.getComponent('#labelslider_host_memory').updateValue(memory_ratio);
        Netra.helper.getComponent('#labelslider_host_disk').updateValue(disk_ratio);
        Netra.helper.getComponent('#labelslider_host_vm').updateValue(vm_ratio);
    },

    showGridPopupMenu: function(grid, index, record, event) 
    {
        console.debug("XenController.showGridPopupMenu : record = ", record);

        var _this = this;

        event.stopEvent();

        if (! this.gridPopupMenu) 
        {
            this.gridPopupMenu = new Ext.menu.Menu({
                items: 
                [
                    {
                        text: 'Start',
                        handler: function() 
                        {
                            _this.doStartVM('btn_server_vm_start');
                            //Netra.task.startVM(_this, _this.poolRecord.data.name,record.data.reference, _this.onStartVMComplete);
                        }
                    }, 
                    {
                        text: 'Reboot',
                        handler: function() 
                        {
                            _this.doRebootVM('btn_server_vm_reboot');
                            //Netra.task.rebootVM(_this, _this.poolRecord.data.name,record.data.reference, _this.onRebootVMComplete);
                        }
                    },
                    {
                        text: 'Shutdown',
                        handler: function() 
                        {
                            _this.doShutdownVM('btn_server_vm_shutdown');
                            //Netra.task.shutdownVM(_this, _this.poolRecord.data.name,record.data.reference, _this.onShutdownVMComplete);
                        }
                    },
                    {
                        text: 'Force Shutdown',
                        handler: function() 
                        {
                            _this.doForceShutdownVM('btn_server_vm_force_shutdown');
                            //Netra.task.brutalShutdownVM(_this, _this.poolRecord.data.name,record.data.reference, _this.onBrutalShutdownVMComplete);
                        }
                    },
/*                    
                    {
                         xtype: 'menuseparator',
                    },
                    {
                        text: 'Destroy',
                        handler: function() 
                        {
                            Netra.task.destroyVM(_this, _this.poolRecord.data.name,record.data.reference, _this.onDestroyVMComplete);
                        }
                    }
*/
                ]
            });
        }
        
        this.gridPopupMenu.showAt(event.getXY());
    },

    updateServerDetailPanel: function()
    {
        var _this = this;

        console.debug("XenController.updateServerDetailPanel : pool_record = ",this.xenserverRecord);

        Netra.service.getXenserverDetail(this.poolRecord.data.name, this.xenserverRecord.data.reference, function(retcode,response) {

            console.debug("XenController.updateServerDetailPanel : response = ",response);


            var disk_size = Netra.helper.getStorageSize(response.data.storage);
            
            Netra.helper.getComponent('#serverdetail_number_state').updateValue('State',response.data.power_state,'');
            Netra.helper.getComponent('#serverdetail_number_cpu').updateValue('CPU',response.data.vcpus_max,'');
            Netra.helper.getComponent('#serverdetail_number_memory').updateValue('Memory',Netra.helper.formatByteNumber(response.data.memory_max),'');
            Netra.helper.getComponent('#serverdetail_number_disk').updateValue('Disk',Netra.helper.formatByteNumber(disk_size['total']),'');


            var params_general = { 'Name': response.data.name,
                            'Reference': response.data.reference,
                            'CPU Count': response.data.vcpus_max,
                            'Base Template': response.data.template_name,
                            'Power State': response.data.power_state,
                            'Total Memory': Netra.helper.formatByteNumber(response.data.memory_max),
                            'Memory Overhead': Netra.helper.formatByteNumber(response.data.memory_overhead),
                            'Template?': response.data.is_template,
                            'PV Args': response.data.pv_args,
                            'PV Bootloader': response.data.pv_bootloader,
                            'UUID': response.data.uuid,
                            'Description': response.data.description,
                            'Host': response.data.host_ref,
            }

            Netra.helper.updatePropertyGrid('#grid_server_general', params_general);

            var params_network = {
                'NIC Count': response.data.network.length,
                'Network Backend': response.data.network_backend,
            }

            for (var index=0; index < response.data.network.length; index++) 
            {
                name_prefix = 'NIC'+index.toString();
                params_network[name_prefix+" Device"] = response.data.network[index].device;
                params_network[name_prefix+" MAC"] = response.data.network[index].mac;
                params_network[name_prefix+" Name"] = response.data.network[index].name;
            }

            Netra.helper.updatePropertyGrid('#grid_server_network', params_network);

            if (response.data.storage != null) {

                var params_disk = {
                    'Storage Count': response.data.storage.length,
                    'Total Size': Netra.helper.formatByteNumber(disk_size['total'])
                }

                var name_prefix = "";
                for (var index=0; index < response.data.storage.length; index++) 
                {
                    name_prefix = 'storage'+index.toString();
                    params_disk[name_prefix+" Name"] = response.data.storage[index].name;
                    params_disk[name_prefix+" Type"] = response.data.storage[index].type;
                    params_disk[name_prefix+" Size"] = Netra.helper.formatByteNumber(response.data.storage[index].size);
                }

                Netra.helper.updatePropertyGrid('#grid_server_disk', params_disk);

            }


            Netra.helper.showCardView('container_xen',1,'','');            
        });

    },

    doShutdownVM: function(focus_control)
    {
        Netra.helper.showWaitDialog("","",focus_control);
        Netra.task.shutdownVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("XenController.onButtonShutdownClick : response = ", json);
            if (json.ret_code == 0) {
                Ext.MessageBox.alert('Success', 'Selected VM has shutdowned successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });

    },

    doStartVM: function(focus_control)
    {

        Netra.helper.showWaitDialog("","",focus_control);

        Netra.task.startVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("XenController.onButtonStartClick : response = ", json);
            if (json.ret_code == 0) {
                Ext.MessageBox.alert('Success', 'Selected VM has started successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });    
    },

    doForceShutdownVM: function(focus_control)
    {
        Netra.helper.showWaitDialog("","",focus_control);
        Netra.task.brutalShutdownVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("XenController.onButtonForceShutdownClick : response = ", json);
            if (json.ret_code == 0) {                
                Ext.MessageBox.alert('Success', 'Selected VM has been shutdowned successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });
    },

    doRebootVM: function(focus_control)
    {

        Netra.helper.showWaitDialog("","",focus_control);
        Netra.task.rebootVM(this, this.poolRecord.data.name, this.xenserverRecord.data.reference, function(_this,retcode,json) 
        {
            console.debug("XenController.onButtonRebootClick : response = ", json);
            if (json.ret_code == 0) {
                Ext.MessageBox.alert('Success', 'Selected VM has rebooted successfully!!!');
            }

            Netra.helper.hideWaitDialog();
        });
    },





    onStart: function()
    {
        console.debug("onStart");

        //this.hostRecord = null;
        //this.refreshData();
        //this.loadXenHostData();
        this.updateXenHostTree2();

    },

    onGridColumnDoubleClick: function(grid, record) 
    {
        console.debug("XenController.onGridColumnDoubleClick : record = ",record);

        this.selectXenserverRecord(record);
        this.updateServerDetailPanel();
    },

    onGridContextMenuClick: function(grid, record, tr, index, event, opts)     
    {
        console.debug("XenController.onGridContextMenuClick");
        
        this.selectXenserverRecord(record);
        this.showGridPopupMenu(grid, index, record, event);
    },

    onXenHostsLoaded: function(records, operation, success)
    {
        console.debug("XenController.onXenHostsLoaded");

        //var store = Netra.manager.getStore("storeXenHosts");
        this.loadXenServerData();
    },

    onXenServersLoaded: function(records, operation, success)
    {
        console.debug("XenController.onXenServersLoaded");

        //var store = Netra.manager.getStore("storeXenHosts");
        //this.updateXenHostTree(records);
    },

    onButtonServerBackClick: function()
    {
        console.debug("XenController.onButtonServerBackClick");
        Netra.helper.showCardView('container_xen',0,'','');   
    },

    onButtonStartClick: function()
    {
        console.debug("XenController.onButtonStartClick : xenserverRecord = ", this.xenserverRecord, " , pool = ", this.poolRecord);

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        this.doStartVM('btn_server_vm_start');
    },

    onButtonShutdownClick: function()
    {
        console.debug("XenController.onButtonShutdownClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        this.doShutdownVM('btn_server_vm_shutdown');
    },

    onButtonForceShutdownClick: function()
    {
        console.debug("XenController.onButtonForceShutdownClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        this.doForceShutdownVM('btn_server_vm_force_shutdown');
    },

    onButtonRebootClick: function()
    {
        console.debug("XenController.onButtonRebootClick");

        if (! this.xenserverRecord) {
            Ext.MessageBox.alert('Error', 'No Assigned VM found!!, please select another user to start');
            return;
        }

        this.doRebootVM('btn_server_vm_reboot');
    },

    onButtonRefreshClick: function()
    {
        console.debug("XenController.onButtonRefreshClick");
        this.refreshData();
    },

    onButtonNewVmClick: function()
    {
        console.debug("XenController.onButtonNewVmClick");
        this.showNewVMWindow();
    },

    onButtonTestClick: function()
    {
        console.debug("XenController.onButtonTestClick");
        
        var params = {
            "pool" : "NIA",
            "vm_name" : "final_test",
            "host_ref": "OpaqueRef:df0b4981-9a06-dd5a-bf1a-1977335cdbc6",
            "template_ref": "OpaqueRef:b562f915-90e9-e738-b5d4-53e6be7b7874"
        };

        var params2 = {
            "ip" : "192.168.10.31",
            "userid" : "root",
            "password" : "vagrant",
            "ssh_port" : "22"
        };

        var params3 = {
            "pool" : "NIA",
            "vm_ref": "OpaqueRef:2951a3d9-2635-223e-b1ff-9e9a649fde23",
        };


        var params4 = {
            "nic" : "eth1",
            "ip_range": "192.168.10.0/24",
        };

        var params5 = {
            "pool" : "NIA"
        };

        //makeCachedTemplates
        var params6 = {
            "pool" : "NIA",
            "template_ref": "OpaqueRef:b562f915-90e9-e738-b5d4-53e6be7b7874"
        };

        var params7 = {
            "pool": "NIA",
            "data" : [
                {
                    "vm_name" : "final_test",
                    "host_ref": "OpaqueRef:bd876940-d36f-5088-233b-46e5e60595ba",
                    "template_ref": "OpaqueRef:a79e5ceb-0dea-ff9a-1c6e-b89d70142936",
                    "description": "description!!!!!!!!!"
                }
            ]
        };


        var params8 = {
            "pool": "NIA",
            "data" : [
                {
                    "vm_ref" : "OpaqueRef:c05f8cb3-2e2c-e0ea-a93a-9311244b10f8"
                },
                {
                    "vm_ref" : "OpaqueRef:7f2f5938-22e4-fa07-c851-517e93308940"
                }                
            ]
        };

        var params9 = {
            "pool": "NIA",
            "vm_ref" : "OpaqueRef:8337864f-c0aa-685b-d487-c29f6d524bee",
        };


        Netra.service.sendRequest("api/provision/async_xenservers", "POST", params7, function(retcode, response) {
            console.debug("retcode = " + retcode + " , response = ",response);
        });

        //Netra.helper.setActivePanelItem(this.getView(),0);
    },

    onNodeClick: function(view, record, item, index, e) 
    {
        var _this = this;

        console.debug("XenController.onNodeClick : index = " + index + " , name = " + record.data.text.toLowerCase());
        

        if (record.data.nodeType == Netra.const.NODE_POOL) 
        {
            this.poolRecord = record.data.nodeRecord;
            this.xenhostRecord = null;
            
            Netra.manager.getStore('storeXenServers').clearFilter();
            this.loadXenserverData(this.poolRecord.data.name);
        }

        if (record.data.nodeType == Netra.const.NODE_HOST) 
        {
            this.poolRecord = record.parentNode.data.nodeRecord;
            this.xenhostRecord = record.data.nodeRecord;

            Netra.manager.getStore('storeXenServers').filter( {
                property: 'host_ref',
                value: this.xenhostRecord.data.reference,
                exactMatch: true,
                caseSensitive: true
            });
        }
        
        console.debug("XenController.onNodeClick : pool record = ", this.poolRecord);
        console.debug("XenController.onNodeClick : host record = ", this.xenhostRecord);
        
    },

    onTreeMenuRendered: function() 
    {
        console.debug("XenController.onTreeMenuRendered");
        //this.selectFirstNode();
    },

    onStartVMComplete: function(controller,retcode, respose)
    {
        console.debug("XenController.onStartVMComplete : respose = ", respose);
    },

    onRebootVMComplete: function(controller,retcode, respose)
    {
        console.debug("XenController.onRebootVMComplete : respose = ", respose);
    },

    onShutdownVMComplete: function(controller, retcode, respose)
    {
        console.debug("XenController.onShutdownVMComplete : respose = ", respose);
    },

    onBrutalShutdownVMComplete: function(controller, retcode, respose)
    {
        console.debug("XenController.onBrutalShutdownVMComplete : respose = ", respose);
    },

    onDestroyVMComplete: function(controller, retcode, respose)
    {
        console.debug("XenController.onDestroyVMComplete : respose = ", respose);
    },

});
