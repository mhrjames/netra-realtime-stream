/**
* @private
* @author Adrian Teodorescu (ateodorescu@gmail.com)
* 
* Layout class for {@link Ext.ux.form.field.CodeMirror} fields. Handles sizing the codemirror field.
*/

if (Ext.getVersion('extjs').isGreaterThan('4.0.7')) {
    Ext.define('Plugin.codemirror.Layout', {
        extend: 'Plugin.codemirror.FieldLayout',
        alias: ['layout.codemirror'],

        toolbarSizePolicy: {
            setsWidth: 0,
            setsHeight: 0
        },

        beginLayout: function(ownerContext) {
            this.callParent(arguments);

            ownerContext.editorContext   = ownerContext.getEl('editorEl');
            ownerContext.toolbarContext  = ownerContext.getEl('toolbar');
        },

        renderItems: Ext.emptyFn,

        getItemSizePolicy: function (item) {
            
            return this.toolbarSizePolicy;
        },

        getLayoutItems: function () {
            var toolbar = this.owner.getToolbar();
            
            return toolbar ? [toolbar] : [];
        },

        getRenderTarget: function() {
            return this.owner.bodyEl;
        },

        publishInnerHeight: function (ownerContext, height) {
            var me = this,
                innerHeight = height - me.measureLabelErrorHeight(ownerContext) -
                              ownerContext.toolbarContext.getProp('height') -
                              ownerContext.bodyCellContext.getPaddingInfo().height;

            
            if (Ext.isNumber(innerHeight)) {
                ownerContext.editorContext.setHeight(innerHeight);
            } else {
                me.done = false;
            }
            
            // hide the toolbar if there is no button visible
            if(this.owner.toolbar.items.length == 0){
                this.owner.toolbar.hide();
            }
        },

        publishInnerWidth: function (ownerContext, width) {
            var me = this;
            
            width = ownerContext.bodyCellContext.el.getWidth();
            if (Ext.isNumber(width)) {
                ownerContext.editorContext.setWidth(width);
                ownerContext.toolbarContext.setWidth(width);
            } else {
                me.done = false;
            }
        }
    });
}