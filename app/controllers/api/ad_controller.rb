class Api::AdController < Api::BaseApiController


    def ad_ou()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.listOrganizationUnit()

      #ldap.listUser()
      #ldap.listUserInOrganizationUnit("mhrinc")

      #filter = Net::LDAP::Filter.eq("objectClass", "person")
      #ldap.search(:filter => filter) do |entry|
        
        #hash = LDAP::Formatter.entryToHash(entry)
        #Xen::Logger.debug("entry = #{entry.to_json}") 
        #Xen::Logger.debug("entry = #{hash}") 
        #entry.sAMAccountName
      #end

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end


    def ad_user()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.listUser()

      #ldap.listUser()
      #ldap.listUserInOrganizationUnit("mhrinc")

      #filter = Net::LDAP::Filter.eq("objectClass", "person")
      #ldap.search(:filter => filter) do |entry|
        
        #hash = LDAP::Formatter.entryToHash(entry)
        #Xen::Logger.debug("entry = #{entry.to_json}") 
        #Xen::Logger.debug("entry = #{hash}") 
        #entry.sAMAccountName
      #end

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def ad_computer()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.listComputer()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def ad_ou_user()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.listUserInOrganizationUnit("mhrinc")

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def ad_ou_computer()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.listComputerInOrganizationUnit("mhrinc")

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def ad_computer_add()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.addComputerAccount("mhrinc","netra")

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def ad_computer_remove()
      
      ldap = LDAP::ActiveDirectory.new()
      response = ldap.removeComputerAccount("mhrinc","netra")

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end


end