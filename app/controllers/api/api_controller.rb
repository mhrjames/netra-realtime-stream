#require 'fog'
require 'pp'
require 'spreadsheet'


class Api::ApiController < Api::BaseApiController
  
    respond_to :json
  
    #before_filter :authenticate_user!, :except => [ :notification_pxe_provisioned, :notification_dhcp_host_updated, 
    #    :reports_upload, :notification_deploy_state, :update_dhcp ]

    # to read more about authentication with devise
    # please read following link!!!
    #
    # https://github.com/ryanb/cancan
    #



    def index
      plugin = Sources.plugin_clazz(params[:kind], params[:source])
      result = plugin.new.get(params)
      
      #Rails.logger.debug "API.index : kind="+params[:kind]+" , source="+params[:source]

      respond_with(result.to_json)
    end

    def test()
      
      #session_param = { :host => '221.147.129.137', :username => 'root', :password => 'vagrant' }
      #xen = Xen::XenServer.createSession(session_param)
      #xen = Xen::XenServer.create(params)
      xen = Xen::XenServer.new( { :host => '192.168.56.50', :username => 'root', :password => 'vagrant' })
      vm_ref = "OpaqueRef:2178ad2e-d345-c68d-564f-c918f53a579c"
      
      #vm_ref = "OpaqueRef:3324ce85-2424-24f7-0e2b-7471aebd850e"  #netra
      server = xen.connection.servers.get(vm_ref)
      xen.injectConfigData(server,"netra","static","0","192.168.131.101","255.255.255.0","192.168.131.2","192.168.131.2","")

      print "server :  #{server.to_json}"

      #Xen::XenEventHandler.attach(xen)
      #Xen::XenEventHandler.handle(xen)
      #Xen::XenEventHandler.detach(xen)

      respond_to do |format|
        format.html { render :json => 'xen.connection.servers' }
        format.json { render :json => "Hello" }
      end

    end

    def test2()
      
      Rails.logger.debug("config = #{NETRA_CONFIG}")

      #xen = Xen::XenServer.new( { :host => '192.168.10.45', :username => 'root', :password => 'vagrant' })
      #xen = Xen::XenServer.new( { :host => '221.147.129.137', :username => 'root', :password => 'vagrant' })

      #xen.createNewVM("foo","CentOS6_base",xen.connection.hosts[0])
      #host_ref = "OpaqueRef:1f9126ad-67ec-c4bb-6d70-787ba29654ad" #xen
      #response = xen.provisionVM( {:vm_name => "hira33", :host_ref => host_ref, :template_ref => "OpaqueRef:58e58732-37ad-d6ca-d272-9e25098105fc"})
      #server = xen.connection.servers.get("OpaqueRef:2951a3d9-2635-223e-b1ff-9e9a649fde23")

      #Xen::Logger.debug("test2 : server =#{server}")

      #xen = Xen::Client2.new('192.168.10.20')
      #xen.login_with_password('root','vagrant')

      #watcher = Xen::XenWatcher.new(xen)
      #watcher.downloadRRDFilesForReport()

      #response = Xen::RRDReporter.dailyAllHostReport(Time.now-1.days,Time.now)

      params[:pool] = "NIA"
      params[:task_id] = 4;

      xen = Xen::XenServer.create(params)
      response = xen.provisionTask(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def test3()
      Rails.logger.debug("config = #{NETRA_CONFIG}")

      host_ref = "OpaqueRef:a9bfb590-8adc-3c1f-f6b2-671db0749bff" #xen2
      host_ref = "OpaqueRef:1f9126ad-67ec-c4bb-6d70-787ba29654ad" #xen
      xen = Xen::XenServer.new( { :host => '192.168.10.45', :username => 'root', :password => 'vagrant' })
      
      host = xen.getHostByRef(host_ref)
      print "xen session = #{host.metrics.uuid}\n"

      watcher = Xen::XenWatcher.new(xen)
      watcher.downloadRRDFilesForReport()

      Xen::RRDReporter.hostStat('192.168.10.45',Time.now-1.days,Time.now, :average, 60)
      #Xen::RRDReporter.vmStat('192.168.10.45','f7405bab-8ba7-4079-8b16-16a6253406c9',Time.now-1.days,Time.now, :average)

      #response = watcher.downloadHostRRDFile(host_ref)
      #response = watcher.downloadHostRRDFile(host_ref)
      #response = watcher.hostPerfData(host_ref)

      respond_to do |format|
        #format.html { render :json => xen.connection.hosts  }
        format.html { render :json => "hello" }
        format.json { render :json => "Hello" }
      end
    end

    def test4()
      
      xen = Xen::XenDesktopClient.new('192.168.10.11','james','vagrant')
      #xen.exec("dir")
      #xen.listVDI("192.168.10.11")
      #xen.listMachineCatalog("192.168.10.11")
      #xen.listDeliveryGroup("192.168.10.11")
      xen.createMachineCatalog("test","Static","PowerManaged","Test")
      xen.disconnect()


      respond_to do |format|
        format.html { render :json => 'response' }
        format.json { render :json => response }
      end

    end

    def test5()
      
      ldap = LDAP::ActiveDirectory.new()
      ldap.listOrganizationUnit()
      #ldap.listUser()
      #ldap.listUserInOrganizationUnit("mhrinc")

      #filter = Net::LDAP::Filter.eq("objectClass", "person")
      #ldap.search(:filter => filter) do |entry|
        
        #hash = LDAP::Formatter.entryToHash(entry)
        #Xen::Logger.debug("entry = #{entry.to_json}") 
        #Xen::Logger.debug("entry = #{hash}") 
        #entry.sAMAccountName
      #end

      respond_to do |format|
        format.html { render :json => 'response' }
        format.json { render :json => 'response' }
      end

    end


    def test5()
      
      ldap = LDAP::ActiveDirectory.new()
      ldap.listOrganizationUnit()
      #ldap.listUser()
      #ldap.listUserInOrganizationUnit("mhrinc")

      #filter = Net::LDAP::Filter.eq("objectClass", "person")
      #ldap.search(:filter => filter) do |entry|
        
        #hash = LDAP::Formatter.entryToHash(entry)
        #Xen::Logger.debug("entry = #{entry.to_json}") 
        #Xen::Logger.debug("entry = #{hash}") 
        #entry.sAMAccountName
      #end

      respond_to do |format|
        format.html { render :json => 'response' }
        format.json { render :json => 'response' }
      end

    end

    def report_xenhost()
      xen = Xen::XenServer.create(params)

      watcher = Xen::XenWatcher.new(xen)
      response = watcher.getLatestHostRRD(params[:host_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end

    def report_xenhost_all()
      xen = Xen::XenServer.create(params)

      watcher = Xen::XenWatcher.new(xen)
      watcher.downloadRRDFilesForReport()

      response = Xen::RRDReporter.dailyAllHostReport(Time.now-1.days,Time.now)

          #result << { :ip => host.ip, 
          #      :avg => data_avg.metrics, :max => data_max.metrics, :min => data_min.metrics,
          #      :total_avg => total_data_avg, :total_max => total_data_max, :total_min => total_data_min
          #    }

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
        format.xls {  

          Xen::Logger.debug("report_xenhost_all : excel!!!")

          reports = Spreadsheet::Workbook.new
          list = reports.create_worksheet :name => 'List of cliets'
          list.row(0).concat %w{Name Surname Age}

          response[:avg].each_with_index { |data, i|
            list.row(i+1).push data.last_updated,data.value
          }

          header_format = Spreadsheet::Format.new :color => :green, :weight => :bold
          list.row(0).default_format = header_format
          #output to blob object
          blob = StringIO.new("")
          clients.write blob
          #respond with blob object as a file
          send_data blob.string, :type => :xls, :filename => "client_list.xls"

        }
      end
    end

    def report_xenhost_all_excel()
      xen = Xen::XenServer.create(params)

      watcher = Xen::XenWatcher.new(xen)
      watcher.downloadRRDFilesForReport()

      response = Xen::RRDReporter.dailyAllHostReportInExcel(Time.now-1.days,Time.now)

      respond_to do |format|
        format.json { render :json => response }
      end
    end


    def perf_xenhost()
      xen = Xen::XenServer.create(params)

      #host = xen.getHostByRef(host_ref)
      #print "xen session = #{host.metrics.uuid}\n"

      watcher = Xen::XenWatcher.new(xen)
      response = watcher.getLatestHostRRD(params[:host_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end

    def perf_xenserver()
      xen = Xen::XenServer.create(params)

      #host = xen.getHostByRef(host_ref)
      #print "xen session = #{host.metrics.uuid}\n"

      watcher = Xen::XenWatcher.new(xen)
      response = watcher.getLatestVMRRD(params[:host_ref], params[:vm_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end

    def xenhost()
      xen = Xen::XenServer.create(params)
      response = xen.listXenHost()

      respond_to do |format|
        #format.html { render :json => xen.connection.hosts.all }
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def xenhost_sr
      xen = Xen::XenServer.create()

      host = xen.getHostByName('xen')
      srs = xen.getXenHostLocalSR(host)

      respond_to do |format|
        format.html { render :json => srs.to_json }
        format.json { render :json => srs }
      end      
    end
    
    def xenhost_pbd
      xen = Xen::XenServer.create()
      host = xen.getHostByName('xen')
      pbds = xen.getXenHostPBD(host)

      respond_to do |format|
        format.html { render :json => xen.to_json(pbds) }
        format.json { render :json => xen.to_json(pbds) }
      end      
    end

    def xenhost_detail()
      xen = Xen::XenServer.create(params)
      response = xen.getXenhostByIp(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end

    def xenserver()
      #params[:pool] = "NIA"
      xen = Xen::XenServer.create(params)

      respond_to do |format|
        format.html { render :json => xen.listVM() }
        format.json { render :json => xen.listVM() }
      end

    end
    
    def xenserver_pool()
      xen = Xen::XenServer.create(params)

      response = xen.listVM()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def xenserver_start()
      xen = Xen::XenServer.create(params)
      response = xen.startVM(params[:vm_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end

    def xenserver_stop()
      xen = Xen::XenServer.create(params)
      response = xen.stopVM(params[:vm_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def xenserver_destroy()
      xen = Xen::XenServer.create(params)
      response = xen.destroyVM(params[:vm_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def xenserver_hard_reboot()
      xen = Xen::XenServer.create(params)
      response = xen.hardRebootVM(params[:vm_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def xenserver_get()
      xen = Xen::XenServer.create(params)
      response = xen.getVM(params[:vm_ref])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end

    def xenserver_brutal_shutdown()
      xen = Xen::XenServer.create(params)
      response = xen.brutalShutdownVM(params[:vm_ref])
      
      Xen::Logger.debug("xenserver_brutal_shutdown : response = #{response}")

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def template
      xen = Xen::XenServer.create(params)
      if params.has_key?('base')
        response = xen.listBaseTemplate()
      else
        response = xen.listTemplate()
      end

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def template_all
      xen_pool = Xen::XenPool.new

      return_templates = {}
      return_templates[:ret_code] = 0
      return_templates[:msg] = "Ok"
      return_templates[:data] = []

      xen_pool.getMasters().each do |pool|

        #print "pool = #{pool}"

        xen = Xen::XenServer.createPoolSession(pool)
        template = xen.listTemplate()
        
        return_templates[:data] = { pool[:pool] => [] }

        template[:data].each do |a_template|
          return_templates[:data][pool[:pool]] << a_template
        end
      end

      respond_to do |format|
        format.html { render :json => return_templates }
        format.json { render :json => return_templates }
      end      
    end

    def template_base
      xen = Xen::XenServer.create(params)

      response = xen.listBaseTemplate()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def snapshot
      conn = Common::NetraXen.getConnection('192.168.10.20','root','vagrant')

      json = Common::NetraXen.toHashSnapshots(conn)
      
      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      
    end

    def sr
      
      params[:pool] = "NIA"

      xen = Xen::XenServer.create(params)
      response = xen.listStorage()

      #
      #xe sr-param-set uuid=<UUID> other-config:o_direct=true
      #xe sr-param-set uuid=33428038-5e50-8aaf-535c-a246119c7681 other-config:o_direct=true
      #xe sr-param-set uuid=33428038-5e50-8aaf-535c-a246119c7681 local_cache_enabled=true
      #json = Common::NetraXen.toHashSnapshots(conn)
      
      #response = xen.connection.storage_repositories.all

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def sr_provision      
      #params[:pool] = "NIA"

      xen = Xen::XenServer.create(params)
      response = xen.listStorageForProvision()
      
      #response = xen.connection.storage_repositories.all

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def pif()
      xen = Xen::XenServer.create(params)

      respond_to do |format|
        format.html { render :json => xen.listPIF() }
        format.json { render :json => json }
      end      

    end

    def pbd()
      params[:pool] = "NIA"
      xen = Xen::XenServer.create(params)

      response = xen.listPBD()
      #response = xen.connection.pbds.all

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      

    end

    def pbd_provision()
      #params[:pool] = "NIA"
      xen = Xen::XenServer.create(params)

      response = xen.listPBDforProvision()
      #response = xen.connection.pbds.all

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      

    end

    def pool()
      xen = Xen::XenPool.new
      response = xen.listPool()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def pool_inspect()
      xen = Xen::XenPool.new()
      response = xen.inspect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def pool_update()
      xen = Xen::XenPool.new()
      response = xen.update(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end
      

    def provision_xenserver()
      xen = Xen::XenServer.create(params)
      response = xen.provisionVM(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def provision_new_provision_task()
      xen = Xen::XenServer.create(params)
      response = xen.createProvisionTask(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def provision_run_task()
      xen = Xen::XenServer.create(params)
      response = xen.provisionTask(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def provision_async_xenservers()
      xen = Xen::XenServer.create(params)
      response = xen.provisionAsyncVMs(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end
    
    def provision_async_destroy_xenservers()
      xen = Xen::XenServer.create(params)
      response = xen.destroyAsyncVMs(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def provision_cache_template()
      xen = Xen::XenServer.create(params)
      response = xen.makeCachedTemplates(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end


    def host
      host_manager = Xen::HostManager.new
      response = host_manager.listHost()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end


    def host_add
      host_manager = Xen::HostManager.new
      response = host_manager.addHost(params)

      respond_to do |format|
        format.json { render :json => response }
      end

    end

    def host_delete
      host_manager = Xen::HostManager.new
      response = host_manager.deleteHost(params)

      respond_to do |format|
        format.json { render :json => response }
      end
    end
    
    def host_update
      host_manager = Xen::HostManager.new
      response = host_manager.updateHost(params)

      respond_to do |format|
        format.json { render :json => response }
      end

    end


    def dhcp()
      dhcp = Dhcp::DhcpManager.new()
      response = dhcp.status()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def dhcp_scan_ip()
      dhcp = Dhcp::DhcpManager.new()
      response = dhcp.scanIp(params[:nic],params[:ip_range])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def dhcp_stop()
      dhcp = Dhcp::DhcpManager.new()
      response = dhcp.stop()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def dhcp_start()
      dhcp = Dhcp::DhcpManager.new()
      response = dhcp.start()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end

    def dhcp_restart()
      dhcp = Dhcp::DhcpManager.new()
      response = dhcp.restart()

      Rails.logger.debug("dhcp_restart : response = #{response}")

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end




    def clusters
      json = Cluster.all
      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      
    end

    def new_clusters
      cluster = Cluster.find_by_name(params[:name])

      if cluster 
        Common::CommonFunction::debug "ApiController.new_clusters : cluster = #{cluster}"  
              
        ret_code = 1
        ret_msg = "Same name is already exists, please use different name!!"
        
        json = build_api_response(ret_code,ret_msg, {:name => params[:name]})

        respond_to do |format|
          format.json { render :json => json }
        end
        return
      end


      new_host = Cluster.new_cluster(params)      
      json = build_api_response(0,"Adding a new cluster is done", {:name => params[:name]})

      respond_to do |format|
        format.json { render :json => json }
      end

    end

    def delete_clusters

      a_cluster = Cluster.find_by_name(params[:name])

      if ! a_cluster 
        ret_code = 1
        ret_msg = "The cluster name is not registered, Please try different cluster name !!"
        
        json = build_api_response(ret_code,ret_msg, {:name => params[:name] })

        respond_to do |format|
          format.json { render :json => json }
        end
        return
      end

      Cluster.delete_record(a_cluster)
      
      json = build_api_response(0,"Deleting Cluster is done", {:name => params[:name]})

      respond_to do |format|
        format.json { render :json => json }
      end

    end

    def member_clusters
      
      Cluster.update_cluster_member(params)
      json = build_api_response(0,"Updating Cluster member is done", {:name => params[:cluster_name]})

      respond_to do |format|
        format.json { render :json => json }
      end      
    end



    def module_repository
      
      json = Common::PuppetManager.list_parameters

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      
    end


    def task
      json = Task.all

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      

    end

    def task_subtask
      json = Subtask.getTask(params[:task_id])

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      

    end

    def task_status
      #params[:pool] = "NIA"
      #params[:task_ref] = "OpaqueRef:c80d9e83-a574-d877-7fb7-0c527abb78fb"

      xen = Xen::XenServer.create(params)
      response = xen.getTaskData(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      

    end

    def task_run
      xen = Xen::XenServer.create(params)
      response = xen.provisionTask(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      

    end

    def task_run_assign
      xen = Xen::XenDesktopClient.new('192.168.10.11','james','vagrant')
      #xen.exec("dir")
      #xen.listVDI("192.168.10.11")
      #xen.listMachineCatalog("192.168.10.11")
      #xen.listDeliveryGroup("192.168.10.11")
      #response = xen.createMachineCatalog("test","Static","PowerManaged","Test")
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => 'response' }
        format.json { render :json => 'response' }
      end      
    end

    def task_delete
      response = Task.delete_task(params[:task_id])      

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      

    end

    def new_tasks
      
      new_task = Task.new_task(params)

      json = build_api_response(0,"Adding new task is done", {:name => params[:package_name], :task_id => new_task.id })

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      

    end


    def bootstrap_server
      #Common::CapManager.tasks()
      Common::CapManager.test()

      json = Host.all

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end      

    end


    def xen_all()
      dhcp = Dhcp::DhcpManager.new()
      response = dhcp.scanIp(params[:nic],params[:ip_range])

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end      
    end



    def stack_list()
      
      #Netra::Loader.load_library()
      #repo_manager = Netra::StackRepository.new()
      #repo_manager.load_stack_from_github()
      #json = repo_manager.repo.to_json()

      @netra_client = NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      repos = @netra_client.get_stackrepo_all()

      CommonFunction::debug "ApiController.stack_list : repos = #{repos}"

      json = get_javascript_compatible_json(repos)

      CommonFunction::debug "ApiController.stack_list : json = #{json}"

      #respond_with(repo_manager.repo.to_json())
      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def stack_get()

      CommonFunction::debug "ApiController.stack_get : name = #{params[:name]}"

      #Netra::LibraryLoader.load_library()

      #Netra::Loader.load_library()
      #Netra::Model::VbaseItem.prepare()

      #repo_manager = Netra::StackRepository.new()
      #repo_manager.load_stack_from_github()
      #netra_yaml = repo_manager.get_stack_template(params[:name])

      #netra_stack = Netra::StackManager.new
      #netra_stack.load_stack_from_string(netra_yaml)
      #netra_stack.stack.dump()

      #json = netra_stack.stack.to_json_simple(netra_yaml)


      @netra_client = NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      netra_yaml = @netra_client.get_stack_template_from_github(params[:name])
      #stack = @netra_client.get_stack(netra_yaml)
      json = @netra_client.get_stack_simple(netra_yaml)

      #CommonFunction::debug "ApiController.stack_get : stack = #{stack}"
      #json = stack
      #json = get_javascript_compatible_json(stack.to_json())
      CommonFunction::debug "ApiController.stack_get : stack_get = #{json}"

      #respond_with(repo_manager.repo.to_json())
      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end



    # to delete stack by stack_id
    # in fact, this is a premature api because allocated resources such as server ,network and applications are not destroyed.
    # just delete from stack_records table only!!
    # in near future, i need to update it with much efforts
    def stack_destroy()

      CommonFunction::debug "ApiController.stack_destroy : stack_id = #{params[:stack_id]}"

      StackRecord.destroy(params[:stack_id])

      response = {
        'stack_id' => params[:stack_id],
        'retcode' => 0
      }

      respond_to do |format|
        format.html { render :json => response.to_json() }
        format.json { render :json => response.to_json() }
      end

    end




    def server_list()
      
      #Netra::Loader.load_library()
      #resource = Netra::ResourceManager.new()
      #resource.load()
      #bootstrapped = resource.bootstrapped

      @netra_client = NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      bootstrapped = @netra_client.get_resouce_bootstrapped()
      CommonFunction::debug "ApiController.server_list : bootstrapped = #{bootstrapped}"

      json = get_javascript_compatible_json(bootstrapped)

      #json = bootstrapped.to_json()

      CommonFunction::debug "ApiController.server_list : json = #{json}"

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end


    def server_add()
      

      #Netra::Loader.load_library()
      resource = Netra::ResourceManager.new()
      resource.load()

      json = resource.server.to_json()

      CommonFunction::debug "ApiController.server_list : json = #{json}"

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end






    def stack_deploy()

      CommonFunction::debug ">>> stack_deploy  : params = #{params}<<<"

      cloud = Common::CloudFormation.new( { :name => params[:Name], :template_name => params[:Template], :version => params[:Version] , :description => params[:Description]})
      cloud.load_from_hash(params)
      #cloud.populate_openstack_template_with_paramters(params)

      a_stack = StackRecord.new()
      a_stack.name = cloud.name
      a_stack.zone = ''
      a_stack.description = cloud.description
      a_stack.logo_url = ''
      a_stack.template_name = cloud.template_name
      a_stack.template_content = cloud.to_json()
      a_stack.save()

      a_deploy = Deploy.new()
      a_deploy.stack_id = a_stack.id
      a_deploy.template_yaml = cloud.to_json()
      a_deploy.state = Netra::Const::DeployState::INIT
      a_deploy.save()

      json_str = build_api_response(0,"ok", { :stack_id => a_stack.id, :deploy_id => a_deploy.id })

      @netra_client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      @netra_client.deploy_netra_stack(a_stack.id, a_deploy.id,a_deploy.template_yaml)

      respond_to do |format|
        format.json { render :json => json_str }
      end

    end

    # 
    # api to redeploy existing openstack
    # the major difference between stack_deploy and stack redeploy is in creation of new stackrecord
    # 
    def stack_redeploy()
      CommonFunction::debug "ApiController.stack_redeploy : params = #{params}"

      a_stack = StackRecord.find(params[:stack_id])

      a_deploy = Deploy.new()
      a_deploy.stack_id = a_stack.id
      a_deploy.template_yaml = a_stack.template_content
      a_deploy.state = Netra::Const::DeployState::RUNNING
      a_deploy.save()


      @netra_client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      @netra_client.redeploy_netra_stack(a_stack.id, a_deploy.id,a_deploy.template_yaml)

      json_str = build_api_response(0,"ok", { :deploy_id => a_deploy.id })

      respond_to do |format|
        format.html { render :json => json_str }
        format.json { render :json => json_str }
      end

    end

    # 
    # api to deploy existing openstack and deploy
    # it does not create any record, instead it just get existing stackrecord and deploy
    # 
    def stack_re_run_deploy()
      CommonFunction::debug "ApiController.stack_re_run_deploy : params = #{params}"

      a_stack = StackRecord.find(params[:stack_id])

      a_deploy = Deploy.find(params[:deploy_id])

      @netra_client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      @netra_client.deploy_netra_stack(a_stack.id, a_deploy.id,a_deploy.template_yaml)

      json_str = build_api_response(0,"ok", { :deploy_id => a_deploy.id })

      respond_to do |format|
        format.html { render :json => json_str }
        format.json { render :json => json_str }
      end

    end


    def stack_deploy_task()

      CommonFunction::debug ">>> stack_deploy_task <<<"

      deploy_id = params[:deploy_id]
      task_id = params[:task_id]

      installer = Common::NetraInstaller.new(deploy_id)
      installer.set_task_index(task_id)

      #deploy = Deploy.find(deploy_id)
      #stack = StackRecord.find(deploy.stack_id)

      CommonFunction::debug "ApiController.stack_deploy_task : id = #{deploy_id} , task_index = #{task_id}"

      task = installer.execute()

      hash = {
        'deploy_id' => deploy_id,
        'task_id' => (task && task.task_id) || -1,
        'retcode' => (task && task.output) || "error"
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end


    def user()
      json = Userinfo.all

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def user_update()
      response = Userinfo.update_user(params)

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def user_delete()
      response = Userinfo.delete_userinfo(params)
      
      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def check_userid()
      CommonFunction::debug "ApiController.check_userid : params = #{params}"

      begin
        msg = "you can not use the user id, it is already taken"
        retcode = 1

        user = User.where(user_id: params[:userid])
      rescue Exception => e
        msg = "you cna use the user id"
        retcode = 0
      end

      hash = {
        'msg' => msg,
        'retcode' => retcode
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end 

    def add_user()
      CommonFunction::debug "ApiController.add_user : params = #{params}"

      #begin
        a_user = User.new()
        a_user.user_id = params[:user_id]
        a_user.password_digest = params[:password]
        a_user.name = params[:name]
        a_user.email = params[:email]
        a_user.description = params[:description]

        a_user.save()

        retcode = 0
        msg = "success"

      #rescue Exception => e 
      #  retcode = 1
      #  msg = e.message
      #end

      hash = {
        'msg' => msg,
        'retcode' => retcode
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def update_user()
      CommonFunction::debug "ApiController.update_user : params = #{params}"

      a_user = User.where(user_id: params[:user_id]).first()

      retcode = 1
      msg = "fail to save"

      if a_user 
        if a_user.password_digest == params[:password]
          a_user.password_digest = params[:password]
          a_user.name = params[:name]
          a_user.email = params[:email]
          a_user.description = params[:description]

          a_user.save()

          retcode = 0
          msg = "success"
        end
      end

      hash = {
        'msg' => msg,
        'retcode' => retcode
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def delete_user()
      CommonFunction::debug "ApiController.delete_user : params = #{params}"

      begin
        User.destroy(params[:id])
        retcode = 0
      rescue Exception => e
        retcode = 1
      end

      hash = {
        'msg' => 'delete user account',
        'retcode' => retcode
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end 

    def config()
      #Common::CommonFunction::debug "ApiController.config : config = #{NETRA_CONFIG}"

      response = Xen::XenResponse.buildRestResponse(Xen::Const::OK,Xen::Const::MSG_OK, NETRA_CONFIG )
      
      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end


    def config_update()
      #Common::CommonFunction::debug "ApiController.config_update : params = #{params}"
      Common::NetraConfig.update_config(params)

      response = Xen::XenResponse.buildRestResponse(Xen::Const::OK,Xen::Const::MSG_OK, NETRA_CONFIG )

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end
    end


    def reports_rawdata()
      CommonFunction::debug "ApiController.reports_rawdata"
      
      a_report = Report.where("stack_id = ? and deploy_id = ?",params[:stack_id],params[:deploy_id]).first()
      respond_to do |format|
        format.html { render :json => a_report }
        format.json { render :json => a_report }
      end

    end

    def reports_log()
      CommonFunction::debug "ApiController.reports_log"
      
      if params[:report_id].nil?
        reportlogs = ReportLog.where("deploy_id = ?",params[:deploy_id])
      else
        if params[:report_id] == "-1"
          reportlogs = ReportLog.where("deploy_id = ?",params[:deploy_id])
        else
          reportlogs = ReportLog.where("deploy_id = ? and report_id = ?",params[:deploy_id], params[:report_id])
        end
      end

      respond_to do |format|
        format.html { render :json => reportlogs }
        format.json { render :json => reportlogs }
      end

    end

    def reports_summary()
      CommonFunction::debug "ApiController.reports_summary"
      
      a_report = Report.where("deploy_id = ?",params[:deploy_id])
      respond_to do |format|
        format.html { render :json => a_report }
        format.json { render :json => a_report }
      end

    end


    def reports_upload()
      CommonFunction::debug "ApiController.reports_upload"
      #CommonFunction::debug "ApiController.reports_upload : class = #{params[:report].class}"
      #CommonFunction::debug "ApiController.reports_upload : result = #{report_data.to_hash()}"

      parser = Reports::ReportParser.new(params[:report])
      report_data = parser.parse()

      Report.create_from_yaml(report_data)

      #create_from_yaml : yaml = {:base=>{"host"=>"netra159.netranet.local", "stack_id"=>"", "deploy_id"=>"", "environment"=>"production", "report_format"=>4, "status"=>"unchanged", "transaction_uuid"=>"09957882-f4a6-4807-bb14-b7aef76a5631", "kind"=>"apply", "configuration_version"=>1396685509, "time"=>2014-03-26 11:29:10 UTC}, 
      #  :metrics=>[{"changes"=>{"total"=>0}}, {"time"=>{"config_retrieval"=>11.4769530296326, "total"=>11.4769530296326}}, {"resources"=>{"out_of_sync"=>0, "failed_to_restart"=>0, "restarted"=>0, "changed"=>0, "scheduled"=>0, "failed"=>0, "skipped"=>0, "total"=>0}}, 
      #  {"events"=>{"success"=>0, "failure"=>0, "total"=>0}}], :logs=>[{"level"=>"info"

      #{}"failed", "changed", or "unchanged"
      if (not report_data.deploy_id.nil?) and (report_data.deploy_id != "-1") 
        CommonFunction::debug "ApiController.reports_upload : deploy_id = #{report_data.deploy_id}"

        a_deploy = Deploy.find(report_data.deploy_id)
        if not a_deploy.nil?
          a_deploy.state = report_data.status
          a_deploy.save()
        end

        params[:msgtype] = "message"
        if report_data.status.casecmp("changed") == 0 
          params = { :retcode => 0, :msg => "Provisioning is successful" }
        else
          params = { :retcode => -1, :msg => "Fail to do provisioning" }
        end

        Common::Rabbitmq.publish(params.to_json)

      end

      response = "hello"

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end



    def test_broadcast()
      CommonFunction::debug "ApiController.test : params = #{params}"

      Common::Rabbitmq.publish_deploy("This is an message from server")

      respond_to do |format|
        format.html { render :json => "Hello" }
        format.json { render :json => "Hello" }
      end

    end

    def test_answer()

      modules = Common::NetraPuppet.get_puppet_classes(nil)
      
      #CommonFunction::debug "ApiController.answer : class = #{modules.class}, moduels = #{modules}"

      Common::NetraPuppet.update_ppuet_node(modules,'ntp', 'netra117.netranet.local')

      response = build_api_response(0,'success')

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def test_chart()
      
      CommonFunction::debug "ApiController.test_chart : params = #{params}"

      data = { :weight => 10, :height => 54, :today => 32 }
      
      datum = []
      datum << data
      datum << data
      datum << data

      #esponse = build_api_response(0,"ok",datum.to_json)

      respond_to do |format|
        format.html { render :json => datum }
        format.json { render :json => datum }
      end

    end

    #
    # this api is to run & test ssh_runner 
    # it simply run 'puppet agent --test'
    #
    def test_deploy()

      CommonFunction::debug "ApiController.test_deploy : params = #{params}"

      a_stack = StackRecord.find(params[:stack_id])
      a_deploy = Deploy.find(params[:deploy_id])

      #a_deploy = Deploy.new()
      #a_deploy.stack_id = a_stack.id
      #a_deploy.template_yaml = a_stack.template_content
      #a_deploy.state = Netra::Const::DeployState::RUNNING
      #a_deploy.save()

      @netra_client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      @netra_client.test_netra_deploy(a_stack.id, a_deploy.id,a_deploy.template_yaml)

      json_str = build_api_response(0,"ok", { :deploy_id => a_deploy.id })

      respond_to do |format|
        format.html { render :json => json_str }
        format.json { render :json => json_str }
      end

    end




    private

    def rmerge(hash,other_hash)
      hash.merge!(other_hash) do |key, oldval, newval| 
          oldval.class == hash.class ? oldval.rmerge!(newval) : newval
      end
    end

    #to make json parsible to javascript
    def get_javascript_compatible_json(json)
      dummy_arr = []
      json.each do |item|
        dummy_arr << item.to_json()
      end

      return dummy_arr.to_json()
    end

    #to make api response quickly 
    def build_api_response(retcode, msg, data)
      hash = {
        'msg' => msg,
        'retcode' => retcode,
        'data' => data
      }

      return hash.to_json()
    end

    def generate_random_string(length=16)
      chars = [*('A'..'Z'), *('a'..'z'), *(0..9)]
      (0..length).map {chars.sample}.join
    end

  end

