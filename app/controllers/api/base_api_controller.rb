class Api::BaseApiController < ApplicationController
    
    respond_to :json

  	protected


    def rmerge(hash,other_hash)
      hash.merge!(other_hash) do |key, oldval, newval| 
          oldval.class == hash.class ? oldval.rmerge!(newval) : newval
      end
    end

    #to make json parsible to javascript
    def get_javascript_compatible_json(json)
      dummy_arr = []
      json.each do |item|
        dummy_arr << item.to_json()
      end

      return dummy_arr.to_json()
    end

    #to make api response quickly 
    def build_api_response(retcode, msg, data)
      hash = {
        'msg' => msg,
        'retcode' => retcode,
        'data' => data
      }

      return hash.to_json()
    end

end