class Api::DesktopController < Api::BaseApiController


    def machine_catalog()
      
      xen = Xen::Desktop::XenDesktopClient.new()
      #xen.exec("dir")
      #xen.listVDI("192.168.10.11")
      response = xen.listMachineCatalog(xen.controller_ip)
      #xen.listDeliveryGroup("192.168.10.11")
      #xen.createMachineCatalog("test","Static","PowerManaged","Test")
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end


    def delivery_group()
            
      xen = Xen::Desktop::XenDesktopClient.new()
      #xen.exec("dir")
      #xen.listVDI("192.168.10.11")
      #xen.listMachineCatalog(xen.controller_ip)
      response = xen.listDeliveryGroup(xen.controller_ip)
      #xen.createMachineCatalog("test","Static","PowerManaged","Test")
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def xenserver()
      
      xen = Xen::Desktop::XenDesktopClient.new()
      response = xen.listVDI(xen.controller_ip)
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def hypervisor_connection()
      
      xen = Xen::Desktop::XenDesktopClient.new()
      response = xen.listHypervisorConnection(xen.controller_ip)
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end


    def machine_catalog_add_machine()

      xen = Xen::Desktop::XenDesktopClient.new()
      response = xen.addMachineToMachineCatalog(xen.controller_ip,'xen\\pcvmuser10','prix')
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end


    def machine_catalog_remove_machine()

      xen = Xen::Desktop::XenDesktopClient.new()
      response = xen.removeMachineToMachineCatalog(xen.controller_ip,'xen\pcvmuser10','prix')
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def do_assign()

      xen = Xen::Desktop::XenDesktopClient.new()
      response = xen.assignVM(params)
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def do_unassign()

      xen = Xen::Desktop::XenDesktopClient.new()
      response = xen.unassignVM(params)
      xen.disconnect()

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

end