class Api::NotificationController < Api::BaseApiController

    def notification_build_pxe_done()
        CommonFunction::debug "ApiController.notification_build_pxe_done : params = #{params}"
        params[:msg] = "Building PXE Template is done!!!"
        params[:msgtype] = "message"

        Common::Rabbitmq.publish(params.to_json)

        respond_to do |format|
          format.html { render :nothing => true }
          format.json { render :nothing => true }
        end

    end


    def notification_pxe_provisioned()
		    #CommonFunction::debug "ApiController.notification_pxe_provisioned !!!"
		    CommonFunction::debug "ApiController.notification_pxe_provisioned : params = #{params}"
		    params[:msg] = Common::NotificationHandler.handle_pxe_provisioned(params)
        params[:msgtype] = "message"

        Common::Rabbitmq.publish(params.to_json)

      	respond_to do |format|
	        format.html { render :nothing => true }
	        format.json { render :nothing => true }
      	end

    end

    def notification_dhcp_host_updated()
      	CommonFunction::debug "ApiController.notification_dhcp_host_updated : params = #{params}"
      	params[:msg] = Common::NotificationHandler.handle_dhcp_host_updated(params[:msg])
        params[:msgtype] = "message"
        Common::Rabbitmq.publish(params.to_json)

      	respond_to do |format|
        	format.html { render :nothing => true }
        	format.json { render :nothing => true }
      	end

    end

    def notification_bootstrap_done()
        CommonFunction::debug "ApiController.notification_bootstrap_done : params = #{params}"

        params[:msgtype] = "command"
        Common::Rabbitmq.publish(params.to_json)

        respond_to do |format|
          format.html { render :nothing => true }
          format.json { render :nothing => true }
        end

    end      

    def notification_deploy_history()

  		deploy_log = DeployHistory.new()
  		deploy_log.deploy_id = params[:deploy_id]
  		deploy_log.fqdn = params[:fqdn]
  		deploy_log.state = params[:state]
  		#deploy_log.substate = params[:substate]
  		#deploy_log.progress = params[:progress]
  		deploy_log.log = params[:log]

  		deploy_log.save()

  		response = {
  		  'retcode' => 0
  		}

  		respond_to do |format|
  			format.html { render :json => response.to_json() }
  			format.json { render :json => response.to_json() }
  		end

    end

    def notification_deploy()

      deploy_id = params[:deploy_id]
      notification_id = params[:notification_id]

      CommonFunction::debug "ApiController.notification_deploy : name = #{deploy_id}"

      new_id = 0
      task_logs = DeployHistory.where("deploy_id=#{deploy_id} and id > #{notification_id}")
      logs = []
      task_logs.each do |task_log|
        logs << task_log.log
        if task_log.id > new_id
            new_id = task_log.id
        end
      end

      hash = {
        'deploy_id' => deploy_id,
        'notification_id' => new_id,
        'server' => "server",
        'log' => logs
        #'log' => generate_random_string(rand(10..100))
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def notification_deploy_old()

      deploy_id = params[:deploy_id]
      notification_id = params[:notification_id]

      CommonFunction::debug "ApiController.notification_deploy : name = #{deploy_id}"

      new_id = 0
      task_logs = TaskLog.where("task_id=#{deploy_id} and id > #{notification_id}")
      logs = []
      task_logs.each do |task_log|
        logs << task_log.log
        if task_log.id > new_id
            new_id = task_log.id
        end
      end

      hash = {
        'deploy_id' => deploy_id,
        'notification_id' => new_id,
        'server' => "server",
        'log' => logs
        #'log' => generate_random_string(rand(10..100))
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def notification_event

      CommonFunction::debug "ApiController.notification_event : params = #{params}"

      deploy_id = params[:deploy_id]
      if deploy_id != "-1"
        deploy = Deploy.find(deploy_id)
        if deploy 
            deploy.state = params[:status]
            deploy.summary = params[:msg]
            deploy.save()
        end
      end

      response = {
        'retcode' => 0
      }

      respond_to do |format|
        format.html { render :json => response.to_json() }
        format.json { render :json => response.to_json() }
      end

    end

    def notification_report

      CommonFunction::debug "ApiController.notification_report : params = #{params}"

      #deploy_id = params[:deploy_id]
      

      task_log = TaskLog.new()
      task_log.task_id = params[:deploy_id]
      task_log.log = params[:log]
      task_log.server_name = params[:node]
      task_log.save()

      msg = { :deploy_id => task_log.task_id, :created_at => task_log.created_at , :node => task_log.server_name, :logs => task_log.log, :msgtype => "message" }

      Common::Rabbitmq.publish_deploy(msg.to_json)

      json_str = build_api_response(0,"ok", {})

      respond_to do |format|
        format.html { render :json => json_str }
        format.json { render :json => json_str }
      end

    end

    def notification_task_log

      #CommonFunction::debug "ApiController.notification_report : params = #{params}"

      deploy_id = params[:deploy_id]

      task_log = TaskLog.new()
      task_log.task_id = deploy_id
      task_log.log = params[:msg]
      task_log.server_name = params[:node]
      task_log.save

      json_str = build_api_response(0,"ok", {})

      respond_to do |format|
        format.html { render :json => json_str }
        format.json { render :json => json_str }
      end

    end

    def notification_deploy_state()

      CommonFunction::debug "ApiController.notification_deploy_state : params = #{params}"
      
      deploy_model = Deploy.find(params[:deploy_id])
      if deploy_model
        deploy_model.task_count = params[:task_count]
        deploy_model.last_task_index = params[:task_index]
        deploy_model.last_task_fqdn = params[:fqdn]
        deploy_model.last_task_result = params[:task_result]
        
        deploy_model.state = "installing"

        #final result!!!        
        if params[:task_count] == params[:task_index] 
          deploy_model.state = "success"
          if params[:task_result] == "false"
            deploy_model.state = "fail"
          end
        end

        deploy_model.save()
      end


      deploy_log = DeployHistory.new()
      deploy_log.deploy_id = params[:deploy_id]
      deploy_log.fqdn = params[:fqdn]
      deploy_log.state = params[:task_result]
      #deploy_log.substate = params[:substate]
      #deploy_log.progress = params[:progress]
      deploy_log.log = params[:logs]
      if params[:task_count] == params[:task_index] 
        deploy_log.whole_log = 1
      end

      deploy_log.save()



      response = {
        'retcode' => 0
      }

      respond_to do |format|
        format.html { render :json => response.to_json() }
        format.json { render :json => response.to_json() }
      end

    end


end
