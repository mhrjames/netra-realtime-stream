require 'date'

 
class Api::OpenstackController < Api::BaseApiController

    def limit_summary()

	    #CommonFunction::debug "ApiController.notification_pxe_provisioned !!!"
	    #CommonFunction::debug "ApiController.limit_summary : params = #{params}"
	    #Common::NetraOpenstack.handle_pxe_provisioned(params[:yaml])

      	respond_to do |format|
	        format.html { render :nothing => true }
	        format.json { render :nothing => true }
      	end

    end

    def images()
    	
    	Common::CommonFunction::debug "ApiController.images : params = #{params}"
    
      respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listImages() }
      	format.json { render :json => Common::NetraOpenstack.listImages() }
      end

    end

    def upload_images()
      
      Common::CommonFunction::debug "ApiController.upload_images : params = #{params}"

      file_name =  upload_file(params)
      disk_format = params['image_disk_format'].split(/-/)[0]
      container_format = getContainerFormat(disk_format)

      Common::CommonFunction::debug "ApiController.upload_images : file_name = #{file_name} , format = #{disk_format}"

      #name, disk_format, file_path, min_ram, min_disk
      image = Common::NetraOpenstack.uploadImageFile( { :name => params[:image_name], :file_path => file_name, 
          :disk_format => disk_format, :container_format => container_format,
          :min_disk => params[:min_disk], :min_ram => params[:min_ram] })      

      response = build_api_response(0,"ok",{ :file => params[:image_name]})

      respond_to do |format|
          format.html { render :json => response }
          format.json { render :json => response }
      end

    end

    def delete_images()
      Common::CommonFunction::debug "ApiController.delete_images : params = #{params}"

      code = 0, msg = "ok"

      params['id'].split(/,/).each do |image_id|
        Common::CommonFunction::debug "ApiController.delete_images : delete image , id = #{image_id}"
        code, msg = Common::NetraOpenstack.deleteImage(image_id)
      end

      response = build_api_response(code,msg,nil)

      respond_to do |format|
          format.json { render :json => response }
      end


    end

    def security_group()

      Common::CommonFunction::debug "ApiController.security_group : params = #{params}"

      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listSecurityGroups() }
        format.json { render :json => Common::NetraOpenstack.listSecurityGroups() }
      end

    end

    def new_security_group()

      Common::CommonFunction::debug "ApiController.new_security_group : params = #{params}"
    
      client = Common::NetraOpenstack.createSecurityGroup(params[:name],params[:description])

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.html { render :json => response }
          format.json { render :json => response }
      end

    end

    def delete_security_group()
      Common::CommonFunction::debug "ApiController.delete_security_group : params = #{params}"

      params['id'].split(/,/).each do |security_id|
        Common::CommonFunction::debug "ApiController.delete_security_group : delete security group , id = #{security_id}"
        Common::NetraOpenstack.deleteSecurityGroup(security_id)
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def new_rule_security_group()
      Common::CommonFunction::debug "ApiController.new_rule_security_group : params = #{params}"
    
      params[:tenant_id] = nil if params[:tenant_id].empty?

      code, msg = Common::NetraOpenstack.createSecurityGroupRule(
        :parent_group_id => params[:parent_group_id].to_i,
        :ip_protocol     => params[:ip_protocol],
        :from_port       => params[:from_port].to_i,
        :to_port         => params[:to_port].to_i,
        :cidr            => params[:cidr],
        :group_id        => params[:tenant_id]
      )

      response = build_api_response(code,msg,nil)

      respond_to do |format|
          format.html { render :json => response }
          format.json { render :json => response }
      end

    end

    def rule_security_group()
      Common::CommonFunction::debug "ApiController.rule_security_group : params = #{params}"
    
      respond_to do |format|
          format.html { render :json => Common::NetraOpenstack.getSecurityRule(params) }
          format.json { render :json => Common::NetraOpenstack.getSecurityRule(params) }
      end

    end
    def delete_rule_security_group()
      Common::CommonFunction::debug "ApiController.delete_rule_security_group : params = #{params}"

      params['id'].split(/,/).each do |security_id|
        Common::CommonFunction::debug "ApiController.delete_rule_security_group : delete security group rule, id = #{security_id}"
        Common::NetraOpenstack.deleteSecurityGroupRule(security_id)
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def key_pair()

      Common::CommonFunction::debug "ApiController.key_pair : params = #{params}"
    
      client = Common::NetraOpenstack.getComputeClient()

      respond_to do |format|
        format.html { render :json => client.key_pairs }
        format.json { render :json => client.key_pairs }
      end

    end

    def ip_pools()
    	
    	Common::CommonFunction::debug "ApiController.ip_pools : params = #{params}"
    
    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listFloatingPools() }
      	format.json { render :json => Common::NetraOpenstack.listFloatingPools() }
    	end

    end

    def floating_ips()
    	
    	Common::CommonFunction::debug "ApiController.floating_ips : params = #{params}"

      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listFloatingIps() }
        format.json { render :json => Common::NetraOpenstack.listFloatingIps() }
      end

    end

    def new_floating_ips()
      
      Common::CommonFunction::debug "ApiController.new_floating_ips : params = #{params}"
    
      params[:count].to_i.times do |index|
        req_response = Common::NetraOpenstack.createFloatingIp(params[:pool])
        Common::CommonFunction::debug "ApiController.new_floating_ips : response = #{req_response}"
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def delete_floating_ips()
      Common::CommonFunction::debug "ApiController.delete_floating_ips : params = #{params}"

      params['id'].split(/,/).each do |floating_id|
        Common::CommonFunction::debug "ApiController.delete_floating_ips : delete floating ip , id = #{floating_id}"
        Common::NetraOpenstack.deleteFloatingIp(floating_id)
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def flavors()
    	
    	Common::CommonFunction::debug "ApiController.flavors : params = #{params}"
    
    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listFlavors() }
      	format.json { render :json => Common::NetraOpenstack.listFlavors() }
    	end

    end

    def new_flavors()
      
      Common::CommonFunction::debug "ApiController.new_flavors : params = #{params}"
    
      params[:is_public] = true
      params[:rxtx_factor] = 1.0
      params[:disabled] = false

      #client = Common::NetraOpenstack.getComputeClient()
      code, msg = Common::NetraOpenstack.createFlavor(params)

      response = build_api_response(code,msg,nil)
      #ApiController.new_flavors : response = {"conflictingRequest": {"message": "Instance Type with name Test already exists.", "code": 409}}

      #Common::CommonFunction::debug "ApiController.new_flavors : response = #{response}"

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def delete_flavors()
      Common::CommonFunction::debug "ApiController.delete_flavors : params = #{params}"

      params['id'].split(/,/).each do |flavor_id|
        Common::CommonFunction::debug "ApiController.delete_flavors : delete flavor , id = #{flavor_id}"
        Common::NetraOpenstack.deleteFlavor(flavor_id)
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end


    def tasklog()
      
      Common::CommonFunction::debug "ApiController.tasklog : params = #{params}"
    
      request_response = Common::NetraOpenstack.getTaskLog(params[:date_start],params[:date_end])      

      Common::CommonFunction::debug "ApiController.tasklog : response = #{request_response}"

      respond_to do |format|
        format.html { render :json => request_response }
        format.json { render :json => request_response }
      end

    end


    def instance()
    	
    	Common::CommonFunction::debug "ApiController.instance : params = #{params}"
    
    	client = Common::NetraOpenstack.getComputeClient()    	
    	response = client.get_server_details(params[:instance_id])

    	respond_to do |format|
      	format.html { render :json => response.body }
      	format.json { render :json => response.body }
    	end

    end

    def console_instances()
      
      Common::CommonFunction::debug "ApiController.console_instances : params = #{params}"
    
      request_response = Common::NetraOpenstack.getInstanceConsole(params[:instance_id],'novnc')      

      response = build_api_response(0,"ok", { :console => request_response.body['console'] })

      respond_to do |format|
        format.json { render :json => response }
      end

    end

    def audit_instances()
      
      Common::CommonFunction::debug "ApiController.audit_instances : params = #{params}"
    
      request_response = Common::NetraOpenstack.getInstanceAudit(params[:date_start],params[:date_end],params[:instance_id])      

      Common::CommonFunction::debug "ApiController.audit_instances : response = #{request_response.to_json}"

      respond_to do |format|
        format.json { render :json => request_response.to_json }
      end

    end

    def output_instances()
      Common::CommonFunction::debug "ApiController.output_instances : params = #{params}"
      
      response = Common::NetraOpenstack.getInstanceOutput(params[:instance_id])

      Common::CommonFunction::debug "ApiController.output_instances : output = #{response.body}"

      response = build_api_response(0,"ok", { :output => response.body['output'] })

      respond_to do |format|
          format.json { render :json => response }
      end
    end

    def assign_floating_instances()
      Common::CommonFunction::debug "ApiController.assign_floating_instances : params = #{params}"
      
      result =   Common::NetraOpenstack.assignFloatingIp(params)
      response = build_api_response(0,"ok", { :output => result })

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def unassign_floating_instances()
      Common::CommonFunction::debug "ApiController.unassign_floating_instances : params = #{params}"
      
      result =   Common::NetraOpenstack.unassignFloatingIp(params)
      response = build_api_response(0,"ok", { :output => result })

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def start_instances()
      Common::CommonFunction::debug "ApiController.start_instances : params = #{params}"
    
      params['item'].each do |item|
        Common::CommonFunction::debug "ApiController.start_instances : start instance , id = #{item}"
        Common::NetraOpenstack.startInstance(item['id'])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def stop_instances()
      Common::CommonFunction::debug "ApiController.stop_instances : params = #{params}"
    
      params['item'].each do |item|
        Common::CommonFunction::debug "ApiController.stop_instances : stop instance , id = #{item}"
        Common::NetraOpenstack.stopInstance(item['id'])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def suspend_instances()
      Common::CommonFunction::debug "ApiController.suspend_instances : params = #{params}"
    
      params['item'].each do |item|
        Common::CommonFunction::debug "ApiController.suspend_instances : suspend instance , id = #{item}"
        Common::NetraOpenstack.suspendInstance(item['id'])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def resume_instances()
      Common::CommonFunction::debug "ApiController.resume_instances : params = #{params}"
    
      params['item'].each do |item|
        Common::CommonFunction::debug "ApiController.resume_instances : resume instance , id = #{item}"
        Common::NetraOpenstack.resumeInstance(item['id'])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def reboot_instances()
      Common::CommonFunction::debug "ApiController.reboot_instances : params = #{params}"
    
      params['item'].each do |item|
        Common::CommonFunction::debug "ApiController.reboot_instances : reboot instance , id = #{item}"
        Common::NetraOpenstack.rebootInstance(item['id'])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def terminate_instances()
      Common::CommonFunction::debug "ApiController.terminate_instances : params = #{params}"
    
      params['item'].each do |instance|
        Common::CommonFunction::debug "ApiController.terminate_instances : terminate instance , id = #{instance}"
        Common::NetraOpenstack.terminateInstance(instance['id'])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def resize_instances()
      Common::CommonFunction::debug "ApiController.resize_instances : params = #{params}"
    
      params['item'].each do |instance|
        Common::CommonFunction::debug "ApiController.resize_instances : instance , id = #{instance}"
        Common::NetraOpenstack.resizeInstance(instance[:id],instance[:flavor_id])
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def snapshot_instances()
      Common::CommonFunction::debug "ApiController.snapshot_instances : params = #{params}"

      current_time = DateTime.now
      snapshot_time = current_time.strftime "%Y%m%d_%H%M"

      Common::CommonFunction::debug "ApiController.snapshot_instances : json = #{params[:_json]} , time = #{snapshot_time}"

      params['item'].each do |instance|
        Common::CommonFunction::debug "ApiController.snapshot_instances : create snapshot instance , id = #{instance[:id]}"
        Common::NetraOpenstack.createSnapshot( :instance_id => instance[:id], :name => "#{instance[:name]}_#{snapshot_time}" )
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end
    end


    def new_instances()
      Common::CommonFunction::debug "ApiController.new_instances : params = #{params}"
    
      Common::NetraOpenstack.createInstance( params )

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def instances()
    	
    	Common::CommonFunction::debug "ApiController.instances : params = #{params}"
    
    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listInstances() }
      	format.json { render :json => Common::NetraOpenstack.listInstances() }
    	end

    end


    def update_quota_tenants()
      Common::CommonFunction::debug "ApiController.update_quota_tenants : params = #{params}"
    
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.updateQuota(params) }
        format.json { render :json => Common::NetraOpenstack.updateQuota(params) }
      end

    end


    def limits()

    	Common::CommonFunction::debug "ApiController.limits : params = #{params}"
    
      if params[:tenant_id]
        result = Common::NetraOpenstack.getLimits(params[:tenant_id])
      else
        result = Common::NetraOpenstack.listLimits() 
      end

    	respond_to do |format|
      	format.html { render :json => result }
      	format.json { render :json => result }
    	end
    end

    def get_limits()

      Common::CommonFunction::debug "ApiController.get_limits : params = #{params}"
    
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.getLimits(params[:tenant_id]) }
        format.json { render :json => Common::NetraOpenstack.getLimits(params[:tenant_id]) }
      end
    end

    def quotas()

    	Common::CommonFunction::debug "ApiController.quotas : params = #{params}"
    
    	client = Common::NetraOpenstack.getComputeClient()    	
    	response = client.get_quota(params[:tenant_id])

    	respond_to do |format|
      	format.json { render :json => response.body['quota_set'] }
    	end
    end

    def meters()

    	Common::CommonFunction::debug "ApiController.meters : params = #{params}"

    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listMeters() }
      	format.json { render :json => Common::NetraOpenstack.listMeters() }
    	end
    end

    def stat_meters()
      Common::CommonFunction::debug "ApiController.stat_meters : params = #{params}"
      
      period = params[:period]
      date_start = params[:start]
      date_end = params[:end]
      group_by = params[:group_by]

      respond_to do |format|
        format.json { render :json => Common::NetraOpenstack.getMeterStat(params[:meter_id], period, date_start, date_end, group_by ) }
      end
    end

    def sample_meters()
      Common::CommonFunction::debug "ApiController.sample_meters : params = #{params}"

      response = Common::NetraOpenstack.getMeterSample(params[:meter_id],params[:start],params[:end])

      Common::CommonFunction::debug "ApiController.sample_meters : response = #{response.body}"


      respond_to do |format|
        format.json { render :json => Common::NetraOpenstack.getMeterStat(params[:meter_id]) }
      end
    end

    def metering_resources()

    	Common::CommonFunction::debug "ApiController.metering_resources : params = #{params}"
    
    	client = Common::NetraOpenstack.getMeteringClient()    	

    	respond_to do |format|
      	format.html { render :json => client.list_resources().body }
      	format.json { render :json => json }
    	end
    end



    def tenants()
    	
    	Common::CommonFunction::debug "ApiController.tenants : params = #{params}"
    	
    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listTenants() }
      	format.json { render :json => Common::NetraOpenstack.listTenants() }
    	end

    end

    def usage_tenants()
      
      Common::CommonFunction::debug "ApiController.usage_tenants : params = #{params}"
    
      response = Common::NetraOpenstack.usageTenants(params[:tenant_id],params[:start],params[:end])      

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def summary_tenants()
      Common::CommonFunction::debug "ApiController.usage_tenants : params = #{params}"
    
      response = Common::NetraOpenstack.usageSummaryTenants(params[:start],params[:end])      

      respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
      end

    end

    def update_tenants()
      
      Common::CommonFunction::debug "ApiController.update_tenants : params = #{params}"
    
      enabled = params[:enabled].downcase == "true"

      if params[:id].empty?
        #response = @identityClient.update_tenant( :id => id, :name => name, :description => description, :enabled => enabled )
        code, msg = Common::NetraOpenstack.createTenant(params[:name], params[:description], enabled)
      else
        code, msg = Common::NetraOpenstack.updateTenant(params[:id], params[:name], params[:description], enabled )
      end

      response = build_api_response(code,msg,nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def delete_tenants()
      
      Common::CommonFunction::debug "ApiController.delete_tenants : params = #{params}"
    
      #client = Common::NetraOpenstack.getComputeClient()
      params['id'].split(/,/).each do |tenant_id|
        Common::CommonFunction::debug "ApiController.delete_tenants : delete tenant instance , id = #{tenant_id}"
        Common::NetraOpenstack.deleteTenant( tenant_id )
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    
    def quota_tenants()
      Common::CommonFunction::debug "ApiController.quota_tenants : params = #{params}"
          
      respond_to do |format|
          format.json { render :json => Common::NetraOpenstack.getTenantQuota( params ) }
      end

    end


    def hosts()
      
      Common::CommonFunction::debug "ApiController.hosts : params = #{params}"
      
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listHosts() }
        format.json { render :json => Common::NetraOpenstack.listHosts() }
      end

    end

    def get_hosts()
      
      Common::CommonFunction::debug "ApiController.get_hosts : params = #{params}"
      
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.getHosts(params[:id]) }
        format.json { render :json => Common::NetraOpenstack.getHosts(params[:id]) }
      end

    end

    def users()
    	
    	Common::CommonFunction::debug "ApiController.users : params = #{params}"
    	
    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listUsers() }
      	format.json { render :json => Common::NetraOpenstack.listUsers() }
    	end

    end

    def update_users()
      
      Common::CommonFunction::debug "ApiController.update_users : params = #{params}"
    
      if (not params[:id].empty?) 
        Common::CommonFunction::debug "update_users"
        client = Common::NetraOpenstack.updateUser(params[:id],params)
      else
        Common::CommonFunction::debug "create_users"
        client = Common::NetraOpenstack.createUser(params[:name], params[:password], params[:email], params[:tenantId] )
      end
        
      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def enable_users()
      
      Common::CommonFunction::debug "ApiController.enable_users : params = #{params}"

      params['id'].split(/,/).each do |user_id|
        Common::CommonFunction::debug "ApiController.enable_users : enable_users user instance , id = #{user_id}"
        Common::NetraOpenstack.enableUser( user_id )
      end
            
      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def disable_users()
      
      Common::CommonFunction::debug "ApiController.disable_users : params = #{params}"

      params['id'].split(/,/).each do |user_id|
        Common::CommonFunction::debug "ApiController.disable_users : disable user instance , id = #{user_id}"
        Common::NetraOpenstack.disableUser( user_id )
      end
            
      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def new_users()
      
      Common::CommonFunction::debug "ApiController.new_users : params = #{params}"
    
      client = Common::NetraOpenstack.createUser(params[:name], params[:password], params[:email], params[:tenantId] )
        
      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def delete_users()
      
      Common::CommonFunction::debug "ApiController.delete_users : params = #{params}"
    
      params['id'].split(/,/).each do |user_id|
        Common::CommonFunction::debug "ApiController.delete_users : delete user instance , id = #{user_id}"
        Common::NetraOpenstack.deleteUser( user_id )
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end


    def volumes()
    	
    	Common::CommonFunction::debug "ApiController.volumes : params = #{params}"
    
    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listVolumes() }
      	format.json { render :json => Common::NetraOpenstack.listVolumes() }
    	end

    end

    def limit_volumes()
      
      Common::CommonFunction::debug "ApiController.limit_volumes : params = #{params}"
    
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.getVolumeLimits() }
        format.json { render :json => Common::NetraOpenstack.getVolumeLimits() }
      end

    end

    def type_volumes()
      
      Common::CommonFunction::debug "ApiController.type_volumes : params = #{params}"
    
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listVolumeTypes() }
        format.json { render :json => Common::NetraOpenstack.listVolumeTypes() }
      end

    end

    def new_volumes()
      
      Common::CommonFunction::debug "ApiController.new_volumes : params = #{params}"

      client = Common::NetraOpenstack.createVolume( params )
        
      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def update_volumes()
      
      record_json = JSON.parse(params[:record])

      Common::CommonFunction::debug "ApiController.update_volumes : params = #{params} , json = #{record_json}"
      
      if (params[:size] > record_json['size'])
        Common::CommonFunction::debug "ApiController.update_volumes : resize volume to params[:size]"        
        response = Common::NetraOpenstack.extendVolume(params[:id], params[:size] )
      end

      response = Common::NetraOpenstack.updateVolume(params[:id],params[:display_name], params[:display_description])
      
      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def delete_volumes()
      
      Common::CommonFunction::debug "ApiController.delete_volumes : params = #{params}"
    
      params['id'].split(/,/).each do |volume_id|
        Common::CommonFunction::debug "ApiController.delete_volumes : delete volume , id = #{volume_id}"
        Common::NetraOpenstack.deleteVolume( volume_id )
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end

    def availability_zone()
      Common::CommonFunction::debug "ApiController.availability_zone : params = #{params}"
    
      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listAvailabilityZone() }
        format.json { render :json => Common::NetraOpenstack.listAvailabilityZone() }
      end
    end      


    def snapshots()
    	
    	Common::CommonFunction::debug "ApiController.snapshots : params = #{params}"

      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listSnapshots() }
        format.json { render :json => Common::NetraOpenstack.listSnapshots() }
      end

    end

    def delete_snapshots()
      Common::CommonFunction::debug "ApiController.delete_snapshots : params = #{params}"

      params['id'].split(/,/).each do |snapshot_id|
        Common::CommonFunction::debug "ApiController.delete_snapshots : delete snapshot , id = #{snapshot_id}"
        Common::NetraOpenstack.deleteSnapshot(snapshot_id)
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
          format.json { render :json => response }
      end

    end


    def hypervisors()
      Common::CommonFunction::debug "ApiController.hypervisors : params = #{params}"
    
      client = Common::NetraOpenstack.getComputeClient()      

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
        format.html { render :json => Common::NetraOpenstack.listHypervisors() }
        format.json { render :json => Common::NetraOpenstack.listHypervisors() }
      end
    end

    def stat_hypervisors()
      Common::CommonFunction::debug "ApiController.stat_hypervisors : params = #{params}"
    
      stat = Common::NetraOpenstack.statHypervisors()
      limit = Common::NetraOpenstack.listLimits()

      limit[0].each do |key,value|
        stat[0][key] = value
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
        format.html { render :json => stat }
        format.json { render :json => Common::NetraOpenstack.statHypervisors() }
      end
    end

    def networks()
    	
    	Common::CommonFunction::debug "ApiController.networks : params = #{params}"

    	respond_to do |format|
      	format.html { render :json => Common::NetraOpenstack.listNetworks() }
      	format.json { render :json => Common::NetraOpenstack.listNetworks() }
    	end

    end

    def storages()
    	
    	Common::CommonFunction::debug "ApiController.storages : params = #{params}"
    
    	client = Common::NetraOpenstack.getStorageClient()    	

      	respond_to do |format|
        	format.html { render :json => client.files }
        	format.json { render :json => client.files }
      	end

    end

    def dsl()
      Common::CommonFunction::debug "ApiController.dsl : params = #{params}"
    
      file = Rails.root.join("test.netra")
      dsl_text = Common::Util.read_file(file)

      puts " dsl_text = #{dsl_text}"

      dsl_instance = Common::NetraDsl.new
      dsl_instance.loadFromString(dsl_text)

      respond_to do |format|
        format.html { render :json => nil }
        format.json { render :json => client.files }
      end

    end

    def attach_volume_instances()
      Common::CommonFunction::debug "ApiController.attach_volume_instances : params = #{params}"

      params[:device] = "auto"

      params['item'].each do |instance|
        Common::CommonFunction::debug "ApiController.attach_volume_instances : attach volume instance , id = #{instance}"

        result = Common::NetraOpenstack.attachVolume( instance[:id], instance[:volume_id] )
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
        format.html { render :json => nil }
        format.json { render :json => response }
      end

    end

    def detach_volume_instances()
      Common::CommonFunction::debug "ApiController.detach_volume_instances : params = #{params}"

      params['item'].each do |instance|
        Common::CommonFunction::debug "ApiController.detach_volume_instances : detach volume instance , instance = #{instance}"

        result = Common::NetraOpenstack.detachVolume( instance[:id], instance[:volume_id] )
      end

      response = build_api_response(0,"ok",nil)

      respond_to do |format|
        format.html { render :json => nil }
        format.json { render :json => response }
      end

    end





    private

    def upload_file(params)
      name =  params['image_file'].original_filename
      directory = "public/data"
      # create the file path
      path = File.join(directory, name)

      File.delete("#{path}") if File.exist?("#{path}")

      # write the file
      File.open(path, "wb") { |f| f.write(params['image_file'].read) }    

      return Rails.root.join(path)
    end

    def getContainerFormat(disk_format)

      return "aki" if disk_format.casecmp("aki") == 0
      return "ami" if disk_format.casecmp("ami") == 0
      return "ari" if disk_format.casecmp("ari") == 0

      return "bare" 
    end

end