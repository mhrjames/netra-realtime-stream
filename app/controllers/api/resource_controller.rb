class Api::ResourceController < Api::BaseApiController


    def node()
      CommonFunction::debug "ApiController.node : params = #{params}"


      client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      result = client.request_node_facts(params[:fqdn])
      json_str = JSON.parse(result.body)

      CommonFunction::debug "ApiController.node : result = #{json_str}"

      respond_to do |format|
        format.html { render :json => json_str }
        format.json { render :json => json_str }
      end

    end

    def bootstrap()
      CommonFunction::debug "ApiController.bootstrap : params = #{params}"

      begin

        client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
        result = client.bootstrap_node(params[:ip],params[:userid],params[:password])
        result_json = JSON.parse(result.body)

        CommonFunction::debug "result : class = #{result.class} , response = #{result['msg']}"

      rescue Exception => e
        retcode = 1
        msg = "fail to save"
        CommonFunction::debug "subnets : Error!!!, #{e}"
      end

      json = build_api_response(result_json['retcode'],result_json['msg'],{})

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end 

    def update_dhcp()
      CommonFunction::debug "ApiController.update_dhcp : params = #{params}"

      begin
        a_subnet = Subnet.find(params[:id])

        client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
        result = client.request_update_dhcp(a_subnet)
        result_json = JSON.parse(result.body)

        CommonFunction::debug "result : class = #{result.class} , response = #{result}"

      rescue Exception => e
        retcode = 1
        msg = "fail to save"
        CommonFunction::debug "subnets : Error!!!, #{e}"
      end

      json = build_api_response(result_json['retcode'],result_json['msg'],{})

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end 


    def kickstart_json()

      	CommonFunction::debug "ApiController.kickstart_json : params = #{params}"

		    client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
		    response = client.request_kickstart_list()
		    result_json = JSON.parse(response.body)

		    CommonFunction::debug "ApiController.kickstart_json : json = #{result_json}"

      	#json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

      	respond_to do |format|
        	format.html { render :json => result_json['data'].to_json }
        	format.json { render :json => result_json['data'].to_json }
      	end

    end


    def kickstart_create()

        CommonFunction::debug "ApiController.kickstart_create : params = #{params}"

        client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
        response = client.request_kickstart_create(params)
        result_json = JSON.parse(response.body)

        CommonFunction::debug "ApiController.request_kickstart_create : json = #{result_json}"

        Common::Rabbitmq.publish("Kickstart `#{params[:name]}` updated successfully")

        #json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

        respond_to do |format|
          format.html { render :json => result_json['data'].to_json }
          format.json { render :json => result_json['data'].to_json }
        end

    end

    def kickstart_delete()

        CommonFunction::debug "ApiController.kickstart_delete : params = #{params}"

        client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
        response = client.request_kickstart_delete(params)
        result_json = JSON.parse(response.body)

        CommonFunction::debug "ApiController.kickstart_delete : json = #{result_json}"
        Common::Rabbitmq.publish("Kickstart `#{params[:name]}` deleted successfully")

        #json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

        respond_to do |format|
          format.html { render :json => result_json['data'].to_json }
          format.json { render :json => result_json['data'].to_json }
        end

    end

    def dhcp_conf()
        
        CommonFunction::debug "ApiController.dhcp_conf : params = #{params}"

        client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
        response = client.request_dhcp_conf()
        result_json = JSON.parse(response.body)

        CommonFunction::debug "ApiController.dhcp_conf : json = #{result_json}"

        #json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

        respond_to do |format|
          format.html { render :json => result_json['data'].to_json }
          format.json { render :json => result_json['data'].to_json }
        end

    end


    def tftp_conf()
        
        CommonFunction::debug "ApiController.tftp_conf : params = #{params}"

        client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
        response = client.request_tftp_conf()
        result_json = JSON.parse(response.body)

        CommonFunction::debug "ApiController.tftp_conf : json = #{result_json}"

        #json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

        respond_to do |format|
          format.html { render :json => result_json['data'].to_json }
          format.json { render :json => result_json['data'].to_json }
        end

    end


    def pxetemplates_download()

      	CommonFunction::debug "ApiController.pxetemplates_download : params = #{params}"

      	#template = Common::Pxe.raw_pxe_template('centos6')

		client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
		response = client.request_template_download(params[:name])
		result_json = JSON.parse(response.body)

		CommonFunction::debug "ApiController.pxetemplates_download : json = #{result_json}"

      	json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

      	respond_to do |format|
        	format.html { render :json => json }
        	format.json { render :json => json }
      	end

    end

    def pxetemplate_render()
      CommonFunction::debug "ApiController.pxetemplate_render : params = #{params}"      
      #require 'erb'
      #require 'ostruct'

      #locals = { :param => "NewStackCtrl"} 
      #filename = Rails.root.join('app','views','layouts','kickstart','centos6.erb')
      #template = open(filename, 'r') {|f| f.read}
      
      #result = ERB.new(template).result(OpenStruct.new(locals).instance_eval { binding })
      
      #render template: "layouts/kickstart/centos6.erb", :locals => { :param => "NewStackCtrl"}  
      #render :partial => "app/views/layouts/kickstart/centos6.erb", :locals => { :param => "NewStackCtrl"}  
      	if params[:id] == "-1"
      		pxetemplate = JSON.parse(params[:data])
      	else
      		pxetemplate = Pxetemplate.find(params[:id])
      	end

		CommonFunction::debug "ApiController.pxetemplate_render : class = #{pxetemplate.class} , pxetemplate = #{pxetemplate}"

		#return "hello"

		client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
		response = client.request_template_render(pxetemplate)
		result_json = JSON.parse(response.body)

		#CommonFunction::debug "ApiController.pxetemplate_render : json = #{response.body}"

      	json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])
      	
      	respond_to do |format|
        	format.html { render :json => json }
        	format.json { render :json => json }
      	end

    end

    def build_pxe_template()
		CommonFunction::debug "ApiController.build_pxe_template : params = #{params}"      
		#require 'erb'
		#require 'ostruct'

		#locals = { :param => "NewStackCtrl"} 
		#filename = Rails.root.join('app','views','layouts','kickstart','centos6.erb')
		#template = open(filename, 'r') {|f| f.read}

		#result = ERB.new(template).result(OpenStruct.new(locals).instance_eval { binding })

		#render template: "layouts/kickstart/centos6.erb", :locals => { :param => "NewStackCtrl"}  
		#render :partial => "app/views/layouts/kickstart/centos6.erb", :locals => { :param => "NewStackCtrl"}  

      	pxetemplate = Pxetemplate.find(params[:id])

		client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
		response = client.request_build_pxe(pxetemplate)
		result_json = JSON.parse(response.body)

		CommonFunction::debug "ApiController.pxetemplates_download : json = #{result_json}"

      	json = build_api_response(result_json['retcode'],result_json['msg'],result_json['data'])

      	respond_to do |format|
        	format.html { render :json => json }
        	format.json { render :json => json }
      	end

    end

    def pxetemplates_status()
      CommonFunction::debug "ApiController.pxetemplates_status : params = #{params}"

      if params[:value] == "on"
        #clear all pxe_templates
        Pxetemplate.update_all(:status => "off")
        Pxetemplate.update(params[:id], {:status => params[:value]})
      end

      retcode = 0

      hash = {
        'msg' => 'update dhcp configuration',
        'retcode' => retcode
      }

      json = hash.to_json()

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

    def subnet_build_host()
        a_subnet = Subnet.where(id: params[:subnet_id]).first()

        CommonFunction::debug "ApiController.subnet_build_host : params = #{params} , subnet = #{a_subnet}"
        Common::SubnetHandler.update_host_file(a_subnet.network_address,a_subnet.domain,a_subnet.name_prefix,a_subnet.start_ip_range,a_subnet.end_ip_range)

        respond_to do |format|
        	format.html { render :nothing => true }
        	format.json { render :nothing => true }
        end

    end

    def deploy_list()

        CommonFunction::debug "ApiController.deploy_list : params = #{params}"

        deploys = Deploy.where(stack_id: params[:stack_id])

        CommonFunction::debug "ApiController.deploy_list : deploys = #{deploys.as_json}"

        respond_to do |format|
          format.html { render json: deploys }
          format.json { render json: deploys }
        end

    end


    def tasklog_list()
        CommonFunction::debug "ApiController.tasklog_list : params = #{params}"

        tasklogs = TaskLog.where(task_id: params[:deploy_id])

        CommonFunction::debug "ApiController.tasklog_list : tasklogs = #{tasklogs.as_json}"

        respond_to do |format|
          format.html { render json: tasklogs }
          format.json { render json: tasklogs }
        end

    end

    def deploy_history_list()
        CommonFunction::debug "ApiController.deploy_history_list : params = #{params}"

        historys = DeployHistory.where(deploy_id: params[:deploy_id])

        CommonFunction::debug "ApiController.deploy_history_list : historys = #{historys.as_json}"

        respond_to do |format|
          format.html { render json: historys }
          format.json { render json: historys }
        end

    end

    def stack_template()

      CommonFunction::debug "ApiController.stack_template : name = #{params[:name]}"

      @netra_client = Common::NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      response = @netra_client.request_stack_list()

      CommonFunction::debug "ApiController.stack_template : response = #{response.body}"

      result_json = JSON.parse(response.body)

      #cloud = Common::CloudFormation.new( { :name => stack['Name'], :template_name => "OpenStack", :version => stack['Version'] , :description => stack['Description']})
      #cloud.load_from_hash(stack)

#      response = {
#        'template_name' => cloud.template_name,
#        'version' => cloud.version,
#        'description' => cloud.description,
#        'template' => cloud.to_json(),
#        'retcode' => 0
#      }

      #CommonFunction::debug "ApiController.stack_template : templates = #{result_json}"

      #json_str = build_api_response(0,"ok", result_json)

      templates = []
      result_json['templates'].each do |template|
        CommonFunction::debug "ApiController.stack_template : templates = #{template}"

        templates << { :name => template['name'], :logo_url => template['logo_url'], 
            :template => template['data'], :description => template['description'], 
            :updated_at => template['updated_at'], :revision => template['revision'], :size => template['data'].length }
      end


      respond_to do |format|
        format.html { render :json => templates }
        format.json { render :json => templates }
      end

    end

    def stack_get_template()

      CommonFunction::debug "ApiController.stack_get_template : name = #{params[:name]}"

      a_stack = StackRecord.where(name: params[:name]).first!
      
      #netra_stack = Netra::StackManager.new()
      #netra_stack.deserialize_stack(a_stack.template_content)
      #json = netra_stack.stack.to_json()

      @netra_client = NetraClient.new(uri:Netra::Const::NETRA_API_URL)
      stack = @netra_client.get_stack(a_stack.template_content)
      

      json = stack.to_json()

      CommonFunction::debug "ApiController.stack_get_template : stack_get_template = #{json}"

      respond_to do |format|
        format.html { render :json => json }
        format.json { render :json => json }
      end

    end

end