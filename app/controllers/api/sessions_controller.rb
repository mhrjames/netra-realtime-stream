class Api::SessionsController < Api::BaseApiController

  #before_filter :authenticate_user!, :except => [:create, :destroy]
  
  #skip_before_filter :authenticate_user_from_token!, :only => [:create, :destroy, :say_hi ]
  #skip_before_filter :ensure_authorized, :only => [:create, :destroy, :say_hi ]
  
  #before_filter :ensure_params_exist, :except => [:say_hi, :destroy, :authenticate_auth_token]

  #respond_to :json
 
  def create
    resource = User.find_for_database_authentication(:email => params[:user_login][:email])
    return invalid_login_attempt unless resource
 
    if resource.valid_password?(params[:user_login][:password])
      sign_in(:user, resource)
      resource.ensure_authentication_token
      render :json=> {:success=>true, :auth_token=>resource.authentication_token, :email=>resource.email}
      return
    end
    invalid_login_attempt
  end
 

  def authenticate_auth_token
    render :json => {:success => true }
  end


  def destroy
    resource = User.find_for_database_authentication(:email => params[:user_login][:email])
    resource.authentication_token = nil
    resource.save
    render :json=> {:success=>true}
  end

  
  def simple_login()

    if (NETRA_CONFIG['general_username'] == params[:userid]) and (NETRA_CONFIG['general_password'] == params[:password])
      response = build_api_response(0, 'ok', { :user => nil } )
    else
      response = build_api_response(-1, 'error', { :user => nil } )
    end

    respond_to do |format|
        format.json { render :json => response }
    end

    return

  end


  def login
    Common::CommonFunction::debug "SessionsController.login : params = #{params}"

    response = build_api_response(0, 'ok', { :user => nil } )

    respond_to do |format|
        format.json { render :json => response }
    end

    return

    retcode = -1
    msg = "User Name or Password are wrong!!!"

    user = Common::NetraOpenstack.getUserByUsername(params[:userid])

    Common::CommonFunction::debug "SessionsController.login : userid = #{params[:userid]} , user = #{user.to_json}"

    if not user.nil?
    
      if user.enabled 

        tenant = Common::NetraOpenstack.getTenant(user.tenant_id)
        
        if not tenant.nil?

          params['tenant_name'] = tenant.name
          result = Common::NetraOpenstack.login(params)

          if result
            retcode = 0
            msg = "ok"
          end

        end
      end

    end

    if retcode != 0

      if not user.nil? and not user.enabled
        msg = "Your account is disabled, please consult with your administrator"
      else
        if params[:login_count].to_i > NETRA_CONFIG['config']['max_login_retry'].to_i
          disable_user(user)
          msg = "You failed to login more than allowed!, your user account is disabled by security policy!!!"
        end
      end
    end

    response = build_api_response(retcode,msg, { :user => user_to_json(user) } )

    respond_to do |format|
        format.json { render :json => response }
    end

  end 





  protected

  def ensure_params_exist
    return unless params[:user_login].blank?
    render :json=>{:success=>false, :message=>"missing user_login parameter"}, :status=>422
  end
 
  def invalid_login_attempt
    render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
  end


  

  private

  def disable_user(user_instance)
    Common::CommonFunction::debug "SessionsController.disable_user : user_instance = #{user_instance.to_json}"

    return if user_instance.nil?

    Common::NetraOpenstack.disableUser(user_instance.id)

  end

  def user_to_json(user_instance)

    return {} if user_instance.nil?

    { :id => user_instance.id, :name => user_instance.name, :enabled => user_instance.enabled , :tenant_id => user_instance.tenant_id, :email => user_instance.email, :password => user_instance.password }

  end

end