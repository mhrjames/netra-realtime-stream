class Api::TaskController < Api::BaseApiController
  
  respond_to :json


  def tasks

    Common::CommonFunction::debug "TaskController.tasks : params = #{params}"

    tasks = Task.all

    respond_to do |format|
      format.html { render :json => tasks }
      format.json { render :json => tasks }
    end

  end

  def update_tasks

    Common::CommonFunction::debug "TaskController.update_tasks : params = #{params}"

    task = nil

    if params[:id].empty? 
      task = Task.new
    else
      task = Task.find(params[:id])
    end

    task.name = params[:name]
    task.script = params[:script]
    task.save()

    response = build_api_response(0,"ok",nil)

    respond_to do |format|
        format.json { render :json => response }
    end

  end

  def delete_tasks

    Common::CommonFunction::debug "TaskController.delete_tasks : params = #{params}"

    params['id'].split(/,/).each do |task_id|
      Common::CommonFunction::debug "TaskController.delete_tasks : delete task , id = #{task_id}"
      Task.delete(task_id)
    end

    response = build_api_response(0,"ok",nil)

    respond_to do |format|
        format.json { render :json => response }
    end

  end

  def syntax_tasks

    Common::CommonFunction::debug "TaskController.syntax_tasks : params = #{params}"

    task = Common::Task::NetraTask.loadFromString("test",params[:script])
    
    retcode = Common::Task::NetraTask.error().length
    msg = Common::Task::NetraTask.get_error_msg()

    response = build_api_response(retcode,"ok",{ :msg => msg })

    respond_to do |format|
        format.json { render :json => response }
    end

  end


  def run_tasks
    Common::CommonFunction::debug "TaskController.run_tasks : params = #{params}"

    
    task_mgr = Common::Task::TaskManager.new()

    params['id'].split(/,/).each do |task_id|
      Common::CommonFunction::debug "TaskController.run_tasks : run task , id = #{task_id}"
      
      script = get_task_script(task_id)
      
      task = Common::Task::NetraTask.loadFromString("test",script)
      
      if Common::Task::NetraTask.error?
        Common::CommonFunction::error "TaskController.run_tasks : run task , id = #{task_id} , script has error , error_msg = #{get_error_msg()}"
        next
      end

      new_report = Report.new
      new_report.task_id = task_id
      new_report.started_at = Common::Util.get_current_datetime()

      task_mgr.set_task_queue(task.get_tasks())
      task_mgr.run()

      new_report.finished_at = Common::Util.get_current_datetime()
      new_report.result = bool2string(task_mgr.reporter.error?())
      new_report.msg = task_mgr.reporter.log.to_json()

      new_report.save()

    end

    #task_mgr.dump_handler()

    response = build_api_response(0,"ok",{ :msg => 'it is ok' })

    respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
    end

  end

  def report_tasks
    Common::CommonFunction::debug "TaskController.report_tasks : params = #{params}"

    reports = nil

    if params[:task_id].nil?
      reports = Report.all
    else 
      reports = Report.where(task_id: params[:task_id])
    end

    respond_to do |format|
      format.html { render :json => reports }
      format.json { render :json => reports }
    end

  end

  def test_tasks

    Common::CommonFunction::debug "TaskController.test_tasks : params = #{params}"

    options = { :tenant_name => "admin", :username => "admin_12", :password => "vagrant" }
    response = Common::NetraOpenstack.login(options)
    
    Common::CommonFunction::debug "TaskController.test_tasks : response = #{response}"
    
    return nil



    file = Rails.root.join('app','controllers','common','task','test.netra')
    dsl_text = File.read(file)

    task = Common::Task::NetraTask.loadFromFile(file)

    task_mgr = Common::Task::TaskManager.new()
    task_mgr.set_task_queue(task.get_tasks())
    #task_mgr.dump_handler()
    task_mgr.run()

    task_mgr.reporter.dump()


    response = build_api_response(0,"ok",{ :msg => 'it is ok' })

    respond_to do |format|
        format.html { render :json => response }
        format.json { render :json => response }
    end

  end



  private

  def get_task_script(task_id)
    task = Task.find(task_id)
    return nil if task.nil?

    return task.script
  end

  def bool2string(value)
    
    return "fail" if value
    
    return "ok"

  end

end