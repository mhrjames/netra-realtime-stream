module Common

	module CommonFunction

		def self.hashie_to_yaml(hashie)
			return YAML::dump(hashie)
		end

		def self.yaml_to_hashie(yaml)
			return YAML::load(yaml)
		end
		
		def self.indent(char, count)
			token = ""
			count.times { token = token + " "}
	    	return token + char
	  	end

	  	def self.info(args)
	  		Rails::logger.info args
	  	end

	  	def self.debug(args)
	  		
	  		if not args.kind_of? Array 
	  			Rails::logger.debug args
	  			return
	  		end

	  		args.each do |arg|
				Rails::logger.debug arg+"\n"
	  		end

	  	end

	  	def self.error(args)
			Rails::logger.error ">>> Error!!! : #{args}"
	  	end

		def self.file_exist?(file_name)
			return File.exists?(file_name)
		end

	  	def self.read_whole_file(file_name)
			return File.read(file_name)
		end

		def self.is_valid_string(str)
			if str 
				if str.length > 0 
					return true
				end
			end

			return false
		end

  		def self.string_between_markers str, marker1, marker2
    		str[/#{Regexp.escape(marker1)}(.*?)#{Regexp.escape(marker2)}/m, 1]
  		end

	end
	
end