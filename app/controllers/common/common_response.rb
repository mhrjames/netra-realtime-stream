module Common

	class CommonResponse

		class << self

			def buildRestResponse(ret_code, msg, data)
				{:ret_code => ret_code, :msg => msg, :data => data}
			end

			def buildResponse(ret_code, msg, data)
				{:ret_code => ret_code, :msg => msg, :data => data}
			end

		end

	end

end