require 'yaml'


module Common

	class ConfigModel
		attr_accessor :proxy_url, :message_url, :message_channel

		#@proxy_url
		#@message_url
		#@message_channel
	end

	class NetraConfig
		@model = ConfigModel.new

		CONFIG_FILE_NAME = "#{Rails.root}/config/netra.yml"

		def self.update_config(params)
			#CommonFunction::debug "NetraConfig.update_config , params = #{params}"
			#json = JSON.parse(params['data'])

			CommonFunction::debug "NetraConfig.update_config , json = #{params}"
			
			update_netra_config(params[:api])
			write_config_file(params[:api])
		end

		def self.update_netra_config(params)
			
			a_config = NETRA_CONFIG
			a_config.each do |key,value|
				CommonFunction::debug "NetraConfig.update_netra_config , key = #{key} , value = #{value}"
				if value != params[key]
					NETRA_CONFIG[key] = params[key]
				end
			end

		end

		def self.load_config_file()
			config_yaml = YAML::load_file(CONFIG_FILE_NAME)
		end

		def self.write_config_file(config)
			#config["config"].each { |key, value| instance_variable_set("@#{key}", value) }
			Common::Util.write_file(CONFIG_FILE_NAME,config.to_yaml())
		end

	end

end