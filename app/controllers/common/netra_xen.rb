#require 'fog'
#require 'fog/xenserver/parser'


module Common

	class NetraXen

		
		class << self

			def create(ip,username,password)			    
			    session = Xen::Session.new(ip)
			    session.authenticate(username, password)

				return session
			end
			
			def getConnection2(ip,username,password)			    
			    session = Xen::Session.new(ip)
			    session.authenticate(username, password)

				return session
			end


			def getConnection(ip,username,password)
				conn = Fog::Compute::XenServer.new({
				  	:xenserver_url => ip,
				  	:xenserver_username => username,
				  	:xenserver_password => password,
				})

				return conn
			end

			def closeConnection(connection)
			
			end


			def getLocalStorageInfo(host)
				
				storage = { :storage_count => 0, :total_size => 0, :used_size => 0 }

				host.pbds.each do |pbd|
					if pbd.sr.name == "Local storage"
						storage[:storage_count] +=  1
						storage[:used_size] +=  pbd.sr.physical_utilisation.to_i
						storage[:total_size] +=  pbd.sr.physical_size.to_i
					end
				end
				return storage
			end

			def getHostHardwareInfo(host)
				host_param = {}
      			host_param['uuid'] = host.uuid
      			host_param['reference'] = host.reference
      			host_param['name'] = host.name
      			host_param['description'] = host.description
      			host_param['enabled'] = host.enabled
      			host_param['ip'] = host.address
      			host_param['hostname'] = host.hostname
      			host_param['cpu_count'] = host.cpu_info['cpu_count']
      			host_param['cpu_speed'] = host.cpu_info['speed']
      			host_param['cpu_model'] = host.cpu_info['modelname']
      			host_param['xenserver_version'] = host.software_version['product_version']
      			host_param['linux'] = host.software_version['linux']
      			host_param['boottime'] = host.other_config['boot_time']
      			host_param['total_memory'] = host.metrics.memory_total
      			host_param['free_memory'] = host.metrics.memory_free

      			return host_param
      		end

			def getHostInfo(connection)
        
        		hosts = []

          		connection.hosts.all.each do |host|
          			host_param = {}
          			
          			#print host.cpu_info
          			
          			hardware = self.getHostHardwareInfo(host)
          			storage = self.getLocalStorageInfo(host)

          			host_param = host_param.merge(hardware)
          			host_param = host_param.merge(storage)

          			hosts << host_param
          		end

          		return hosts
			end

			def getDetailedHost(connection)

				host = []

				connection.servers.all.each do |server|

					if server.is_a_snapshot or server.is_a_template
						continue
					end

					found_host = connection.hosts.get(server.affinity)
					if not found_host.nil?
						continue
					end

					found = self.findKeyInArray(host,'uuid', server.affinity)

					server_hash = {}

				end

			end

			def getXenHostSR(session,host_ref)
				
				host = session.hosts.get(host_ref)
				if host.nil?
					return nil
				end

				return host
			end

			def getXenHostLocalSR(session,host_ref)
				
				sr = []

				host = session.hosts.get(host_ref)
				if host.nil?
					return nil
				end

				pbds = session.pbds.all
				pbds.each do |pbd|
					if pbd.host.reference == host_ref
						if self.is_sr_local_storage(pbd.sr)
							sr << pbd.sr
							print "host sr = #{pbd.sr.name}\n"
						end
					end
				end

				return sr
			end

			def creataNewServer(conn,host,vm_name,template_name)

				vm_template = self.getTemplateVM(conn, template_name)
				print "template_vm = #{vm_template.name} \n"

				vm_new = conn.clone_server(vm_name, vm_template)

				# We will create the VDI in this SR
				sr = self.getXenHostLocalSR(conn,host.reference)[0]

				# Create a ~8GB VDI in storage repository 'Local Storage'
				#vdi = conn.vdis.create :name => "#{vm_name}-disk99", 
                #       :storage_repository => sr,
                #       :description => "#{vm_name}-disk1",
                #       :virtual_size => '8589934592' # ~8GB in bytes



				vm = conn.servers.new :name => "#{vm_name}",
				                      :affinity => host,
				                      :template_name => template_name

				vm.save :auto_start => false
				
				# Add the required VBD to the VM 
				#conn.vbds.create :server => vm, :vdi => vdi

				vm.set_attribute 'PV_bootloader', 'eliloader'
				vm.set_attribute 'other_config', {
					#'mac_seed'           => '9299540c-660d-acbf-9001-f1ee2254dfea',
					'linux_template'     => 'true',
					#'install-methods'    => 'http,ftp',
					#'install-repository' => 'http://archive.ubuntu.com/ubuntu', 
					#'install-arch'       => 'i386',
					#'debian-release'     => 'lucid', 
					'disks'              => "<provision><disk device='0' size='8589934592' sr='#{sr.uuid}' bootable='true' type='system'/></provision>",
					#'install-distro'     => 'debianlike'
				}

				vm.provision
				
				#self.start_on(conn, vm, host)

				puts 'Done!'

			end


			def creataNewServer99(conn,host,vm_name,template_name)
				net = conn.networks.find { |n| n.name == "Integration-VLAN" }

				# We will create the VDI in this SR
				sr = conn.storage_repositories.find { |sr| sr.name == 'Local storage' }

				# Create a ~8GB VDI in storage repository 'Local Storage'
				vdi = conn.vdis.create :name => "#{vm_name}-disk1", 
				                       :storage_repository => sr,
				                       :description => "#{vm_name}-disk1",
				                       :virtual_size => '8589934592' # ~8GB in bytes

				#host = conn.hosts.first

				# Create the VM (from scratch no template) but do not start/provision it
				vm = conn.servers.new :name => "#{vm_name}",
				                      :affinity => host,
				                      :pv_bootloader => 'pygrub'

				vm.save()

				puts "VM UUID: #{vm.uuid}"

				# Add a network to the VM
				conn.vifs.create :server => vm, :network => conn.default_network

				# Add the required VBD to the VM 
				conn.vbds.create :server => vm, :vdi => vdi
				vm.provision
				puts 'Done!'

			end

			def creataNewServer3(connection,host,server_name,template_name)
				
				#connection.hosts.get_record(host.reference)

				vm = connection.servers.new :name => server_name, :template_name => template_name, :affinity => host.reference

				#self.affinity(host.reference)
				self.set_affinity(connection, vm.reference, host.reference )

				#vm.set_affinity = host
				#vm.__affinity = host.uuid
				vm.save :auto_start => false
				#vm.set_attribute :__affinity => host
				#connection.vifs.create :server => vm, :network => connection.default_network

				vm.provision()
				#vm.start

				return vm
			end

			def creataNewServer2(connection,host,server_name,template_name)
				
				vm = connection.servers.new :name => server_name, 
											:__affinity => host,
											:template_name => template_name

				vm.save :auto_start => false, :__affinity => host
				vm.set_attribute('affinity', host)
				#connection.vifs.create :server => vm, :network => connection.default_network

				vm.provision()
				#vm.start

				return vm
			end


			def toHashServers(connection)
				
				servers = []
				connection.servers.all.each do |server|
					if server.is_a_snapshot or server.is_a_template
						continue
					end
					servers << self.toServerHash(server)
				end

				return servers
			end

			def toHashTemplates(connection)
				connection.servers.custom_templates				
			end

			def toHashSnapshots(connection)
				
				snapshots = []
				connection.servers.all.each do |server|
					next if server.is_a_snapshot == false
					snapshots << self.toServerHash(server)
				end

				return snapshots
			end

			def toServerHash(server)
				server_hash = {}

				server_hash[:name] = server.name
				server_hash[:uuid] = server.uuid
				server_hash[:power_state] = server.power_state
				server_hash[:vcpus_max] = server.vcpus_max
				server_hash[:affinity] = server.__affinity
				server_hash[:reference] = server.reference

				return server_hash
			end

			def toHostHash(host)
				host_hash = {}
      			
      			hardware = self.getHostHardwareInfo(host)
      			storage = self.getLocalStorageInfo(host)

      			host_hash = host_hash.merge(hardware)
      			host_hash = host_hash.merge(storage)

      			return host_hash
			end

			def getVMsInXenHost(connection,host_uuid)

				connection.servers.all(:include_snapshots => false).each do |server|
					Common::CommonFunction.debug ">>>server = #{server.name}, host = #{server.affinity.name}"

				end
			end


			def findHostByNetwork(connection,network)

				connection.hosts.all.each do |host|
					#Common::CommonFunction.debug "host = #{host.name}"
					
					host.pifs.each do |pif|
						#Common::CommonFunction.debug "pif.mac = #{pif.mac}, network.name = #{pif.network.name}"

						if pif.network.uuid == network 
							return host
						end
					end

					return nil
				end

			end






			def getTemplateVM(session, template_name)
				session.servers.get_by_name(template_name)
			end

			def findKeyInArray(arr, key, value)
				if h = ary.find { |h| h[key] == value }
				  return h
				end

				return nil
			end

  			def set_affinity(connection, vm_ref, host_ref )
          		print "set_affinity : vm = #{vm_ref} , host = #{host_ref}\n"
          		#config = { :vm_ref => vm_ref, :affinity => host_ref}

          		#connection.service.request({:parser => Fog::Parsers::XenServer::Base.new, :method => 'VM.set_affinity'}, config)

          		connection.service.request({:parser => Fog::Parsers::XenServer::Base.new, :method => 'VM.set_affinity'}, host_ref, host_ref)
          		
          		#@connection.request({:parser => Fog::Parsers::XenServer::Base.new, :method => 'VBD.create'}, default_config )
 				 #ref = @connection.request({:parser => Fog::Parsers::XenServer::Base.new, :method => 'VM.clone'},template_ref, server_name)          		       
        	end

        	def start_on(conn,vm_ref,host_ref)
          		conn.service.request({:parser => Fog::Parsers::XenServer::Base.new, :method => 'VM.start'}, vm_ref, host_ref, false, false)
          	end

          	def is_sr_local_storage(sr)
          		if sr.name.casecmp("Local storage") == 0
          			return true
          		end
          		return false
          	end
		end

	end

end
