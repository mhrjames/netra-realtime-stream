require 'yaml'


module Common

	class NotificationHandler

		def self.handle_pxe_provisioned(params)
			
			CommonFunction::debug "handle_pxe_provisioned : params = #{params}"
			
			a_host = parse_facter_yaml(params)
			save_host_to_db(a_host)

			msg = "Host, #{a_host.ip_addr} is provisioned!!!"
		end

		def self.handle_dhcp_host_updated(msg)
			#CommonFunction::debug "handle_dhcp_host_updated : msg = #{msg}"
			#broadcast(msg)

			msg = "DHCP host updated!!!"
		end


		private

		def self.parse_facter_yaml(params)
			#attr_accessible :description, :facter, :ip_addr, :mac, :name, :os_name, :os_ver, :role
			#attr_accessible :kernel_ver, :memory_size, :ruby_ver, :cpu, :cpu_count, :os_family, :puppet_ver, :disk_size

			yaml = YAML.load(params[:yaml])

			ip_to_find = params[:ip].nil? ? yaml['ipaddress'] : params[:ip]

			CommonFunction::debug "parse_facter_yaml : ip_to_find = #{ip_to_find} , params[:ip] = #{params[:ip].nil?}"

			host_record = Host.where(ip_addr: ip_to_find)[0]
			a_host = host_record.nil? ? Host.new : host_record
			
			a_host.facter = yaml
			a_host.ip_addr = ip_to_find
			a_host.mac = yaml['macaddress']
			a_host.name = yaml['fqdn'].nil? ? yaml['hostname'] : yaml['fqdn']
			a_host.os_name = yaml['operatingsystem']
			a_host.os_ver = yaml['operatingsystemrelease']
			a_host.kernel_ver = yaml['kernelversion']
			a_host.memory_size = yaml['memorysize']
			a_host.ruby_ver = yaml['rubyversion']
			a_host.cpu = yaml['processor0']
			a_host.cpu_count = yaml['processorcount']
			a_host.os_family = yaml['osfamily']
			a_host.ruby_ver = yaml['rubyversion']
			a_host.puppet_ver = yaml['puppetversion']
			a_host.disk_size = yaml['blockdevice_sda_size']

			a_host.userid = params[:userid] if not params[:userid].nil? 
			a_host.password = params[:password] if not params[:password].nil? 

			return a_host
		end

		def self.save_host_to_db(a_host)
			#attr_accessible :description, :facter, :ip_addr, :mac, :name, :os_name, :os_ver, :role

			a_host.save()
		end

      	def self.broadcast(body)
      		channel = "#{NETRA_CONFIG['config']['message_channel']}"
      		body[:msgtype] = "message"
      		Common::Rabbitmq.publish(body)
      		#Common::FayeClient.broadcast(channel,body)
      	end

	end

end