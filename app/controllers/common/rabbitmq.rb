require "bunny"

module Common
	
	class Rabbitmq

		RABBIT_SERVER = NETRA_CONFIG['config']['rabbitmq_ip']
		RABBIT_USER = NETRA_CONFIG['config']['rabbitmq_userid']
		RABBIT_PASSWORD = NETRA_CONFIG['config']['rabbitmq_password']
		TOPIC_NOTIFICATION = NETRA_CONFIG['config']['message_channel']
		TOPIC_DEPLOY = "deploy"
		

		def self.publish(msg)

			@conn = Bunny.new(:host => RABBIT_SERVER, :user => RABBIT_USER, :password => RABBIT_PASSWORD)
			@conn.start()

			@channel = @conn.create_channel()
			@exchange = @channel.topic('amq.topic', :auto_delete => true)
			@exchange.publish(msg, :routing_key => TOPIC_NOTIFICATION)

			save_to_db(TOPIC_NOTIFICATION,msg)

		end


		def self.publish_deploy(msg)

			@conn = Bunny.new(:host => RABBIT_SERVER, :user => RABBIT_USER, :password => RABBIT_PASSWORD)
			@conn.start()

			@channel = @conn.create_channel()
			@exchange = @channel.topic('amq.topic', :auto_delete => true)
			@exchange.publish(msg, :routing_key => TOPIC_DEPLOY)

			#save_to_db(TOPIC,msg)

		end

  		def self.save_to_db(channel, msg)
  			a_message = Message.new
  			a_message.channel = channel
  			a_message.body = msg
  			a_message.user_id = -1
  			a_message.save()
  		end

	end
end
