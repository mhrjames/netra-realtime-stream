module Common::Task

	class BaseTaskHandler
		
		attr_accessor :type, :task, :reporter

		def initialize(reporter)
			
			@reporter = reporter
			@openstack = Common::NetraOpenstack

		end

		def set_reporter(reporter)
			@reporter = reporter
		end

		def execute(options = {})

		end

		def dump()
			puts "BaseTaskHandler"
		end
	end


end