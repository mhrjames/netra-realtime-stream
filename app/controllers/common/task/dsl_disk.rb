module Common::Task

   class DslDisk

    	attr_accessor :name

    	def initialize(parent, &block)
    		@parent = parent
    		@name = "vm0"
    		@template_name = "default"
    		@image_name = "cirros"

    		instance_eval(&block)
    	end

    	def create( options = {} )

    		@parent.add_task("disk", "create", options)

    		puts "Disk.create : options = #{options}"
    	end

    	def delete( options = {})
    		@parent.add_task("disk", "delete", options)

			puts "Disk.delete : options = #{options}"
    	end

    	def backup( options = {})
    		@parent.add_task("disk", "backup", options)

			puts "Disk.backup : options = #{options}"
    	end

    	def attach_to(options = {})
    		@parent.add_task("disk", "attach_to", options)

    		puts "Disk.attach_to : options = #{options}"
    	end

    	def detach_from(options = {})
    		@parent.add_task("disk", "detach_from", options)

    		puts "Disk.detach_from : options = #{options}"
    	end

    end
    
end