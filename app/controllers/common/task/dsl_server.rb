module Common::Task

   class DslServer
    	
    	attr_accessor :id

    	def initialize(parent, &block)
    		@parent = parent

    		@name = "vm0"
    		@template_name = "default"
    		@image_name = "cirros"
    		@security_group = "Defaults"
			@created_at = Time.now.to_s
			@id = nil

    		instance_eval(&block)
    	end

    	def create(options = {})
    		@name = options[:name] if options[:name]
    		@template_name = options[:template] if options[:template]
    		@image_name = options[:image] if options[:image]
    		@security_group = options[:security_group] if options[:security_group]

    		@parent.add_task("server", "create", options)

    		puts "Server.create : options = #{options}"
    	end

    	def associate_floating_ip(options = {})
    		@template_name = options[:to]

			@parent.add_task("server", "associate_floating_ip", options)

    		puts "Server.associate_floating_ip : options = #{options}"
    	end

    	def disassociate_floating_ip(options = {})
    		#@image_name = image_name
			@parent.add_task("server", "disassociate_floating_ip", options)

    		puts "Server.disassociate_floating_ip : options = #{options}"
    	end

    	def delete(options = {})
    		#@disk_type = disk_type
    		@parent.add_task("server", "delete", options)

    		puts "Server.delete : options = #{options}"
    	end

    	def snapshot(options = {})
    		@parent.add_task("server", "snapshot", options)

    		puts "Server.snapshot : options = #{options}"
    	end


    	def method_missing(method_name, *args, &block) 		
    		#{}`([^']*)'	
 			msg = "You tried to call the method `#{method_name}' There is no such method."
    		puts "Error >>> Server.method_missing : #{method_name} is no such method."
    		raise NoMethodError, msg
    	end

    end

end
