module Common::Task::Handler
	
	class DiskAttachHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "disk"
			@task = "attach_to"
		end


		def execute(options = {})

			@reporter.info(self, "start execution")			
			
 			volume = @openstack.getVolumeByName(options[:disk])
 			if volume == nil
 				@reporter.error(self, 'fail to find virtual disk named `#{options[:disk]}`')
 				return
 			end

 			if volume.status.casecmp("in-use") == 0 
 				server_id = volume.attachments[0]['server_id']
				@reporter.error(self, "Disk `#{options[:disk]}` is alreay attached to virtual instance, `#{server_id}` ")
				return
 			end

 			instance = @openstack.getInstanceByName(options[:server])
 			if instance == nil
 				@reporter.error(self, 'fail to find virtual instance named `#{options[:server]}`')
 				return
 			end
			
			options[:volume_id]	= volume.id
			options[:instance_id] = instance.id

			options[:device] = "auto" if not options.has_key?(:device)

			puts "#{self.class} execute : options = #{options}"

 			response = @openstack.attachVolume(options)

			@reporter.info(self, "finish execution")
		end

	end
end