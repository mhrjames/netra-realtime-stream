module Common::Task::Handler
	

	class DiskBackupHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "disk"
			@task = "backup"
		end


		def execute(options = {})

			@reporter.info(self, "start execution")			
			
			puts "#{self.class} execute"

 			volume = @openstack.getVolumeByName(options[:disk])
 			if volume == nil
 				@reporter.error(self, 'fail to find virtual disk named `#{options[:disk]}`')
 				return
 			end

			#@volumeClient.create_volume_snapshot( params[:volume_id] , params[:backup_name], params[:description] )
			
			options[:volume_id]	= volume.id
 			response = @openstack.backupVolume(options)

			@reporter.info(self, "finish execution")
		end

	end
end