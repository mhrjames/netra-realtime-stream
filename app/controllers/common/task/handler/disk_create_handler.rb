module Common::Task::Handler
	

	class DiskCreateHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "disk"
			@task = "create"
		end

		def execute(options = {})

			@reporter.info(self, "start execution")			
			
			puts "#{self.class} execute"


			#'display_name'        => params[:display_name],
			#'display_description' => params[:display_description],
			#'size'                => params[:size],
			#'volume_type'         => params[:volume_type],
			#'availability_zone'   => params[:availability_zone]

			options[:display_name] = options[:name]
			options[:volume_type] = options[:type]
			options[:availability_zone] = "nova"
			options[:display_description] = options[:description]

 			volume = @openstack.createVolume(options)


			@reporter.info(self, "finish execution")
		end

	end
end