module Common::Task::Handler
	

	class DiskDeleteHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "disk"
			@task = "delete"
		end

		def execute(options = {})

			@reporter.info(self, "start execution")			
			
			puts "#{self.class} execute"

 			volume = @openstack.getVolumeByName(options[:disk])
 			if volume == nil
 				@reporter.error(self, 'fail to find virtual disk named `#{options[:disk]}`')
 				return
 			end

 			response = @openstack.deleteVolume(volume.id)

			@reporter.info(self, "finish execution")
		end

	end
end