module Common::Task::Handler
	
	class DiskDetachHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "disk"
			@task = "detach_from"
		end


		def execute(options = {})

			@reporter.info(self, "start execution")			
			
 			volume = @openstack.getVolumeByName(options[:disk])
 			if volume == nil
 				@reporter.error(self, 'fail to find virtual disk named `#{options[:disk]}`')
 				return
 			end

 			if not volume.status.casecmp("in-use") == 0 
				@reporter.error(self, "Disk `#{options[:disk]}` is not attached to virtual server ")
				return
 			end
 			
 			instance = @openstack.getInstanceByName(options[:server])
 			if instance == nil
 				@reporter.error(self, 'fail to find virtual instance named `#{options[:server]}`')
 				return
 			end
			
			attachment = find_attachment_by_instance(volume, instance.id)
			if attachment.nil?
				@reporter.error(self, 'fail to find a disk matched `#{options[:server]}`')
				return
			end

			options[:attachment_id] = attachment['id']
			options[:instance_id] = instance.id

			puts "#{self.class} execute : options = #{options}"

			#@computeClient.detach_volume(params[:instnace_id], params[:attachment_id])

 			response = @openstack.detachVolume(options)

			@reporter.info(self, "finish execution")

		end



		private

		def find_attachment_by_instance(volume,instance_id)
			
			volume.attachments.each do |attachment|
				if attachment['server_id'] == instance_id
					return attachment
				end
			end

			return nil
		end

	end

end