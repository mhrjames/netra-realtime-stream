module Common::Task::Handler
	

	class ServerCreateHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "server"
			@task = "create"
		end

		def execute(options = {})
			puts "start execution : options = #{options}"
			@reporter.info(self, "start execution : options = #{options}")			

			image_instance = @openstack.getImageByName(options[:image])
 			if image_instance == nil
 				@reporter.error(self, 'fail to find image named `#{options[:image]}`')
 				return
 			end
			
			flavor_instance = @openstack.getFlavorByName(options[:template])
 			if flavor_instance == nil
 				@reporter.error(self, 'fail to find template named `#{options[:template]}`')
 				return
 			end
			
			puts "#{self.class} execute : image_id = #{image_instance.id} , flavor_id = #{flavor_instance.id}"

			options[:image_id] = image_instance.id
			options[:flavor_id] = flavor_instance.id

			response = @openstack.bootstrapInstance(options)

			@reporter.info(self, "finish execution")
		end

	end

	
end