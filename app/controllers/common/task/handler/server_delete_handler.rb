module Common::Task::Handler
	

	class ServerDeleteHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "server"
			@task = "delete"
		end

		def execute(options = {})
			puts "#{self.class} execute"

			@reporter.debug(self, "start execution")			

 			instance = @openstack.getInstanceByName(options[:server])
 			if instance == nil
 				@reporter.error(self, 'fail to find virtual instance named `#{options[:server]}`')
 				return
 			end

 			response = @openstack.terminateInstance(instance.id)

			@reporter.debug(self, "finish execution")
		end

	end
end
