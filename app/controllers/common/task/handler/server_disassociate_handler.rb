module Common::Task::Handler
	

	class ServerDisassociateHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "server"
			@task = "disassociate_floating_ip"
		end

		def execute(options = {})

			@reporter.info(self, "start execution")			

 			instance = @openstack.getInstanceByName(options[:server])
 			if instance == nil
 				@reporter.error(self, 'fail to find virtual instance named `#{options[:server]}`')
 				return
 			end
 			
 			if not options.has_key?(:ip)
 				@reporter.error(self, 'Floating IP address is not specified. Please give IP to disassociate!!!')
 				return 			
			end

 			options[:floating_ip] = options[:ip]
 			options[:instance_id] = instance.id

			puts "#{self.class} execute : id = #{instance.id} , floating = #{options[:floating_ip]}"

 			@openstack.disassociateFloatingIp(options)

			@reporter.info(self, "finish execution")
		end


		private

		def pick_floating_ip(floatings)

			floatings.each do |floating_ip|
				puts "ServerAssociateHandler.pick_floating_ip : floating_ip = #{floating_ip}"
				if floating_ip['instance_id'].nil?
					return floating_ip
				end
			end

			return nil
		end

		def get_floating_instance()
		
 			floating_ips = @openstack.listFloatingIps()
 			floating_ip = pick_floating_ip(floating_ips)
 			
 			return floating_ip if not floating_ip.nil?


			ip_pools = @openstack.listFloatingPools()
			if ip_pools.length == 0 
				@reporter.error(self, 'NO floating ip pool is avilable. please create ip pool first')
				return
			end

			ip_pool = ip_pools[0][:name]
			floating_ip = @openstack.createFloatingIp(ip_pool)

			return floating_ip

		end

	end
end