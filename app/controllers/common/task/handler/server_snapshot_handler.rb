module Common::Task::Handler
	

	class ServerSnapshotHandler < Common::Task::BaseTaskHandler

		def initialize(reporter)

			super

			@type = "server"
			@task = "snapshot"
		end

		def execute(options = {})
			@reporter.info(self, "start execution")			
			
			puts "#{self.class} execute"

 			instance = @openstack.getInstanceByName(options[:server])
 			if instance == nil
 				@reporter.error(self, 'fail to find virtual instance named `#{options[:server]}`')
 				return
 			end

			#def self.createSnapshot( params = {} )
				#@computeClient.create_image( params[:instance_id], params[:name] )
			#end
			
			options[:instance_id] = instance.id
			options[:name] = options[:snapshot_name]

 			response = @openstack.createSnapshot(options)


			@reporter.info(self, "finish execution")
		end

	end
end