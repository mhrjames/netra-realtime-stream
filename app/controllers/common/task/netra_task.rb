#require 'netra_task_handler.rb'

module Common::Task


	class NetraTaskItem

		attr_accessor :id, :type, :task, :options

		def to_json()
			{ :id => @id, :type => @type, :task => @task, :options => @options }
		end
	end


    class NetraTask
    	
    	METHODS = ['new']

    	#alias_method :new, :initialize

    	class << self

			def setter(*method_names)
				method_names.each do |name|
					send :define_method, name do |data|
						instance_variable_set "@#{name}".to_sym, data #
					end
				end
			end

			def varags_setter(*method_names)
				method_names.each do |name|
					send :define_method, name do |*data|
						instance_variable_set "@#{name}".to_sym, data 
					end
				end
			end


	    	def new_task(name)
	    		puts "new_task : name = #{name}"
	    		
	    		@netra_task.name = name

	    		return @netra_task
	    	end


	    	def error()
	    		return @errors
	    	end

	    	def error?()
	    		return (@errors.length > 0)
	    	end

	    	def get_error_msg()
	    		msg = []
	    		@errors.each do |error|
	    			msg << "#{error[:type]} :: #{error[:msg]}"
	    		end

	    		return msg
	    	end

			def search_keyword(script,keyword)

				index = 1
				script.split("\n").each do |line|
					if line.scan(/\b#{keyword}\b/i).length > 0
						return index
					end 
					index = index + 1
				end

				return -1
			end

			def get_string_between(my_string, start_at, end_at)
			    my_string = " #{my_string}"
			    ini = my_string.index(start_at)
			    return my_string if ini == 0
			    ini += start_at.length
			    length = my_string.index(end_at, ini).to_i - ini
			    my_string[ini,length]
			end

			def extract_method_name_from_message(err_msg)
				name = err_msg.scan(/`([^']*)'/)[0][0]
				puts "extract_method_name_from_message : name = #{name}"

				return name
			end

	    	def add_error(error_type,error_msg)
	    		puts "Error!!! : type = #{error_type} , msg = #{error_msg}"
	    		@errors << { :type => error_type, :msg => error_msg }
	    	end

	    	def eval_script(file_name,script_text)

				@netra_task = Common::Task::NetraTask.new()
				@errors = []

				begin
					@netra_task.instance_eval(script_text)

					puts "eval_script : netra_task = #{@netra_task}"

				rescue SyntaxError => syn_error
					
					msg = "Error on line #{syn_error.message}"
					#puts "eval_script.Error!!! : msg = #{msg}"

					add_error(syn_error.class,syn_error.message.sub("(eval)","line"))

					return false

				#rescue NoMethodError => no_method_error
				#	add_error(no_method_error.class,no_method_error.message)

				rescue Exception => err

					extracted_method_name = extract_method_name_from_message(err.message)
					line_number = search_keyword(@script, extracted_method_name)

					puts "eval_script.Error!!! : method_name = #{extracted_method_name} , msg = #{err.message}"

					add_error(err.class, "#{err.message} at line:#{line_number}")

					return false
				end				

	    	end

			def loadFromString(file_name,script_text)
				@script = script_text
				
				eval_script(file_name,@script)

				return @netra_task
			end

			def loadFromFile(file_name)
				@file_name = file_name
				contents = File.read(file_name)

				return loadFromString(@file_name,contents)
			end		 

		end

		
		attr_accessor :name

    	def initialize()
    		@errors = []
    		@tasks = []

    		puts "NetraTask.initialize : name = #{@name}"
    	end

    	def clear()
    		@errors = []
    		@tasks = []
    	end

    	def is_error?
    		@errors.length > 0
    	end

    	def server(&block)

    		@server_instance = Common::Task::DslServer.new(self, &block)

    	end

    	def disk(&block)

			@disk_instance = Common::Task::DslDisk.new(self,&block)
    	end

    	def run
    		puts "NetraTask.run!!! : task count = #{get_task_count()}"

    		return false if is_error? 

    		#dump_tasks()
    	end

    	


    	def get_task_count()
    		@tasks.length
    	end

    	def get_tasks()
    		return @tasks
    	end

    	def add_task(task_type, task_name, options)
    		task_item = Common::Task::NetraTaskItem.new
    		task_item.id = @tasks.length
    		task_item.type = task_type
    		task_item.task = task_name
    		task_item.options = options 

    		@tasks << task_item
    	end

    	def dump_tasks()

    		puts ">>> NetraTask.dump_tasks : name = #{@name} <<<"
    		@tasks.each do |task|
				puts "id = #{task.id} , type = #{task.type} , name = #{task.task} , options = #{task.options}"
    		end
    		puts ">>> end <<<"
    	end



    	def method_missing(method_name, *args, &block)
 			
# 			if METHODS.include?(method_name.to_s) 
# 				return 
# 			end

 			msg = "You tried to call the method `#{method_name}' There is no such method."
    		puts "Error >>> NetraTask.method_missing : #{method_name} is no such method."

    		raise NoMethodError, msg
    	end


    end

end
