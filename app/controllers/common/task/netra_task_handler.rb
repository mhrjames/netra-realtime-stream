module Common

	class BaseTaskHandler
		
		attr_accessor :type, :name, :reporter

		def initialize()

		end

		def set_reporter(reporter)
			@reporter = reporter
		end

		def execute(options = {})

		end

		def dump()
			puts "BaseTaskHandler"
		end
	end


	class ServerCreateHandler < BaseTaskHandler
		
		attr_accessor :type, :task

		@type = "server"
		@task = "create"

		def execute(options = {})

		end

	end


	class TaskManager

		attr_accessor :task_queue

		def initialize()
			@handler = {}
			@task_queue = nil

			load_task_handler()
		end

		def set_task_queue(queue)
			@task_queue = queue
		end

		def add_task_handler(handler)
			
			@handler[get_handler_key(handler.type,handler.task)] = handler
		end

		def load_task_handler()
			
			add_task_handler(ServerCreateHandler.new())

		end

		def pop_task()

			return nil if @task_queue.length == 0 

			task_item = @task_queue.shift

			return task_item
		end

		def find_handler(type, task)
			handler_key = get_handler_key(type,task)
			
			return nil if @handler.has_key?(handler_key)

			return @handler[handler_key]
		end

		def run()
			
			puts "TaskManager.run!!!"

			while (@task_queue.length > 0) do
				
				task_item = pop_task()
				puts task_item.to_json()

				handler = find_handler(task_item.type,task_item.task)
				if (! handler)
					puts ">>> Taskmanager.run : Error!!! , fail to find task hander for #{task_item.to_json}"
				end

				puts "Taskmanager.run : task hander for #{task_item.to_json}"
			end 
		end


		private

		def get_handler_key(type,task)
			"#{type}::#{task}"
		end
	end

end
