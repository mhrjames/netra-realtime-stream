module Common::Task

	class TaskManager

		attr_accessor :task_queue, :reporter

		def initialize()
			@handler = {}
			@task_queue = []

			@reporter = Common::Task::TaskReporter.new()

			load_task_handler()
		end

		def set_task_queue(queue)
			@task_queue = queue
		end

		def add_to_task_queue(queue)
			@task_queue = @task_queue.concat(queue)
		end

		def add_task_handler(handler)
			
			@handler[get_handler_key(handler.type,handler.task)] = handler
		end

		def load_task_handler()
			
			#server related handler			
			add_task_handler(Common::Task::Handler::ServerCreateHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::ServerDeleteHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::ServerSnapshotHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::ServerAssociateHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::ServerDisassociateHandler.new(@reporter))

			#disk related handler
			add_task_handler(Common::Task::Handler::DiskCreateHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::DiskDeleteHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::DiskBackupHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::DiskAttachHandler.new(@reporter))
			add_task_handler(Common::Task::Handler::DiskDetachHandler.new(@reporter))

		end

		def pop_task()

			return nil if @task_queue.length == 0 

			task_item = @task_queue.shift

			return task_item
		end

		def find_handler(type, task)
			handler_key = get_handler_key(type,task)
			
			return nil if ! @handler.has_key?(handler_key)

			return @handler[handler_key]
		end

		def run()
			
			puts "TaskManager.run!!!"

			@reporter.clear()

			while (@task_queue.length > 0) do
				
				task_item = pop_task()
				#puts task_item.to_json()

				handler = find_handler(task_item.type,task_item.task)
				if (! handler)
					puts ">>> Taskmanager.run : Error!!! , fail to find task hander for #{task_item.to_json}"
					next
				end

				#puts "Taskmanager.run : task hander for #{task_item.to_json}"

				begin
					handler.execute(task_item.options)
				rescue Exception => e
					@reporter.error(handler,e.message) 
				end
			end 
		end


		def dump_handler()
			
			puts ">>> Taskmanager.dump_handler <<<"
			@handler.each do |key,value|
				puts "key = #{key} , value = #{value}"
			end
			puts ">>> end <<<"
		end

		private

		def get_handler_key(type,task)
			"#{type}::#{task}"
		end
	end

end



#file = Rails.root.join("test.netra")
file = "test.netra"
dsl_text = File.read(file)

#puts " dsl_text \n  #{dsl_text} \n\n"

#dsl_instance = Common::NetraTask.new("Test")
#task = Task::NetraTask.loadFromFile(file)

#puts "netra_task = #{task}"

#task_mgr = TaskManager.new()
#task_mgr.set_task_queue(task.get_tasks())
#task_mgr.dump_handler()
#task_mgr.run()


