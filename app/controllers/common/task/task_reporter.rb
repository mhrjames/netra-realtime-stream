module Common::Task

	class TaskReporter
		
		attr_accessor :log

		def initialize()
			clear()
		end

		def clear()
			@error_count = 0
			@log = []
		end

		def error?()
			(@error_count > 0)
		end

		def debug(handler, msg)
			write_log("debug",handler,msg)
		end

		def info(handler, msg)
			write_log("info",handler,msg)
		end

		def error(handler, msg)
			@error_count = @error_count + 1
			write_log("error",handler,msg)
		end

		#def to_json()
		#end

		def dump()
			
			puts ">>> TaskReporter.dump <<<"
			@log.each_with_index do |log,index|
				puts "#{index} : logs = #{log}"
			end
			puts ">>> end <<<"
		end


		private

		def write_log(type, handler, msg)
			log_item = { :type => type, :handler => handler.class.name, :datetime => get_current_datetime(), :msg => msg }
			@log << log_item
		end

		def get_current_datetime()
			Time.now.strftime("%Y-%m-%d %H:%M")
		end
	end

end