module Common::Util

  # james
  class << self

    def dos2unix(str)
      str.encode("UTF-8").gsub(/\r\n/, "\n")
    end

    def write_file(file_name, contents)
      File.open(file_name, 'w') {|f| f.write(dos2unix(contents)) }
    end

    def read_file(file_name)
      
      contents = ""
      if File.exists?(file_name)
        contents = File.read(file_name)
      end

      return contents
    end

    def make_directory(path)
      FileUtils.mkdir_p path
    end

    def execute_bash(cmd)

      logger.debug "Proxy::Util.execute_bash : cmd = #{cmd}"

      stdout_result = []
      stderr_result = []

      Open3::popen3(cmd) do |stdin, stdout, stderr|
        stdout.each do |line|
          stdout_result << line
          logger.debug "[#{line}"
        end
        stderr.each do |line|
          stderr_result << line
          logger.debug "Proxy::Util.execute_bash : #{line}"
          #raise PortInUse if line["socket.error: [Errno 98] Address already in use"]
        end
      end

      return stdout_result, stderr_result
    end

    def get_current_datetime()
      return Time.now.strftime("%Y-%m-%d %H:%M")
    end

  end

end
