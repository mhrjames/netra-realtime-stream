module Common

  require "xmlrpc/client"
  #require 'dispatcher'

  class XenSession
    
    attr_reader :key

    def login_with_password(uri, username, password, timeout = 1200)
      begin
        @uri = uri
        @client = XMLRPC::Client.new2(@uri, nil, timeout)
        @session = @client.proxy("session")

        response = @session.login_with_password(username, password)
        raise XenAPI::ErrorFactory.create(*response['ErrorDescription']) unless response['Status'] == 'Success'

        @key = response["Value"]

        #Let's check if it is a working master. It's a small pog due to xen not working as we would like
        print "sdfsdf : ",self.pool.get_all

        self
      rescue Exception => exc
        #print exc
        #error = Common::ErrorFactory.wrap(exc)
        #if @block
          # returns a new session
        #  @block.call(error)
        #else
        #  raise error
        #end
      end
    end

    def close
      @session.logout(@key)
    end

    def vm_ref(uuid)
      self.VM.get_by_uuid(uuid)
    end

    def vm_record(ref)
      self.VM.get_record(ref)
    end

    def host_record(ref)
      self.host.get_record(ref)
    end
  end
end