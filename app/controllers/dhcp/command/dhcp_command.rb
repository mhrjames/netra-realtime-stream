module Dhcp
	module Command
		module DhcpCommand

			def scanIp(nic,ip_range)
				
				ips = Dhcp::IpScanner.scan(nic,ip_range)

				return Dhcp::DhcpResponse.buildRestResponse(Const::ERR_IP_SCAN,Const::ERRMSG_IP_SCAN,ips) if ips.nil?
				
				return Dhcp::DhcpResponse.buildRestResponse(Const::OK,Const::MSG_OK,ips)
			end

			def start
				cmd = "service dnsmasq start"
				stdout, stderr = Dhcp::ShellExecutor.execute(cmd)

				return Dhcp::DhcpResponse.buildRestResponse(Const::ERR_DHCP_START,Const::ERRMSG_DHCP_START,"error") if not stderr.size == 0
				
				return Dhcp::DhcpResponse.buildRestResponse(Const::OK,Const::MSG_OK,"ok")
			end

			def stop
				cmd = "service dnsmasq stop"
				stdout, stderr = Dhcp::ShellExecutor.execute(cmd)

				return Dhcp::DhcpResponse.buildRestResponse(Const::ERR_DHCP_STOP,Const::ERRMSG_DHCP_STOP,"error") if not stderr.size == 0
				
				return Dhcp::DhcpResponse.buildRestResponse(Const::OK,Const::MSG_OK,"ok")

			end

			def restart
				cmd = "service dnsmasq restart"
				stdout, stderr = Dhcp::ShellExecutor.execute(cmd)

				return Dhcp::DhcpResponse.buildRestResponse(Const::ERR_DHCP_RESTART,Const::ERRMSG_DHCP_RESTART,"error") if not stderr.size == 0
				
				return Dhcp::DhcpResponse.buildRestResponse(Const::OK,Const::MSG_OK,"ok")

			end

			def status()
				return Dhcp::DhcpResponse.buildRestResponse(Const::OK,Const::MSG_OK,@dnsmasq.to_json)
			end
		end
	end
end