module Dhcp

	class ConfParser

		class << self
		
			INTERFACE = "interface="
			DHCP_RANGE = "dhcp-range="
			DOMAIN = "domain="
			DHCP_HOST = "dhcp-host="
			DHCP_GATEWAY = "dhcp-option=3,"
			DNS_SERVER = "dhcp-option=6,"

			def parse(contents)

				dnsmasq = Dhcp::DhcpModel.new

				contents.each do |line|
					trimmed_line = line.strip!
					#Rails.logger.debug("ConfParser.parse : line = #{trimmed_line}")
					next if trimmed_line.length == 0

					if isInterface?(line)
						dnsmasq.interface = extractValue(INTERFACE,line)
					elsif isDomain?(line)
						dnsmasq.domain = extractValue(DOMAIN,line)
					elsif isDhcpRange?(line)
						parseDhcpRange(dnsmasq, line)
					elsif isDhcpHost?(line)
						dnsmasq.dhcp_host = extractValue(DHCP_HOST,line)
					elsif isGateway?(line)
						#Rails.logger.debug("gateway = #{line}")
						dnsmasq.gateway = extractValue(DHCP_GATEWAY,line)
					elsif isDnsServer?(line)
						dnsmasq.dns_server =extractValue(DNS_SERVER,line)
					end

				end

				return dnsmasq
			end

			def isInterface?(line)
				matchKeyword(INTERFACE,line)
			end

			def isDhcpRange?(line)
				matchKeyword(DHCP_RANGE,line)
			end

			def isDhcpHost?(line)
				matchKeyword(DHCP_HOST,line)
			end

			def isDomain?(line)
				matchKeyword(DOMAIN,line)
			end

			def isGateway?(line)
				matchKeyword(DHCP_GATEWAY,line)
			end

			def isDnsServer?(line)
				matchKeyword(DNS_SERVER,line)
			end

			def matchKeyword(keyword,line)
				#line_head = line[keyword.length..-1]
				#Rails.logger.debug("matchKeyword : keyword = #{keyword}")
				return true if line.downcase.index(keyword.downcase) == 0
				return false
			end

			def extractValue(keyword,line)
				line.sub(keyword,'')
			end

			def parseDhcpRange(dnsmasq, line)
				extracted = extractValue(DHCP_RANGE,line)
				tokens = extracted.split(',')

				dnsmasq.range_interface = tokens[0]
				dnsmasq.ip_range_start = tokens[1]
				dnsmasq.ip_range_end = tokens[2]
				dnsmasq.ip_range_duration = tokens[3]
			end
		end

	end

end