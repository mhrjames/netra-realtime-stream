module Dhcp

	class ConfigHandler

		class << self
	 		
	 		def load(conf_file)
				contents = nil
				if File.exists?(conf_file)
					contents = File.readlines(conf_file)
				end

				return contents
	 		end

	 		def save(conf_file, contents)
				File.open(conf_file, 'w') {|f| f.write(dos2unix(contents)) }
	 		end

			def dos2unix(str)
				str.encode("UTF-8").gsub(/\r\n/, "\n")
			end


			def delete_line_from_file(filename, regex)
				# create empty file string
				tmp_file = ''

				# iterate over every line and skip lines that match
				File.open(filename, "r").each_line do |l|
					tmp_file += l unless regex.match(l.strip)
				end

				# write tmp file without matching lines
				File.open(filename, 'w') { |file| file.write tmp_file }

				# clear memory
				tmp_file = nil
			end

	 	end

	end

end
