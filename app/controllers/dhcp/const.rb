module Dhcp

	module Const
		OK  = 0
		MSG_OK = "ok"

		ERR_IP_SCAN = -1
		ERRMSG_IP_SCAN = "Fail to scan ip address"

		ERR_DHCP_STOP = -2
		ERRMSG_DHCP_STOP = "Error occurred while stopping dhcp server"

		ERR_DHCP_START = -3
		ERRMSG_DHCP_START = "Error occurred while starting dhcp server"

		ERR_DHCP_RESTART = -4
		ERRMSG_DHCP_RESTART = "Error occurred while restarting dhcp server"

	end

end