module Dhcp

	class DhcpManager

		include Dhcp::Command::DhcpCommand


		def initialize()
			#Rails.logger.debug("netra_config = #{Xen::Application::NETRA_CONFIG}")			
			@conffile = NETRA_CONFIG['config']['dnsmasq_conf']
			
			@conf_contents = Dhcp::ConfigHandler.load(@conffile)

			#Rails.logger.debug("contents = #{@conf_contents}")

			@dnsmasq = Dhcp::ConfParser.parse(@conf_contents)
		end
		
	end

end