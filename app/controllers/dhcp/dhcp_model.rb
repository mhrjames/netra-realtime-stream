module Dhcp

	class DhcpModel

		attr_accessor :range_interface
		attr_accessor :ip_range_start
		attr_accessor :ip_range_end
		attr_accessor :ip_range_duration
		attr_accessor :domain
		attr_accessor :interface
		attr_accessor :gateway
		attr_accessor :dns_server
		attr_accessor :dhcp_host


		def to_json()
			{
				:interface => interface,
				:domain => domain,
				:range_interface => range_interface,
				:ip_range_start => ip_range_start,
				:ip_range_end => ip_range_end,
				:ip_range_duration => ip_range_duration,
				:dhcp_host => dhcp_host,
				:gateway => gateway,
				:dns_server => dns_server
			}

		end
	end

end