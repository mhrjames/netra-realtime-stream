module Dhcp

	class IpModel

		MATCH_IP4 = /^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3}$/

		attr_accessor :ip
		attr_accessor :mac
		attr_accessor :name

 		def ipv4_valid?(ipv4)
    		if not ipv4.blank? and IpModel::MATCH_IP4.match(ipv4) then true else false end
 		end

	end

end