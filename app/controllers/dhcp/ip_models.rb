module Dhcp

	class IpModels

		attr_accessor :ip

		def initialize()
			@ip = []
		end

		def create(ip_addr, mac_addr, name)
			a_ip = Dhcp::IpModel.new()
			a_ip.ip = ip_addr
			a_ip.mac = mac_addr
			a_ip.name = name
				
			add(a_ip)
		end

		def add(ip_model)
			return nil if ip_model.nil?

			@ip << ip_model
		end

		def delete(index)
			@ip.delete_at(index)
		end

		def deleteByIp(ip_addr)
			found_ip_index = findByIp(ip_model.ip)
			if found_ip_index.nil?
				return nil
			end

			delete(found_ip_index)
		end

		def findByIp(ip_addr)
			@ip.each_with_index do |ip_model, index|
				if ip_model.ip.casecmp(ip_addr) == 0
					return index
				end
			end

			return nil
		end
	end

end