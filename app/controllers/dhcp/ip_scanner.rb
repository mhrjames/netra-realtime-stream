require 'ipaddr'


module Dhcp
	
	class IpScanner
		
		class << self

			def scan(nic,ip_range)
				
				cmd = "arp-scan --interface=#{nic} #{ip_range}"
				stdout, stderr = Dhcp::ShellExecutor.execute(cmd)

				#Rails.logger.debug("DHCP.IpScanner.scan : stdout = #{stderr}")

				return nil if not stderr.length == 0

				return parse(stdout)

			end

			def parse(stdout)

				ips = Dhcp::IpModels.new

				stdout.each do |line|
					trimmed_line = line.strip!
					#Rails.logger.debug("parse : line = #{trimmed_line}")

					tokens = trimmed_line.split
					next if not isIPAddressToken?(tokens)

					ips.create(tokens[0],tokens[1],tokens[2]) 
				end

				return ips
			end

			def isIPAddr?(ip)
				!(IPAddr.new(ip) rescue nil).nil?
			end

			def isIPAddressToken?(tokens)
				return false if not tokens.length == 3
				return false if not isIPAddr?(tokens[0])
				
				return true
			end

		end

	end
	
end