module Dhcp

	class ShellExecutor

		class << self
			
			def execute(cmd)
				#Rails.logger.debug "DHCP::ShellExecutor.execute : cmd = #{cmd}"

				stdout_result = []
				stderr_result = []

				Open3::popen3(cmd) do |stdin, stdout, stderr|
					stdout.each do |line|
						stdout_result << line
						#Rails.logger.debug "[#{line}"
					end
				
					stderr.each do |line|
						stderr_result << line
						#logger.debug "Proxy::Util.execute_bash : #{line}"
						#raise PortInUse if line["socket.error: [Errno 98] Address already in use"]
					end
				end

				return stdout_result, stderr_result

			end

		end

	end

end