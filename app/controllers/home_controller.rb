class HomeController < ApplicationController

	def index
    
    	respond_to do |format|
    		format.html { render template: "home/index.html.erb" }
	    end  

    end

end
