#require 'net/ldap'


module LDAP
	
	#
	# following link has good reference of ldap
	# https://gist.github.com/jeffjohnson9046/7012167
	#
	class ActiveDirectory


		def initialize()

			@ldap = Net::LDAP.new  :host => NETRA_CONFIG['ad_ip'], 
                  	:port => NETRA_CONFIG['ad_port'].to_i, 
                  	#:encryption => :simple_tls,
                  	:base => NETRA_CONFIG['ad_base'],
                  	:auth => {
                    	:method => :simple,
                    	:username => NETRA_CONFIG['ad_username'], 
                    	:password => NETRA_CONFIG['ad_password']
                  	}

            return @ldap
        end


        def getOrganizationUnit()
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		

			result = []
      		
      		filter = Net::LDAP::Filter.eq("objectCategory", "organizationalUnit")
			result_attrs = ["dn", "ou", "distinguishedname", "name"]

      		@ldap.search(:filter => filter, :attributes => result_attrs) do |entry|
      			Xen::Logger.debug("listOrganizationUnit : entry = #{entry.to_json}")
      			#Xen::Logger.debug("listOrganizationUnit : entry = #{entry.sAMname}")
      			result << LDAP::Formatter.ouToHash(entry)
      		end

      		return result
        end

        def getUser()
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		

      		filter = Net::LDAP::Filter.eq("objectClass", "person")
      		result_attrs = ["dn", "ou", "cn", "distinguishedname", "name", "objectcategory", "samaccountname", "userprincipalname" ,"lastlogon", "lastlogoff"]

      		result = []
      		@ldap.search(:filter => filter, :attributes => result_attrs) do |entry|
      			#Xen::Logger.debug("listOrganizationUnit : entry = #{entry.to_json}")
      			#Xen::Logger.debug("listUser : entry = #{entry.to_json}")
      			result << LDAP::Formatter.userToHash(entry)
      		end

      		return result
        end

        def getComputer()
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		

      		filter = Net::LDAP::Filter.eq("objectClass", "computer")
      		result_attrs = ["dn", "ou", "cn", "distinguishedname", "name", "objectcategory", "samaccountname", "displayname", "dnshostname" ,"lastlogon", "lastlogoff", "serviceprincipalname"]

      		result = []
      		@ldap.search(:filter => filter, :attributes => result_attrs) do |entry|
      			#Xen::Logger.debug("listOrganizationUnit : entry = #{entry.to_json}")
      			#Xen::Logger.debug("getComputer : entry = #{entry.to_json}")
      			result << LDAP::Formatter.computerToHash(entry)
      		end

      		return result
        end

        def getUserInOrganizationUnit(ou_name)
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		
			
			treebase = "OU=#{ou_name},DC=xen,DC=com"
      		
      		person_filter = Net::LDAP::Filter.eq("objectClass", "person")
			result_attrs = ["dn", "ou", "cn", "distinguishedname", "name", "objectcategory", "samaccountname", "userprincipalname" ,"lastlogon", "lastlogoff"]

			result = []      		
      		@ldap.search(:base => treebase, :filter => person_filter, :attributes => result_attrs) do |entry|
      			#Xen::Logger.debug("listOrganizationUnit : entry = #{entry.to_json}")
      			#Xen::Logger.debug("listUserInOrganizationUnit : entry = #{entry.to_json}")
      			result << LDAP::Formatter.userToHash(entry)
      		end

      		return result
        end

        def getComputerInOrganizationUnit(ou_name)
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		
			
			treebase = "OU=#{ou_name},DC=xen,DC=com"
      		
      		person_filter = Net::LDAP::Filter.eq("objectClass", "computer")
			result_attrs = ["dn", "ou", "cn", "distinguishedname", "name", "objectcategory", "samaccountname", "displayname", "dnshostname" ,"lastlogon", "lastlogoff", "serviceprincipalname"]

			result = []      		
      		@ldap.search(:base => treebase, :filter => person_filter, :attributes => result_attrs) do |entry|
      			result << LDAP::Formatter.computerToHash(entry)
      		end

      		return result
        end

        #
		# following link has really good example on querying AD
		# http://social.technet.microsoft.com/wiki/contents/articles/5392.active-directory-ldap-syntax-filters.aspx
        #
        def listOrganizationUnit()
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		

      		ous = getOrganizationUnit()
      		Xen::Logger.debug("listOrganizationUnit : ous = #{ous}")

      		return ous
        end

        def listUser()
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		

			users = getUser()
			return users
        end

        def listComputer()
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		

			return getComputer()
        end

        def listUserInOrganizationUnit(ou_name)
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		
			
			return getUserInOrganizationUnit(ou_name)
        end

        def listComputerInOrganizationUnit(ou_name)
			#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}      		
			
			return getComputerInOrganizationUnit(ou_name)
        end

		def get_ldap_response(ldap)
			msg = "Response Code: #{ ldap.get_operation_result.code }, Message: #{ ldap.get_operation_result.message }"

			raise msg unless ldap.get_operation_result.code == 0
		end


		def filter()
			#######################################################################################################################
			# SOME SIMPLE LDAP SEARCHES
			#######################################################################################################################
			 
			# GET THE DISPLAY NAME AND E-MAIL ADDRESS FOR A SINGLE USER
			search_param = ""# the AD account goes here
			result_attrs = ["sAMAccountName", "displayName", "mail"] # Whatever you want to bring back in your result set goes here
			 
			# Build filter
			search_filter = Net::LDAP::Filter.eq("sAMAccountName", search_param)
			 
			# Execute search
			ldap.search(:filter => search_filter, :attributes => result_attrs) { |item| 
			  puts "#{item.sAMAccountName.first}: #{item.displayName.first} (#{item.mail.first})" 
			}
			 
			get_ldap_response(ldap)
		end


		def userinfo

			#{"myhash":{"dn":["CN=jenny ryoo,OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","person","organizationalPerson","user"],"cn":["jenny ryoo"],"sn":["ryoo"],"givenname":["jenny"],"distinguishedname":["CN=jenny ryoo,OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045242.0Z"],"whenchanged":["20141031045243.0Z"],"displayname":["jenny ryoo"],"usncreated":["49195"],"usnchanged":["49202"],"name":["jenny ryoo"],"objectguid":["��~\u003C�\u0015�N��I�C���"],"useraccountcontrol":["66048"],"badpwdcount":["0"],"codepage":["0"],"countrycode":["0"],"badpasswordtime":["0"],"lastlogoff":["0"],"lastlogon":["0"],"pwdlastset":["130592047628411430"],"primarygroupid":["513"],"objectsid":["\u0001\u0005\u0000\u0000\u0000\u0000\u0000\u0005\u0015\u0000\u0000\u0000N�\u0007�\u0000�W�����J\u0006\u0000\u0000"],"accountexpires":["9223372036854775807"],"logoncount":["0"],"samaccountname":["jenny"],"samaccounttype":["805306368"],"userprincipalname":["jenny@xen.com"],"objectcategory":["CN=Person,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045242.0Z","16010101000000.0Z"]}}

		end


		def addComputerAccount(ou_name,computer_name)
			#[{"dn":"CN=vmuser1,OU=mhrinc,DC=xen,DC=com","cn":"vmuser1","ou":null,"name":"vmuser1","displayname":null,"dnshostname":null,"distinguishedname":"CN=vmuser1,OU=mhrinc,DC=xen,DC=com","serviceprincipalname":null,"objectcategory":"CN=Computer,CN=Schema,CN=Configuration,DC=xen,DC=com","account":"VMUSER1$","last_logon":"0","last_logoff":"0"},{"dn":"CN=pcvmuser10,OU=mhrinc,DC=xen,DC=com","cn":"pcvmuser10","ou":null,"name":"pcvmuser10","displayname":null,"dnshostname":null,"distinguishedname":"CN=pcvmuser10,OU=mhrinc,DC=xen,DC=com","serviceprincipalname":null,"objectcategory":"CN=Computer,CN=Schema,CN=Configuration,DC=xen,DC=com","account":"PCVMUSER10$","last_logon":"0","last_logoff":"0"}]
			
			dn = "CN=#{computer_name}, OU=#{ou_name},dc=xen,dc=com"
			attrs = { 	:cn => computer_name, 
						:objectclass => ['computer'], 
						:name => computer_name,
						:samaccountname => computer_name
					}

			entry = @ldap.add( :dn => dn, :attributes => attrs)

			#get_ldap_response(@ldap)

			Xen::Logger.debug("error = #{@ldap.get_operation_result.message}")

			#return LDAP::Formatter.computerToHash(entry)
		end

		def addComputerAccountByDn(dn,computer_name)
			#[{"dn":"CN=vmuser1,OU=mhrinc,DC=xen,DC=com","cn":"vmuser1","ou":null,"name":"vmuser1","displayname":null,"dnshostname":null,"distinguishedname":"CN=vmuser1,OU=mhrinc,DC=xen,DC=com","serviceprincipalname":null,"objectcategory":"CN=Computer,CN=Schema,CN=Configuration,DC=xen,DC=com","account":"VMUSER1$","last_logon":"0","last_logoff":"0"},{"dn":"CN=pcvmuser10,OU=mhrinc,DC=xen,DC=com","cn":"pcvmuser10","ou":null,"name":"pcvmuser10","displayname":null,"dnshostname":null,"distinguishedname":"CN=pcvmuser10,OU=mhrinc,DC=xen,DC=com","serviceprincipalname":null,"objectcategory":"CN=Computer,CN=Schema,CN=Configuration,DC=xen,DC=com","account":"PCVMUSER10$","last_logon":"0","last_logoff":"0"}]
			
			#dn = "CN=#{computer_name}, OU=#{ou_name},dc=xen,dc=com"
			attrs = { 	:cn => computer_name, 
						:objectclass => ['computer'], 
						:name => computer_name,
						:samaccountname => "#{computer_name.upcase}$"
					}

			entry = @ldap.add( :dn => dn, :attributes => attrs)

			#get_ldap_response(@ldap)

			Xen::Logger.debug("msg = #{@ldap.get_operation_result.message}")

			#return LDAP::Formatter.computerToHash(entry)
			return @ldap.get_operation_result.code
		end

		def removeComputerAccount(ou_name,computer_name)
			dn = "CN=#{computer_name}, OU=#{ou_name},dc=xen,dc=com"

			@ldap.delete :dn => dn

			Xen::Logger.debug("removeComputerAccount.msg = #{@ldap.get_operation_result.message}")

		end

	end

end