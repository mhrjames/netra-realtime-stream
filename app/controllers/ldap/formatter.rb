module LDAP

	class Formatter

		class << self

			def entryToHash(entry)
				hash = {}

				hash[:name] = entry.name
				hash[:account] = entry.sAMAccountName
				hash[:last_logon] = entry.lastlogon
				hash[:last_logoff] = entry.lastlogoff


				return hash
			end

			def userToHash(entry)
				#result_attrs = ["dn", "ou", "cn", "distinguishedname", "name", "objectcategory", "samaccountname", "userprincipalname" ,"lastlogon", "lastlogoff"]
				hash = {}

				hash[:dn] = entry[:dn].first
				hash[:cn] = entry[:cn].first
				hash[:ou] = entry[:ou].first
				hash[:name] = entry[:name].first
				hash[:distinguishedname] = entry[:distinguishedname].first
				hash[:userprincipalname] = entry[:userprincipalname].first
				hash[:objectcategory] = entry[:objectcategory].first
				hash[:account] = entry[:sAMAccountName].first
				hash[:last_logon] = entry[:lastlogon].first
				hash[:last_logoff] = entry[:lastlogoff].first

				return hash
			end

			def computerToHash(entry)
				#result_attrs = ["dn", "ou", "cn", "distinguishedname", "name", "objectcategory", 
				#{}"samaccountname", "displayname", "dnshostname" ,"lastlogon", "lastlogoff", 
				#{}"serviceprincipalname"]

				hash = {}

				hash[:dn] = entry[:dn].first
				hash[:cn] = entry[:cn].first
				hash[:ou] = entry[:ou].first
				hash[:name] = entry[:name].first
				hash[:displayname] = entry[:displayname].first
				hash[:dnshostname] = entry[:dnshostname].first
				hash[:distinguishedname] = entry[:distinguishedname].first
				hash[:serviceprincipalname] = entry[:serviceprincipalname].first
				hash[:objectcategory] = entry[:objectcategory].first
				hash[:account] = entry[:sAMAccountName].first
				hash[:last_logon] = entry[:lastlogon].first
				hash[:last_logoff] = entry[:lastlogoff].first

				return hash
			end

			def ouToHash(entry)
				#{"myhash":{"dn":["OU=mhrinc,DC=xen,DC=com"],"objectclass":["top","organizationalUnit"],"ou":["mhrinc"],"distinguishedname":["OU=mhrinc,DC=xen,DC=com"],"instancetype":["4"],"whencreated":["20141031045157.0Z"],"whenchanged":["20141031045157.0Z"],"usncreated":["49191"],"usnchanged":["49192"],"name":["mhrinc"],"objectguid":["�q�����K��%\u000E\u0010��{"],"objectcategory":["CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=xen,DC=com"],"dscorepropagationdata":["20141031045157.0Z","20141031045157.0Z","16010101000000.0Z"]}}
			
				hash = {}
				hash[:dn] = entry[:dn].first
				hash[:name] = entry[:name].first
				hash[:ou] = entry[:ou].first
				hash[:distinguishedname] = entry[:distinguishedname].first

				return hash
			end

		end

	end

end