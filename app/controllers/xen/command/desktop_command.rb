module Xen
	module Command
		module DesktopCommand

			def loadCitrixModule()
				cmd = "Asnp Citrix.*"
				return exec(cmd)
			end

			def listVDI(admin_ip)
				# http://himikebrown.com/citrix/export-list-of-xendesktop-vdis-to-csv-with-powershell/
				
				cmd = loadCitrixModules()
				cmd += "Get-BrokerMachine -AdminAddress #{admin_ip}\n"
				#cmd = "Get-BrokerDesktop -AdminAddress servername -MaxRecordCount 1000 -DesktopKind Private | sort desktopgroupname | export-csv outputfile.csv"

				response = execScript(cmd)

				return Xen::Parser::DesktopParser.parseListVDI(response)
			end

			def listMachineCatalog(admin_ip)
				# http://himikebrown.com/citrix/export-list-of-xendesktop-vdis-to-csv-with-powershell/
				
				cmd = loadCitrixModules()
				cmd += "Get-BrokerCatalog -AdminAddress #{admin_ip}\n"
				#cmd = "Get-BrokerDesktop -AdminAddress servername -MaxRecordCount 1000 -DesktopKind Private | sort desktopgroupname | export-csv outputfile.csv"

				response = execScript(cmd)

				return Xen::Parser::DesktopParser.parseMachineCatalogList(response)
			end

			def listDeliveryGroup(admin_ip)
				# http://himikebrown.com/citrix/export-list-of-xendesktop-vdis-to-csv-with-powershell/
				
				cmd = loadCitrixModules()
				cmd += "Get-BrokerDesktopGroup -AdminAddress #{admin_ip}\n"
				#cmd = "Get-BrokerDesktop -AdminAddress servername -MaxRecordCount 1000 -DesktopKind Private | sort desktopgroupname | export-csv outputfile.csv"

				response = execScript(cmd)

				return Xen::Parser::DesktopParser.parseDeliveryGroupList(response)
			end

			def listHypervisorConnection(admin_ip)
				cmd = loadCitrixModules()
				cmd += "Get-BrokerHypervisorConnection -AdminAddress #{admin_ip}\n"

				response = execScript(cmd)

				return Xen::Parser::DesktopParser.parseHypervisorConnectionList(response)
			end

			
			#def addMachineToMachineCatalog(username,machine_name,catalog_uid,group_uid,hypervisor_uid)
			def addMachineToMachineCatalog(params)
				cmd = loadCitrixModules()
				
				#catalog_uid = "6"
				#hypervisor_uid = "3"
				#group_uid = "5"
				#machine_uid = "8df6bbb8-fe66-7535-b4dd-1a729444afa4"				
				#new_machine_name = "xen\pcvmuser10"
				#username = "xen\\vmuser10"

				cmd += "New-BrokerMachine -MachineName \"#{params[:machine_name]}\" -catalogUid #{params[:catalog_uid]} -HypervisorConnectionUid #{params[:hypervisor_uid]} -HostedMachineId #{params[:machine_uuid]}\n"
				cmd += "Add-BrokerMachine -MachineName \"#{params[:domain]}\\#{params[:machine_name]}\" -DesktopGroup #{params[:group_uid]}\n"
				cmd += "Add-BrokerUser -Name \"#{params[:username]}\" -Machine \"#{params[:domain]}\\#{params[:machine_name]}\" "

				response = execScript(cmd)

				return response
			end

			#def removeMachineToMachineCatalog(admin_ip,machine_name,delivery_name)
			def removeMachineToMachineCatalog(params)
				cmd = loadCitrixModules()
				
				catalog_uid = "6"
				hypervisor_uid = "3"
				group_uid = "5"
				machine_uid = "4"
				machine_uuid = "8df6bbb8-fe66-7535-b4dd-1a729444afa4"				
				#new_machine_name = "xen\pcvmuser10"

				cmd += "Get-BrokerMachine -Uid #{params[:machine_uid]} | Remove-BrokerMachine -DesktopGroup #{params[:group_uid]} \n"
				cmd += "Remove-BrokerMachine -MachineName #{params[:machine_name]}\n"
				
				response = execScript(cmd)

				return response
			end

			def createMachineCatalog(name,allocation_type,catalog_kind,description)
				
				cmd = loadCitrixModules()
				cmd += "New-BrokerCatalog"
				cmd += appendParameter("AllocationType",allocation_type)
				cmd += appendParameter("CatalogKind", catalog_kind)
				cmd += appendStringParameter("Description", description)
				cmd += appendStringParameter("Name",name)
				cmd += "\n"

				response = execScript(cmd)

				Xen::Logger.debug("createMachineCatalog : response = #{response}")
				#Xen::Parser::DesktopParser.parseDeliveryGroupList(response)

				return response

			end

			def createProvScheme(name)

				cmd = loadCitrixModules()
				cmd += "New-ProvScheme"
				cmd += appendParameter("ProvisioningSchemeName",allocation_type)
				cmd += appendParameter("MasterImageVM", catalog_kind)
				cmd += "\n"

				response = execScript(cmd)

				Xen::Logger.debug("createMachineCatalog : response = #{response}")
				#Xen::Parser::DesktopParser.parseDeliveryGroupList(response)

				return response

			end

			
			def assignVM(params)
				#username,machine_name,catalog_uid,group_uid,hypervisor_uid)
				
				ldap = LDAP::ActiveDirectory.new()
      			xen = Xen::XenServer.create(params)


				success_count = 0
				params[:items].each do |item|
					
					ou = ::Common::CommonFunction.string_between_markers(item[:dn],"OU=",",")
					cn = ::Common::CommonFunction.string_between_markers(item[:dn],"CN=",",")
					domain = ::Common::CommonFunction.string_between_markers(item[:dn],"DC=",",")

					dn = item[:dn].sub! cn, item[:computer]
					
					if ldap.addComputerAccountByDn(dn, item[:computer]) !=0 
						Xen::Logger.warning("DesktopCommand.assignVM : Fail to create computer account, #{dn} , computer = #{item[:computer]}")
					end

					Xen::Logger.debug("DesktopCommand.assignVM : dn = #{dn} , computer = #{item[:computer]}")

					json = {:username => item[:account], 
							:domain => domain, 
							:machine_name => item[:computer], 
							:catalog_uid => params[:catalog_uid], 
							:group_uid => params[:group_uid], 
							:hypervisor_uid => params[:hypervisor_uid],
							:machine_uuid => item[:machine] }

					result = addMachineToMachineCatalog(json)


					#Xen::Logger.debug("DesktopCommand.assignVM : addMachineToMachineCatalog = #{result}")

					if not result.index("HypHypervisorConnectionUid").nil?
						
						found_userinfo = Userinfo.findByPrincipalName(json[:username])
						if not found_userinfo.nil?

							xen.injectConfigDataByUUID(json[:machine_uuid],found_userinfo)
							success_count += 1
						end

					else 
						Xen::Logger.warning("DesktopCommand.assignVM : Fail to assign VM, msg = #{result}")
					end
				end

				return Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK, {:total => params[:items].size, :success => success_count} )
			end

			
			#
			#unassign VM not multiple VM!!!
			#
			def unassignVM(params)

				json = {:machine_uid => params[:machine_uid], 
						:group_uid => params[:group_uid], 
						:machine_name => params[:machine_name] }
				
				result = removeMachineToMachineCatalog(json)

				Xen::Logger.debug("DesktopCommand.unassignVM : removeMachineToMachineCatalog = #{result}")

				if not result.nil?
					Xen::Logger.warning("DesktopCommand.unassignVM : Fail to unassign VM, msg = #{result}")
					return Xen::XenResponse.buildRestResponse(Const::ERR_DESKTOP_UNASSIGN_VM,Const::ERRMSG_DESKTOP_UNASSIGN_VM, { :msg => result} )
				end

				return Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK, nil )
			end


		end
	end
end