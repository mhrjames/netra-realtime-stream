module Xen
	module Command
		module HostCommand

			def listHost()

				hosts = []
				Host.all.each do |host|
					hosts << toHostHash(host)
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,hosts)

				return response
	      	end      

	      	def addHost(params)
				if not isValidParameter(params)
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

      			found_host = Host.find_by_ip(params[:ip])

      			if not found_host.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_HOST_EXIST,Const::ERRMSG_HOST_EXIST,nil)
					return response
				end

				new_host = Host.new_host(params)
   				
   				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,toHostHash(new_host))

				return response
			end

			def deleteHost(params)
				if not params.has_key?("ip")
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_host = Host.find_by_ip(params[:ip])
      			if found_host.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_HOST_NOT_FOUND,Const::ERRMSG_HOST_NOT_FOUND,nil)
					return response
				end

				Host.delete_record(found_host)
				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,nil)

				return response
			end

			def updateHost(params)
				if not params.has_key?("ip")
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_host = Host.find_by_ip(params[:ip])
      			if found_host.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_HOST_NOT_FOUND,Const::ERRMSG_HOST_NOT_FOUND,nil)
					return response
				end

      			Host.update_host(found_host, params)
				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,nil)

				return response

			end

		end
	end
end