module Xen
	module Command
		module PoolCommand

			def inspect()
				Xen::Logger.debug("Xen::Command:PoolCommand.inspect")

				if getRegisteredHostCount() == 0
					return Xen::XenResponse.buildRestResponse(Const::ERR_NO_REGISTERED_HOST,Const::ERRMSG_NO_REGISTERED_HOST,nil)
				end

				masters = scanPoolMasters()
				rebuild(masters)

				Xen::Logger.debug("PoolCommand.inspect : masters = #{masters}")

				if masters.size > 0
					response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,nil)
				else
					response = Xen::XenResponse.buildRestResponse(Const::ERR_NO_MASTER_FOUND,Const::ERRMSG_NO_MASTER_FOUND,nil)
				end

				return response
			end

			def update(params)
				Xen::Logger.debug("Xen::Command:PoolCommand.inspect")
				
				found_pool = Pool.find_by_name(params[:name])
				if found_pool.nil?
					return Xen::XenResponse.buildRestResponse(Const::ERR_NO_MASTER_FOUND,Const::ERRMSG_NO_MASTER_FOUND,nil)
				end

				Pool.update_pool(found_pool,params)

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,nil)
				return response
			end

			def listPool()
				Xen::Logger.debug("Xen::Command:PoolCommand.listPool")

				pools = []
				Pool.all.each do |pool|
					pools << toPoolHash(pool)
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,pools)
				return response				
			end

		end
	end
end
