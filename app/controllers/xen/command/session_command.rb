module Xen
	module Command
		module SessionCommand

		    def getCurrentPool()
		        pool_ref = request("pool.get_all", nil)
		        pool = @session.pool.get_record(pool_ref)

		        return pool
		    end

		    def registerEvents()
		      self.event.register(["*"])
		    end

		    def unregisterEvents()
		      self.event.unregister(["*"])
		    end

		    def createTask(name, description)
		        self.task.create name, description
		    end

		    def getTask(task_ref)
		      self.task.get_record task_ref
		    end

		    def destroyTask(task_ref)
		      self.task.destroy task_ref
		    end


		    def asyncVMCopy(template_ref,template_name,pbd_ref)
		    	task_ref = self.asyncRequest("Async.VM.copy", template_ref, template_name, pbd_ref)
		    	return task_ref
		    end

		    def asyncVMProvision(server_ref)
		    	task_ref = self.asyncRequest("Async.VM.provision", server_ref)
		    	return task_ref
		    end

		    def asyncVMDestroy(server_ref)
		    	task_ref = self.asyncRequest("Async.VM.destroy", server_ref)
		    	return task_ref
		    end
		end
	end
end