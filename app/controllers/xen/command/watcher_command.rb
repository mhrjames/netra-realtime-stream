module Xen
	module Command
		module WatcherCommand

			def hostPerfData(host_ref)

			      host = @xen.getHostByRef(host_ref)
			      
			      host_pref = Xen::Models::HostMetric.new()
			      host_pref.last_updated = host.metrics.last_updated
			      host_pref.live = host.metrics.live
			      host_pref.memory_free = host.metrics.memory_free
			      host_pref.memory_total = host.metrics.memory_total

			      host.pifs.each do |pif|
			      	
			      	pif_metric = @xen.connection.pifs_metrics.get(pif.metrics)

			      	#print "connection = #{pif_metric.uuid}\n"

			      	network_perf = Xen::Models::NetworkMetric.new()
			      	network_perf.device_id = pif_metric.device_id
			      	network_perf.device_name = pif_metric.device_name
			      	network_perf.io_read_kbs = pif_metric.io_read_kbs
			      	network_perf.io_write_kbs = pif_metric.io_write_kbs
			      	network_perf.speed = pif_metric.speed

			      	host_pref.network << network_perf

			      end

			      return host_pref
			end


			def getLatestHostRRD(host_ref)
				#wget http://<server>/rrd_updates?session_id=OpaqueRef:<SESSION HANDLE>&start=10258122541&host=true
				
				host_ip = getHostIPByRef(host_ref)
				url = getHostRRDUpdateUrl(host_ip,1,"AVERAGE",1)

				response = Excon.get(url)

				print "response : #{response.body}\n"

				host_metric = Xen::RRDParser.parseHostXml(response.body)
				host_metric.ip = host_ip
				
				#metrics = Xen::Models::HostMetric.new

				#host_metric.data.each do |a_metric|
				#	if a_metric.type.casecmp("host") == 0
				#		metrics.addLatestData(a_metric.name,a_metric.value)
				#	end
				#end

				#return Common::CommonResponse.buildResponse(0,'ok',host_metric.toHash())
				return Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,host_metric.toHash())
			end

			def getLatestVMRRD(host_ref, vm_ref)
				#wget http://<server>/rrd_updates?session_id=OpaqueRef:<SESSION HANDLE>&start=10258122541

				host_ip = getHostIPByRef(host_ref)
				url = getVMRRDUpdateUrl(host_ip,1,"AVERAGE",1)

				response = Excon.get(url)
				
				print "response : #{response.body}\n"

				host_metric = Xen::RRDParser.parseVMXml(response.body,vm_ref)
				host_metric.ip = host_ip
				host_metric.reference = vm_ref

				#return Common::CommonResponse.buildResponse(0,'ok',host_metric.toHash())
				return Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,host_metric.toHash())
			end

		end
	end
end