module Xen
	module Command
		module XenCommand

			def listVM()
				Xen::Logger.debug("XenCommand.listVM : class = #{@connection.class.name}")

				servers = []
				@connection.servers.all.each do |server|
					if server.is_a_snapshot or server.is_a_template
						continue
					end
					servers << toServerHash(server)
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,servers)
				
				return response
			end

			def startVM(vm_ref)
				Xen::Logger.debug("XenCommand.startVM : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				if isVMRunning(found_server)
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_IS_RUNNING,Const::ERRMSG_VM_IS_RUNNING,nil)
					return response
				end

				begin
					found_server.start()
					return Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
				rescue Exception
					return Xen::XenResponse.buildResponse(Const::ERR_XENSERVER_ERROR,Const::ERRMSG_XENSERVER_ERROR,{:data => e.message})
				end

			end

			def stopVM(vm_ref)
				Xen::Logger.debug("XenCommand.stopVM : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				if not isVMRunning(found_server)
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_IS_NOT_RUNNING,Const::ERRMSG_VM_IS_NOT_RUNNING,nil)
					return response
				end

				begin
					found_server.stop()
					return Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
				rescue Exception
					return Xen::XenResponse.buildResponse(Const::ERR_XENSERVER_ERROR,Const::ERRMSG_XENSERVER_ERROR,{:data => e.message})
				end
				
			end

			def brutalShutdownVM(vm_ref)
				Xen::Logger.debug("XenCommand.brutalShutdownVM : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				if not isVMRunning(found_server)
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_IS_NOT_RUNNING,Const::ERRMSG_VM_IS_NOT_RUNNING,nil)
					return response
				end

				begin
					found_server.hard_shutdown()
					response = Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
					
					#Xen::Logger.debug("XenCommand.brutalShutdownVM : response = #{response}")

					return response
				rescue Exception
					return Xen::XenResponse.buildResponse(Const::ERR_XENSERVER_ERROR,Const::ERRMSG_XENSERVER_ERROR,{:data => e.message})
				end

			end

			def cleanShutdown(vm_ref)
				Xen::Logger.debug("XenCommand.cleanShutdown : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				if not isVMRunning(found_server)
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_IS_NOT_RUNNING,Const::ERRMSG_VM_IS_NOT_RUNNING,nil)
					return response
				end

				found_server.clean_shutdown()
				response = Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
			end

			def destroyVM(vm_ref)
				Xen::Logger.debug("XenCommand.destroyVM : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				if isVMRunning(found_server)
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_IS_RUNNING,Const::ERRMSG_VM_IS_RUNNING,nil)
					return response
				end

				begin
					found_server.destroy()
					return Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
				rescue Exception
					return Xen::XenResponse.buildResponse(Const::ERR_XENSERVER_ERROR,Const::ERRMSG_XENSERVER_ERROR,{:data => e.message})
				end

			end

			def hardRebootVM(vm_ref)
				Xen::Logger.debug("XenCommand.hardRebootVM : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				if not isVMRunning(found_server)
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_IS_NOT_RUNNING,Const::ERRMSG_VM_IS_NOT_RUNNING,nil)
					return response
				end

				begin
					found_server.hard_reboot()
					return Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
				rescue Exception
					return Xen::XenResponse.buildResponse(Const::ERR_XENSERVER_ERROR,Const::ERRMSG_XENSERVER_ERROR,{:data => e.message})
				end
			end



			def getVM(vm_ref)
				Xen::Logger.debug("XenCommand.getVM : vm_ref = #{vm_ref}")

				if vm_ref.size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				found_server = @connection.servers.get(vm_ref)
				if found_server.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_VM_NOT_FOUND,Const::ERRMSG_VM_NOT_FOUND,nil)
					return response
				end

				Xen::Logger.debug("XenCommand.getVM : xenserver = #{found_server.to_json}")

				response = Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,toServerHash(found_server))
			end


			def listTemplate
				templates = []
				@connection.servers.custom_templates.each do |template|
					if template.is_a_template
						templates << toTemplateHash(template)
					end
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,templates)
				
				return response
			end

			def listBaseTemplate
				host_names = getHostNamesInHash()

				templates = []				
				@connection.servers.custom_templates.each do |template|
					if template.is_a_template
						tokens = template.name.split('_')

						if tokens.size == 0
							templates << toTemplateHash(template)
							continue
						end

						last_token = tokens[tokens.size-1]

						#Xen::Logger.debug("listBaseTemplate : last_toen = #{last_token} , tokens = #{tokens}")

						if not host_names.has_key?(last_token)
							templates << toTemplateHash(template)
						end
					end
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,templates)
				
				return response
			end


			def listXenHost
				hosts = []
				
				vms = getVMbyHost()

				@connection.hosts.all.each do |host|
					hosts << toHostHash(host, vms)
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,hosts)

				return response
			end


			def listPIF()
				Xen::Logger.debug("XenCommand.listPIF : class = #{@connection.class.name}")

				pifs = []
				@connection.pifs.all.each do |pif|
					pifs << toPIFHash(pif)
				end

				return pifs
			end


			def listPool()
				Xen::Logger.debug("XenCommand.listPool")

				pools = []
				@connection.pools.all.each do |pool|
					pools << toPoolHash(pool)
				end

				return pools
			end

			def listPBD()
				Xen::Logger.debug("XenCommand.listPBD")

				pbds = []
				@connection.pbds.all.each do |pbd|
					pbds << toPBDHash(pbd)
				end

				return pbds
			end
			
			def listPBDforProvision()
				Xen::Logger.debug("XenCommand.listPBDforProvision")

				pbds = []
				@connection.pbds.all.each do |pbd|
					sr = pbd.sr
					if ['lvm','ext','nfs','lvmoiscsi'].include?(sr.type)
						pbds << toPBDHash(pbd)
					end
				end

				return pbds
			end

			def listStorageForProvision()
				Xen::Logger.debug("XenCommand.listStorageForProvision")

				storages = []
				@connection.storage_repositories.all.each do |storage|
					if ['lvm','ext','nfs','lvmoiscsi'].include?(storage.type)
						storages << toSRHash(storage)
					end
				end

				return storages
			end


			def listStorage()
				Xen::Logger.debug("XenCommand.listStorage : class = #{@connection.class.name}")

				storages = []
				@connection.storage_repositories.all.each do |storage|
					storages << toSRHash(storage)
				end

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK,storages)
				
				return response
			end

			def provisionVM(params)
				Xen::Logger.debug("XenCommand.provisionVM : params = #{params}")

				if (params[:vm_name].size == 0) or (params[:template_ref].size == 0) or (params[:host_ref].size == 0)
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				template = @connection.servers.get(params[:template_ref])
				if template.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_TEMPLATE_NOT_FOUND,Const::ERRMSG_TEMPLATE_NOT_FOUND,nil)
					return response
				end

				host = @connection.hosts.get(params[:host_ref])
				if host.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_HOST_NOT_FOUND,Const::ERRMSG_HOST_NOT_FOUND,nil)
					return response
				end

				host_template = getTemplateInXenHost(host, template.name)
				if host_template.nil?
					Xen::Logger.debug("XenCommand.provisionVM : template does not exist in #{host.name}")
					host_template = copyTemplateToHost(template, host)
					Xen::Logger.debug("XenCommand.provisionVM : cached template is created")
				end

				new_vm = createVMFromTemplate(params[:vm_name], host_template, host, params[:description])
				#new_vm.start

				Xen::Logger.debug("XenCommand.provisionVM : VM named '#{params[:vm_name]}' created by template '#{host_template.name}' on host '#{host.name}'")

				response = Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,toServerHash(new_vm))
			end

			def provisionAsyncVMs(params)
				Xen::Logger.debug("XenCommand.provisionAsyncVMs : params = #{params}")

				if params[:data].size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				session = Xen::XenServer.createSession(@params)

				vms = []
				params[:data].each do |param|
				
					if (param[:vm_name].size == 0) or (param[:template_ref].size == 0) or (param[:host_ref].size == 0)
						response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
						return response
					end

					template = @connection.servers.get(param[:template_ref])
					if template.nil?
						response = Xen::XenResponse.buildResponse(Const::ERR_TEMPLATE_NOT_FOUND,Const::ERRMSG_TEMPLATE_NOT_FOUND,nil)
						return response
					end

					host = @connection.hosts.get(param[:host_ref])
					if host.nil?
						response = Xen::XenResponse.buildResponse(Const::ERR_HOST_NOT_FOUND,Const::ERRMSG_HOST_NOT_FOUND,nil)
						return response
					end

					host_template = getTemplateInXenHost(host, template.name)
					if host_template.nil?
						Xen::Logger.debug("XenCommand.provisionVM : template does not exist in #{host.name}")
						host_template = copyTemplateToHost(template, host)
						Xen::Logger.debug("XenCommand.provisionVM : cached template is created")
					end

					#task_ref = createVMFromTemplate(param[:vm_name], template, host)
					task_ref = createAsyncVMFromTemplate(session,param[:vm_name], template, host, param[:description])

					vms << { :pool => params[:pool], :vm_name => param[:vm_name], :host_ref => param[:host_ref], :task_ref => task_ref}
				end

				session.close()

				response = Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,vms)

			end

			def createProvisionTask(params)
				Xen::Logger.debug("XenCommand.createProvisionTask : params = #{params}")
				
				response = Task.new_task("new_provision_task",params)

				return Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
			end

			def provisionTask(params)
				Xen::Logger.debug("XenCommand.provisionTask : params = #{params}")
				
				found_task = Task.getTask(params[:task_id])
				if found_task.nil?
					return Xen::XenResponse.buildResponse(Const::ERR_NO_TASK_ITEM,Const::ERRMSG_NO_TASK_ITEM,params)
				end


				provision_params = YAML.load(found_task.parameters)

				Xen::Logger.debug("XenCommand.provisionTask : found = #{provision_params}")

				host = @connection.hosts.get(provision_params['host_ref'])

				for index in 1..provision_params['vm_count']
					vm_name = "#{provision_params['vm_name']}#{index+provision_params['vm_start_index']-1}"

					Xen::Logger.debug("XenCommand.provisionTask : index = #{index} , vm_name = #{vm_name}")
					
					subtask_ref = asyncCopyVM(vm_name,provision_params['template_ref'],provision_params['storage_ref'])
					Xen::Logger.debug("XenCommand.provisionTask : copy subtask = #{subtask_ref}")

					a_subtask = Subtask.new_subtask(subtask_ref, found_task.id, "Async VM Copy", "#{vm_name},#{provision_params['template_ref']},#{provision_params['storage_ref']}")
					#copyAsyncVMFromTemplate(session, vm_name, template, host, sr, provision_params['vm_description'])
				end

				Task.set_running_status(found_task)
				
				return Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,nil)
			end




			def destroyAsyncVMs(params)
				Xen::Logger.debug("XenCommand.destroyAsyncVMs : params = #{params}")

				if params[:data].size == 0
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				session = Xen::XenServer.createSession(@params)

				vms = []
				params[:data].each do |param|
				
					if (param[:vm_ref].size == 0)
						response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
						return response
					end

					task_ref = destroyAsyncVM(session, param[:vm_ref])

					vms << { :vm_ref => param[:vm_ref], :task_ref => task_ref}
				end

				session.close()

				response = Xen::XenResponse.buildResponse(Const::OK,Const::MSG_OK,vms)

			end



			def makeCachedTemplates(params)
				Xen::Logger.debug("XenCommand.makeCachedTemplates : params = #{params}")

				if (params[:pool].size == 0) or (params[:template_ref].size == 0)
					response = Xen::XenResponse.buildResponse(Const::ERR_NO_PARAMETER,Const::ERRMSG_NO_PARAMETER,nil)
					return response
				end

				template = @connection.servers.get(params[:template_ref])
				if template.nil?
					response = Xen::XenResponse.buildResponse(Const::ERR_TEMPLATE_NOT_FOUND,Const::ERRMSG_TEMPLATE_NOT_FOUND,nil)
					return response
				end

				session = Xen::XenServer.createSession(@params)

				cached_hosts = []

				@connection.hosts.all.each do |host|
					
					Xen::Logger.debug("XenCommand.makeCachedTemplates : host = #{host.name} , template = #{template.name}")

					host_template = getTemplateInXenHost(host, template.name)
					if host_template.nil?
						
						Xen::Logger.debug("XenCommand.makeCachedTemplates : template does not exist in #{host.name}")
						task_ref = copyAsyncTemplateToHost(session, template, host)
						
						cached_hosts << {:host => host.name, :host_ref => host.reference, :template => template.name, :template_ref => template.reference, :task_ref => task_ref}

						#Xen::Logger.debug("XenCommand.makeCachedTemplates : cached template is created")
					end

				end

				session.close()

				response = Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK, cached_hosts)

				return response

			end

			def getXenhostByIp(params)				
				found_host = getHostByIp(params[:ip])
				json = toHostHash(found_host,nil)

				Xen::Logger.debug("XenCommand.getXenhost : host = #{found_host.to_json}")

				return Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK, json)
			end


			def getTaskData(params)
				
				subtasks = Subtask.where(task_id: params[:task_id])
				
				#Xen::Logger.debug("XenCommand.getTaskData : subtasks = #{subtasks}")

				datum = []
				subtasks.each do |subtask|
					task = getTaskRecord(subtask.task_ref)
					
					if task.nil?
						Xen::Logger.warning("XenCommand.getTaskData : Fail to get task status, task_ref = #{subtask.task_ref}")
					end

					if not task.nil?
						task_hash = toTaskHash(task)
						Subtask.update_status(subtask,task_hash)

						datum << task_hash
					end
				end

				Xen::Logger.debug("XenCommand.getTaskData : result = #{datum}")

				return Xen::XenResponse.buildRestResponse(Const::OK,Const::MSG_OK, datum)
			end



		end

	end
end