module Xen
	class Common
		
		class << self
			def getRRDPath()
		    	Rails.root.join('rrd')
		    end

		    def getRRDXMLPath()
		    	Rails.root.join('rrd','xml')
		    end

		    def doesFileExists?(file_name)
		    	File.file?(file_name)
		    end
		end

	end
end