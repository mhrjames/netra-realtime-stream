module Xen

	module Const
		OK  = 0
		MSG_OK = "ok"

		ERR_VM_NOT_FOUND = -1
		ERRMSG_VM_NOT_FOUND = "VM is not found, please check your VM ref value"

		ERR_VM_IS_RUNNING = -2
		ERRMSG_VM_IS_RUNNING = "The VM is running, so the request can not be done"

		ERR_NO_PARAMETER = -3
		ERRMSG_NO_PARAMETER = "Required parameter is not given, please check your parameter"

		ERR_VM_IS_NOT_RUNNING = -4
		ERRMSG_VM_IS_NOT_RUNNING = "The VM is not running, it can not be stopped"

		ERR_TEMPLATE_NOT_FOUND = -5
		ERRMSG_TEMPLATE_NOT_FOUND = "Template is not found, please check your template ref value"

		ERR_HOST_NOT_FOUND = -6
		ERRMSG_HOST_NOT_FOUND = "XEN host is not found, please check your Xen host ref value"

		ERR_HOST_EXIST = -7
		ERRMSG_HOST_EXIST = "Same host exists, please check host ip address"

		ERR_NO_MASTER_FOUND = -8
		ERRMSG_NO_MASTER_FOUND = "No master server in pool found, please check Xen Host status"

		ERR_NO_REGISTERED_HOST = -9
		ERRMSG_NO_REGISTERED_HOST = "No registered xenhost, please add xen host first before issuing this API"

		ERR_NO_HOST_RRDDATA = -10
		ERRMSG_NO_HOST_RRDDATA = "No Host RRD Data!!!"

		ERR_NO_TASK_ITEM = -11
		ERRMSG_NO_TASK_ITEM = "No Task Item Data!!!"

		ERR_DESKTOP_UNASSIGN_VM = -80
		ERRMSG_DESKTOP_UNASSIGN_VM = "Fail to unassign VM from Xen Desktop"

		ERR_XENSERVER_ERROR = -99
		ERRMSG_XENSERVER_ERROR = "XENServer raised an exception!!!"

	end

end