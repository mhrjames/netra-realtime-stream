module Xen

	module Desktop

		class XenDesktopClient

			include Xen::Command::DesktopCommand

			attr_accessor :ssh
			attr_accessor :controller_ip
			
			
			def initialize()
				@controller_ip = NETRA_CONFIG['xd_ip']
				@userid = NETRA_CONFIG['xd_username']
				@password = NETRA_CONFIG['xd_password']
				@port = NETRA_CONFIG['xd_port']

				connect(@controller_ip,@port.to_i,@userid,@password)
			end

			def connect(ip,port, userid,password)
				return @ssh if not @ssh.nil? 

				@ssh = Net::SSH.start( ip, userid, :password => password, :port => port )
			end


			def disconnect()
				@ssh.close()
			end


			def exec(cmd)
					
				Xen::Logger.debug("Exec.Command = #{cmd}")

				command_line = "powershell.exe #{cmd}"
				res = @ssh.exec!(command_line)

				return res
			end


			def execScript(script)

				return exec(script)

			end

			def loadCitrixModules()
				cmd = "Asnp Citrix.*\n"
			end

			def appendParameter(key, value)
				return " -#{key} #{value}"
			end

			def appendStringParameter(key, value)
				return " -#{key} \"#{value}\""
			end
		end

	end

end