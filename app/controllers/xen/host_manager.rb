module Xen

	class HostManager
		
		include Xen::Command::HostCommand

		def toHostHash(host)
			hash = {}
			hash[:id] = host.id
			hash[:ip] = host.ip
			hash[:name] = host.name
			hash[:cluster_name] = host.cluster_name
			hash[:description] = host.description
			hash[:userid] = host.userid
			hash[:updated_at] = host.updated_at
			hash[:created_at] = host.created_at
			hash[:ssh_port] = host.ssh_port

			return hash
		end

		def isValidParameter(params)
			required_keys = [ "ip", "userid", "password", "ssh_port"]

			required_keys.each do |key|
				if not params.has_key?(key)
					return false 
				end
			end

			return true
		end
	end

end