module Xen


  class Model
    
    class << self

      def to_obj(hash)

        hash.each do |k,v|
          if v.kind_of? Hash
            v.to_obj
          end
          
          k=k.gsub(/\.|\s|-|\/|\'/, '_').downcase.to_sym
          
          ## create and initialize an instance variable for this key/value pair          
          hash.instance_variable_set("@#{k}", v)

          ## create the getter that returns the instance variable
          hash.class.send(:define_method, k, proc{hash.instance_variable_get("@#{k}")})

          ## create the setter that sets the instance variable
          hash.class.send(:define_method, "#{k}=", proc{|v| hash.instance_variable_set("@#{k}", v)})
        end 
        
        return self

      end

    end
  end
end
