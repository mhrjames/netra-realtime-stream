module Xen
	module Models
		class DesktopHypervisor
			
			attr_accessor :data
			attr_accessor :items

			def initialize()
				@data = {}
				@items = []
			end

			def toHash()
				@items
			end
			
		end
	end
end