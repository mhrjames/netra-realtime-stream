module Xen
	module Models
		class DesktopVM

			attr_accessor :catalogName
			attr_accessor :catalogUid
			attr_accessor :deliveryType
			attr_accessor :desktopGroupName
			attr_accessor :desktopGroupUid
			attr_accessor :desktopKind
			attr_accessor :hostedMachineId
			attr_accessor :hostedMachineName
			attr_accessor :hypervisorConnectionName
			attr_accessor :hypervisorConnectionUid
			attr_accessor :isAggigned
			attr_accessor :isPhysical
			attr_accessor :machineInternalState
			attr_accessor :machineName
			attr_accessor :machineUid
			attr_accessor :powerState
			attr_accessor :registrationState
			attr_accessor :sid
			attr_accessor :provisioningType

			attr_accessor :data
			attr_accessor :items


			def initialize()
				@data ={}
				@items = []
			end


			def hasProperty?(name)
				instance_variables.each do |property|
					
					Xen::Logger.debug("hasProperty : name = #{property}")

					if property.casecmp(name) == 0
						return true
					end
				end
				return false
			end

			def toHash()
				hash = {
					:catalogName => @catalogName,
					:catalogUid => @catalogUid,
					:deliveryType => @deliveryType,
					:desktopGroupName => @desktopGroupName
				}

				return @data
			end

		end
	end
end