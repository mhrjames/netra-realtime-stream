module Xen
	module Models
		module Formatter

			def toServerHash(server)
				hash = {}

				hash[:name] = server.name
				hash[:description] = server.description
				hash[:uuid] = server.uuid
				hash[:power_state] = server.power_state
				hash[:vcpus_max] = server.vcpus_max
				hash[:memory_max] = server.memory_static_max
				hash[:memory_overhead] = server.memory_overhead
				hash[:host_ref] = server.__affinity
				hash[:template_name] = server.other_config['base_template_name']
				hash[:reference] = server.reference
				hash[:is_template] = server.is_a_template
				hash[:pv_bootloader] = server.pv_bootloader
				hash[:pv_args] = server.pv_args



				vdis = []
				begin
					server.vbds.each do |vbd|
						Xen::Logger.debug("vbd : uuid = #{vbd.uuid} , vdi = #{vbd.vdi}")
						
						if not vbd.vdi.nil?
							vdis << { :name => vbd.vdi.name, :size => vbd.vdi.virtual_size, :type => vbd.vdi.type }
						end
					end
					hash[:storage] = vdis
				rescue Exception => e
					Xen::Logger.warning("toServerHash : Error , #{e.message}")
				end

				vifs = []
				server.vifs.each do |vif|
					vifs << { :name => vif.network.name, 
								:mac => vif.mac, 
								:device => vif.device
								#:description => vif.network.description
							}
				end
				hash[:network] = vifs

				if isVMRunning(server) and not server.__guest_metrics.nil?
					metric = { #:disk => server.guest_metrics.disk,
							:last_updated => server.guest_metrics.last_updated,
							:live => server.guest_metrics.live,
							:memory => server.guest_metrics.memory,
							:os_version => server.guest_metrics.os_version,
							:networks => server.guest_metrics.networks
						}
					hash[:metric] = metric
				end

				consoles = []
				server.consoles.each do |console|
					consoles << {
						:uuid => console.uuid, 
						:reference => console.reference, 
						:location => console.location, 
						:other_config => console.other_config, 
						:protocol => console.protocol
					}
				end

				#hash[:console] = consoles
				#hash[:metrics] = server.__metrics

				return hash
			end

			def toTemplateHash(server)
				hash = {}

				hash[:name] = server.name
				hash[:description] = server.description
				hash[:uuid] = server.uuid
				hash[:vcpus_max] = server.vcpus_max
				hash[:memory_max] = server.memory_static_max
				hash[:reference] = server.reference

				vdis = []
				server.vbds.each do |vbd|
					#Xen::Logger.debug("vbd : uuid = #{vbd.uuid} , vdi = #{vbd.vdi}")
					if not vbd.vdi.nil?
						vdis << { :name => vbd.vdi.name, :size => vbd.vdi.virtual_size, :type => vbd.vdi.type }
					end
				end
				hash[:storage] = vdis

				vifs = []
				server.vifs.each do |vif|
					vifs << { :name => vif.network.name, 
								:device => vif.device
							}
				end
				hash[:network] = vifs

				return hash
			end
			
			def toHostHash(host, vms)
				hash = {}
	  			
	  			hash['uuid'] = host.uuid
	  			hash['reference'] = host.reference
	  			hash['name'] = host.name
	  			hash['description'] = host.description
	  			hash['enabled'] = host.enabled
	  			hash['ip'] = host.address
	  			hash['hostname'] = host.hostname
	  			hash['cpu_count'] = host.cpu_info['cpu_count']
	  			hash['cpu_speed'] = host.cpu_info['speed']
	  			hash['cpu_model'] = host.cpu_info['modelname']
	  			hash['cpu_flags'] = host.cpu_info['flags']
	  			hash['xenserver_version'] = host.software_version['product_version']
	  			hash['network_backend'] = host.software_version['network_backend']
	  			hash['xcp'] = host.software_version['xcp:main']
	  			hash['linux'] = host.software_version['linux']
	  			hash['boottime'] = host.other_config['boot_time']
	  			hash['iscsi_iqn'] = host.other_config['iscsi_iqn']
	  			hash['total_memory'] = host.metrics.memory_total
	  			hash['free_memory'] = host.metrics.memory_free
				hash['memory_overhead'] = host.memory_overhead
				hash['bios_version'] = host.bios_strings['bios-version']
				hash['system_manufacturer'] = host.bios_strings['system-manufacturer']
				hash['system_serial'] = host.bios_strings['system-serial-number']

				
				storages = []
				host.pbds.each do |pbd|
					storages << toSRHash(pbd.sr)
				end
				hash[:storage] = storages

				pifs = []
				host.pifs.each do |pif|
					pifs << toPIFHash(pif)
				end
				hash[:network] = pifs

				hash[:xenserver] = []
				if not vms.nil?
					if vms.has_key?(host.reference)
						hash[:xenserver] = vms[host.reference]
					end
				end

	  			return hash
			end

			def toHostSimpleHash(host)
				hash = {}
	  			
	  			hash['name'] = host.name	  			
	  			hash['reference'] = host.reference
	  			hash['uuid'] = host.uuid
	  			hash['ip'] = host.address
	  			hash['hostname'] = host.hostname

	  			return hash
	  		end

			def toSRHash(storage)
				hash = {}
				hash['name'] = storage.name
				hash['description'] = storage.description
				hash['uuid'] = storage.uuid
				hash['reference'] = storage.reference
				hash['type'] = storage.type
				hash['physical_size'] = storage.physical_size
				hash['physical_utilisation'] = storage.physical_utilisation
				hash['virtual_allocation'] = storage.virtual_allocation
				hash['shared'] = storage.shared

				return hash
			end

			def toPBDHash(pbd)
				hash = {}
				hash['uuid'] = pbd.uuid
				hash['reference'] = pbd.reference
				hash['host'] = pbd.host.name
				hash['sr'] = pbd.sr.name
				hash['sr_size'] = pbd.sr.physical_size
				hash['sr_used_size'] = pbd.sr.physical_utilisation
				hash['host_reference'] = pbd.__host
				hash['sr_reference'] = pbd.__sr
				hash['attached'] = pbd.currently_attached

				return hash
			end

			def toPIFHash(pif)
				hash = {}
				hash[:device] = pif.device
				hash[:device_name] = pif.device_name
				#hash[:physical] = pif.physical
				hash[:mac] = pif.mac
				hash[:ip] = pif.ip
				hash[:dns] = pif.dns
				hash[:netmask] = pif.netmask
				hash[:gateway] = pif.gateway

				return hash
			end


			def toPoolHash(pool)
				hash = {}
				hash[:name] = pool.name
				hash[:reference] = pool.reference
				hash[:uuid] = pool.uuid
				hash[:description] = pool.description
				hash[:master] = toHostSimpleHash(pool.master)

				return hash
			end

			def toTaskHash(task)
				hash = {}
				hash[:uuid] = task[:uuid]
				hash[:name_label] = task[:name_label]
				hash[:name_description] = task[:name_description]
				hash[:status] = task[:status]
				hash[:resident_on] = task[:resident_on]
				hash[:progress] = task[:progress]
				hash[:type] = task[:type]
				hash[:result] = task[:result]
				hash[:started_at] = xmlrpcDateToString(task[:created])
				hash[:finished_at] = xmlrpcDateToString(task[:finished])

				return hash
			end


			private

			def xmlrpcDateToString(hash)
				#Xen::Logger.debug("dateHashToString : class = #{hash.class.name} , hash = #{hash}")

				return "#{hash.year}-#{hash.month}-#{hash.day} #{hash.hour}:#{hash.min}:#{hash.sec}"
			end
		end
	end
end