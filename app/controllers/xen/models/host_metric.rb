module Xen
	module Models
		class HostMetric

			attr_accessor :ip
			#attr_accessor :aggregation
			attr_accessor :reference
			attr_accessor :rows
			attr_accessor :columns
			attr_accessor :steps
			attr_accessor :start

			attr_accessor :metrics
			attr_accessor :vms


			def initialize()
				@metrics = {}
				@vms = {}
			end

			def newMetrics(name)
				if @metrics.has_key?(name)
					return @metrics[name]
				end

				a_metrics = Xen::Models::MetricDatum.new
				a_metrics.name = name

				@metrics[name] = a_metrics

				return @metrics[name]
			end

			def getMetricsByName(name)
				@metrics[name]
			end

			def newVMMetrics(vm)
				if @vms.has_key?(vm)
					return @vms[vm]
				end

				a_metrics = Xen::Models::VMMetrics.new

				@vms[vm] = a_metrics

				return @vms[vm]				
			end

			def toHash()
				hash = {}
				hash[:ip] = @ip
				#hash[:aggregation] = @aggregation
				hash[:start] = @start
				hash[:rows] = @rows
				hash[:columns] = @columns
				hash[:reference] = @reference
				
				metrics_hash = {}
				@metrics.each do |name,metric|
					metrics_hash[name] = metric.toHash()
				end
				hash[:metrics] = metrics_hash

				vms_hash = {}
				@vms.each do |name,vm|
					vms_hash[name] = vm.toHash()
				end
				hash[:vms] = vms_hash

				return hash
			end

			def dump()

				Xen::Logger.debug(">>> HostMetric.dump : Start")

				@metrics.each do |name,metric|
					Xen::Logger.debug("#{metric.dump()}")
				end
				
				@vms.each do |name,vm|
					Xen::Logger.debug("#{vm.dump()}")
				end

				Xen::Logger.debug(">>> HostMetric.dump : Finish")

			end

		end
	end
end