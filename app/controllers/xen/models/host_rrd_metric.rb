module Xen
	module Models

		class HostRrdMetric

			##
			# Metric Data Model for host RRD metric data
			#

			attr_accessor :rows
			attr_accessor :columns
			attr_accessor :steps
			attr_accessor :start

			attr_accessor :legends
			attr_accessor :data


			def initialize()
				@data = []
			end

			def addData(data)
				@data << data
			end

			def addMetricData(epoch_time, aggregation, type, reference, name , value)
				a_data = MetricData.new()
				a_data.epoch_time = epoch_time
				a_data.aggregation = aggregation
				a_data.type = type
				a_data.reference = reference
				a_data.name = name
				a_data.value = value
				
				addData(a_data)

				return a_data
			end

			def dump()

				Xen::Logger.debug(">>> HostMetric.dump : Start")

				@data.each do |data|
					Xen::Logger.debug("#{data.toHash()}")
				end
				
				Xen::Logger.debug(">>> HostMetric.dump : Finish")

			end

		end

	end
end
