module Xen
	module Models
		class MetricDatum
			
			attr_accessor :aggregation
			attr_accessor :type
			attr_accessor :reference
			attr_accessor :items
			attr_accessor :name


			def initialize()
				@items = []
			end

			def keyExist?(key)
				@items.each do |item|
					if item.has_key?(item)
						return item
					end
				end
				return nil
			end

			def addMetricData(last_updated, value)

				a_data = MetricData.new()
				a_data.last_updated = last_updated
				a_data.value = value
				
				@items << a_data

				return a_data
			end

			def toHash()
				hash = []
				#hash[name] = []

				@items.each do |item|
					hash << item
				end

				return hash
			end

			def dump()
				Xen::Logger.debug(">>> MetricDataum Dump Start")
				@items.each do |item|
					Xen::Logger.debug("#{item}")
				end
				Xen::Logger.debug(">>> MetricDataum Dump Finish")
			end

		end
	end
end