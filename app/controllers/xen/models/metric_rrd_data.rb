module Xen
	module Models

		class MetricRrdData
			attr_accessor :epoch_time
			attr_accessor :aggregation
			attr_accessor :type
			attr_accessor :reference
			attr_accessor :name
			attr_accessor :value

			def initialize()
				
			end

			def toHash()
				return {
					:epoch_time => @epoch_time,
					:aggregation => @aggregation,
					:type => @type,
					:reference => @reference,
					:name => @name,
					:value => @value
				}

			end

		end

	end
end
