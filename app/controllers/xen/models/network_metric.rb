module Xen
	module Models
		class NetworkMetric
			attr_accessor :device_id
			attr_accessor :device_name
			attr_accessor :io_read_kbs
			attr_accessor :io_write_kbs
			attr_accessor :speed
		end
	end
end