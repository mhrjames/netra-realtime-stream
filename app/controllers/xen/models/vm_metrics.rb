module Xen
	module Models
		class VMMetrics

			attr_accessor :ip
			attr_accessor :aggregation
			attr_accessor :reference
			attr_accessor :rows
			attr_accessor :columns
			attr_accessor :steps
			attr_accessor :start

			attr_accessor :metrics


			def initialize()
				@metrics = {}
				@vms = {}
			end

			def newMetrics(name)
				if @metrics.has_key?(name)
					return @metrics[name]
				end

				#Xen::Logger.debug("VMMetrics.newMetrics : new!!! name = #{name}")


				a_metrics = Xen::Models::MetricDatum.new
				a_metrics.name = name

				@metrics[name] = a_metrics

				return @metrics[name]
			end

			def getMetricsByName(name)
				@metrics[name]
			end

			def toHash()
				hash = {}
				@metrics.each do |name,metric|
					#hash[name] = []
					hash[name] = metric.toHash
				end
				return hash
			end

			def dump()

				Xen::Logger.debug(">>> VMMetric.dump : Start")

				@metrics.each do |name,metric|
					Xen::Logger.debug("#{metric.dump()}")
				end
				
				Xen::Logger.debug(">>> VMMetric.dump : Finish")

			end

		end
	end
end