require 'yaml'

module Xen
	module Parser
		class DesktopParser

			class << self

				def parseListVDI(str)

					a_desktop_model = Xen::Models::DesktopVM.new
					a_desktop_model.items = extractHashedData(str)
					
					#Xen::Logger.debug("parseDesktopList : model = #{a_desktop_model.toHash()}")

					return a_desktop_model
				end

				def parseMachineCatalogList(str)

					a_desktop_catalog = Xen::Models::DesktopCatalog.new
					a_desktop_catalog.items = extractHashedData(str)

					#Xen::Logger.debug("parseMachineCatalogList : model = #{a_desktop_catalog.toHash()}")

					return a_desktop_catalog					
				end


				def parseDeliveryGroupList(str)
					a_desktop_delivery = Xen::Models::DesktopDelivery.new
					a_desktop_delivery.items = extractHashedData(str)

					#Xen::Logger.debug("parseDeliveryGroupList : model = #{a_desktop_delivery.toHash()}")

					return a_desktop_delivery
				end

				def parseHypervisorConnectionList(str)
					a_desktop_hypervisor = Xen::Models::DesktopHypervisor.new
					a_desktop_hypervisor.items = extractHashedData(str)

					#Xen::Logger.debug("parseHypervisorConnectionList : model = #{a_desktop_hypervisor.toHash()}")

					return a_desktop_hypervisor
				end


				def extractHashedData(str)

					result = []
					hash = nil

					if str.nil?
						return result
					end

					
					key_count = 0
					first_line = true

					str.split("\n").each do |line|
						
						Xen::Logger.debug("parseDesktopList : line = #{line}")

						stripped_line = line.strip()

						if stripped_line.length == 0 and first_line == true
							hash = Hash.new
							result << hash
							first_line = false
							key_count = 0
						end

						if stripped_line.length > 0
									
							tokens = stripped_line.split(":")

							name = tokens[0].strip
							value = tokens[1].strip if not tokens[1].nil?

							if doesContainValue(value)								
								hash[name] = value
								key_count += 1
							end
							
							first_line = true

						end

					end
					
					result.pop
					return result
				end


				def doesContainValue(str)
					return false if str.nil?
					return false if str.strip.length == 0
					return false if str.casecmp("{}") == 0 
					return true
				end

			end

		end
	end
end