require 'rrd'


module Xen

	class RRDHandler < RRD::Base

		def fetch(consolidation_function, options = {})

			#options = {:start => Time.now - 1.day, :end => Time.now}.merge options

			options[:start] = options[:start].to_i
			options[:end] = options[:end].to_i

			line_params = RRD.to_line_parameters(options)

			RRD::Wrapper.fetch(rrd_file, consolidation_function.to_s.upcase, *line_params)
		end

	end

end