module Xen
	class RRDParser

		class << self
			
			def toXml(xml_string)
				return Nokogiri::XML(xml_string)
			end

			def parseVMXml(xml_string)
				xml = Nokogiri::XML(xml_string)
				legends = xml.xpath('//xport//meta//legend//entry')

				#print "legend = #{legends}"
			end

			def parseHostXml(xml_string)
				xml = Nokogiri::XML(xml_string)

				start = xml.xpath('//xport//meta//start')
				rows = xml.xpath('//xport//meta//rows')
				columns = xml.xpath('//xport//meta//columns')
				steps = xml.xpath('//xport//meta//step')

				legends = xml.xpath('//xport//meta//legend//entry').map {|node| node.children.text}


				metrics = Xen::Models::HostMetric.new()
				metrics.start = start.text
				metrics.rows = rows.text
				metrics.columns = columns.text
				metrics.steps = steps.text

				xml.xpath('//xport//data//row').each_with_index do |row,index|
					epoch_time = row.search('t').text

					row.search('v').each_with_index do |data,index2|
						tokens = legends[index2].split(":")
						type = tokens[1]
						reference = tokens[2]
						name = tokens[3]
						value = data.text

						if type.casecmp("host") == 0
							a_metrics = metrics.newMetrics(name)
						else
							vm_metrics = metrics.newVMMetrics(reference)
							a_metrics = vm_metrics.newMetrics(name)
							#print "name = #{name} , reference = #{reference} , vm_metrics = #{vm_metrics} , a_metrics = #{a_metrics.name}\n"
						end

						#print "data = #{name} , value = #{value}\n"

						a_metrics.addMetricData(epoch_time, value )
					end

				end

				#metrics.dump()

				return metrics
			end


			def parseVMXml(xml_string, vm_ref)
				xml = Nokogiri::XML(xml_string)

				start = xml.xpath('//xport//meta//start')
				rows = xml.xpath('//xport//meta//rows')
				columns = xml.xpath('//xport//meta//columns')
				steps = xml.xpath('//xport//meta//step')

				legends = xml.xpath('//xport//meta//legend//entry').map {|node| node.children.text}

				metrics = Xen::Models::HostMetric.new()
				metrics.start = start.text
				metrics.rows = rows.text
				metrics.columns = columns.text
				metrics.steps = steps.text

				xml.xpath('//xport//data//row').each_with_index do |row,index|
					epoch_time = row.search('t').text

					row.search('v').each_with_index do |data,index2|
						tokens = legends[index2].split(":")
						type = tokens[1]
						reference = tokens[2]
						name = tokens[3]
						value = data.text

						#next if (vm_ref != reference)
						
						a_metrics = metrics.newMetrics(name)

						a_metrics.addMetricData(epoch_time, value )							


					end

				end

				return metrics
			end


			def parseFetchData(datum)
				#Xen::Logger.debug("parseFetchData : data = #{data.class.name}")

				metrics = Xen::Models::VMMetrics.new
				
				columns = nil
				last_updated = nil

				datum.each_with_index do |data,index|
					
					
					if index == 0 
						columns = data
						columns.each do |column|
							if column.casecmp("time") != 0
								metrics.newMetrics(column)
							end
						end

					else

						columns.each_with_index do |column,column_index|

							if column_index > 0
								value = data[column_index]

								a_metric = metrics.getMetricsByName(column)
								a_metric.addMetricData(data[0],value)

								#Xen::Logger.debug("parseFetchData : index=#{column_index} , name = #{columns[column_index]} , data = #{data[column_index]}")
							end
						end

					end
				end

				return metrics
			end

			def parseFetchDataForExcel(datum)
				#Xen::Logger.debug("parseFetchData : data = #{data.class.name}")

				metrics = Xen::Models::VMMetrics.new
				
				columns = nil
				last_updated = nil

				datum.each_with_index do |data,index|
					
					
					if index == 0 
						columns = data
						columns.each do |column|
							if column.casecmp("time") != 0
								metrics.newMetrics(column)
							end
						end

					else

						columns.each_with_index do |column,column_index|

							if column_index > 0
								value = data[column_index]

								a_metric = metrics.getMetricsByName(column)
								a_metric.addMetricData(data[0],value)

								#Xen::Logger.debug("parseFetchData : index=#{column_index} , name = #{columns[column_index]} , data = #{data[column_index]}")
							end
						end

					end
				end

				return metrics
			end





			def sanitizeValue(value)
				#print " value = #{value.class.name} , value = #{value}\n"

				if value.casecmp("nan") == 0
					return 0
				end

				return value.to_f
			end

		end
	end
end