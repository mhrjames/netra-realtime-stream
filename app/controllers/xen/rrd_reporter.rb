require 'rrd'

module Xen

	class RRDReporter

		class << self

			def dailyHostReport(host_ip,start_time, end_time)
				
				data_avg = hostStat(host_ip,start_time, end_time, :average, 60*60)
				data_max = hostStat(host_ip,start_time, end_time, :max, 60*60)
				data_min = hostStat(host_ip,start_time, end_time, :min, 60*60)

				total_avg = getTotalAverage(data_avg)

				result = {:ip => host_ip, :avg => data_avg, :max => data_max, :min => data_min}

				return Common::CommonResponse.buildResponse(0,'ok',result)
			end

			def weeklyHostReport(host_ip,start_time, end_time)
				data_avg = hostStat(host_ip,start_time, end_time, :average, 60*60*24)
				data_max = hostStat(host_ip,start_time, end_time, :max, 60*60*24)
				data_min = hostStat(host_ip,start_time, end_time, :min, 60*60*24)

				return {:avg => data_avg, :max => data_max, :min => data_min}
			end

			def monthlyHostReport(host_ip,start_time, end_time, cf)
				data_avg = hostStat(host_ip,start_time, end_time, :average, 60*60*24)
				data_max = hostStat(host_ip,start_time, end_time, :max, 60*60*24)
				data_min = hostStat(host_ip,start_time, end_time, :min, 60*60*24)
				
				return {:avg => data_avg, :max => data_max, :min => data_min}				
			end

			def yearlyHostReport(host_ip,start_time, end_time, cf)
				data_avg = hostStat(host_ip,start_time, end_time, :average, 60*60*24*30)
				data_max = hostStat(host_ip,start_time, end_time, :max, 60*60*24*30)
				data_min = hostStat(host_ip,start_time, end_time, :min, 60*60*24*30)

				return {:avg => data_avg, :max => data_max, :min => data_min}
			end

			def customHostReport(host_ip,start_time, end_time, cf)
				data_avg = hostStat(host_ip,start_time, end_time, :average, 60*60*24)
				data_max = hostStat(host_ip,start_time, end_time, :max, 60*60*24)
				data_min = hostStat(host_ip,start_time, end_time, :min, 60*60*24)

				return {:avg => data_avg, :max => data_max, :min => data_min}
			end

			def dailyAllHostReport(start_time, end_time)
				Xen::Logger.debug("RRDReporter.dailyAllHostReport : start_time = #{start_time}")

				result = []
				Host.all.each do |host|
					#Xen::Logger.debug("RRDReporter.dailyAllHostReport : host = #{host.ip}")

					data_avg = hostStat(host.ip,start_time, end_time, :average, 60*60)
					data_max = hostStat(host.ip,start_time, end_time, :max, 60*60)
					data_min = hostStat(host.ip,start_time, end_time, :min, 60*60)

					total_data_avg = getTotalAverage(data_avg)
					total_data_max = getTotalAverage(data_max)
					total_data_min = getTotalAverage(data_min)

					result << { :ip => host.ip, 
								:avg => data_avg.metrics, :max => data_max.metrics, :min => data_min.metrics,
								:total_avg => total_data_avg, :total_max => total_data_max, :total_min => total_data_min
							}
				end

				return ::Common::CommonResponse.buildResponse(0,'ok',result)
			end


			def dailyAllHostReportInExcel(start_time, end_time)
				Xen::Logger.debug("RRDReporter.dailyAllHostReportInExcel : start_time = #{start_time}")

				reports = Spreadsheet::Workbook.new
				
				worksheet = reports.create_worksheet :name => 'List of cliets'
				worksheet.row(0).concat %w{ip, Surname Age}

				#response.each_with_index { |host, i|

				#host.each_with_index do |data,index|
				#	list.row(index+1).push data.ip,data.last_updated,data.value
				#end

				#}

				#header_format = Spreadsheet::Format.new :color => :green, :weight => :bold
				#list.row(0).default_format = header_format


				result = []
				Host.all.each do |host|
					#Xen::Logger.debug("RRDReporter.dailyAllHostReport : host = #{host.ip}")

					data_avg = hostStat(host.ip,start_time, end_time, :average, 60*60)
					#data_avg = hostStatInExcel(host.ip,start_time, end_time, :average, 60*60)
					#data_max = hostStat(host.ip,start_time, end_time, :max, 60*60)
					#data_min = hostStat(host.ip,start_time, end_time, :min, 60*60)

					data_avg.metrics.each do |datum|

						datum.each do |data|
							#data.to_hash()
							Xen::Logger.debug("data = #{data.to_hash}")
							#Xen::Logger.debug("name = #{name} , data = #{data}")
						end
					end


				end

				return ::Common::CommonResponse.buildResponse(0,'ok',result)
			end





			##
			# Resolution unit is second!!!
			#
			# for more information about rrd fetch, refer to followning link
			# http://oss.oetiker.ch/rrdtool/doc/rrdfetch.en.html
			#
			def hostStat(host_ip,start_time, end_time, cf, resolution)
				#Xen::Logger.debug("RRDReporter.hostStat")

				filename = "#{Xen::Common.getRRDPath()}/host_#{host_ip}.rrd"
				rrd = Xen::RRDHandler.new(filename)

				if not Xen::Common.doesFileExists?(filename)
					xml_filename = "#{Xen::Common.getRRDXMLPath()}/host_#{host_ip}.xml"
					rrd.restore(xml_filename, :force_overwrite => true)

					#Xen::Logger.debug("hostStat : xml = #{xml_filename}")
				end

				option = { :start => start_time, :end => end_time, :resolution => resolution }
				datum = rrd.fetch(cf, option)

				metrics = Xen::RRDParser.parseFetchData(datum)

				return metrics
			end

			def hostStatInExcel(host_ip,start_time, end_time, cf, resolution)
				#Xen::Logger.debug("RRDReporter.hostStat")

				filename = "#{Xen::Common.getRRDPath()}/host_#{host_ip}.rrd"
				rrd = Xen::RRDHandler.new(filename)

				if not Xen::Common.doesFileExists?(filename)
					xml_filename = "#{Xen::Common.getRRDXMLPath()}/host_#{host_ip}.xml"
					rrd.restore(xml_filename, :force_overwrite => true)

					#Xen::Logger.debug("hostStat : xml = #{xml_filename}")
				end

				option = { :start => start_time, :end => end_time, :resolution => resolution }
				datum = rrd.fetch(cf, option)

				metrics = Xen::RRDParser.parseFetchDataForExcel(datum)

				return metrics
			end


			def vmStat(host_ip,vm_ref, start_time, end_time, cf, resolution)
				filename = "#{Xen::Common.getRRDPath()}/vm_#{host_ip}_#{vm_ref}.rrd"
				rrd = Xen::RRDHandler.new(filename)

				if not Xen::Common.doesFileExists?(filename)
					xml_filename = "#{Xen::Common.getRRDXMLPath()}/vm_#{host_ip}_#{vm_ref}.xml"
					rrd.restore(xml_filename, :force_overwrite => true)

					Xen::Logger.debug("vmStat : xml = #{xml_filename}")
				end

				option = { :start => start_time, :end => end_time, :resolution => resolution }
				datum = rrd.fetch(cf, option)

				metrics = Xen::RRDParser.parseFetchData(datum)

				calced_sum = getSum(metrics)
				Xen::Logger.debug("vmStat : sum = #{calced_sum}")

				#print "\n\n>>>metrics : #{metrics.toHash()}"

			end




			def getTotalAverage(data)
				values = {}
				data.metrics.each do |key,a_metric|
					sum , count = 0, 0
					a_metric.items.each do |metric_data|
						#Xen::Logger.debug("getSum : class = #{metric_data.value.class.name} , value = #{metric_data.value}")
						if not metric_data.value.nan?
							sum += metric_data.value
							count += 1
						end
					end
					
					if count > 0
						values[key] = (sum/count).round(2)
					else
						values[key] = 0
					end

				end

				return values
			end

			def getSum(data)
				values = {}
				data.metrics.each do |key,a_metric|
					#Xen::Logger.debug("getSum : key = #{key}")

					sum , count = 0, 0
					a_metric.items.each do |metric_data|
						#Xen::Logger.debug("getSum : class = #{metric_data.value.class.name} , value = #{metric_data.value}")
						if not metric_data.value.nan?
							sum += metric_data.value
							count += 1
						end
					end
					values[key] = sum
				end

				return values
			end

		end
	end

end