module Xen

	class XenEventHandler

		class << self

			def attach(session)
				session.registerEvents()
			end

			def detach(session)
				session.unregisterEvents()
			end
			
			def handle(session)
				
				events = session.event.next()

                events.each do |event|
                	Xen::Logger.debug("Xen::XenEventHandler.handle : event = #{event}")
                end

			end

		end

	end

end