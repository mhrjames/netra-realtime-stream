module Xen

	class XenPool
		include Xen::Command::PoolCommand


		class << self
			def getPoolByName(pool_name)
				found_pool = Pool.where(:name => pool_name).first
				return found_pool
			end

			def getXenConnectionParameter(param)
				found_pool = getPoolByName(param[:pool])
				if found_pool.nil?
					Xen::Logger.warning("Xen::XenPool.getXenConnectionParameter : no pool named #{param[:pool]} found")
					raise Xen::NoHostError.new
				end

				found_host = Host.where(:ip => found_pool.master_ip).first
				if found_host.nil?
					Xen::Logger.warning("Xen::XenPool.getXenConnectionParameter : no pool named #{param[:pool]} master found")					
					raise Xen::NoPoolMasterError.new
				end

				params = { :host => found_host.ip, :username => found_host.userid, :password => found_host.password }

				return params

			end
		end



		def rebuild(masters)
			rebuild_count = 0

			if masters.size == 0
				Xen::Logger.warning("Xen::XenPool.rebuild : Error No master found!!!")
				return nil
			end

			cleanPool()

			masters.each do |master|
				
				Xen::Logger.debug("Xen::XenPool.rebuild : master = #{master}")

				session = Xen::XenServer.new(master)

				if session.connection.pools.size == 0
					Xen::Logger.warning("Xen::XenPool.rebuild : Master has no pool!!!")
					next
				end

				pool = session.connection.pools.first

				params = { 
					:name => pool.name,
					:description => pool.description,
					:uuid => pool.uuid,
					:reference => pool.reference,
					:master_ip => pool.master.address,
					:master_ref => pool.__master,
					:data => pool.to_json
				}

				Pool.new_pool(params)
				
				updatePoolMasterData(pool.master.address, pool.name)

				rebuild_count += 1
				Xen::Logger.debug("Xen::XenPool.rebuild : class = #{pool.class.name} , pool = #{pool}")

			end

			return rebuild_count
		end

		def getMasters()
			masters = []

			Host.selectMasters().each do |host|
				if host.cluster_name != ''
					masters << { :pool => host.cluster_name, :host => host.ip, :username => host.userid, :password => host.password }
				end
			end

			Xen::Logger.debug("XenPool.getMasters : #{masters}")

			return masters
		end

		def scanPoolMasters()
			Xen::Logger.debug("PoolCommand.scanPoolMasters")

			masters = []

			Host.all.each do |host|
				params = { :pool => host.cluster_name, :host => host.ip, :username => host.userid, :password => host.password }
				
				Xen::Logger.debug("PoolCommand.inspect : host = #{params}")

				begin
					session = Xen::XenServer.createSession(params)
					session.close()

					masters << params
				rescue
					Xen::Logger.debug("PoolCommand.inspect : error host = #{params}")
				ensure
					if not session.nil?
						session.close()
					end
				end
			end

			return masters
		end

		def updatePoolMasterData(ip,pool_name)
			Host.update_cluster_name(ip,pool_name)
		end

		def getRegisteredHostCount()
			Host.all.length
		end

		def cleanPool()
			Pool.delete_all()
		end

		def toPoolHash(pool)
			hash = {}
			hash[:name] = pool.name
			hash[:description] = pool.description
			hash[:master_ip] = pool.master_ip
			hash[:master_ref] = pool.master_ref
			hash[:reference] = pool.reference

			return hash
		end

	end

end