module Xen
	class XenRRDFileHandler

		class << self

		    def dos2unix(str)
		      	str.encode("UTF-8").gsub(/\r\n/, "\n")
		    end

		    def writeFile(file_name, contents)
		      	File.open(file_name, 'w') {|f| f.write(dos2unix(contents)) }
		    end

		    def read_file(file_name)
		      
				contents = ""
				if File.exists?(file_name)
					contents = File.read(file_name)
				end

				return contents
		    end

		    def doesDirectoryExist?(path)
				File.directory?(path)
		    end

		    def makeDirectory(path)
		      FileUtils.mkdir_p path
		    end

		    def makeFileName(type,ip)
		    	time = Time.now.strftime("%Y%m%d_%H%M")
		    	"#{type}_#{ip}"
		    end


			def writeHostRRD(ip, contents)
				return nil if contents.size == 0

				filename = makeFileName('host',ip)
				full_filename = "#{Xen::Common.getRRDXMLPath()}/#{filename}.xml"
				
				makeDirectory(Xen::Common.getRRDXMLPath()) if not doesDirectoryExist?(Xen::Common.getRRDXMLPath())

				writeFile(full_filename, contents)

				return full_filename
			end

			def writeVMRRD(ip, vm_uuid, contents)
				return nil if contents.size == 0

				filename = makeFileName('vm',ip+"_"+vm_uuid)
				full_filename = "#{Xen::Common.getRRDXMLPath()}/#{filename}.xml"
				
				makeDirectory(Xen::Common.getRRDXMLPath()) if not doesDirectoryExist?(Xen::Common.getRRDXMLPath())

				writeFile(full_filename, contents)

				return full_filename
			end


			def getLatestRRDFiles()
				rrd_path = "#{Xen::Common.getRRDXMLPath()}/*.xml"
				#Xen::Logger.debug("getLatestRRDFiles : path = #{rrd_path}")

				files = []
				Dir[rrd_path].each do |file|
					file_name = File.basename file
					files << { :created => File.ctime(file), :name => file_name}
					#print "file = #{file_name} , time = #{File.ctime(file)}\n"
				end

				return files
			end

		end

	end
end