module Xen

	class XenServer

		include Xen::Command::XenCommand
		include Xen::Models::Formatter


		attr_reader :connection

		class << self

			def createSession(params)
				connection = Xen::XenSession.new(params[:host])
				connection.authenticate(params[:username],params[:password])

				#session = connection.session

				return connection
			end

			def testCreate()
				params = { :host => '192.168.204.38', :username => 'root', :password => 'P@ssw0rd' }
				Xen::XenServer.new(params)
			end

			def create(params)
				#params = Xen::XenPool.getXenConnectionParameter(params)
				params = { :host => NETRA_CONFIG['general_master_ip'], 
						 	:username => NETRA_CONFIG['general_master_username'],
						 	:password => NETRA_CONFIG['general_master_password'] }

				Xen::XenServer.new(params)
			end

			def createPoolSession(params)
				Xen::XenServer.new(params)
			end
		end


		def initialize(params)
			@params = params
			@connection = Xen::NetraXenServer.new({
			  	:xenserver_url => params[:host],
			  	:xenserver_username => params[:username],
			  	:xenserver_password => params[:password],
			})

			#@session = @connection.getConnection()
			#print "@session = #{@session.class.name}"

			return self
		end

		def getCredential()
			conn = @connection.instance_variable_get('@connection')
			credentials = conn.instance_variable_get('@credentials')
			return credentials
		end
		
		def getConnection()
			@connection.instance_variable_get('@connection')
		end


		def getRequest()
			return @connection.instance_method_get('request')
		end

		def createNewVM(vm_name,template_name,host, description)
			Xen::Logger.debug("XenServer.createNewVM : vm_name = #{vm_name} , template = #{template_name} , host = #{host.name}")

			template = self.getTemplateByName(template_name)

			host_template = getTemplateInXenHost(host, template_name)
			if host_template.nil?
				Xen::Logger.debug("XenServer.createNewVM : template does not exist in #{host.name}")
				host_template = copyTemplateToHost(template.first, host)
			end

			createVMFromTemplate(vm_name, host_template, host, description)

		end

		def createVMFromTemplate(vm_name, template, host, description)
			Xen::Logger.debug("XenServer.createVMFromTemplate : vm = #{vm_name} , template = #{template.name}, description = #{description}")

			new_vm = @connection.servers.new :name => "#{vm_name}",
			                      		 	 :template_name => "#{template.name}"

			host.reference = host.reference
			#new_vm.set_attribute('affinity', host.reference)

			new_vm.save :auto_start => false
			setDescription(new_vm.reference,description)

			new_vm.provision	

			return new_vm
		end

		def createAsyncVMFromTemplate(session, vm_name, template, host, description)
			Xen::Logger.debug("XenServer.createAsyncVMFromTemplate : vm = #{vm_name} , template = #{template.name}")

			new_vm = @connection.servers.new :name => "#{vm_name}",
			                      		 	 :template_name => "#{template.name}"

			host.reference = host.reference
			#new_vm.set_attribute('affinity', host.reference)
			new_vm.save :auto_start => false
			setDescription(new_vm.reference,description)

			task_ref = session.asyncVMProvision(new_vm.reference)

			return task_ref
		end

		#method to create a vm with dedicated storage
		def copyAsyncVMFromTemplate(session, vm_name, template, host, sr, description)
			Xen::Logger.debug("XenServer.copyAsyncVMFromTemplate : vm = #{vm_name} , template = #{template.name}")

			storage_size = getStorageTotalSize(template)

			vdi = @connection.vdis.create :name => "#{vm_name}-disk1", 
                       						:storage_repository => sr,
                       						:description => "#{vm_name}-disk1",
                       						:virtual_size => '8589934592' #storage_size.to_i # ~8GB in bytes


			new_vm = @connection.servers.new :name => "#{vm_name}",
											 :affinity => host,
			                      		 	 :template_name => "#{template.name}"

			#host.reference = host.reference
			#new_vm.set_attribute('affinity', host.reference)
			
			new_vm.save :auto_start => false
			#setDescription(new_vm.reference,description)

			@connection.vbds.create :server => new_vm, :vdi => vdi

			new_vm.provision()
			#task_ref = session.asyncVMProvision(new_vm.reference)
			#return task_ref
		end

		def copyTemplateToHost(template, host)
			Xen::Logger.debug("XenServer.copyTemplateToHost : template = #{template.name} , host = #{host.name}")
			
			srs = getXenHostLocalSR(host)
			
			session = Xen::XenServer.createSession(@params)

			template_name = getHostTemplateName(host,template.name)

			copied_vm_ref = session.VM.copy(template.reference, "#{template_name}", srs[0].reference)
			#copied_vm = session.VM.get


			Xen::Logger.debug("XenServer.copyTemplateToHost : copied_vm = #{copied_vm_ref}")

			copied_vm = @connection.servers.get(copied_vm_ref)
			copied_vm.set_attribute 'affinity', host.reference

			session.close()

			return copied_vm
		end


		def copyAsyncTemplateToHost(session, template, host)
			Xen::Logger.debug("XenServer.copyAsyncTemplateToHost : template = #{template.name} , host = #{host.name}")
			
			srs = getXenHostLocalSR(host)
			
			template_name = getHostTemplateName(host,template.name)

			task_ref = session.asyncVMCopy(template.reference, "#{template_name}", srs[0].reference)
			#task_ref = session.asyncRequest("Async.VM.copy", template.reference, "#{template_name}", srs[0].reference)
			#copied_vm_ref = session.VM.copy(template.reference, "#{template_name}", srs[0].reference)

			Xen::Logger.debug("XenServer.copyAsyncTemplateToHost : task_ref = #{task_ref}")

			#copied_vm = @connection.servers.get(copied_vm_ref)
			#copied_vm.set_attribute 'affinity', host.reference

			return task_ref
		end

		def destroyAsyncVM(session,server_ref)
			Xen::Logger.debug("XenServer.destroyAsyncVM : server_ref = #{server_ref}")
			task_ref = session.asyncVMDestroy(server_ref)
			return task_ref
		end

		def injectConfigDataByUUID(vm_uuid,userinfo)
			found_vm = @connection.servers.get_by_uuid(vm_uuid)
			if found_vm.nil?
				return nil
			end
			
			if userinfo.nil?
				return nil
			end

			injectConfigData(found_vm,userinfo.hostname,userinfo.bootproto,userinfo.device_id,userinfo.ip,userinfo.netmask,userinfo.gateway,userinfo.dns1,"")
		end

		def injectConfigData(vm,hostname,boot,device_id,ip,netmask,gateway,dns1,dns2)
			xenstore_data = {}
			xenstore_data['vm-data/hostname'] = hostname
			xenstore_data['vm-data/device_id'] = device_id.to_s()
			xenstore_data['vm-data/bootproto'] = boot
			xenstore_data['vm-data/ip'] = ip
			xenstore_data['vm-data/gateway'] = gateway
			xenstore_data['vm-data/netmask'] = netmask
			xenstore_data['vm-data/dns1'] = dns1
			xenstore_data['vm-data/dns2'] = dns2

			Xen::Logger.debug("XenServer.injectConfigData : vm = #{vm} , xenstore_data = #{xenstore_data}")

			vm.set_attribute('xenstore_data', xenstore_data)
		end


		def getTemplateByName(template_name)
			templates = []
			@connection.servers.templates.each do |server|
				if server.is_a_template == true
					if server.name.casecmp(template_name) == 0
						#print "server : name = #{server.name} , template_name = #{template_name} , #{server.is_a_template}\n"
						templates << server
					end
				end
			end

			return templates
		end

		def getTemplateInXenHost(host, template_name)
				
			templates = getTemplateByName(getHostTemplateName(host,template_name))
			Xen::Logger.debug("XenServer.createNewVM : Templates = #{templates.size}")

			if templates.length == 0
				Xen::Logger.warning("XenServer.getTemplateInXenHost : No templates found!!!")
				return nil
			end

			return templates.first
		end

		def getXenHostPBD(host)
			Xen::Logger.debug("XenServer.getXenHostPBD : host = #{host.name}")

			pbds = []
			@connection.pbds.all.each do |pbd|
				#Xen::Logger.debug("XenServer.getXenHostPBD : pbd = #{pbd}")
				if pbd.__host.casecmp(host.reference) == 0
					pbds << pbd
					#pbds[pbd.reference] = pbd.to_json
				end
			end

			return pbds
		end

		def getXenHostLocalSR(host)
			pbds = getXenHostPBD(host)
			Xen::Logger.debug("XenServer.getXenHostLocalSR : host = #{host.name} , pbds_count = #{pbds.size}")

			srs = []
			pbds.each do |pbd|
				#Xen::Logger.debug("class = #{pbd.class.name} , pbd = #{pbd}")
				if isSRLocalStorage(pbd.sr)
					srs << pbd.sr
					#print "host sr = #{pbd.sr.name}\n"
				end
				#Xen::Logger.debug("333")
			end

			return srs
		end

		def getHostNamesInHash()
			hash = {}
			@connection.hosts.all.each do |host|
				hash[host.name] = host.reference
			end
			return hash
		end

		def getHostByIp(ip)
			Xen::Logger.debug("XenServer.getHostByIp : host_ip = #{ip}")

			@connection.hosts.all.each do |host|
				if host.address.casecmp(ip) == 0
					return host
				end
			end
			return nil
		end

		def getHostByName(host_name)
			Xen::Logger.debug("XenServer.getHostByName : host_name = #{host_name}")
			
			@connection.hosts.all.each do |host|
				if host.name.casecmp(host_name) == 0
					return host
				end
			end
			return nil
		end

		def getHostByRef(host_ref)
			return @connection.hosts.get(host_ref)
		end

		def getVMbyHost()
			hosts = {}
			@connection.servers.all.each do |server|
				if not hosts.has_key?(server.__affinity)
					hosts[server.__affinity] = []
				end
				hosts[server.__affinity] << server.reference
			end
			return hosts
		end

		def setDescription(vm_ref,descrition)
			conn = getConnection()

			conn.request({
				:parser => Fog::Parsers::XenServer::Base.new, :method => 'VM.set_name_description'},
				vm_ref,
				descrition
			)
		end


		def copyVM(vm_name,vm_ref,sr_ref)
			conn = getConnection()

			conn.request({
				:parser => Fog::Parsers::XenServer::Base.new, :method => 'VM.copy'},
				vm_ref,
				vm_name,
				sr_ref
			)
		end

		def asyncCopyVM(vm_name,vm_ref,sr_ref)
			conn = getConnection()

			conn.request({
				:parser => Fog::Parsers::XenServer::Base.new, :method => 'Async.VM.copy'},
				vm_ref,
				vm_name,
				sr_ref
			)
		end

		def getTaskRecord(task_ref)
			conn = getConnection()

			begin
				conn.request({
					:parser => Fog::Parsers::XenServer::Base.new, :method => 'task.get_record'},
					task_ref
				)
			rescue
				return nil
			end
		end

		def getTaskStatus(task_ref)
			conn = getConnection()
			begin
				conn.request({
					:parser => Fog::Parsers::XenServer::Base.new, :method => 'task.get_status'},
					task_ref
				)
			rescue
				return nil
			end
		end


		def to_json(arr)
			new_arr = []
			arr.each do |an_arr|
				new_arr << an_arr.to_json
			end

			return new_arr
		end

		def getStorageTotalSize(server)
			size = 0
			vdis = []
			server.vbds.each do |vbd|
				if not vbd.vdi.nil?
					#Xen::Logger.debug("vbd : uuid = #{vbd.uuid} , vdi_type = #{vbd.vdi.type}")
					
					vdis << { :name => vbd.vdi.name, :size => vbd.vdi.virtual_size, :type => vbd.vdi.type }
					size += vbd.vdi.virtual_size.to_i()
				end
			end

			return size
		end

		private

		def isVMRunning(server)
			return server.running?
		end

      	def isSRLocalStorage(sr)
      		if sr.name.casecmp("Local storage") == 0
      			return true
      		end
      		return false
      	end

      	def getHostTemplateName(host, template_name)
      		"#{template_name}_#{host.name}"
      	end

      	def executeCommand()

      	end
	end

end