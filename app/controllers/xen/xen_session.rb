module Xen

  require "xmlrpc/client"
  #require 'dispatcher'


  class XenSession
    
    include Xen::Command::SessionCommand
    #include Xen::VirtualMachine
    #include Xen::Vdi
    #include Xen::Vbd
    #include Xen::Storage
    #include Xen::Task
    #include Xen::Network

    attr_reader :credentials
    attr_reader :client
    attr_reader :session
    
    def initialize(host, timeout = 1200)
      #
      # caution!!!!
      # when deploying new netra please change port to 80
      #
      @client = XMLRPC::Client.new(host, '/',80)
      @client.set_parser(Xen::NokogiriStreamParser.new)
      @client.timeout = timeout

      @session = @client.proxy("session")
    end

    def authenticate( username, password )
        #response = @client.call('session.login_with_password', username.to_s, password.to_s)
        
        response = @session.login_with_password(username.to_s, password.to_s)
        raise Xen::AuthenticationError.new unless response["Status"] =~ /Success/

        @credentials = response["Value"]

        return self
    end

    def request(name, *args)
      begin
        response = @session.send(name, *args)
        
        Xen::Logger.debug("request = #{response}")

        #raise ErrorFactory.create(*response['ErrorDescription']) unless response['Status'] == 'Success'
        response['Value']
      rescue Exception => exc
        #error = Xen::ErrorFactory.wrap(exc)
        if @error_callback
          @error_callback.call(error) do |new_session|
            prefix = @proxy.prefix.delete(".").to_sym
            dispatcher = new_session.send(prefix)
            dispatcher.send(name, *args)
          end
        else
          #raise error
        end
      end
    end

    def asyncRequest(name, *args)
      begin
        Xen::Logger.debug("XenSession.asyncRequest : name  = #{name.to_s}")

        response = @client.call(name.to_s, @credentials, *args)

        #Xen::Logger.debug("XenSession.asyncRequest : Response = #{response}")

        #raise ErrorFactory.create(*response['ErrorDescription']) unless response['Status'] == 'Success'
        response['Value']
      rescue Exception => exc
        Xen::Logger.warning("XenSession.asyncRequest : Error = #{exc}")
        #error = Xen::ErrorFactory.wrap(exc)
        if @error_callback
          @error_callback.call(error) do |new_session|
            prefix = @proxy.prefix.delete(".").to_sym
            dispatcher = new_session.send(prefix)
            dispatcher.send(name, *args)
          end
        else
          #raise error
        end

        return nil
      end
    end


    def request2(options, *params)
      begin
        parser   = options.delete(:parser)
        method   = options.delete(:method)

        if params.empty?
          response = @factory.call(method, @credentials)
        else
          if params.length.eql?(1) and params.first.is_a?(Hash)
            response = @factory.call(method, @credentials, params.first)
          elsif params.length.eql?(2) and params.last.is_a?(Array)
            response = @factory.call(method, @credentials, params.first, params.last)
          else
            response = eval("@factory.call('#{method}', '#{@credentials}', #{params.map {|p|  p.is_a?(String) ? "'#{p}'" : p}.join(',')})")
          end
        end
        raise RequestFailed.new("#{method}: " + response["ErrorDescription"].to_s) unless response["Status"].eql? "Success"
        if parser
          parser.parse( response["Value"] )
          response = parser.response
        end

        response
      end
    end

    def close
      @session.logout(@credentials)
    end


    def to_hash_object(hashed_string)
      JSON.parse(hashed_string.gsub(/:([a-zA-z]+)/,'"\\1"').gsub('=>', ': ')).symbolize_keys
    end




    # Avoiding method missing to get lost with Rake Task
    # (considering Xen tasks as Rake task (???)
    def task(*args)
      method_missing("task", *args)
    end

    def method_missing(name, *args)
      #print "method_missing : name = #{name}\n"
      raise Xen::UnauthenticatedClient.new unless @credentials
      
      #Xen::Logger.debug("session.method_missing = #{name}")
      #case name.to_s
      #when /^async/i
      #  AsyncDispatcher2.new(self, :_call)
      #else
      #  Dispatcher2.new(self, meth, :_call)
      #end

      proxy = @client.proxy(name.to_s, @credentials, *args)
      Dispatcher.new(proxy, &@block)
    end
  end
end
