module Xen

	require 'excon'

	class XenWatcher

		##
		# One thing i should keep it mind is "time zone" issue when collecting RRD data from xenserver
		# If Netra Server and Xen server do not have same time zone and setting, rrd_update request will mismatch and return nothing
		# So, making every servers same time zone is quite important
		# i spend few hours to solve this problem...
		#

		include Xen::Command::WatcherCommand

		def initialize(xen)
			@xen = xen
		end


		def getHostIPByRef(host_ref)
			host = @xen.getHostByRef(host_ref)
			if host.nil?
				return nil
			end
			return host.address
		end

		def getVMRRDUrl(host_ip, vm_uuid)
			#wget http://<server>/vm_rrd?session_id=OpaqueRef:<SESSION HANDLE>&uuid=<VM UUID>
			url = "http://#{host_ip}/vm_rrd?session_id=#{@xen.getCredential()}&uuid=#{vm_uuid}"
			return url			
		end

		def getHostRRDUrl(host_ip)
			url = "http://#{host_ip}/host_rrd?session_id=#{@xen.getCredential()}"
			return url
		end

		def getHostRRDUpdateUrl(ip,time_diff,cf,interval)
			time = Time.now.to_i - time_diff.minutes.to_i
			#url = "http://#{ip}/rrd_updates?session_id=#{@xen.getCredential()}&start=#{time}"
			url = "http://#{ip}/rrd_updates?session_id=#{@xen.getCredential()}&start=#{time}&host=true&interval=#{interval}&cf=#{cf}"

			return url
		end

		def getVMRRDUpdateUrl(ip,time_diff,cf,interval)
			time = Time.now.to_i - time_diff.minutes.to_i
			url = "http://#{ip}/rrd_updates?session_id=#{@xen.getCredential()}&start=#{time}&interval=#{interval}&cf=#{cf}"

			return url
		end

		def downloadHostRRDFileByRef(host_ref)
			host_ip = getHostIPByRef(host_ref)
			return downloadAllHostRRDFile(host_ip)
		end

		def downloadHostRRDFile(host_ip)
			url = getHostRRDUrl(host_ip)

			response = Excon.get(url)
			print "downloadHostRRDFile : url = #{url} , response = #{response.body}"

			if response.body.length == 0
				#return Common::CommonResponse.buildResponse(ERR_NO_HOST_RRDDATA,ERRMSG_NO_HOST_RRDDATA,response.body)
				return nil
			end

			file_name = Xen::XenRRDFileHandler.writeHostRRD(host_ip,response.body)
			
			#return Common::CommonResponse.buildResponse(0,'ok',response.body)
			return file_name
		end

		def downloadAllHostRRDFile()
			files = []
			Host.all.each do |a_host|
				if a_host.name[0..2].casecmp("xen") == 0
					#Xen::Logger.debug("XenWatcher.downloadAllHostRRDFile : host = #{a_host.ip}")
					files << downloadHostRRDFile(a_host.ip)
				end
			end

			return files
		end

		def downloadRRDFilesForReport()
			files = Xen::XenRRDFileHandler.getLatestRRDFiles()
			if (files.length == 0) or (Date.today > files[0][:created])
				rrd_host_files = downloadAllHostRRDFile()
				rrd_vm_files = downloadAllVMRRDFile()
			end

			#print "downloadRRDFileForReport : time = #{file_time} , time diff = #{time_diff_in_hour}"
			#Xen::Logger.debug("downloadRRDFileForReport : rrd_files = #{rrd_host_files}")

			return rrd_host_files, rrd_vm_files
		end


		def downloadVMRRDFile(host_ip,vm_uuid)
			url = getVMRRDUrl(host_ip,vm_uuid)
			response = Excon.get(url)
			#print "downloadVMRRDFile : url = #{url} , response = #{response.body}"

			return nil if response.body.length == 0
				
			return Xen::XenRRDFileHandler.writeVMRRD(host_ip,vm_uuid,response.body)
		end


		def downloadAllVMRRDFile()			
			files = []
			Host.all.each do |a_host|
				if a_host.name[0..2].casecmp("xen") == 0
					
					#Xen::Logger.debug("XenWatcher.downloadAllVMRRDFile : host = #{a_host.ip}")
					
					host_metric = getHostRRD(a_host.ip)
					host_metric.vms.each do |vm_ref,value|
						files << downloadVMRRDFile(host_metric.ip, vm_ref)
					end
				end
			end

			return files
		end

		def getHostRRD(host_ip)
			url = getHostRRDUpdateUrl(host_ip,1,"AVERAGE",60)
			response = Excon.get(url)

			#print "response : #{response.body}\n"

			host_metric = Xen::RRDParser.parseHostXml(response.body)
			host_metric.ip = host_ip
			
			return host_metric
		end

	end

end