class Host < ActiveRecord::Base

	def self.find_by_ip(ip)
		begin
			return Host.where(ip: ip).take!
		rescue
			return nil
		end
	end

	def self.update_host(instance, params)
		
		instance.name = params[:name]        
		instance.userid = params[:userid]
		instance.password = params[:password]
		instance.description = params[:description]
		instance.ssh_port = params[:ssh_port]
		instance.save()

		return instance
	end

	def self.new_host(params)

		new_host = Host.new
		new_host.ip = params[:ip]
		new_host.name = params[:name]        
		new_host.userid = params[:userid]
		new_host.password = params[:password]
		new_host.description = params[:description]
		new_host.ssh_port = params[:ssh_port]
		new_host.save()
		
		return new_host
	end

	def self.delete_record(instance)
		instance.delete()
	end

	def self.reset_cluster_name(cluster_name, value)
		
		Host.where(:cluster_name => cluster_name).update_all(:cluster_name => value)

	end

	def self.selectMasters()
		
		Host.where.not(:cluster_name => nil)

	end

	def self.update_cluster_name(ip,cluster_name)
		
		Host.where(:ip => ip).update_all(:cluster_name => cluster_name)

	end

end
