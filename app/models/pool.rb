class Pool < ActiveRecord::Base

	def self.find_by_name(name)
		begin
			return Pool.where(name: name).take!
		rescue
			return nil
		end
	end

	def self.new_pool(params)

		new_pool = Pool.new
		new_pool.name = params[:name]        
		new_pool.description = params[:description]
		new_pool.reference = params[:reference]
		new_pool.uuid = params[:uuid]
		new_pool.master_ip = params[:master_ip]
		new_pool.master_ref = params[:master_ref]
		new_pool.data = params[:data]
		
		new_pool.save()
		
		return new_pool
	end

	def self.update_pool(instance, params)
		
		instance.name = params[:name]        
		instance.master_ip = params[:master_ip] if not params[:master_ip].nil? 
		instance.description = params[:description]
		instance.save()

		return instance
	end

	def self.delete_record(instance)
		instance.delete()
	end

	def self.update_pool_member(params)
		
		Host.reset_Pool_name(params[:Pool_name], nil)
		
		params['data'].each do |item|
			item.each do |key,value|
				print "key = #{key}, value = #{value}"
				Host.update_Pool_name(value,params[:Pool_name])
			end
		end

	end

end
