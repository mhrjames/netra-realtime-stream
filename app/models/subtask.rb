class Subtask < ActiveRecord::Base

	def self.new_subtask(task_ref, task_id, command, params)

		new_subtask = Subtask.new
		new_subtask.task_ref = task_ref
		new_subtask.task_id = task_id
		new_subtask.command = command
		new_subtask.status = "created"
		new_subtask.progress = "0"
		new_subtask.params = params
		new_subtask.started_at = Time.now
		
		new_subtask.save()
		
		return new_subtask
	end

	def self.getTask(id)
		begin
			return Subtask.where(task_id: id)
		rescue
			return nil
		end
	end

	def self.update_status(subtask, task_hash)

		subtask.status = task_hash[:status]
		subtask.progress = task_hash[:progress]
		subtask.task_type = task_hash[:type]
		subtask.result = task_hash[:result]
		subtask.name_label = task_hash[:name_label]
		subtask.name_description = task_hash[:name_description]
		subtask.resident_on = task_hash[:resident_on]
		subtask.started_at = task_hash[:started_at]
		subtask.finished_at = task_hash[:finished_at]

		subtask.save()

	end
end
