class Task < ActiveRecord::Base

	def self.new_task(command, params)

		new_task = Task.new
		new_task.name = params[:task_name]        
		new_task.description = params[:task_description]
		new_task.command = command
		new_task.status = "created"
		new_task.progress = "0"
		new_task.parameters = params.to_yaml
		
		new_task.save()
		
		return new_task
	end

	def self.getTask(id)
		begin
			Task.find(id)	
		rescue
			return nil
		end
	end

	def self.set_running_status(task)
		task.status = "Running"
		task.started_at = Time.now

		task.save()
	end

	def self.set_finished_status(task, status)
		task.status = status
		task.finished_at = Time.now
		
		task.save()
	end

	def self.delete_task(task_id)
		
		Task.destroy(task_id)
		Xen::XenResponse.buildRestResponse(Xen::Const::OK,Xen::Const::MSG_OK, nil)

	end
end
