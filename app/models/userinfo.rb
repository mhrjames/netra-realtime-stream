class Userinfo < ActiveRecord::Base


	def self.update_user(params)

		new_user = findByAccount(params[:account])
		if new_user.nil?
			new_user = Userinfo.new
		end

		Xen::Logger.debug("update_user : new_user = #{new_user}")

		new_user.device_id = params[:device_id]
		new_user.hostname = params[:hostname]
		new_user.ip = params[:ip]
		new_user.netmask = params[:netmask]
		new_user.gateway = params[:gateway]
		new_user.dns1 = params[:dns1]
		new_user.bootproto = "static"
		
		new_user.account = params[:account]
		new_user.principalusername = params[:userprincipalname]
		new_user.machine_uuid = params[:machine_uuid]
		new_user.dn = params[:dn]
		new_user.machine_reference = params[:machine_reference]
		
		new_user.save()
		
		Xen::XenResponse.buildRestResponse(Xen::Const::OK,Xen::Const::MSG_OK, new_user)
	end

	def self.delete_userinfo(params)
		
		Userinfo.destroy(params[:id])
		Xen::XenResponse.buildRestResponse(Xen::Const::OK,Xen::Const::MSG_OK, nil)

	end


	def self.findByAccount(account)
		begin
			return Userinfo.where(account: account).take!
		rescue
			return nil
		end
	end

	def self.findByPrincipalName(name)
		begin
			return Userinfo.where(principalusername: name).take!
		rescue
			return nil
		end
	end

	
end
