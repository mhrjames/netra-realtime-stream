json.array!(@reports) do |report|
  json.extract! report, :id, :task_id, :runner, :result, :msg, :created_at, :started_at, :finished_at
  json.url report_url(report, format: :json)
end
