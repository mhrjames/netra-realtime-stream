json.array!(@tasks) do |task|
  json.extract! task, :id, :name, :description, :author, :script, :created_at, :updated_at
  json.url task_url(task, format: :json)
end
