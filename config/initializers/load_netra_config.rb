# to load netra config when app start
configfile = "#{Rails.root}/config/netra.yml"

puts "Read Netra configuration from file , #{configfile}"

if not File.exist? configfile
	puts "netra_config.yml file is missing, please check it!!!"
end

NETRA_CONFIG = YAML.load_file(configfile)