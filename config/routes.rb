Xen::Application.routes.draw do

  resources :reports

  resources :tasks

    #match "/report/notification" => "home#notification_index_v2"
  
    namespace :api do
      #namespace :openstack do
      #openstack related
      get "/host" => "api#host"      
      post "/host/add" => "api#host_add"
      post "/host/delete" => "api#host_delete"
      post "/host/update" => "api#host_update"


      post "/xenhost" => "api#xenhost"
      get "/xenhost/pbd" => "api#xenhost_pbd"
      get "/xenhost/sr" => "api#xenhost_sr"
      post "/xenhost/detail" => "api#xenhost_detail"


      get "/xenserver" => "api#xenserver"
      post "/xenserver" => "api#xenserver_pool"
      post "/xenserver/get" => "api#xenserver_get"
      post "/xenserver/start" => "api#xenserver_start"
      post "/xenserver/stop" => "api#xenserver_stop"
      post "/xenserver/destroy" => "api#xenserver_destroy"
      post "/xenserver/hard_reboot" => "api#xenserver_hard_reboot"
      post "/xenserver/brutal_shutdown" => "api#xenserver_brutal_shutdown"


      post "/provision/xenserver" => "api#provision_xenserver"
      post "/provision/async_xenservers" => "api#provision_async_xenservers"
      post "/provision/async_destroy_xenservers" => "api#provision_async_destroy_xenservers"
      post "/provision/cache_template" => "api#provision_cache_template"
      post "/provision/new_provision_task" => "api#provision_new_provision_task"
      post "/provision/run_task" => "api#provision_run_task"


      get "/template/all" => "api#template_all"
      post "/template" => "api#template"
      post "/template/base" => "api#template_base"
      get "/snapshot" => "api#snapshot"
      

      match "/pool" => "api#pool", :via => [:get, :post]
      get "/pool/inspect" => "api#pool_inspect"
      post "/pool/update" => "api#pool_update"


      get "/sr" => "api#sr"
      post "/sr/provision" => "api#sr_provision"
      get "/pif" => "api#pif"
      get "/pbd" => "api#pbd"
      post "/pbd/provision" => "api#pbd_provision"


      get "/clusters" => "api#clusters"
      post "/clusters/new" => "api#new_clusters"
      post "/clusters/update" => "api#update_clusters"
      post "/clusters/delete" => "api#delete_clusters"
      post "/clusters/member" => "api#member_clusters"

      
      post "/perf/xenhost" => "api#perf_xenhost"
      post "/perf/xenserver" => "api#perf_xenserver"


      post "/report/xenhost/all/excel" => "api#report_xenhost_all_excel"
      post "/report/xenhost/all" => "api#report_xenhost_all"
      post "/report/xenhost" => "api#report_xenhost"


      get "/dhcp" => "api#dhcp"
      post "/dhcp/scan_ip" => "api#dhcp_scan_ip"
      get "/dhcp/stop" => "api#dhcp_stop"
      get "/dhcp/start" => "api#dhcp_start"
      get "/dhcp/restart" => "api#dhcp_restart"


      get "/task" => "api#task"
      post "/task/subtask" => "api#task_subtask"
      post "/task/status" => "api#task_status"
      post "/task/run" => "api#task_run"
      post "/task/run_assign" => "api#task_run_assign"
      post "/task/delete" => "api#task_delete"

  
      get "/xen/all" => "api#xen_all"
      

      get "/ad/ou" => "ad#ad_ou"
      get "/ad/user" => "ad#ad_user"
      get "/ad/ou/user" => "ad#ad_ou_user"
      get "/ad/ou/computer" => "ad#ad_ou_computer"
      get "/ad/computer" => "ad#ad_computer"
      get "/ad/computer/add" => "ad#ad_computer_add"
      get "/ad/computer/remove" => "ad#ad_computer_remove"


      get "/desktop/machine_catalog/add_machine" => "desktop#machine_catalog_add_machine"
      get "/desktop/machine_catalog/remove_machine" => "desktop#machine_catalog_remove_machine"
      get "/desktop/machine_catalog" => "desktop#machine_catalog"
      get "/desktop/delivery_group" => "desktop#delivery_group"
      get "/desktop/xenserver" => "desktop#xenserver"
      get "/desktop/hypervisor_connection" => "desktop#hypervisor_connection"
      post "/desktop/do_assign" => "desktop#do_assign"
      post "/desktop/do_unassign" => "desktop#do_unassign"


      get "/user" => "api#user"
      #post "/user/add" => "api#user_add"
      post "/user/update" => "api#user_update"
      post "/user/delete" => "api#user_delete"


      post "/config" => "api#config"
      post "/config/update" => "api#config_update"
      #match "/task" => "task#tasks", via: :all
      #match "/task/report" => "task#report_tasks", via: :all
      #post "/task/get" => "task#get_tasks"
      #post "/task/update" => "task#update_tasks"
      #post "/task/delete" => "task#delete_tasks"
      #post "/task/syntax" => "task#syntax_tasks"
      #post "/task/run" => "task#run_tasks"
      #match "/task/test" => "task#test_tasks", via: :all


      get "/test" => "api#test"
      get "/test2" => "api#test2"
      get "/test3" => "api#test3"
      get "/test4" => "api#test4"
      get "/test5" => "api#test5"

      post "/login" => "sessions#simple_login"
      #end
    end

    root :to => 'home#index'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
