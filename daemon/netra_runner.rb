require 'sinatra/base'


# Our simple hello-world app
class NetraRunner < Sinatra::Base
    # threaded - False: Will take requests on the reactor thread
    #            True:  Will queue request for background thread
    configure do
      set :threaded, true
    end

    # Request runs on the reactor thread (with threaded set to false)
    get '/hello' do
      'Hello World'
    end

    # Request runs on the reactor thread (with threaded set to false)
    # and returns immediately. The deferred task does not delay the
    # response from the web-service.
    get '/delayed-hello' do
      EM.defer do
        sleep 5
      end
      'I\'m doing work in the background, but I am still free to take requests'
    end

end