class RenameCluster < ActiveRecord::Migration

  def change
    rename_table "clusters", "pools"
  end

end
