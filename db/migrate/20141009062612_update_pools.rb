class UpdatePools < ActiveRecord::Migration
  	
  	def change
  	 	add_column 'pools', 'master_ip', :string
  	 	add_column 'pools', 'master_ref', :string
  	 	add_column 'pools', 'data', :text
  	end

end
