class UpdatePools2 < ActiveRecord::Migration
  def change
	add_column 'pools', 'reference', :string
	add_column 'pools', 'default_sr_ref', :string
	add_column 'pools', 'uuid', :string
  end
end
