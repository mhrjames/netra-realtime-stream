class CreateUserinfos < ActiveRecord::Migration
  def change
    create_table :userinfos do |t|
      t.string :account
      t.string :hostname
      t.string :ip
      t.string :netmask
      t.string :gateway
      t.string :dns1
      t.string :dns2
      t.text :data

      t.timestamps
    end
  end
end
