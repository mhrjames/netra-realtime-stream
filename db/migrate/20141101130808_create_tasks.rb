class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :command
      t.text :description
      t.text :parameters
      t.string :status
      t.string :progress

      t.timestamps
    end
  end
end
