class UpdateTask < ActiveRecord::Migration
  	
  	def change
  	  	add_column 'tasks', 'started_at', :date
  	  	add_column 'tasks', 'finished_at', :date
  	  	add_column 'tasks', 'task_ref', :string
  	  	add_column 'tasks', 'task_uuid', :string
  	end

end
