class CreateSubtasks < ActiveRecord::Migration
  def change
    create_table :subtasks do |t|
      t.integer :task_id
      t.string :command
      t.date :started_at
      t.date :finished_at
      t.date :last_checked_at
      t.string :status
      t.string :progress
      t.text :response

      t.timestamps
    end
  end
end
