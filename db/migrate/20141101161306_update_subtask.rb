class UpdateSubtask < ActiveRecord::Migration
	
	def change
  	  	add_column 'subtasks', 'task_ref', :string
  	  	add_column 'subtasks', 'params', :text
  	end

end
