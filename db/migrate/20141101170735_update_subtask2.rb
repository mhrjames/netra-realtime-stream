class UpdateSubtask2 < ActiveRecord::Migration
	
	def change
		add_column 'subtasks', 'uuid', :string
		add_column 'subtasks', 'name_label', :string
		add_column 'subtasks', 'name_description', :string
		add_column 'subtasks', 'resident_on', :string
		add_column 'subtasks', 'type', :string
		add_column 'subtasks', 'result', :string
	end

end
