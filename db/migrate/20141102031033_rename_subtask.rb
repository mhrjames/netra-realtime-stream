class RenameSubtask < ActiveRecord::Migration
  	
  	def change
  		rename_column 'subtasks', 'type', 'task_type'
  	end

end
