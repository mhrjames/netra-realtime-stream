class UpdateUserinfo < ActiveRecord::Migration
  	
  	def change
  		add_column 'userinfos', 'machine_uuid', :string
  		add_column 'userinfos', 'machine_reference', :string
  		add_column 'userinfos', 'group_name', :string
  		add_column 'userinfos', 'group_uid', :string
  		add_column 'userinfos', 'delivery_name', :string
  		add_column 'userinfos', 'delivery_uid', :string
  		add_column 'userinfos', 'dn', :string
  		add_column 'userinfos', 'principalusername', :string
  	end

end
