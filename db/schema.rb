# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141103135652) do

  create_table "hosts", force: true do |t|
    t.string   "name"
    t.string   "ip"
    t.string   "mac"
    t.string   "cluster_name"
    t.string   "cpu"
    t.string   "memory"
    t.string   "disk"
    t.string   "os"
    t.string   "userid"
    t.string   "password"
    t.text     "description"
    t.text     "facter"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ssh_port"
  end

  create_table "pools", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "creator"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "master_ip"
    t.string   "master_ref"
    t.text     "data"
    t.string   "reference"
    t.string   "default_sr_ref"
    t.string   "uuid"
  end

  create_table "subtasks", force: true do |t|
    t.integer  "task_id"
    t.string   "command"
    t.date     "started_at"
    t.date     "finished_at"
    t.date     "last_checked_at"
    t.string   "status"
    t.string   "progress"
    t.text     "response"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "task_ref"
    t.text     "params"
    t.string   "uuid"
    t.string   "name_label"
    t.string   "name_description"
    t.string   "resident_on"
    t.string   "task_type"
    t.string   "result"
  end

  create_table "tasks", force: true do |t|
    t.string   "name"
    t.string   "command"
    t.text     "description"
    t.text     "parameters"
    t.string   "status"
    t.string   "progress"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "started_at"
    t.date     "finished_at"
    t.string   "task_ref"
    t.string   "task_uuid"
  end

  create_table "userinfos", force: true do |t|
    t.string   "account"
    t.string   "hostname"
    t.string   "ip"
    t.string   "netmask"
    t.string   "gateway"
    t.string   "dns1"
    t.string   "dns2"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "machine_uuid"
    t.string   "machine_reference"
    t.string   "group_name"
    t.string   "group_uid"
    t.string   "delivery_name"
    t.string   "delivery_uid"
    t.string   "dn"
    t.string   "principalusername"
    t.string   "bootproto"
    t.integer  "device_id"
  end

end
