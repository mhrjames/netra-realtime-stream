#
# Working version of IP Changer
# Created at 2014.10.31 21:55
#

function getComputer
{
	return Get-WmiObject Win32_ComputerSystem
}

function changeHostName
{
    #Write-Output "old name = $computerName.Name , new host name = $hostname"
    $hostname = $session.GetValue("vm-data/hostname").value    
    if (! $hostname) 
    {
        Write-Output "changeHostName : exit due to no given hostname"
        return
    }
    
	$computerName = getComputer		
	$computername.Rename($hostname)
	
	Restart-Computer -Force 
}

function setStaticNetwork
{	
    Write-Output "setStaticNetwork"
    
    $device_id = $session.GetValue("vm-data/device_id").value    
    $ip = $session.GetValue("vm-data/ip").value
    $netmask = $session.GetValue("vm-data/netmask").value
    $gateway = $session.GetValue("vm-data/gateway").value
    $dns1 = $session.GetValue("vm-data/dns1").value
    $dns2 = $session.GetValue("vm-data/dns2").value

    $wmi = Get-WmiObject win32_networkadapterconfiguration -filter "ipenabled = 'true' and Description like '%#$device_id%'"
    Write-Output $wmi

    if (!$wmi) 
    {
        Write-Output "setStaticNetwork : exit due to fail to find NIC , index = $device_id"
        return
    }
    
    if (! ($wmi.IPAddress -match $ip)) 
    {
        $wmi.EnableStatic($ip, $netmask)
    }
    if (! ($wmi.DefaultGateWay -match $gateway))
    {
        $wmi.SetGateways($gateway, 1)
    }
    if (! ($wmi.DNSServerSearchOrder -match $dns1))
    {
        $wmi.SetDNSServerSearchOrder($dns1)
    }
}

function setDHCPNetwork
{
    Write-Output "setDHCPNetwork"
    
    $device_id = $session.GetValue("vm-data/device_id").value    
	
    $wmi = Get-WmiObject win32_networkadapterconfiguration -filter "ipenabled = 'true' and Description like '%#$device_id%'"
    Write-Output $wmi
    if (!$wmi) {
        Write-Output "changeDHCPNetwork : exit due to fail to found NIC"
        return
    }

    if ($wmi.DHCPEnabled) 
    {
        Write-Output "changeDHCPNetwork : exit due to same configuration"
        return
    }
        
    $wmi.EnableDHCP()
    $wmi.SetDNSServerSearchOrder()
}

function doChangeHostName
{
    $new_name = $session.GetValue("vm-data/hostname").value
    if (! $new_name) {
        Write-Output "doChangeHostName : exit due to no given host name"
        return
    }
    
    $current_computer = getComputer
    $current_name = $current_computer.Name
    Write-Output "Computer Name = $current_name, new name = $new_name"     
    
    if (! ($current_name -match $new_name))
    {
        Write-Output "doChangeHostName : Change host name"    
        changeHostName
    }
}

function doNetworkConfiguration
{
    $bootproto = $session.GetValue("vm-data/bootproto").value
    if ($bootproto -match "static") 
    {
        setStaticNetwork
    } else {
        setDHCPNetwork
    }

}

# Locate the base object 
$base = gwmi -n root\wmi -cl CitrixXenStoreBase 

# Create a session 
$sid = $base.AddSession("MyNewSession") 
$session = gwmi -n root\wmi -q "select * from CitrixXenStoreSession where SessionId=$($sid.SessionId)" 

# Read the current VM's name 
# $session.GetValue("name").value 

doChangeHostName
doNetworkConfiguration