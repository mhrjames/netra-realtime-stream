#!/usr/bin/python -tt

import os,sys

HWADDR = "HWADDR="

def read_file(file_name):
	file_pointer = open(file_name, 'r')
	contents = file_pointer.readlines()
	file_pointer.close()
	return contents


def write_file(file_name, contents):
	file_pointer = open(file_name, 'w+')
	file_pointer.writelines(contents)
	file_pointer.close()


def does_file_exist(file_name):
	return os.path.exists(file_name)


def get_real_mac_addr(eth_name):
	mac_file = "/sys/class/net/%s/address" % (eth_name)
	return read_file(mac_file)


def update_mac_addr(contents,mac):
	new_contents = []
	for line in contents:
		if HWADDR in line.upper():
			new_line = "%s%s" % (HWADDR, mac)
		else:
			new_line = line
		new_contents.append(new_line)

	return new_contents


def get_mac_from_cfgfile(contents):
	for line in contents:
		if HWADDR in line.upper():
			return line.upper().replace(HWADDR,'')

	return ""


def delete_dev_file():
	file_name = "/etc/udev/rules.d/70-persistent-net.rules"
	if os.path.exists(file_name):
		os.remove(file_name)
		return True
	
	return False


def reboot():
	os.system('reboot now')

def restart_network_service():
	os.system('service network restart')

def check_mac_addr():

	print "Running nic_init to inspect nic and ifcfg files....\n"

	mac_dirs = os.listdir("/sys/class/net")

	update_count = 0
	for mac_dir in mac_dirs:
		real_mac = get_real_mac_addr(mac_dir)[0]

		#eths = os.listdir("/etc/sysconfig/network-scripts/ifcfg-%s" % mac)
		eth_cfg_file = "/etc/sysconfig/network-scripts/ifcfg-%s" % (mac_dir)
		if not does_file_exist(eth_cfg_file):
			continue

		eth_cfg_contents = read_file(eth_cfg_file)
		eth_mac = get_mac_from_cfgfile(eth_cfg_contents)

		#print "mac_dir = %s , real_mac_addr = %s , eth_mac = %s" % (mac_dir, real_mac, eth_mac)

		if len(eth_mac) > 0:
			if real_mac.lower() != eth_mac.lower():
				new_contents = update_mac_addr(eth_cfg_contents,real_mac)
				write_file("%s" % (eth_cfg_file) ,new_contents)
				#write_file("/root/%s" % (mac_dir) ,new_contents)
				update_count += 1

	if update_count>0:
		delete_dev_file()
		restart_network_service()
		#reboot()



check_mac_addr()