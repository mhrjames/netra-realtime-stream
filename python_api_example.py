#-*- coding: utf-8 -*-

import requests, json



def buildFullUrl(action):
	#Netra API Server 주소, 실제 접속할때는 192.168.1.32:3000을 사용해야 함.
	return "http://192.168.1.15:3000/api/%s" % (action)
	#return "http://210.114.88.124:11026/api/%s" % (action)

def parseResponse(json):
	#print "class = %s" % json.__class__.__name__
	return json['data']

def sendGetRequest(url):
	full_url = buildFullUrl(url)
	response = requests.get(url=full_url)
	return parseResponse(response.json())

def sendPostRequest(url,params):
	full_url = buildFullUrl(url)
	response = requests.post(url=full_url, data=params)
	return parseResponse(response.json())



def getPoolList():
	pools = sendGetRequest('pool')
	return pools

def getPoolName():
	#첫번째 pool의 이름을 반환하는 함수 
	pools = getPoolList()
	for pool in pools:
		print "pool_name = %s" % pool['name']
		return pool['name']
	return ""

def getXenHostList(pool):
	#사용자가 지정한 pool에 있는 Xen Host들을 반환하는 함수
	params = { 'pool': pool}
	xenhosts = sendPostRequest('xenhost',params)
	#print "xenhosts = %s", xenhosts
	return xenhosts

def getXenServerList(pool):
	#현재 생성된 Xen Server들을 반환하는 함수
	xenservers = sendPostRequest('xenserver', { 'pool':pool })
	print "xenservers = %s", xenservers
	return xenservers

def getXenBaseTemplateList(pool):
	#Netra API Server로부터 base template 리스트를 가져오는 함수
	templates = sendPostRequest('template/base', { 'pool':pool })
	#print "templates = %s", templates
	return templates


def getRefByName(items, key):
	for item in items:
		if item['name'].lower() == key.lower():
			return item['reference']
	return None


def getVMRefByName(xenservers, vm_name):
	#이름으로 VM Reference값을 찾아오는 함수 
	return getRefByName(xenservers,vm_name)

def getXenHostRefByName(xenhosts, host_name):
	#이름으로 XenHost Reference값을 찾아오는 함수 
	return getRefByName(xenhosts,host_name)

def getTemplateRefByName(templates, template_name):
	#이름으로 Template Reference값을 찾아오는 함수 
	return getRefByName(templates,template_name)


def getResidentVMinXenHost(xenhosts):
	#XenHost별로 몇개의 VM이 생성되어 있는지를 배열로 알려주는 함수
	hosts = []
	for xenhost in xenhosts:
		hosts.append( {'host': xenhost['name'], 'reference': xenhost['reference'], 'vm_count': len(xenhost['xenserver']) } )
	return hosts


def startVM(pool,vm_ref):
	#VM을 실행하는 함수
	print "startVM : pool = %s , vm_Ref = %s" % (pool, vm_ref)
	params = { 'pool': pool, 'vm_ref': vm_ref}
	response = sendPostRequest('xenserver/start',params)
	return response

def destroyVM(pool, vm_ref):
	#VM을 중지하는 함수
	params = { 'pool': pool, 'vm_ref': vm_ref}
	response = sendPostRequest('xenserver/destroy',params)
	return response

def stopVM(pool, vm_ref):
	#VM을 중지하는 함수
	params = { 'pool': pool, 'vm_ref': vm_ref}
	response = sendPostRequest('xenserver/stop',params)
	return response

def brutalShutdownVM(pool, vm_ref):
	#VM을 중지하는 함수
	params = { 'pool': pool, 'vm_ref': vm_ref}
	response = sendPostRequest('xenserver/brutal_shutdown',params)
	return response

def provisionVM(pool,xenhost_ref,template_ref,vm_name,description):
	#새로운 VM을 생성하는 함수
	params = { 'pool': pool, 'template_ref': template_ref, 'host_ref': xenhost_ref, 'vm_name': vm_name, 'description': description}
	response = sendPostRequest('provision/xenserver',params)
	return response

def asyncProvisionVM(pool,xenhost_ref,template_ref,vm_name,description):
	#새로운 VM을 생성하는 함수

	vm_param = {'template_ref': template_ref, 'host_ref': xenhost_ref, 'vm_name': vm_name, 'description': description }
	data = [vm_param]
	params = { 'pool': pool, 'data': data.json}

	print "asyncProvisionVM : params = %s" % (params)


	response = sendPostRequest('provision/async_xenservers',params)
	return response

def cacheTemplate(pool,template_ref):
	#새로운 VM을 생성하는 함수
	params = { 'pool': pool, 'template_ref': template_ref}
	response = sendPostRequest('provision/cache_template',params)
	return response


#
# Netra API 서버를 활용하기 위해서는 pool, xenhosts, xenservers, template와 같은 정보를 먼저 가져와야 한다.
# Netra API는 pool을 중심으로 실행되기 때문제 VM Sart, VM Stop등 여러 명령들을 실행할 때 어떤 pool에 명령을 실행하는 것인지를 항상 지정하도록 되어있다.
#
pool_name = getPoolName()

xenhosts = getXenHostList(pool_name)
xenservers = getXenServerList(pool_name)
templates = getXenBaseTemplateList(pool_name)


# API를 실행하기 위채 필요한 정보들을 수집했으니, 필요한 reference 값들을 얻기 위해 다음의 명령들을 실행한다.
vm_ref = getVMRefByName(xenservers,'tester3')
xenhost_ref = getVMRefByName(xenhosts,'xen2')
template_ref = getVMRefByName(templates,'CentOS6_Minimal')
#template_ref2 = getVMRefByName(templates,'NIA_CentOS6_Intermediate')

#XenHost별로 생성되어 있는 VM 수를 반환한다.
#print getResidentVMinXenHost(xenhosts)

#startVM(pool_name,vm_ref)
#stopVM(pool_name,vm_ref)
#destroyVM(pool_name,vm_ref)
#response = provisionVM(pool_name,xenhost_ref,template_ref,"tester3"," description!!!!!!!")
response = asyncProvisionVM(pool_name,xenhost_ref,template_ref,"testerx"," description!!!!!!!")
#response = cacheTemplate(pool_name,template_ref2)
#response = brutalShutdownVM(pool_name,vm_ref)

#print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
#print "response = %s" % (response)
