import os, pprint
import XenAPI, provision
import xml.dom.minidom


def provisionServer(session):
    # Choose the PIF with the alphabetically lowest device
    # (we assume that plugging the debian VIF into the same network will allow
    # it to get an IP address by DHCP)
    pifs = session.xenapi.PIF.get_all_records()
    lowest = None
    for pifRef in pifs.keys():
        if (lowest is None) or (pifs[pifRef]['device'] < pifs[lowest]['device']):
            lowest = pifRef
    print "Choosing PIF with device: ", pifs[lowest]['device']

    network = session.xenapi.PIF.get_network(lowest)
    print "Chosen PIF is connected to network: ", session.xenapi.network.get_name_label(network)

    # List all the VM objects
    vms = session.xenapi.VM.get_all_records()
    print "Server has %d VM objects (this includes templates):" % (len(vms))

    templates = []
    for vm in vms:
        record = vms[vm]
        ty = "VM"
        if record["is_a_template"]:
            ty = "Template"
            # Look for a debian template
            if record["name_label"].startswith("Debian"):
                templates.append(vm)
        print "  Found %8s with name_label = %s" % (ty, record["name_label"])

    print "Choosing a template to clone"
    if templates == []:
        print "Could not find any Debian templates. Exitting"
        sys.exit(1)

    template = templates[0]
    print "  Selected template: ", session.xenapi.VM.get_name_label(template)
    print "Installing new VM from the template"
    vm = session.xenapi.VM.clone(template, "new")
    print "  New VM has name: new"
    print "Creating VIF"
    vif = { 'device': '0',
            'network': network,
            'VM': vm,
            'MAC': "",
            'MTU': "1500",
            "qos_algorithm_type": "",
            "qos_algorithm_params": {},
            "other_config": {} }
    session.xenapi.VIF.create(vif)
    print "Adding noniteractive to the kernel commandline"
    session.xenapi.VM.set_PV_args(vm, "noninteractive")
    print "Choosing an SR to instaniate the VM's disks"
    pool = session.xenapi.pool.get_all()[0]
    default_sr = session.xenapi.pool.get_default_SR(pool)
    default_sr = session.xenapi.SR.get_record(default_sr)
    print "Choosing SR: %s (uuid %s)" % (default_sr['name_label'], default_sr['uuid'])
    print "Rewriting the disk provisioning XML"
    spec = provision.getProvisionSpec(session, vm)
    spec.setSR(default_sr['uuid'])
    

    provision.setProvisionSpec(session, vm, spec)
    print "Asking server to provision storage from the template specification"
    session.xenapi.VM.provision(vm)
    print "Starting VM"
    session.xenapi.VM.start(vm, False, True)
    print "  VM is booting"

    print "Waiting for the installation to complete"
    # Here we poll because we don't generate events for metrics objects currently
    
    def read_os_name(vm):
        vgm = session.xenapi.VM.get_guest_metrics(vm)
        try:
            os = session.xenapi.VM_guest_metrics.get_os_version(vgm)
            if "name" in os.keys():
                return os["name"]
            return None
        except:
            return None
    def read_ip_address(vm):
        vgm = session.xenapi.VM.get_guest_metrics(vm)
        try:
            os = session.xenapi.VM_guest_metrics.get_networks(vgm)
            if "0/ip" in os.keys():
                return os["0/ip"]
            return None
        except:
            return None

    while read_os_name(vm) == None: time.sleep(1)
    print "Reported OS name: ", read_os_name(vm)
    while read_ip_address(vm) == None: time.sleep(1)
    print "Reported IP: ", read_ip_address(vm)

    session.xenapi.session.logout()


def provisionServer2(session):
    # Choose the PIF with the alphabetically lowest device
    # (we assume that plugging the debian VIF into the same network will allow
    # it to get an IP address by DHCP)
    # List all the VM objects
    vms = session.xenapi.VM.get_all_records()
    print "Server has %d VM objects (this includes templates):" % (len(vms))

    templates = []
    for vm in vms:
        record = vms[vm]
        ty = "VM"
        if record["is_a_template"]:
            ty = "Template"
            # Look for a debian template
            if record["name_label"].startswith("CentOS6_base"):
                templates.append(vm)
        #print "  Found %8s with name_label = %s" % (ty, record["name_label"])

    print "Choosing a template to clone"
    if templates == []:
        print "Could not find any Debian templates. Exitting"
        sys.exit(1)

    
    template = templates[0]
    print "  Selected template: ", session.xenapi.VM.get_name_label(template)
    print "Installing new VM from the template"

    vm = session.xenapi.VM.clone(template, "new")
    print "  New VM has name: new"

    
    pool = session.xenapi.pool.get_all()[0]
    default_sr = session.xenapi.pool.get_default_SR(pool)
    
    print "default_sr = %s" % (default_sr)

    default_sr = session.xenapi.SR.get_record(default_sr)
    print "Choosing SR: %s (uuid %s)" % (default_sr['name_label'], default_sr['uuid'])
    print "Rewriting the disk provisioning XML"
    spec = provision.getProvisionSpec(session, vm)
    spec.setSR(default_sr['uuid'])


    provision.setProvisionSpec(session, vm, spec)
    
    print "Asking server to provision storage from the template specification"
    session.xenapi.VM.provision(vm)
    
    print "Starting VM"
    session.xenapi.VM.start(vm, False, True)
    print "  VM is booting"

    print "Waiting for the installation to complete"
    # Here we poll because we don't generate events for metrics objects currently
    
    def read_os_name(vm):
        vgm = session.xenapi.VM.get_guest_metrics(vm)
        try:
            os = session.xenapi.VM_guest_metrics.get_os_version(vgm)
            if "name" in os.keys():
                return os["name"]
            return None
        except:
            return None
    def read_ip_address(vm):
        vgm = session.xenapi.VM.get_guest_metrics(vm)
        try:
            os = session.xenapi.VM_guest_metrics.get_networks(vgm)
            if "0/ip" in os.keys():
                return os["0/ip"]
            return None
        except:
            return None

    while read_os_name(vm) == None: time.sleep(1)
    print "Reported OS name: ", read_os_name(vm)
    while read_ip_address(vm) == None: time.sleep(1)
    print "Reported IP: ", read_ip_address(vm)

    session.xenapi.session.logout()


session = XenAPI.Session("https://192.168.10.20")
session.xenapi.login_with_password("root", "vagrant")

all = session.xenapi.VM.get_all()
vms = []
hosts = []
for vm in all:
    record = session.xenapi.VM.get_record(vm)
    if not(record["is_a_template"]) and not(record["is_control_domain"]) and record["power_state"] == "Running":
        vms.append(vm)
        hosts.append(record["resident_on"])
        #print "name = %s\n" % (record)

found_hosts = session.xenapi.host.get_by_name_label('xen')
host_record = session.xenapi.host.get_record(found_hosts[0])

found_vms = session.xenapi.VM.get_by_name_label('CentOS6_test')
vm_record = session.xenapi.VM.get_record(found_vms[0])

srs = session.xenapi.SR.get_all()
for sr in srs:
	print "sr = %s " % (sr)
	sr_record = session.xenapi.SR.get_record(sr)
	#pprint.pprint(sr_record)

pprint.pprint(host_record)
#pprint.pprint(vm_record)

#session.VM.set_affinity(vm_record["uuid"],host_record["uuid"])

#print "host = %d, vm = %d" % (len(found_hosts), len(found_vms))


#provisionServer2(session)