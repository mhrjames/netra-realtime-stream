require 'fog/core'
require 'fog/xml'
require 'fog/utilities'
require 'fog/model'
require 'fog/collection'

module Xen

  module XenServer
    autoload :Connection, './xen_server/connection'
    autoload :InvalidLogin, './xen_server/invalid_login'
    autoload :NokogiriStreamParser, './xen_server/nokogiri_stream_parser'
    autoload :NotFound, './xen_server/not_found'
    autoload :RequestFailed, './xen_server/request_failed'

    extend Fog::Provider

    service(:compute, 'Compute')
  end

  module Compute
    autoload :XenServer, './compute/xen_server'
  end

  module Parsers
    autoload :XenServer, './parsers/xen_server'
  end

end