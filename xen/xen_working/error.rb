require 'openssl'

module Xen

  class Error < RuntimeError
  end
  
  class NotFound < Error
  end
  
  class ConnectionError < Error
  end
  
  class AuthenticationError < Error
  end
  
  class ExpirationError < Error
  end
  
  class TimeoutError < Error
  end
  
  class UnauthenticatedClient < Error
    def initialize(message = "Client needs to be authenticated first")
      super
    end
  end

  class NotMasterError < Error
		attr_accessor :master_ip

  	def initialize(message)
  	  super
			@master_ip = (/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/).match(message)[0]
  	end
  end
  
end
