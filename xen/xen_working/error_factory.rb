module Xen
  
	  module ErrorFactory
		  
		  API_ERRORS = {
		    "SESSION_AUTHENTICATION_FAILED" => AuthenticationError,
		    "HOST_IS_SLAVE" => NotMasterError,
		    "HOST_STILL_BOOTING" => ConnectionError
		  }
		  
		  TRANSLATIONS = {
		    EOFError => ExpirationError,
		    OpenSSL::SSL::SSLError => ExpirationError,
		    Errno::EHOSTUNREACH => ConnectionError,
		    Errno::ECONNREFUSED => ConnectionError,
		    Errno::EPIPE => ConnectionError,
		    Timeout::Error => TimeoutError
		  }
		  
		  def create(*args)
		    key = args[0]
		    message = args.join(": ")
		    error_class = API_ERRORS[key]
		    
		    return error_class.new message if error_class
		    Error.new message
	    end
		  
		def wrap(error)
			return error if error.is_a? Error
			
			error_class = TRANSLATIONS[error.class]
			return error_class.new error.to_s if error_class
			Error.new "<#{error.class}> #{error}"
		end

	    module_function :create, :wrap
	end
end