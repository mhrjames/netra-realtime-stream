module Xen

  require "xmlrpc/client"
  #require "nokogiri"
  #require 'dispatcher'



  class NokogiriStreamParser < XMLRPC::XMLParser::AbstractStreamParser
    def initialize
      require 'nokogiri/xml/sax/document'
      require 'nokogiri/xml/sax/parser'

      @parser_class = Class.new(Nokogiri::XML::SAX::Document) do

        include XMLRPC::XMLParser::StreamParserMixin

        alias_method :start_element, :startElement
        alias_method :end_element,   :endElement
        alias_method :characters,    :character
        alias_method :cdata_block,   :character

        def parse(str)
          Nokogiri::XML::SAX::Parser.new(self).parse(str)
        end

      end
    end
  end




  class Session
    
    include Xen::VirtualMachine
    include Xen::Vdi
    include Xen::Vbd
    include Xen::Storage
    include Xen::Task
    include Xen::Network

    attr_reader :credentials

    
    def initialize(host, timeout = 1200)
      @client = XMLRPC::Client.new(host, '/')
      @client.set_parser(NokogiriStreamParser.new)
      @client.timeout = timeout
      @session = @client.proxy("session")
    end

    def authenticate( username, password )
        #response = @client.call('session.login_with_password', username.to_s, password.to_s)
        
        response = @session.login_with_password(username.to_s, password.to_s)
        raise Xen::AuthenticationError.new unless response["Status"] =~ /Success/

        @credentials = response["Value"]

        return self
    end

    def request(options, *params)
      begin
        parser   = options.delete(:parser)
        method   = options.delete(:method)

        if params.empty?
          response = @factory.call(method, @credentials)
        else
          if params.length.eql?(1) and params.first.is_a?(Hash)
            response = @factory.call(method, @credentials, params.first)
          elsif params.length.eql?(2) and params.last.is_a?(Array)
            response = @factory.call(method, @credentials, params.first, params.last)
          else
            response = eval("@factory.call('#{method}', '#{@credentials}', #{params.map {|p|  p.is_a?(String) ? "'#{p}'" : p}.join(',')})")
          end
        end
        raise RequestFailed.new("#{method}: " + response["ErrorDescription"].to_s) unless response["Status"].eql? "Success"
        if parser
          parser.parse( response["Value"] )
          response = parser.response
        end

        response
      end
    end

    def close
      @session.logout(@credentials)
    end





    # Avoiding method missing to get lost with Rake Task
    # (considering Xen tasks as Rake task (???)
    def task(*args)
      method_missing("task", *args)
    end

    def method_missing(name, *args)
      print "method_missing : name = #{name}\n"
      raise Xen::UnauthenticatedClient.new unless @credentials

      proxy = @client.proxy(name.to_s, @credentials, *args)
      Dispatcher.new(proxy, &@block)
    end
  end
end
